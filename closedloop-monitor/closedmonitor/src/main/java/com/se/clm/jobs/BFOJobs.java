package com.se.clm.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.se.cl.constants.BFOStatus;
import com.se.cl.constants.OppStatus;
import com.se.cl.partner.manager.NotificationManagerFactory;

public class BFOJobs
{
	
	private static final Logger logger = LoggerFactory.getLogger(BFOJobs.class);
	@Autowired
	NotificationManagerFactory notificationManagerFactory;
	
	//@Scheduled(cron = "${bfo.cron.expression}")
public void sendUpdestoBFO(){
		
		logger.info("BFO Notifications JOB STARTED");
		try {
			
			//FRANCE BFO JOBS
			
			notificationManagerFactory.getNotificationManager("FRN").sendNewOppToIF();
			notificationManagerFactory.getNotificationManager("FRN").sendAcceptRejectToBFOIF(OppStatus.ACCEPTED.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.INSTALLATION.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.WON.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.QUOTATION.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.MEETING.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.LOST.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.CANCEL.name());
                      
			//AUSRALIA BFO JOBS WITH IFW FRAMEWORK
            
			notificationManagerFactory.getNotificationManager("AUN").sendNewOppToIF();
			notificationManagerFactory.getNotificationManager("AUN").sendAcceptRejectToBFOIF(OppStatus.ACCEPTED.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToIF(BFOStatus.INSTALLATION.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToIF(BFOStatus.WON.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToIF(BFOStatus.QUOTATION.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToIF(BFOStatus.MEETING.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToIF(BFOStatus.LOST.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToIF(BFOStatus.CANCEL.name());
			
			//SWEDEN BFO JOBS WITH IFW FRAMEWORK AT PRESENT COMMENTED
			
			/*
			 
			notificationManagerFactory.getNotificationManager("SWN").sendNewOppToIF();
			notificationManagerFactory.getNotificationManager("SWN").sendAcceptRejectToBFOIF(OppStatus.ACCEPTED.name());
			notificationManagerFactory.getNotificationManager("SWN").sendStatusHistToIF(BFOStatus.INSTALLATION.name());
			notificationManagerFactory.getNotificationManager("SWN").sendStatusHistToIF(BFOStatus.WON.name());
			notificationManagerFactory.getNotificationManager("SWN").sendStatusHistToIF(BFOStatus.QUOTATION.name());
			notificationManagerFactory.getNotificationManager("SWN").sendStatusHistToIF(BFOStatus.MEETING.name());
			notificationManagerFactory.getNotificationManager("SWN").sendStatusHistToIF(BFOStatus.LOST.name());
			notificationManagerFactory.getNotificationManager("SWN").sendStatusHistToIF(BFOStatus.CANCEL.name());
			
			*/

						
			//BFO JOBS FOR FRANCE AUS USING SOAP  
			
			/*
			
			notificationManagerFactory.getNotificationManager("FRN").sendNewOppToBFO();
			notificationManagerFactory.getNotificationManager("FRN").sendAcceptRejectToBFO(OppStatus.ACCEPTED.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToBFO(BFOStatus.INSTALLATION.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToBFO(BFOStatus.WON.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToBFO(BFOStatus.QUOTATION.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToBFO(BFOStatus.MEETING.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToBFO(BFOStatus.LOST.name());
			notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToBFO(BFOStatus.CANCEL.name());
			
			*/
			
			/*
			  		 
			notificationManagerFactory.getNotificationManager("AUN").sendNewOppToBFO();
			notificationManagerFactory.getNotificationManager("AUN").sendAcceptRejectToBFO(OppStatus.ACCEPTED.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.INSTALLATION.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.WON.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.QUOTATION.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.MEETING.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.LOST.name());
			notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.CANCEL.name());
			
			*/
			
		} catch (Exception e) {
			// TODO AUNto-generated catch block
			logger.error("Error in BFO Jobs");
			e.printStackTrace();
			
		}
		logger.info("BFO Notifications JOB DONE !");
	}
	
}

package com.se.clm.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.se.cl.partner.manager.PartnerManagerFactory;

public class AssignOpportunityJob {
	
	private static final Logger logger = LoggerFactory.getLogger(AssignOpportunityJob.class);
	
	@Autowired
	PartnerManagerFactory partnerfactFactory;
	
	@Autowired
	BFOJobs bfoJobs;
	
	@Autowired
	PushNotificationsJob pushNotificationsJob;
	@Autowired
	Solution30UpdatesJob solution30UpdatesJob;
	
	@Async
	@Scheduled(cron = "${opp.cron.expression}")
	public void assignOppsToPartners()
	{
		try
		{
		
		logger.info("Assign Opportunity JOB STARTED");
		partnerfactFactory.getPartnerManger("AU").assignPartnerToOpportunity();
		partnerfactFactory.getPartnerManger("FR").assignPartnerToOpportunity();	
		partnerfactFactory.getPartnerManger("SW").assignPartnerToOpportunity();	

		logger.info("Assign Opportunity JOB DONE !");
		
		
		//logger.info("Assign Folder Permissions JOB STARTED");
		
		//partnerfactFactory.getPartnerManger("SW").assignFolderPermissionsToPartners("SW");	

		//logger.info("Assign Folder Permissions JOB END");

		 
		logger.info("PUSH JOB STARTED");
		
		pushNotificationsJob.sendNotifications();
		
		logger.info("PUSH JOB ENDED");
		
		
		
		logger.info("SOL30 JOB STARTED");
	    
		solution30UpdatesJob.updateSol30Status();
	    
		logger.info("SOL30 JOB ENDED");
	   
	     
	     	    
		logger.info("BFO JOB STARTED");
	    
		bfoJobs.sendUpdestoBFO();
	    
		logger.info("BFO JOB ENDED");
	
	
		
	}catch(Exception e)
	{
		logger.info("Exception"+e.getMessage());
	}

}
	
}

package com.se.clm.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.se.cl.sol30.service.impl.Solution30Service;




public class Solution30UpdatesJob
{
	
	private static final Logger logger = LoggerFactory.getLogger(Solution30UpdatesJob.class);
 	
	@Autowired
	Solution30Service solution30Service;
	
	//@Scheduled(cron = "${sol30.cron.expression}")
	public void updateSol30Status()
	{
	    try
	    {
	    	solution30Service.updateOpportunityHistory();
	    }catch(Exception e)
		{
	    	logger.error("Exception in Solution30UpdatesJob");
	    	e.printStackTrace();
		}
	}
	

}

package com.se.clm.jobs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.se.cl.dao.OpportunityDAO;

public class ClosedLoopJobs {
	
	private static final Logger logger = LoggerFactory.getLogger(ClosedLoopJobs.class);
	@Autowired
	OpportunityDAO  opportunityDAO;
	
	
	/**
	 * For france and sweden revoke opportunities after 48 hrs 
	 */
	@Scheduled(cron = "${revoke.opp.expression}")
	public void revokeOpportunities(){
		
		logger.debug("revoke opportunity started");
		List<String> oppAndPartnerList=opportunityDAO.getOppAndPartner("FR");
		
		if (oppAndPartnerList!=null && oppAndPartnerList.size()>0){
			
			for(String oppPartner:oppAndPartnerList){
				String[] ids = oppPartner.split("#");
				logger.debug("Partner Id:"+ids[1]+" opportunity Id :"+ids[0]);
				opportunityDAO.revokeOppFromPartner(ids[0], ids[1]);
			}
		}
		
		List<String> oppAndPartnerListSweden=opportunityDAO.getOppAndPartner("SW");
		
		if (oppAndPartnerListSweden!=null && oppAndPartnerListSweden.size()>0){
			
			for(String oppPartner:oppAndPartnerListSweden){
				String[] ids = oppPartner.split("#");
				logger.debug("Partner Id:"+ids[1]+" opportunity Id :"+ids[0]);
				opportunityDAO.revokeOppFromPartner(ids[0], ids[1]);
			}
		}

		
		logger.debug("revoke opportunity end");
		
	}
		
	}
	


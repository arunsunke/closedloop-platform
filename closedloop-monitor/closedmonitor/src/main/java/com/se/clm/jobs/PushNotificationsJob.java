package com.se.clm.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.se.cl.exception.AppException;
import com.se.cl.partner.manager.NotificationManagerFactory;

public class PushNotificationsJob {
	
	private static final Logger logger = LoggerFactory.getLogger(PushNotificationsJob.class);
	@Autowired
	NotificationManagerFactory notificationManagerFactory;
	
	//@Scheduled(cron = "${push.cron.expression}")
	public void sendNotifications(){
		
		System.out.println("Push Notifications JOB STARTED");
		try {
			
			//PUSH NOTIFICATION JOBS FOR AUSTRALIA	
			
			logger.info("New opportunity notification started");
			notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForNewOpportunity("AU");
			logger.info("New opportunity notification end");
			
			logger.info("New opportunity notification SLA-step1 START");
			notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForSALRemider(1,"AU");
			logger.info("New opportunity notification SLA-Step1 END");
			
			logger.info("New opportunity notification SLA-step2 START");
			notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForSALRemider(2,"AU");
			logger.info("New opportunity notification SLA-step2 End");
			
			logger.info("New opportunity notification SLA-step3 START");
			notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForSALRemider(3,"AU");
			logger.info("New opportunity notification SLA-step3 End");
			
						
			logger.info("E3 for french start");
			notificationManagerFactory.getNotificationManager("AUN").sendMailToConsumerPartnerAccepted("AU",false);
			logger.info("E3 for french end");
			
			logger.info("E9 for french start");
			notificationManagerFactory.getNotificationManager("AUN").sendMailToConsumerOppReceived("AU");
			logger.info("E9 for french end");
			
			
			
			//PUSH NOTIFICATION JOBS FOR FRANCE	
			
			logger.info("New opportunity notification started");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForNewOpportunity("FR");
			logger.info("New opportunity notification end");
			
			logger.info("New opportunity notification SLA-step1 START");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForSALRemider(1,"FR");
			logger.info("New opportunity notification SLA-Step1 END");
			
			logger.info("New opportunity notification SLA-step2 START");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForSALRemider(2,"FR");
			logger.info("New opportunity notification SLA-step2 END");
			
			logger.info("New opportunity notification SLA-step3 START");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForSALRemider(3,"FR");
			logger.info("New opportunity notification SLA-step3 END");
			
			
			logger.info("New opportunity notification NO partner accept START");
			notificationManagerFactory.getNotificationManager("FRN").sendMailNoPartnerAccepted("FR");
			logger.info("New opportunity notification NO partner accept end");
			
			logger.info("New opportunity notification all partner reject start");
			notificationManagerFactory.getNotificationManager("FRN").sendMailAllPartnerDeclined("FR");
			logger.info("New opportunity notification all partner reject end");
			
			logger.info("E3 for french start");
			notificationManagerFactory.getNotificationManager("FRN").sendMailToConsumerPartnerAccepted("FR",false);
			logger.info("E3 for french end");
			
			logger.info("E9 for french start");
			notificationManagerFactory.getNotificationManager("FRN").sendMailToConsumerOppReceived("FR");
			logger.info("E9 for french end");
			
			
			logger.info("New opportunity notification for N4 START");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForN4toN6(2,"N4","FR");
			logger.info("New opportunity notification for N4  END");
			
			logger.info("New opportunity notification for N5 START");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForN4toN6(7,"N5","FR");
			logger.info("New opportunity notification for N5  END");
			
			logger.info("New opportunity notification for N6 START");
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForN4toN6(21,"N6","FR");
			logger.info("New opportunity notification for N6  END");			
			
			
			//PUSH NOTIFICATION JOBS FOR SWEDEN
	
			
			logger.info("New opportunity notification started");
			notificationManagerFactory.getNotificationManager("SWN").notifyPartnerForNewOpportunity("SW");
			logger.info("New opportunity notification end");
			
			
			
			/*
			logger.info("New opportunity notification SLA-step1 START");
			notificationManagerFactory.getNotificationManager("SWN").notifyPartnerForSALRemider(1,"SW");
			logger.info("New opportunity notification SLA-Step1 END");
			
			logger.info("New opportunity notification SLA-step2 START");
			notificationManagerFactory.getNotificationManager("SWN").notifyPartnerForSALRemider(2,"SW");
			logger.info("New opportunity notification SLA-step2 START");
			
			logger.info("New opportunity notification SLA-step3 START");
			notificationManagerFactory.getNotificationManager("SWN").notifyPartnerForSALRemider(4,"SW");
			logger.info("New opportunity notification SLA-step3 START");
			*/
			
			
			logger.info("E3 for Sweden start");
			notificationManagerFactory.getNotificationManager("SWN").sendMailToConsumerPartnerAccepted("SW",true);
			logger.info("E3 for Sweden end");
			
			logger.info("E9 for SWench start");
			notificationManagerFactory.getNotificationManager("SWN").sendMailToConsumerOppReceived("SW");
			logger.info("E9 for Sweden end");
			
			
			logger.info("opportunity notification for CANCEL START");
			notificationManagerFactory.getNotificationManager("SWN").notifyPartnerForCancel("SW","CANCEL");
			logger.info("opportunity notification for CANCEL  END");
			
			logger.info("opportunity SMS for CANCEL START");
		    notificationManagerFactory.getNotificationManager("SWN").sendSmsTopartnerwhenConsCancelled("SW",true,"CANCEL");
			logger.info("opportunity SMS for CANCEL  END");
		  
			
			logger.info("opportunity EMAIL for MEETING START");
		    notificationManagerFactory.getNotificationManager("SWN").sendMailToConsumerWhenPartnerBookedMeeting("SW");
			logger.info("opportunity EMAIL for MEETING  END");
		
			
			logger.info("opportunity SMS for MEETING START");
		    notificationManagerFactory.getNotificationManager("SWN").sendSmsToConsumerWhenMeetingBooked("SW",true,"MEETING");
			logger.info("opportunity SMS for MEETING  END");
			
			
			logger.info("opportunity EMAIL for INVOICE START");
		    notificationManagerFactory.getNotificationManager("SWN").sendMailToAminForInvoice("SW","INSTALLED");
			logger.info("opportunity EMAIL for INVOICE  END");
		
		    
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error in Pushnotification Job:::"+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Push Notifications JOB DONE !");
	}
	

}

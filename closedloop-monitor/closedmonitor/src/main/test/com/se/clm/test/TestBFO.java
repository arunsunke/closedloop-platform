package com.se.clm.test;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.se.cl.constants.BFOStatus;
import com.se.cl.constants.OppStatus;
import com.se.cl.exception.AppException;
import com.se.cl.partner.manager.NotificationManagerFactory;
import com.se.clm.bfo.client.HttpsClientImpl;
import com.se.clm.bfo.manager.BFOManagerFactory;
import com.se.clm.mail.IEmailService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class TestBFO {
	
	@Autowired
	BFOManagerFactory bfoManagerFactory;
	@Autowired
	IEmailService iEmailService;
	@Autowired
	NotificationManagerFactory notificationManagerFactory;
	
	
	
	
	public void testSayHello() {
		//Assert.assertEquals("Hello world!", );
		//bfoManager.getOpportunityRequest();
		Map<String,String> reqs=bfoManagerFactory.getBFOManager("AUB").getOpportunityRequest();
		HttpsClientImpl httpsClient=new HttpsClientImpl();
		String token=httpsClient.getToken();
		
		
		Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
		
		for (Entry entry : entrySet) {
		
			Map<String,String> responseMap =httpsClient.sendCompositRequestToBFO(entry.getValue().toString(), token);
			
			//bfoManager.updateBFOOppStatus(responseMap, entry.getKey().toString());
		}
		
		
		//bfoc.sendToBFO(reqs.get(0), token);
	}

	@Test
	public void testBFOStatus(){
		//notificationManagerFactory.getNotificationManager("FRN").sendNewOppToBFO();
		//notificationManagerFactory.getNotificationManager("FRN").sendAcceptRejectToBFO(OppStatus.ACCEPTED.name());
		//notificationManagerFactory.getNotificationManager("AUN").sendStatusHistToBFO(BFOStatus.MEETING.name());
		notificationManagerFactory.getNotificationManager("FRN").sendStatusHistToIF(BFOStatus.QUOTATION.name());
		
		//notificationManager.sendAcceptRejectToBFO(OppStatus.ACCEPTED.name());
		//notificationManagerFactory.getNotificationManager("AUN").sendMailAllPartnerDeclined();
	}
	
	public void testAcceptRejectBFO(){
		try {
			notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForNewOpportunity("AU");
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  //notificationManager.sendAcceptRejectToBFO(OppStatus.DECLINED.name());
		 // notificationManager.sendStatusHistToBFO(BFOStatus.INSTALLATION.name());
	}
	
	
	public void testPushNotifications(){
		
		try {
			//notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForNewOpportunity("AU");
			
			notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForSALRemider(1,"AU");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

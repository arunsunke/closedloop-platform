package com.se.clm.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.se.cl.partner.manager.PartnerManagerFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class TestAssignOpportunity {
	
	@Autowired
	PartnerManagerFactory partnerfactFactory;
	
	@Test
	public void assignOpportunity(){
		
		System.out.println("Assign Opportunity JOB STARTED");
		
		//partnerfactFactory.getPartnerManger("AU").assignPartnerToOpportunity();
		partnerfactFactory.getPartnerManger("FR").assignPartnerToOpportunity();
		
		System.out.println("Assign Opportunity JOB DONE !");
		
		
	}

}

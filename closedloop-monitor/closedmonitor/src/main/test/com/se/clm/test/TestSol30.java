package com.se.clm.test;

import javax.xml.ws.Holder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.Solution30DAO;
import com.se.cl.exception.AppException;
import com.se.cl.model.Opportunity;
import com.se.cl.sol30.service.ActivityReturn;
import com.se.cl.sol30.service.impl.Solution30Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class TestSol30 {
	
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	Solution30Service solution30Service;
	
	
	@Autowired
	Solution30DAO solution30DAO;
	
	
	//@Test
	public void testSendtoSol30(){
		
		//notificationManager.sendAcceptRejectToBFO(OppStatus.ACCEPTED.name());
		
		Opportunity opportunity=null;;
		String oppId="261";
		try {
			opportunity = opportunityDAO.loadOpportunityById(Long.parseLong(oppId));
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Holder<ActivityReturn> activityReturn=solution30Service.sendOpportunityToSol30(opportunity);
		
		//Holder<ActivityReturn> activityReturn=solution30Service.sendOpportunityToSol30(opp);

		if (activityReturn!=null){
			//updated success status to opportunity table
			solution30DAO.updateOpportunity(oppId, activityReturn.value.getIdSol30());
			
		}else{
			//update status so that it will retry for failed to send earlier
			solution30DAO.updateOpportunity(oppId, "999111999");
		}
		
		
		
		
		
	}
	
	
	//@Test
	public void testgetSol30Status(){
		solution30Service.getOpportunityStatus("OP123-2");
	}
	
	@Test
	public void testupdateSol30Status(){
		solution30Service.updateOpportunityHistory();
	}


}


package oasis.names.tc.saml._1_0.protocol;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "RequestPortType", targetNamespace = "urn:oasis:names:tc:SAML:1.0:protocol")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    com.cordys.schemas.general._1.ObjectFactory.class,
    oasis.names.tc.saml._1_0.assertion.ObjectFactory.class,
    oasis.names.tc.saml._1_0.protocol.ObjectFactory.class,
    org.w3._2000._09.xmldsig.ObjectFactory.class
})
public interface RequestPortType {


    /**
     * 
     * @param body
     * @return
     *     returns oasis.names.tc.saml._1_0.protocol.ResponseType
     * @throws CordysFaultDetail
     */
    @WebMethod(operationName = "RequestOperation")
    @WebResult(name = "Response", targetNamespace = "urn:oasis:names:tc:SAML:1.0:protocol", partName = "body")
    public ResponseType requestOperation(
        @WebParam(name = "Request", targetNamespace = "urn:oasis:names:tc:SAML:1.0:protocol", partName = "body")
        RequestType body)
        throws CordysFaultDetail
    ;

}

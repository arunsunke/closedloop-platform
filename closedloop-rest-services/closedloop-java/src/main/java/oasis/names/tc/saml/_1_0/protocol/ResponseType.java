
package oasis.names.tc.saml._1_0.protocol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import oasis.names.tc.saml._1_0.assertion.AssertionType;


/**
 * <p>Java class for ResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:oasis:names:tc:SAML:1.0:protocol}ResponseAbstractType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:oasis:names:tc:SAML:1.0:protocol}Status"/>
 *         &lt;element ref="{urn:oasis:names:tc:SAML:1.0:assertion}Assertion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{urn:oasis:names:tc:SAML:1.0:protocol}AssertionArtifact" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseType", propOrder = {
    "status",
    "assertion",
    "assertionArtifact"
})
public class ResponseType
    extends ResponseAbstractType
{

    @XmlElement(name = "Status", required = true)
    protected StatusType status;
    @XmlElement(name = "Assertion", namespace = "urn:oasis:names:tc:SAML:1.0:assertion")
    protected List<AssertionType> assertion;
    @XmlElement(name = "AssertionArtifact")
    protected String assertionArtifact;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setStatus(StatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the assertion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assertion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssertion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssertionType }
     * 
     * 
     */
    public List<AssertionType> getAssertion() {
        if (assertion == null) {
            assertion = new ArrayList<AssertionType>();
        }
        return this.assertion;
    }

    /**
     * Gets the value of the assertionArtifact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssertionArtifact() {
        return assertionArtifact;
    }

    /**
     * Sets the value of the assertionArtifact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssertionArtifact(String value) {
        this.assertionArtifact = value;
    }

}

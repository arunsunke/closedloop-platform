
package com.cordys.schemas.general._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cordys.schemas.general._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FaultRelatedException_QNAME = new QName("http://schemas.cordys.com/General/1.0/", "FaultRelatedException");
    private final static QName _NestedMessageLocalizableMessage_QNAME = new QName("http://schemas.cordys.com/General/1.0/", "LocalizableMessage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cordys.schemas.general._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NestedMessage }
     * 
     */
    public NestedMessage createNestedMessage() {
        return new NestedMessage();
    }

    /**
     * Create an instance of {@link FaultDetails }
     * 
     */
    public FaultDetails createFaultDetails() {
        return new FaultDetails();
    }

    /**
     * Create an instance of {@link com.cordys.schemas.general._1.LocalizableMessage }
     * 
     */
    public com.cordys.schemas.general._1.LocalizableMessage createLocalizableMessage() {
        return new com.cordys.schemas.general._1.LocalizableMessage();
    }

    /**
     * Create an instance of {@link NestedMessage.LocalizableMessage }
     * 
     */
    public NestedMessage.LocalizableMessage createNestedMessageLocalizableMessage() {
        return new NestedMessage.LocalizableMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.cordys.com/General/1.0/", name = "FaultRelatedException")
    public JAXBElement<String> createFaultRelatedException(String value) {
        return new JAXBElement<String>(_FaultRelatedException_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NestedMessage.LocalizableMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.cordys.com/General/1.0/", name = "LocalizableMessage", scope = NestedMessage.class)
    public JAXBElement<NestedMessage.LocalizableMessage> createNestedMessageLocalizableMessage(NestedMessage.LocalizableMessage value) {
        return new JAXBElement<NestedMessage.LocalizableMessage>(_NestedMessageLocalizableMessage_QNAME, NestedMessage.LocalizableMessage.class, NestedMessage.class, value);
    }

}

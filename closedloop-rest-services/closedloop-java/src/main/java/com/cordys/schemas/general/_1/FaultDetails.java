
package com.cordys.schemas.general._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.cordys.com/General/1.0/}LocalizableMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "localizableMessage"
})
@XmlRootElement(name = "FaultDetails")
public class FaultDetails {

    @XmlElement(name = "LocalizableMessage", required = true)
    protected LocalizableMessage localizableMessage;

    /**
     * Gets the value of the localizableMessage property.
     * 
     * @return
     *     possible object is
     *     {@link LocalizableMessage }
     *     
     */
    public LocalizableMessage getLocalizableMessage() {
        return localizableMessage;
    }

    /**
     * Sets the value of the localizableMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalizableMessage }
     *     
     */
    public void setLocalizableMessage(LocalizableMessage value) {
        this.localizableMessage = value;
    }

}

package com.se.cl.partner.manager;

public interface NotificationManagerFactory {

	NotificationManager getNotificationManager(String managerName);
	
	
}

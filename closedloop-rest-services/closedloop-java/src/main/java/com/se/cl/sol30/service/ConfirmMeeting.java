
package com.se.cl.sol30.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for confirmMeeting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="confirmMeeting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientData" type="{http://ws.schneider.pc30.fr/}ClientData"/>
 *         &lt;element name="activityData" type="{http://ws.schneider.pc30.fr/}ActivityData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmMeeting", propOrder = {
    "clientData",
    "activityData"
})
public class ConfirmMeeting {

    @XmlElement(required = true)
    protected ClientData clientData;
    @XmlElement(required = true)
    protected ActivityData activityData;

    /**
     * Gets the value of the clientData property.
     * 
     * @return
     *     possible object is
     *     {@link ClientData }
     *     
     */
    public ClientData getClientData() {
        return clientData;
    }

    /**
     * Sets the value of the clientData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientData }
     *     
     */
    public void setClientData(ClientData value) {
        this.clientData = value;
    }

    /**
     * Gets the value of the activityData property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityData }
     *     
     */
    public ActivityData getActivityData() {
        return activityData;
    }

    /**
     * Sets the value of the activityData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityData }
     *     
     */
    public void setActivityData(ActivityData value) {
        this.activityData = value;
    }

}

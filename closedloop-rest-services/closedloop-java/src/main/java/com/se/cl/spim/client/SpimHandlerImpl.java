package com.se.cl.spim.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.model.IMS;
import com.se.cl.model.OpportunityImages;
import com.se.cl.partner.login.manager.ImsLoginManager;
import com.se.cl.partner.login.manager.impl.ImsLoginManagerImpl;

public class SpimHandlerImpl implements SpimHandler
{
	private static final Logger logger = LoggerFactory.getLogger(SpimHandlerImpl.class);
	//private String CreateRootFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443//spim/items";
	//private String CreateSubFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443//spim/items/objectId/childItems?type=workspace";
    //private String UploadImageUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/childItems";
	//private String DownloadImageUrl="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/bytes";		
	//private String FetchSubFolders= https://spim-qa.blcm.schneider-electric.com:443/spim/items/c1856d01-f14c-43a6-b649-9657c3a928fb/childItems
	
	private  @Value("${CreateRootFolderUrl}") String CreateRootFolderUrl;
	private  @Value("${CreateSubFolderUrl}") String CreateSubFolderUrl;
	private  @Value("${UploadImageUrl}") String UploadImageUrl;
	private  @Value("${DownloadImageUrl}") String DownloadImageUrl;
	private  @Value("${FetchSubFoldersUrl}") String FetchSubFoldersUrl;
	private  @Value("${sharePermissionsUrl}") String sharePermissionsUrl;
	
	private  @Value("${partnerRootFolderObjId}") String partnerRootFolderObjId;
	private  @Value("${consumerRootFolderObjId}") String consumerRootFolderObjId;
	private  @Value("${partnerRootFolderName}") String partnerRootFolderName;
	private  @Value("${consumerRootFolderName}") String consumerRootFolderName;

	private  @Value("${AuthorizationBearerToken}") String AuthorizationBearerToken;
	
	private  @Value("${spim.adminUser}") String spimAdminUser;
	private  @Value("${spim.adminPassword}") String spimAdminPassword;
	private  @Value("${spim.adminUserImsFederatedId}") String spimAdminUserImsFederatedId;
	private  @Value("${spim.adminUserEmailId}") String spimAdminUserEmailId;

	    

	@Autowired
	ImsLoginManager imsLoginManager;
	
	@Autowired
	PartnerDAO partnerDAO;
	
	
	@Override
	public String CreateProject(String projetctName,String imsToken,String spimUrl)
	{
		String response = "";
		String startDate = getDateISO8601Format();
		String endDate = getDateISO8601Format();
		URL url;
		
		HashMap<String ,String>  postdataMap = new HashMap<String,String>();
		postdataMap.put("name", projetctName);
		postdataMap.put("description", "closedloop project");
		postdataMap.put("client", "SE");
		postdataMap.put("status", "Process");
		postdataMap.put("startDate", "2014-12-05T00:00:00.000+08:00");		
		postdataMap.put("endDate", "2014-12-05T00:00:00.000+08:00");
        ObjectMapper objMapper = new ObjectMapper();
	    try
	    {
        
	    String jsonData = objMapper.writeValueAsString(postdataMap);
	    logger.info("jsonData::::::"+jsonData);
	    url = new URL(spimUrl);
		long CreateFolderlStartTime = System.currentTimeMillis();

	    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	    con.setDoOutput(true);
	    con.setRequestMethod("POST");
	    
	    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
	    {
		    con.setRequestProperty("Authorization",AuthorizationBearerToken);
		    con.setRequestProperty("Authentication",imsToken);

	    }else
	    {
		    con.setRequestProperty("Authorization",imsToken);

	    }	
	    
	    
	    con.setRequestProperty("Content-Type", "application/json");
	    
	    con.setDoInput(true);		   
	    OutputStream outputStream = con.getOutputStream();
	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
	    outputStreamWriter.write(jsonData);
	    outputStreamWriter.flush();
	    outputStream.close();
	    con.setReadTimeout(1000);
	    StringBuffer strBuffer = new StringBuffer();
        InputStream inputStream = con.getInputStream();

        int i;
        while ((i = inputStream.read()) != -1) {
            Writer writer = new StringWriter();
            writer.write(i);
            strBuffer.append(writer.toString());
        }			    
        logger.info("Response data  :"+ strBuffer.toString());
        
		long createFolderlendTime = System.currentTimeMillis();
		
	 	long createFolderdifference = createFolderlendTime - CreateFolderlStartTime;
	 	
	 	logger.info("Total Elapsed Time to Create Folder:::"+createFolderdifference);
        
        String jsonStr = strBuffer.toString();
        ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0)
		{
			try {
				org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
				String objectId=rootNode.path("objectId").getTextValue();
				logger.info("objectId:::"+objectId);
				if(objectId!=null && objectId.length()>0)
				{
					response=objectId;
				}

			} catch (Exception e) 
			{
				logger.error("Error occurred while Parsing the Values from JSON  "+ e);
				e.printStackTrace();

			}
		}

        return response;
		 } catch (MalformedURLException e) {
				e.printStackTrace();
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		
		
			
	  return response	;
	}
	
	@Override
	public String CreateSubFolder(String subFolderName,String imsToken,String rootFolderObjectId,String createSubFolderUrl)
	{
		String response = "";
		String startDate = getDateISO8601Format();
		String endDate = getDateISO8601Format();
		URL url;
		
		HashMap<String ,String>  postdataMap = new HashMap<String,String>();
		createSubFolderUrl = createSubFolderUrl.replaceAll("objectId", rootFolderObjectId);
		//logger.info("createSubFolderUrl:::"+createSubFolderUrl);
		postdataMap.put("name", subFolderName);
		postdataMap.put("description", "closedloop project");
		postdataMap.put("client", "SE");
		postdataMap.put("status", "Process");
		postdataMap.put("startDate", "2014-12-05T00:00:00.000+08:00");		
		postdataMap.put("endDate", "2014-12-05T00:00:00.000+08:00");
        ObjectMapper objMapper = new ObjectMapper();
	    try
	    {
        
	    String jsonData = objMapper.writeValueAsString(postdataMap);
	    logger.info("jsonData::::::"+jsonData);
	    url = new URL(createSubFolderUrl);
	    
		long CreateSubFolderStartTime = System.currentTimeMillis();
	    
	    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	    con.setDoOutput(true);
	    con.setRequestMethod("POST");
	    //con.setRequestProperty("Authorization",imsToken);
	    
	    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
	    {
		    con.setRequestProperty("Authorization",AuthorizationBearerToken);
		    con.setRequestProperty("Authentication",imsToken);

	    }else
	    {
		    con.setRequestProperty("Authorization",imsToken);

	    }	
	    con.setRequestProperty("Content-Type", "application/json");
	    con.setDoInput(true);		   
	    OutputStream outputStream = con.getOutputStream();
	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
	    outputStreamWriter.write(jsonData);
	    outputStreamWriter.flush();
	    outputStream.close();
	    con.setReadTimeout(1000);
	    StringBuffer strBuffer = new StringBuffer();
        InputStream inputStream = con.getInputStream();

        int i;
        while ((i = inputStream.read()) != -1) {
            Writer writer = new StringWriter();
            writer.write(i);
            strBuffer.append(writer.toString());
        }	
        
        long createSubFolderEndTime = System.currentTimeMillis();
	 	long createSubFolderdifference = createSubFolderEndTime - CreateSubFolderStartTime;
	 	logger.info("Total Elapsed Time to Create Sub Folder:::"+createSubFolderdifference);
        
        logger.info("Response data  :"+ strBuffer.toString());
        String jsonStr = strBuffer.toString();
        ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0)
		{
			try {
				org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
				String objectId=rootNode.path("objectId").getTextValue();
				logger.info("objectId:::"+objectId);
				if(objectId!=null && objectId.length()>0)
				{
					response=objectId;
				}

			} catch (Exception e) 
			{
				logger.error("Error occurred while Parsing the Values from JSON  "+ e);
				e.printStackTrace();

			}
		}

        return response;
		 } catch (MalformedURLException e) {
				e.printStackTrace();
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		
		
			
	  return response	;
	}

	@Override
	public String uploadImages(String subFolderObjectId,String imageName,String imsToken,String uploadImageUrl,String base64ContentStream)
	{
		String response = "";
		URL url;
		HashMap<String ,String>  postdataMap = new HashMap<String,String>();
		uploadImageUrl = uploadImageUrl.replaceAll("objectId", subFolderObjectId);
		//logger.info("uploadImageUrl:::"+uploadImageUrl);
		postdataMap.put("name", imageName);
		postdataMap.put("description", "closedloop project");
		postdataMap.put("contentStream", base64ContentStream);
		postdataMap.put("mimeType", "image/jpeg");		
		ObjectMapper objMapper = new ObjectMapper();
	    try
	    {
        
	    String jsonData = objMapper.writeValueAsString(postdataMap);
	    logger.debug("jsonData::::::"+jsonData);
	    url = new URL(uploadImageUrl);
	    
		long CreateUploadImageStartTime = System.currentTimeMillis();
	    
	    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	    con.setDoOutput(true);
	    con.setRequestMethod("POST");
	    //con.setRequestProperty("Authorization",imsToken);
	    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
	    {
		    con.setRequestProperty("Authorization",AuthorizationBearerToken);
		    con.setRequestProperty("Authentication",imsToken);

	    }else
	    {
		    con.setRequestProperty("Authorization",imsToken);

	    }	
	    con.setRequestProperty("Content-Type", "application/json");
	    con.setDoInput(true);		   
	    OutputStream outputStream = con.getOutputStream();
	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
	    outputStreamWriter.write(jsonData);
	    outputStreamWriter.flush();
	    outputStream.close();
	    //con.setReadTimeout(1000);
	    StringBuffer strBuffer = new StringBuffer();
        InputStream inputStream = con.getInputStream();

        int i;
        while ((i = inputStream.read()) != -1) {
            Writer writer = new StringWriter();
            writer.write(i);
            strBuffer.append(writer.toString());
        }			    
        
		long CreateUploadImagendTime = System.currentTimeMillis();
        long differenceUploadImage = CreateUploadImagendTime-CreateUploadImageStartTime;
        logger.info("Total elapsed Time to Upload Image  :"+ differenceUploadImage);
        
        logger.info("Response data  :"+ strBuffer.toString());
        String jsonStr = strBuffer.toString();
        ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0)
		{
			try {
				org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
				String objectId=rootNode.path("objectId").getTextValue();
				logger.info("objectId:::"+objectId);
				if(objectId!=null && objectId.length()>0)
				{
					response=objectId;
				}

			} catch (Exception e) 
			{
				logger.error("Error occurred while Parsing the Values from JSON  "+ e);
				e.printStackTrace();

			}
		}

        return response;
		 } catch (MalformedURLException e) {
				e.printStackTrace();
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		
	  return response	;
	}
	
	@Override

	public Map<String,Object> FetchProjectFolder(String imsToken,String spimUrl)
	{
		String response = "";
		URL url;
		Map<String ,Object>  ResponseMap = null;
		logger.info("spimUrl:::"+spimUrl);
	    try
	    {
	    
		long FetchProjectFolderStartTime = System.currentTimeMillis();

	    url = new URL(spimUrl);
	    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	    con.setDoOutput(true);
	    con.setRequestMethod("GET");
	    //con.setRequestProperty("Authorization",imsToken);
	    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
	    {
		    con.setRequestProperty("Authorization",AuthorizationBearerToken);
		    con.setRequestProperty("Authentication",imsToken);

	    }else
	    {
		    con.setRequestProperty("Authorization",imsToken);

	    }	
	    con.setRequestProperty("Content-Type", "application/json");
	    con.setDoInput(true);		   
	    StringBuffer strBuffer = new StringBuffer();
        InputStream inputStream = con.getInputStream();
        int i;
        while ((i = inputStream.read()) != -1) 
        {
            Writer writer = new StringWriter();
            writer.write(i);
            strBuffer.append(writer.toString());
        }
		
        long FetchProjectFolderEndTime = System.currentTimeMillis();
	 	long Folderdifference = FetchProjectFolderEndTime - FetchProjectFolderStartTime;
	 	logger.info("Total Elapsed Time For Fetching Root Folder:::"+Folderdifference);
	 	
        logger.info("FetchProjectFolder response data  :"+ strBuffer.toString());
        String jsonStr = strBuffer.toString();
        JsonFactory factory = new JsonFactory(); 
        ObjectMapper mapper = new ObjectMapper(factory); 
        TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};
        ResponseMap = mapper.readValue(jsonStr, typeRef); 
        System.out.println("Got :::" + ResponseMap);         
        ResponseMap.put("responseProject",jsonStr);
        return ResponseMap;
		 } catch (MalformedURLException e) {
				e.printStackTrace();
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		
	  return ResponseMap	;
	
	}
	
	@Override
		public Map<String,Object> FetchProjectSubFolder(String rootFolderObjectId,String imsToken,String subFolderSpimUrl)
		{
			String response = "";
			URL url;
			Map<String ,Object>  ResponseMap = null;
			//logger.info("subFolderSpimUrl:::"+subFolderSpimUrl);
		    try
		    {
	
		    subFolderSpimUrl = subFolderSpimUrl.replaceAll("objectId", subFolderSpimUrl);
		    url = new URL(subFolderSpimUrl);
		    
			long FetchProjectSubFolderStartTime = System.currentTimeMillis();

		    
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    con.setDoOutput(true);
		    con.setRequestMethod("GET");
		    //con.setRequestProperty("Authorization",imsToken);
		    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
		    {
			    con.setRequestProperty("Authorization",AuthorizationBearerToken);
			    con.setRequestProperty("Authentication",imsToken);

		    }else
		    {
			    con.setRequestProperty("Authorization",imsToken);

		    }	
		    con.setRequestProperty("Content-Type", "application/json");
		    con.setDoInput(true);		   
		    StringBuffer strBuffer = new StringBuffer();
	        InputStream inputStream = con.getInputStream();
	        int i;
	        while ((i = inputStream.read()) != -1) 
	        {
	            Writer writer = new StringWriter();
	            writer.write(i);
	            strBuffer.append(writer.toString());
	        }			    
	        
			long FetchProjectSubFolderEndTime = System.currentTimeMillis();

	        
	        logger.info("FetchProjectSubFolder response data  :"+ strBuffer.toString());
	        String jsonStr = strBuffer.toString();
	        JsonFactory factory = new JsonFactory(); 
	        ObjectMapper mapper = new ObjectMapper(factory); 
	        TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};
	        ResponseMap = mapper.readValue(jsonStr, typeRef); 
	        System.out.println("Got :::" + ResponseMap);         
			
			 } catch (MalformedURLException e) {
					e.printStackTrace();
				 } catch (Exception e) {
					e.printStackTrace();
				 }
			
		  return ResponseMap	;
			
		}
		
		/*
		 *   Method to get the List of Image Object Id's
		 */
		
	@Override

		public List<Map<String,String>> FetchProjectSubFolderImageObjectIds(String subFolderObjectId,String imsToken,String ImageSpimUrl,String userType)
		{
			URL url;
			Map<String ,Object>  ResponseMap = null;
			ArrayList<Map<String,String>> SubFolderImageObjectIds=new ArrayList<Map<String,String>>();
			HashMap<String,String> imageHashMap =new HashMap<String,String>();
			logger.info("ImageSpimUrl:::"+ImageSpimUrl);
		    try
		    {
		    	
		    ImageSpimUrl = ImageSpimUrl.replaceAll("objectId", subFolderObjectId);
		    url = new URL(ImageSpimUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    con.setDoOutput(true);
		    con.setRequestMethod("GET");
		    //con.setRequestProperty("Authorization",imsToken);
		    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
		    {
			    con.setRequestProperty("Authorization",AuthorizationBearerToken);
			    con.setRequestProperty("Authentication",imsToken);

		    }else
		    {
			    con.setRequestProperty("Authorization",imsToken);

		    }	
		    con.setRequestProperty("Content-Type", "application/json");
		    con.setDoInput(true);		   
		    StringBuffer strBuffer = new StringBuffer();
	        InputStream inputStream = con.getInputStream();
	        int i;
	        while ((i = inputStream.read()) != -1) 
	        {
	            Writer writer = new StringWriter();
	            writer.write(i);
	            strBuffer.append(writer.toString());
	        }			    
	        
	        //logger.info("FetchProjectSubFolderImages response data  :"+ strBuffer.toString());
	        JSONParser parser = new JSONParser();
	        Object obj = parser.parse(strBuffer.toString());
            JSONObject jsonObject = (JSONObject) obj;
            Long spimTotalObjectCount = (Long) jsonObject.get("spimTotalObjectCount");
            JSONArray spimObjectList = (JSONArray) jsonObject.get("spimObject");	 
            //logger.debug("spimTotalObjectCount: " + spimTotalObjectCount);
            //logger.debug("\nspimObject List:");
            @SuppressWarnings("unchecked")
			Iterator<Object> iterator = spimObjectList.listIterator();
            //logger.info("count:::::"+spimObjectList.size());
            //while (iterator.hasNext())
            for(int j=0;j<spimObjectList.size();j++)
            {
            	     //logger.info("j::::"+j+"");
            		 JSONObject spimJsonObj = (JSONObject)iterator.next();
            		 String objectId = (String) spimJsonObj.get("objectId");
                     String title = (String) spimJsonObj.get("title");
                     logger.debug("objectId: " + objectId);
                     logger.debug("title: " + title);
                     imageHashMap = new HashMap<String ,String>();
                     imageHashMap.put("objectId",objectId);
                     imageHashMap.put("title",title);
                     imageHashMap.put("userType",userType);
                     SubFolderImageObjectIds.add(imageHashMap);
            }
			
		    } catch (MalformedURLException e) {
					e.printStackTrace();
				 } catch (Exception e) {
					e.printStackTrace();
				 }
			
		  return SubFolderImageObjectIds;
			
			
		}
		
	@Override

		public Map<String,Object> FetchProjectSubFolderImageContent(String imageObjectId,String imsToken,String refreshToken,String ImageDownloadUrl)
		{
			URL url;
			byte[] buffer = new byte[0xFFFF];
			Map<String ,Object>  ResponseMap = new HashMap<String,Object>();
			List<String> SubFolderImageObjectIds = new ArrayList<String>();
		    try
		    {
	        
				ImageDownloadUrl = ImageDownloadUrl.replaceAll("objectId",imageObjectId);
				long ImageByteContent_StartTime = System.currentTimeMillis();

				//logger.info("ImageDownloadUrl:::"+ImageDownloadUrl);
				url = new URL(ImageDownloadUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("GET");
			    //con.setRequestProperty("Authorization",imsToken);
			    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
			    {
				    con.setRequestProperty("Authorization",AuthorizationBearerToken);
				    con.setRequestProperty("Authentication",imsToken);

			    }else
			    {
				    con.setRequestProperty("Authorization",imsToken);

			    }	
			    con.setRequestProperty("Content-Type", "application/json");
			    con.setDoInput(true);		   
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();
		        buffer = getBytesFromInputStream(inputStream);
		        
		        logger.info("buffer"+buffer);
		       /* int i;
		        while ((i = inputStream.read()) != -1) 
		        {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		            
		        }*/		
		        
		        //logger.info("data:"+strBuffer.toString());
		        
		        //String base64Data  = new sun.misc.BASE64Encoder().encode(strBuffer.toString().getBytes());  
		    
				long ImageByteContent_endTime = System.currentTimeMillis();

		        long  ImageByteContent_difference = ImageByteContent_endTime-ImageByteContent_StartTime;
		        
		        logger.info("FetchProjectSubFolderImageContent:::Total Elapsed Time to Download Image ByTe Content:::"+ImageByteContent_difference);
		        
	            ResponseMap.put("imageObjectId",imageObjectId );
	            ResponseMap.put("imsToken", imsToken);
	            ResponseMap.put("refreshToken", refreshToken);
	            ResponseMap.put("byteContent",buffer);
	            //ResponseMap.put("byteContent",base64Data);
		    
			 } catch (MalformedURLException e) {
					e.printStackTrace();
				 } catch (Exception e) {
					e.printStackTrace();
				 }
			
		  return ResponseMap;
			
			
		}
	
	@Override
		public String CheckProjectFolderExists(String imsToken,String spimUrl,String rootFolderTitle)
		{
			String rootFolderObjectId="";
			boolean rootfolderExists = false;
			URL url;
			Map<String ,Object>  ResponseMap = null;
			//logger.info("spimUrl:::"+spimUrl);
		    try
		    {
		    	
			long lStartTime = System.currentTimeMillis();
	
		    url = new URL(spimUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    con.setDoOutput(true);
		    con.setRequestMethod("GET");
		    //con.setRequestProperty("Authorization",imsToken);
		    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
		    {
			    con.setRequestProperty("Authorization",AuthorizationBearerToken);
			    con.setRequestProperty("Authentication",imsToken);

		    }else
		    {
			    con.setRequestProperty("Authorization",imsToken);

		    }	
		    con.setRequestProperty("Content-Type", "application/json");
		    con.setDoInput(true);		   
		    StringBuffer strBuffer = new StringBuffer();
	        InputStream inputStream = con.getInputStream();
	        int i;
		        while ((i = inputStream.read()) != -1) 
		        {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        
		        JSONParser parser = new JSONParser();
		        Object obj = parser.parse(strBuffer.toString());
	 
				long lEndTime = System.currentTimeMillis();

				long TotalTime = lEndTime - lStartTime;
				
				logger.info("CheckProjectFolderExists:::Totla Elapsed Time To Check Project Fodler Exists::"+TotalTime);

	            JSONObject jsonObject = (JSONObject) obj;
	 
	            String status = (String) jsonObject.get("status");
	            Long spimTotalObjectCount = (Long) jsonObject.get("spimTotalObjectCount");
	            JSONArray spimObjectList = (JSONArray) jsonObject.get("spimObject");	 
	            //System.out.println("status: " + status);
	            //System.out.println("spimTotalObjectCount: " + spimTotalObjectCount);
	            //System.out.println("\nspimObject List:");
	            @SuppressWarnings("unchecked")
				Iterator<Object> iterator = spimObjectList.iterator();
	            while (iterator.hasNext())
	            {
	                JSONObject spimJsonObj = (JSONObject)iterator.next();
	                String title = (String)spimJsonObj.get("title");
	                String objectId = (String) spimJsonObj.get("objectId");
	                if(title!=null&& title.equalsIgnoreCase(rootFolderTitle))
	                {	
	                	//System.out.println("title: " + title);
	                	//System.out.println("objectId: " + objectId);
	                	rootFolderObjectId = objectId;
	                	break;
	                }
	            }
		        
		    } catch (MalformedURLException e)
		    {
				e.printStackTrace();
			} catch (Exception e) 
		    {
				e.printStackTrace();
		    } 
		    
		   return  rootFolderObjectId;
		
		}
	@Override
		public String CheckProjectSubFolderExists(String imsToken,String spimSubFolderUrl,String rootFolderObjId,String subFolderName)
		{
			String subFolderObjectId="";
			URL url;
			logger.info("spimSubFolderUrl:::"+spimSubFolderUrl);
		    try
		    {
		    spimSubFolderUrl = spimSubFolderUrl.replaceAll("objectId", rootFolderObjId)	;
		    
		    long lStartTime = System.currentTimeMillis();
			
		    url = new URL(spimSubFolderUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    con.setDoOutput(true);
		    con.setRequestMethod("GET");
		    //con.setRequestProperty("Authorization",imsToken);
		    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
		    {
			    con.setRequestProperty("Authorization",AuthorizationBearerToken);
			    con.setRequestProperty("Authentication",imsToken);

		    }else
		    {
			    con.setRequestProperty("Authorization",imsToken);

		    }	
		    con.setRequestProperty("Content-Type", "application/json");
		    con.setDoInput(true);		   
		    StringBuffer strBuffer = new StringBuffer();
	        InputStream inputStream = con.getInputStream();
	        int i;
		        while ((i = inputStream.read()) != -1) 
		        {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }
		        
			    long lEndTime = System.currentTimeMillis();
                
			    long timeDifference = lEndTime-lStartTime;
			    
			    logger.info("CheckProjectSubFolderExists :::Total Time difference:::"+timeDifference);
		        
		        JSONParser parser = new JSONParser();
		        Object obj = parser.parse(strBuffer.toString());
	 
	            JSONObject jsonObject = (JSONObject) obj;
	 
	            String status = (String) jsonObject.get("status");
	            Long spimTotalObjectCount = (Long) jsonObject.get("spimTotalObjectCount");
	            JSONArray spimObjectList = (JSONArray) jsonObject.get("spimObject");	 
	            //System.out.println("status: " + status);
	            //System.out.println("spimTotalObjectCount: " + spimTotalObjectCount);
	            //System.out.println("\nspimObject List:");
	            @SuppressWarnings("unchecked")
				Iterator<Object> iterator = spimObjectList.iterator();
	            while (iterator.hasNext())
	            {
	                JSONObject spimJsonObj = (JSONObject)iterator.next();
	                String title = (String)spimJsonObj.get("title");
	                String objectId = (String) spimJsonObj.get("objectId");
	            	//System.out.println("title: " + title);
	                if(title!=null&& title.equalsIgnoreCase(subFolderName))
	                {	
	                	//System.out.println("title: " + title);
	                	//System.out.println("objectId: " + objectId);
	                	subFolderObjectId = objectId;
	                	break;
	                }
	            }
		        
		    } catch (MalformedURLException e)
		    {
				e.printStackTrace();
			} catch (Exception e) 
		    {
				e.printStackTrace();
		    } 
		    
		   return  subFolderObjectId;
		
		}
	@Override

		public boolean ShareFolderPermissions(String adminimsToken,List<String> userImsTokens,List<String> useremailId,String ObjectId,String sharePermissionsUrl)
		{
			boolean response =false;
			URL url;
			try
		    {
			
				sharePermissionsUrl = sharePermissionsUrl.replaceAll("objectId", ObjectId);
				//logger.info("sharePermissionsUrl:::"+sharePermissionsUrl);
				HashMap<String,Object> permissionsDetails =new HashMap<String,Object>();
				ArrayList<HashMap<String,Object>>  hashMapArray = new ArrayList<HashMap<String,Object>>();
				HashMap<String,Object> shareObjectAclMap = new HashMap<String,Object>();
				//logger.info("userImsTokens count"+userImsTokens.size());
				int totalImsTokens = userImsTokens.size();
				
				for(int j=0 ;j < totalImsTokens; j++)
				{	
					permissionsDetails.put("userName", userImsTokens.get(j));
					permissionsDetails.put("email", useremailId.get(j));
					permissionsDetails.put("isRegistered", "true");
					ArrayList<String> PermissionArrayList = new ArrayList<String>();
					PermissionArrayList.add("read");
					permissionsDetails.put("permission", PermissionArrayList);
					hashMapArray.add(permissionsDetails);
					shareObjectAclMap.put("isInherit", "false");
					shareObjectAclMap.put("shareObjectAcl", hashMapArray);
				}
				
				
				ObjectMapper objMapper = new ObjectMapper();
			    String jsonData = objMapper.writeValueAsString(shareObjectAclMap);
			    //logger.info("jsonData::::::"+jsonData);
			 	long lStartTime_PERMISION = System.currentTimeMillis();

			    url = new URL(sharePermissionsUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("POST");
			    //con.setRequestProperty("Authorization",adminimsToken);
			    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
			    {
				    con.setRequestProperty("Authorization",AuthorizationBearerToken);
				    con.setRequestProperty("Authentication",adminimsToken);

			    }else
			    {
				    con.setRequestProperty("Authorization",adminimsToken);

			    }	
			    con.setRequestProperty("Content-Type", "application/json");
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(jsonData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();
		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        //logger.info("Response data  :"+ strBuffer.toString());
		        long lEndTime_PERMISION = System.currentTimeMillis();
		        
			 	long permission_difference = lEndTime_PERMISION - lStartTime_PERMISION;
			 	
			 	logger.info("Total Elapsed Time to Set Permissions:::"+permission_difference);
		        
		        String jsonStr = strBuffer.toString();
		        ObjectMapper mapper = new ObjectMapper();
				if (jsonStr!=null && jsonStr.length()>0)
				{
					try {
						org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
						String status=rootNode.path("status").getTextValue();
						//logger.info("statuscode:::"+status);
						if(status!=null && status.equalsIgnoreCase("True"))
						{
							response=true;
						}
	
					} catch (Exception e) 
					{
						logger.error("Error occurred while Parsing the Values from JSON  "+ e);
						e.printStackTrace();
	
					}
				}

		    } catch (MalformedURLException e) {
					e.printStackTrace();
				 } catch (Exception e) {
					e.printStackTrace();
				 }
			
			
				
		  return response	;
		}
	@Override

		public boolean RevokeFolderPermissions(String imsToken,List<String> userImsTokens,String ObjectId,String sharePermissionsUrl)
		{
			boolean response =false;
			URL url;
			try
		    {
			
				sharePermissionsUrl = sharePermissionsUrl.replaceAll("objectId", ObjectId);
				logger.info("RevokeFolderPermissions:::"+sharePermissionsUrl);
	
				HashMap<String,Object> permissionsDetails =new HashMap<String,Object>();
				ArrayList<HashMap<String,Object>>  hashMapArray = new ArrayList<HashMap<String,Object>>();
				HashMap<String,Object> shareObjectAclMap = new HashMap<String,Object>();
				logger.info("userImsTokens count"+userImsTokens.size());
				int totalImsTokens = userImsTokens.size();
				for(int j=0 ;j < totalImsTokens; j++)
				{	
					permissionsDetails.put("userName", userImsTokens.get(j));
					ArrayList<String> PermissionArrayList = new ArrayList<String>();
					PermissionArrayList.add("read");
					permissionsDetails.put("permission", PermissionArrayList);
					hashMapArray.add(permissionsDetails);
					shareObjectAclMap.put("isInherit", "false");
					shareObjectAclMap.put("shareObjectAcl", hashMapArray);
				}
			    
				ObjectMapper objMapper = new ObjectMapper();
			    String jsonData = objMapper.writeValueAsString(shareObjectAclMap);
			    logger.info("jsonData::::::"+jsonData);
			    url = new URL(sharePermissionsUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("PUT");
			    //con.setRequestProperty("Authorization",imsToken);
			    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
			    {
				    con.setRequestProperty("Authorization",AuthorizationBearerToken);
				    con.setRequestProperty("Authentication",imsToken);

			    }else
			    {
				    con.setRequestProperty("Authorization",imsToken);

			    }	
			    con.setRequestProperty("Content-Type", "application/json");
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(jsonData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();
		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        String jsonStr = strBuffer.toString();
		        ObjectMapper mapper = new ObjectMapper();
				if (jsonStr!=null && jsonStr.length()>0)
				{
					try {
						org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
						String status=rootNode.path("status").getTextValue();
						logger.info("statuscode:::"+status);
						if(status!=null && status.equalsIgnoreCase("True"))
						{
							response=true;
						}
	
					} catch (Exception e) 
					{
						logger.error("Error occurred while Parsing the Values from JSON  "+ e);
						e.printStackTrace();
	
					}
				}

		    } catch (MalformedURLException e) {
					e.printStackTrace();
				 } catch (Exception e) {
					e.printStackTrace();
				 }
			
			
				
		  return response	;
		}
	
		public String getDateISO8601Format()
		{
			
			TimeZone tz = TimeZone.getTimeZone("UTC");
		    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		    df.setTimeZone(tz);
		    String nowAsISO = df.format(new Date());
			
			return nowAsISO;
		}
		
		@Override

		  public OpportunityImages uploadPartnerImages(String imsToken,String refreshToken,String userType,String opportunityTitle,String base64ContentStream,String imageTitle,String oppId,String userId)
		  {
	    	boolean response=false;
			OpportunityImages oppImages = new OpportunityImages();
			try {
		    	 
				logger.info("uploadPartnerImages:::CreateRootFolderUrl>>>"+CreateRootFolderUrl);

		    	/*
		    	1. schneiderclims@gmail.com
		    	    password:Mphasis123
		    	2. epartner.closedloop1@gmail.com
		    	    password:Mphasis123
		    	3. epartner.closedloop2@gmail.com
		    	    password:Mphasis123
		    	 */
		    	/* String CreateRootFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items";
		    	 String CreateSubFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/childItems?type=workspace";
		    	 String UploadImageUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/childItems";
		         String sharePermissionsUrl = "https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/ACLs";*/		 
		         //imsToken = "be2f0ebe-3cb1-4a44-8979-7052fd2720dc";
		         String rootFolderName=""; 
		         
		         if(userType!=null && userType.equalsIgnoreCase("partner"))
				 {
				 		String imsId = imsLoginManager.getImsUserId(imsToken);	
				 		logger.debug("imsId:::::"+imsId);				 		
				 		if(imsId!=null && imsId.equalsIgnoreCase("unauthorized"))
				 		{
				 			
				 			IMS ims=null;
				    		ims = imsLoginManager.getAccessTokenFromRefreshToken(refreshToken);	
				    		
				    		if(ims!=null && ims.getAccessToken().length()>0)
				    		{
				    			imsToken = ims.getAccessToken();
						 		imsId = imsLoginManager.getImsUserId(ims.getAccessToken());	
						 		logger.debug("imsId:::::"+imsId);	
				    		} 
				 		}	
				 		
			    		oppImages.setImsId(imsId);
				 }	
		         
		         if(userType!=null && userType.equalsIgnoreCase("partner"))
		         {
		        	 
		        	 //rootFolderName = "CLOSELDLOOP_PARTNER";
		        	 rootFolderName = partnerRootFolderName;
		        	 
		         }else if(userType!=null && userType.equalsIgnoreCase("consumer"))
		         {
		        	 //rootFolderName = "CLOSELDLOOP_CONSUMER";
		        	 rootFolderName = consumerRootFolderName;

		         }
		        
		        /*String adminUser="schneiderclims@gmail.com"; 
		        String adminPassword="Mphasis123";		
		         */
		        String User="bhargavi.p@mphasis.com";
		    	String Password="Mphasis1";
		        
		    	String User1 = "epartner.closedloop1@gmail.com";
		    	String Password1="Mphasis123";
		    	
		    	String User2 = "epartner.closedloop2@gmail.com";
		    	String Password2="Mphasis123";
		    	
		    	logger.info("imsToken::::"+imsToken);
		        
				long lStartTime_CF = System.currentTimeMillis();

		        String projObjectId="";
		        
	             projObjectId = CheckProjectFolderExists(imsToken,CreateRootFolderUrl,rootFolderName);
			         
		        logger.info("projObjectId:::"+projObjectId); 	
		        if(projObjectId!=null && projObjectId.length()>0)
		        {
			        //logger.info("ROOT FOLDER CLOSEDLOOP_CONSUMER EXISTS..."); 	
			        logger.info("ROOT FOLDER" +rootFolderName+ "EXISTS..."); 	
		        	
		        }
		        else
		        {
		        	 projObjectId = CreateProject(rootFolderName,imsToken, CreateRootFolderUrl);
		 	         logger.info("projObjectId:::"+projObjectId); 	
		        }
			     
		        logger.info("projObjectId:::"+projObjectId); 	

		        oppImages.setArtifactRootObjectId(projObjectId);

				long lEndTime_CF = System.currentTimeMillis();
			 	
				long difference_CF = lEndTime_CF - lStartTime_CF;
			 	
		    	logger.info("difference_CF INLUDE BL::::"+difference_CF);
		        
		    	String subFolderObjectId="";
		        
				long lStartTime_CSF = System.currentTimeMillis();

		
		        subFolderObjectId = CheckProjectSubFolderExists(imsToken,CreateSubFolderUrl, projObjectId, opportunityTitle);
		        
		        oppImages.setArtifactSubFolderId(subFolderObjectId);
		        
		        logger.info("subFolderObjectId:::"+subFolderObjectId); 	
	            
		        if(subFolderObjectId!=null && subFolderObjectId.length()>0)
		        {
			        logger.info("PROJECT SUB FOLDER EXISTS..."); 	
		        }
		        else
		        {
		        
		        	subFolderObjectId = CreateSubFolder(opportunityTitle,imsToken,projObjectId,CreateSubFolderUrl);
		        }
		        
		        logger.info("subFolderObjectId::::"+subFolderObjectId);
		    	
				long lEndTime_CSF = System.currentTimeMillis();
			 	
				long difference_CSF = lEndTime_CSF - lStartTime_CSF;
			 	
			 	logger.info("difference_CSF::::"+difference_CSF);

		        
		        //Uploading the Image
			 	long lStartTime_UPLOADIMAGE = System.currentTimeMillis();

		        
			 	String imageObjId = uploadImages(subFolderObjectId,imageTitle,imsToken,UploadImageUrl,base64ContentStream);
		        logger.info("imageObjId::::"+imageObjId);
		        
		        if(imageObjId!=null && imageObjId.length()>0)
		        {
		        	 response=true;
		        }
		        
		        long lEndTime_UPLOADIMAGE = System.currentTimeMillis();
			 	
		        long difference_UPLOADIMAGE = lEndTime_UPLOADIMAGE - lStartTime_UPLOADIMAGE;
			 	
			 	//logger.info("difference_UPLOADIMAGE::::"+difference_UPLOADIMAGE);
			 	
		        oppImages.setArtifactObjectId(imageObjId);
		        
		        oppImages.setUserType(userType);
		        
		        oppImages.setArtifactFormat(".jpg");
       
		        oppImages.setArtifactName(imageTitle);
                 
		        oppImages.setOppTitle(opportunityTitle);
		        
		        oppImages.setOppId(oppId);
		        
		        oppImages.setUserId(userId);
		        
		        // Share Permissions to other Users Block
		        
		    	/*
		    	1.  schneiderclims@gmail.com
		    	    password:Mphasis123
		    	    owner : 57155541-38aa-4099-84ba-fa8133daf9f2
		    	2. epartner.closedloop1@gmail.com
		    	    password:Mphasis123
		    	    owner : a654a1d0-2cee-47cd-9634-97e87a253056
		    	3. epartner.closedloop2@gmail.com
		    	    password:Mphasis123
		            owner : 57155541-38aa-4099-84ba-fa8133daf9f2        	    
		    	4.  bharghavi.p@mphasis.com
		    	    owner : b67c9e95-5321-4e8b-9aff-273abc2bee84
		    	    
		    	 Prod ims consumer Account details   
		    	    
		    	    schneiderclims02@gmail.com
		    	    password : Mphasis123*
		    	    owner : 98df487e-6316-4183-947e-6bcdbf32ad00
		    	     
		    	 */
			 	
			 	//long lStartTime_PERMISION = System.currentTimeMillis();
                		        
		        if(userType!=null && userType.equalsIgnoreCase("consumer"))
		        {	
			        boolean sharePermissions=false;
			    	List<String> partnerImsTokensList = new ArrayList<String>();
			    	List<String> partnerImsEmailIdList = new ArrayList<String>();
			    	
			        partnerImsTokensList.add(spimAdminUserImsFederatedId);
			        partnerImsEmailIdList.add(spimAdminUserEmailId);
			        partnerImsTokensList.add("a654a1d0-2cee-47cd-9634-97e87a253056");
			        partnerImsEmailIdList.add(spimAdminUserEmailId);
			        sharePermissions = ShareFolderPermissions(imsToken,partnerImsTokensList,partnerImsEmailIdList,imageObjId,sharePermissionsUrl);
			    	//logger.info("sharePermissions::::::"+sharePermissions);
			    	
			    	partnerImsTokensList.add(spimAdminUserImsFederatedId);
				    partnerImsEmailIdList.add(spimAdminUserEmailId);
			    	partnerImsTokensList.add("57155541-38aa-4099-84ba-fa8133daf9f2");
				    partnerImsEmailIdList.add(spimAdminUserEmailId);
			    	sharePermissions = ShareFolderPermissions(imsToken,partnerImsTokensList,partnerImsEmailIdList,imageObjId,sharePermissionsUrl);
			    	//logger.info("sharePermissions::::::"+sharePermissions);
			       
		        }
		        
		       
		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Exception in uploadPartnerImages:::"+e.getMessage());
			}
		     
		     return oppImages;
		  }
		
		
		/* Method to upload Consumer Images
		 * (non-Javadoc)
		 * @see com.se.cl.spim.client.SpimHandler#uploadConsumerImages(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.List, java.util.List, java.lang.String, java.lang.String)
		 */
		@Override

		  public List<OpportunityImages> uploadConsumerImages(String imsToken,String refreshToken,String userType,String opportunityTitle,List<String> base64ContentStream,List<String> imageTitle,String oppId,String userId)
		  {
	    	boolean response=false;
			OpportunityImages oppImages = null;
			List<OpportunityImages> finalConsumerUploadedImages = new ArrayList<OpportunityImages>();
			try {
		    	 
				logger.info("uploadConsumerImages:::CreateRootFolderUrl>>>"+CreateRootFolderUrl);
		         String rootFolderName=""; 
		         
		         if(userType!=null && userType.equalsIgnoreCase("partner"))
		         {
		        	 
		        	 rootFolderName = partnerRootFolderName;
		        	 
		         }else if(userType!=null && userType.equalsIgnoreCase("consumer"))
		         {
		        	 rootFolderName = consumerRootFolderName;

		         }
		        
		        String adminUser="schneiderclims@gmail.com"; 
		        String adminPassword="Mphasis123";		
		         
		        String User="bhargavi.p@mphasis.com";
		    	String Password="Mphasis1";
		        
		    	String User1 = "epartner.closedloop1@gmail.com";
		    	String Password1="Mphasis123";
		    	
		    	String User2 = "epartner.closedloop2@gmail.com";
		    	String Password2="Mphasis123";
		    	
		    	if(userType!=null && userType.equalsIgnoreCase("consumer"))
		    	{	
		    		IMS ims=null;
		    		ims = imsLoginManager.getAuthenticateUserToken(spimAdminUser, spimAdminPassword);	
		    		if(ims!=null)
		    		{
		    			imsToken = ims.getAccessToken();
		    		}
		    	}
		    	if(imsToken!=null && imsToken.length()>0)
		    		
		    		imsToken = imsToken;
		    	
		    	logger.info("imsToken::::"+imsToken);
		        
				long lStartTime_CF = System.currentTimeMillis();

		        String projObjectId="";
		        
		        projObjectId = CheckProjectFolderExists(imsToken,CreateRootFolderUrl,rootFolderName);
			         
		        logger.info("projObjectId:::"+projObjectId); 	
		        if(projObjectId!=null && projObjectId.length()>0)
		        {
			        //logger.info("ROOT FOLDER CLOSEDLOOP_CONSUMER EXISTS..."); 	
			        logger.info("ROOT FOLDER" +rootFolderName+ "EXISTS..."); 	
		        	
		        }
		        else
		        {
		        	 projObjectId = CreateProject(rootFolderName,imsToken, CreateRootFolderUrl);
		 	         logger.info("projObjectId:::"+projObjectId); 	
		        }
			     
		        
		        logger.info("projObjectId:::"+projObjectId); 	
	        
		        //oppImages.setArtifactRootObjectId(projObjectId);

				long lEndTime_CF = System.currentTimeMillis();
			 	
				long difference_CF = lEndTime_CF - lStartTime_CF;
			 	
		    	logger.info("difference_CF INLUDE BL::::"+difference_CF);
		        
		    	String subFolderObjectId="";
		        
				long lStartTime_CSF = System.currentTimeMillis();

		
		        subFolderObjectId = CheckProjectSubFolderExists(imsToken,CreateSubFolderUrl, projObjectId, opportunityTitle);
		        
		        
		        logger.info("subFolderObjectId:::"+subFolderObjectId); 	
	            
		        if(subFolderObjectId!=null && subFolderObjectId.length()>0)
		        {
			        logger.info("PROJECT SUB FOLDER EXISTS..."); 	
		        }
		        else
		        {
		        
		        	subFolderObjectId = CreateSubFolder(opportunityTitle,imsToken,projObjectId,CreateSubFolderUrl);
		        }
		        
		        logger.info("subFolderObjectId::::"+subFolderObjectId);
		    	
				long lEndTime_CSF = System.currentTimeMillis();
			 	
				long difference_CSF = lEndTime_CSF - lStartTime_CSF;
			 	
			 	logger.info("difference_CSF::::"+difference_CSF);

		        //Uploading the Image
			 	long lStartTime_UPLOADIMAGE = System.currentTimeMillis();
			 	String imageObjId="";
		        
			 	List<String> partnerImsFederatedIdsList =  getPartnerImsFederatedIDsByCountry(CountryCode.SWEDEN.getValue());
			 	if(partnerImsFederatedIdsList!=null && partnerImsFederatedIdsList.size()>0)
			 	{
			 		logger.debug("partnerImsFederatedIdsList:Size is"+partnerImsFederatedIdsList.size());
			 	}	
			 	for(int i=0;i<base64ContentStream.size();i++)
		        {	
		        	
		        	logger.info("imageTitle::::"+imageTitle.get(i));
		        	//logger.info("imageData::::"+base64ContentStream.get(i));

		        	oppImages = new OpportunityImages(); 
		        	imageObjId = uploadImages(subFolderObjectId,imageTitle.get(i),imsToken,UploadImageUrl,base64ContentStream.get(i));
		        	logger.info("imageObjId::::"+imageObjId);
		        
			        if(imageObjId!=null && imageObjId.length()>0)
			        {
			        	 response=true;
			        }
		        
			        long lEndTime_UPLOADIMAGE = System.currentTimeMillis();
			 	
			        long difference_UPLOADIMAGE = lEndTime_UPLOADIMAGE - lStartTime_UPLOADIMAGE;
			 	
			        //logger.info("difference_UPLOADIMAGE::::"+difference_UPLOADIMAGE);
			 	
			        oppImages.setArtifactRootObjectId(projObjectId);

			        oppImages.setArtifactSubFolderId(subFolderObjectId);
			        
			        oppImages.setArtifactObjectId(imageObjId);
			        
			        oppImages.setUserType(userType);
			        
			        oppImages.setArtifactFormat(".jpg");
	     
			        oppImages.setArtifactName(imageTitle.get(i));
	               
			        oppImages.setOppTitle(opportunityTitle);
			        
			        oppImages.setOppId(oppId);
			        
			        oppImages.setUserId(userId);
			        
			        finalConsumerUploadedImages.add(oppImages);
			        
			        if(userType!=null && userType.equalsIgnoreCase("consumer"))
			        {	
				        boolean sharePermissions=false;
				    	List<String> partnerImsTokensList = new ArrayList<String>();
				    	List<String> partnerImsEmailIdList = new ArrayList<String>();
				    	
				        /*partnerImsTokensList.add("3805bb22-aa40-4223-b296-4f6acb380ef1");
				        partnerImsEmailIdList.add("schneiderclims@gmail.com");
				        partnerImsTokensList.add("a654a1d0-2cee-47cd-9634-97e87a253056");
				        partnerImsEmailIdList.add("schneiderclims@gmail.com");
				        sharePermissions = ShareFolderPermissions(imsToken,partnerImsTokensList,partnerImsEmailIdList,imageObjId,sharePermissionsUrl);
				    	//logger.info("sharePermissions::::::"+sharePermissions);
				    	
				    	partnerImsTokensList.add("3805bb22-aa40-4223-b296-4f6acb380ef1");
					    partnerImsEmailIdList.add("schneiderclims@gmail.com");
				    	partnerImsTokensList.add("57155541-38aa-4099-84ba-fa8133daf9f2");
					    partnerImsEmailIdList.add("schneiderclims@gmail.com");
				    	sharePermissions = ShareFolderPermissions(imsToken,partnerImsTokensList,partnerImsEmailIdList,imageObjId,sharePermissionsUrl);
				    	//logger.info("sharePermissions::::::"+sharePermissions);
				       */
			    	    
				    	if(partnerImsFederatedIdsList!=null && partnerImsFederatedIdsList.size()>0)
				    	{	
					    	for(String partnerImsFedId :partnerImsFederatedIdsList)
					    	{
					    		logger.debug("spimAdminUserImsFederatedId:"+spimAdminUserImsFederatedId);
					    		logger.debug("partnerImsFedId:"+partnerImsFedId);
					    		logger.debug("spimAdminUserEmailId:"+spimAdminUserEmailId);

					    		partnerImsTokensList.add(spimAdminUserImsFederatedId);
						        partnerImsEmailIdList.add(spimAdminUserEmailId);
						        partnerImsTokensList.add(partnerImsFedId);
						        partnerImsEmailIdList.add(spimAdminUserEmailId);
						        sharePermissions = ShareFolderPermissions(imsToken,partnerImsTokensList,partnerImsEmailIdList,imageObjId,sharePermissionsUrl);
						    	logger.info("sharePermissions::::::"+sharePermissions);					    	
					    		
					    	}
				    	}
				    	
				    
				       
			        }
			        
		        }
		        
		        		          
		       		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		     
		     return finalConsumerUploadedImages;
		  }
		

		
		@Override

		public List<Map<String,String>> getListOfUploadedImages(String imsToken,String userType,String opportunityTitle)
		  {
	    	boolean response=false;
	    	List<Map<String,String>> responseImageFolderObjIdList=null;
			try {
		    	 
		    	/*
		    	 String CreateRootFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items";
		    	 String CreateSubFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/childItems?type=workspace";
		    	 String UploadImageUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/childItems";
		    	 String DownloadImageUrl="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/bytes";
		    	 String FetchSubFoldersUrl= "https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/childItems";*/
		         String rootFolderName=""; 
		         if(userType!=null && userType.equalsIgnoreCase("partner"))
		         {
		        	 
		        	 rootFolderName = "CLOSELDLOOP_PARTNER";
		        	 
		         }else if(userType!=null && userType.equalsIgnoreCase("consumer"))
		         {
		        	 rootFolderName = "CLOSELDLOOP_CONSUMER";

		         }
		        logger.info("imsToken::::"+imsToken);
		        String projObjectId="";
		        projObjectId = CheckProjectFolderExists(imsToken,CreateRootFolderUrl,rootFolderName);
		        logger.info("projObjectId:::"+projObjectId); 	
		        String subFolderObjectId="";
		        subFolderObjectId = CheckProjectSubFolderExists(imsToken,CreateSubFolderUrl, projObjectId, opportunityTitle);
		        logger.info("subFolderObjectId:::"+subFolderObjectId); 	
	            logger.info("subFolderObjectId::::"+subFolderObjectId);
		    	
		       	// Get The Map for all Images Byte Content
		        
			 	long lStartTime_GETIMAGEIDS = System.currentTimeMillis();

		        responseImageFolderObjIdList = new ArrayList<Map<String ,String>>();
		    	responseImageFolderObjIdList = FetchProjectSubFolderImageObjectIds(subFolderObjectId,imsToken,FetchSubFoldersUrl,userType);
		    	logger.info("responseImageFolderObjIdList::::"+responseImageFolderObjIdList);
		    	
		    	 long lEndTime_GETIMAGEIDS = System.currentTimeMillis();
				 long difference_GETIMAGEIDS = lEndTime_GETIMAGEIDS - lStartTime_GETIMAGEIDS;
				 logger.info("difference_GETIMAGEIDS::::"+difference_GETIMAGEIDS);
		    	
		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		     
		     return responseImageFolderObjIdList;
		  }
		
		@Override
 
		public Map<String,Object> getImagesData(String imsToken,String refreshToken,String userType,String opportunityTitle,String imageObjectId)
		  {
	    	Map<String,Object> imageDataMap=new HashMap<String,Object>();
			try {
		    	
				// String CreateRootFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items";
		    	// String CreateSubFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/childItems?type=workspace";
		    	// String DownloadImageUrl="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/bytes";
		    	 String rootFolderName=""; 
		         if(userType!=null && userType.equalsIgnoreCase("partner"))
		         {
		        	 
		        	 rootFolderName = "CLOSELDLOOP_PARTNER";
		        	 
		         }else if(userType!=null && userType.equalsIgnoreCase("consumer"))
		         {
		        	 rootFolderName = "CLOSELDLOOP_CONSUMER";

		         }
		        
		       	// Get The Images Byte Content
		        if(userType!=null && userType.equalsIgnoreCase("partner"))
			 	{
			 		String imsId = imsLoginManager.getImsUserId(imsToken);	
			 		logger.debug("imsId:::::"+imsId);				 		
			 		if(imsId!=null && imsId.equalsIgnoreCase("unauthorized"))
			 		{
			 			
			 			IMS ims=null;
			    		ims = imsLoginManager.getAccessTokenFromRefreshToken(refreshToken);	
			    		
			    		if(ims!=null && ims.getAccessToken().length()>0)
			    		{
			    			imsToken = ims.getAccessToken();
			    			refreshToken=ims.getRefreshToken();
					 		imsId = imsLoginManager.getImsUserId(ims.getAccessToken());	

			    		} 
			 		}	
			 		
		    		
			 	}	
				imageDataMap =  FetchProjectSubFolderImageContent(imageObjectId,imsToken, refreshToken,DownloadImageUrl);
		    	//logger.info("imageDataMap::::"+imageDataMap);
		    	
		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		     
		     return imageDataMap;
		  }
	
		@Override

		public List<Map<String,String>> getListOfConsumerUploadedImages(String imsToken,String userType,String opportunityTitle)
		  {
	    	boolean response=false;
	    	List<Map<String,String>> responseImageFolderObjIdList=null;
			try {
		    	 
		    	
		    	 //String CreateRootFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items";
		    	 //String CreateSubFolderUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/childItems?type=workspace";
		    	 //String UploadImageUrl ="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/childItems";
		    	 //String DownloadImageUrl="https://spim-qa.blcm.schneider-electric.com:443/spim-document/items/objectId/bytes";
		    	 //String FetchSubFoldersUrl= "https://spim-qa.blcm.schneider-electric.com:443/spim/items/objectId/childItems";
		         String rootFolderName=""; 
		         if(userType!=null && userType.equalsIgnoreCase("partner"))
		         {
		        	 
		        	 rootFolderName = "CLOSELDLOOP_PARTNER";
		        	 
		         }else if(userType!=null && userType.equalsIgnoreCase("consumer"))
		         {
		        	 rootFolderName = "CLOSELDLOOP_CONSUMER";

		         }
		        
		         logger.info("imsToken::::"+imsToken);
		        
		        // Get The Map for all Images Byte Content
		        
		        responseImageFolderObjIdList = new ArrayList<Map<String ,String>>();
		    	responseImageFolderObjIdList =  new SpimHandlerImpl().FetchConsumerProjectSubFolderImageObjectIds("",imsToken,CreateRootFolderUrl,userType);
		    	//logger.info("responseImageFolderObjIdList::::"+responseImageFolderObjIdList);
		    	
		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		     
		     return responseImageFolderObjIdList;
		  }
		
		
		/**
		 * @param subFolderObjectId
		 * @param imsToken
		 * @param ImageSpimUrl
		 * @param userType
		 * @return
		 */
		
		@Override
		
		public List<Map<String,String>> FetchConsumerProjectSubFolderImageObjectIds(String subFolderObjectId,String imsToken,String ImageSpimUrl,String userType)
		{
			URL url;
			Map<String ,Object>  ResponseMap = null;
			ArrayList<Map<String,String>> SubFolderImageObjectIds=new ArrayList<Map<String,String>>();
			HashMap<String,String> imageHashMap =new HashMap<String,String>();
			logger.info("ImageSpimUrl:::"+ImageSpimUrl);
		    try
		    {
	
		    //ImageSpimUrl = ImageSpimUrl.replaceAll("objectId", subFolderObjectId);
		    url = new URL(ImageSpimUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    con.setDoOutput(true);
		    con.setRequestMethod("GET");
		    //con.setRequestProperty("Authorization",imsToken);
		    if(AuthorizationBearerToken!=null && AuthorizationBearerToken.length()>0)
		    {
			    con.setRequestProperty("Authorization",AuthorizationBearerToken);
			    con.setRequestProperty("Authentication",imsToken);

		    }else
		    {
			    con.setRequestProperty("Authorization",imsToken);

		    }	
		    con.setRequestProperty("Content-Type", "application/json");
		    con.setDoInput(true);		   
		    StringBuffer strBuffer = new StringBuffer();
	        InputStream inputStream = con.getInputStream();
	        int i;
	        while ((i = inputStream.read()) != -1) 
	        {
	            Writer writer = new StringWriter();
	            writer.write(i);
	            strBuffer.append(writer.toString());
	        }			    
	        
	        //logger.info("FetchProjectSubFolderImages response data  :"+ strBuffer.toString());
	        JSONParser parser = new JSONParser();
	        Object obj = parser.parse(strBuffer.toString());
            JSONObject jsonObject = (JSONObject) obj;
            Long spimTotalObjectCount = (Long) jsonObject.get("spimTotalObjectCount");
            JSONArray spimObjectList = (JSONArray) jsonObject.get("spimObject");	 
            logger.debug("spimTotalObjectCount: " + spimTotalObjectCount);
            logger.debug("\nspimObject List:");
            @SuppressWarnings("unchecked")
			Iterator<Object> iterator = spimObjectList.listIterator();
            logger.info("count:::::"+spimObjectList.size());
            //while (iterator.hasNext())
            for(int j=0;j<spimObjectList.size();j++)
            {
            	     logger.info("j::::"+j+"");
            		 JSONObject spimJsonObj = (JSONObject)iterator.next();
            		 String objectId = (String) spimJsonObj.get("objectId");
                     String title = (String) spimJsonObj.get("title");
                     String mimeType=(String) spimJsonObj.get("mimeType");
                     logger.info("mimeType"+mimeType);
                     int cnt=0;
                     if(mimeType!=null && mimeType.equalsIgnoreCase("image/jpeg"))
                     { 	 
	                    // cnt = cnt+1;
                    	 logger.info("inside if block");
                    	 logger.debug("objectId: " + objectId);
	                     logger.debug("title: " + title);
	                     imageHashMap = new HashMap<String ,String>();
	                     imageHashMap.put("objectId"+"",objectId);
	                     imageHashMap.put("title",title);
	                     imageHashMap.put("userType",userType);
	                     SubFolderImageObjectIds.add(imageHashMap);
                     }
            }
			
		    } catch (MalformedURLException e) {
					e.printStackTrace();
				 } catch (Exception e) {
					e.printStackTrace();
				 }
			
		  return SubFolderImageObjectIds;
			
			
		}
		
		public static byte[] getBytesFromInputStream(InputStream is) throws IOException {

		    int len;
		    int size = 1024;
		    byte[] buf;

		    if (is instanceof ByteArrayInputStream) {
		      size = is.available();
		      buf = new byte[size];
		      len = is.read(buf, 0, size);
		    } else {
		      ByteArrayOutputStream bos = new ByteArrayOutputStream();
		      buf = new byte[size];
		      while ((len = is.read(buf, 0, size)) != -1)
		        bos.write(buf, 0, len);
		      buf = bos.toByteArray();
		    }
		    return buf;
		  }

		@Override
		public String getPartnerImsFederatedID(String partnerId) {
			// TODO Auto-generated method stub
			
			return partnerDAO.getPartnerImsFederatedID(partnerId);
			
		}
		@Override
		public List<String> getPartnerImsFederatedIDsByCountry(String countryCode) 
		{
			// TODO Auto-generated method stub
			
			return partnerDAO.getPartnerImsFederatedIDsByCountry(countryCode);
			
		}
		
}
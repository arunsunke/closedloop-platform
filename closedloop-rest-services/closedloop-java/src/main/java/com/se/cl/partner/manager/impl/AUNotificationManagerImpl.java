package com.se.cl.partner.manager.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.notnoop.exceptions.NetworkIOException;
import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.ConsumerDAO;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerPartner;
import com.se.cl.model.Opportunity;
import com.se.cl.model.PartnerDevice;
import com.se.cl.partner.manager.NotificationManager;
import com.se.cl.partner.manager.PartnerManagerFactory;
import com.se.cl.util.PropertyLoader;
import com.se.clm.bfo.client.HttpsClient;
import com.se.clm.bfo.manager.BFOManagerFactory;
import com.se.clm.mail.EmailTemplates;
import com.se.clm.mail.FREmailTemplates;
import com.se.clm.mail.IEmailService;
@Component("AustraliaNotification")
public class AUNotificationManagerImpl implements NotificationManager{

	@Autowired
	PartnerDAO partnerDAO;
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	PartnerManagerFactory partnerFactory;
	
	@Autowired
	IEmailService iEmailService;
	
	@Autowired
	BFOManagerFactory bfoManagerFactory;

	@Autowired
	HttpsClient httpsClient;
	
	@Autowired
	ConsumerDAO consumerDAO;
	
	@Autowired
	OpportunityManager opportunityManager;




	private @Value("${se.admin.email}") String seAdminEmail;
	private @Value("${accept-sla-step1}") Long slaStep1;
	private @Value("${accept-sla-step2}") Long slaStep2;
	private @Value("${accept-sla-step3}") Long slaStep3;

	

	//private @Value("${partner-count}") String propertyField;

	private @Value("${google.api.key}") String googleApiKey;
	private @Value("${google.push.notification.url}") String googlePushNotificationURL;
	private @Value("${ios.certificate.password}") String iosCertificatePassword;

	private @Value("${solution30.proxy}") String solution30Proxy;

	private static final Logger LOGGER = LoggerFactory.getLogger(AUNotificationManagerImpl.class);
    private static final String bfoManagerName="AUB";


	@Override
	public void notifyPartnerForSALRemider(int slaStep,String country) throws AppException {
		
	
		try{
			HashMap<String,List<String>>  map=partnerDAO.getPartnerListForSLAReminder(slaStep,country);
			
			notifyPartner(map,"SLA",slaStep);
			sendEmailsToPartners(map,"SLA",slaStep);
			//
		}catch(Exception e){
			e.printStackTrace();
		}
		//send emails
	
		


	}

 private void sendEmailsToPartners(HashMap<String,List<String>> map,String noteType,int slaStep){
	 
	 
	 
		
		if (map.get("OPP-PARTNER")!=null && map.get("OPP-PARTNER").size()>0){
			
			List<String> partnerIds=map.get("OPP-PARTNER");
			List<String> template=new ArrayList<String>();
			
			if (slaStep==0){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);

					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					
					template = EmailTemplates.sendEmailtoPartnerForNewRequest(
							opp.getPartner().getEmail(),opp.getId(),opp.getPartner()
									.getName(), opp.getTitle(), opp
									.getCustomerDetails().getFirstName(), opp
									.getAddress().getZip(), opp.getBudget(),opp.getOpportunityDate(),opp.getCustomerDetails().getPhone(),opp.getCustomerDetails().getEmail());
					
					try {
						
					
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						Thread.sleep(3000);
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			
			if (slaStep==1){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = EmailTemplates
							.sendEmailtoPartnerForRemainderOne(opp.getPartner()
									.getEmail(), opp.getId(),opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget(),opp.getOpportunityDate(),opp.getCustomerDetails().getPhone(),opp.getCustomerDetails().getEmail());
					

					try {
						
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						Thread.sleep(3000);
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			if (slaStep==2){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = EmailTemplates
							.sendEmailtoPartnerFOrRemainderTwo(opp.getPartner()
									.getEmail(), opp.getId(),opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget(),opp.getOpportunityDate(),opp.getCustomerDetails().getPhone(),opp.getCustomerDetails().getEmail());
					

					try {
						
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						Thread.sleep(3000);
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			
			if (slaStep==3){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = EmailTemplates
							.sendEmailtoPartnerFOrRemainderThree(opp.getPartner()
									.getEmail(),opp.getId() ,opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget(),opp.getOpportunityDate(),opp.getCustomerDetails().getPhone(),opp.getCustomerDetails().getEmail());
					

					try {
						
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						Thread.sleep(3000);
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
						iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			//update opp with sla trigger status
			if(map.get("OPPORTUNITY")!=null && map.get("OPPORTUNITY").size()>0){
				try {
					opportunityManager.updateOpportunity(null,map.get("OPPORTUNITY"),noteType,slaStep, null, UpdSource.SLA,false);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//opportunityDAO.updateOppNotiStatus(map.get("OPPORTUNITY") ,noteType,slaStep);
				//update notification status

			}


		}
		
		
 }


	public void notifyPartnerForNewOpportunity(String country) throws AppException {

		try{
			HashMap<String,List<String>>  map=partnerDAO.getPartnerListForNewOpps(country);
			slaStep1=Long.valueOf(PropertyLoader.getSparQlProperty("fr.accept.sla.step1"));
			
			notifyPartner(map,"NEWOPP",0);
			sendEmailsToPartners(map,"NEWOPP",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		//return null;
	}




	private void notifyPartner(HashMap<String,List<String>> map,String noteType,int slaType) throws AppException{

		if (map.get("PARTNER")!=null && map.get("PARTNER").size()>0){

			List<PartnerDevice> deviceList=getDeviceIdsForPartnes( map.get("PARTNER"));

			if (deviceList!=null && deviceList.size()>0){
				try{	
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
					for(PartnerDevice partnerDevice:deviceList){
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS")){
							iosList.add(partnerDevice.getDeviceId());
						}else{
							androidList.add(partnerDevice.getDeviceId());
						}
					}

					//below hardcoded for now .. needs to be computed from prop file
					if(slaType==0){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("New Opportunity", "Clipsal Connect new lead. You have a customer lead and you can view all the details by logging in to the Clipsal Connect mobile app. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("New Opportunity", "Clipsal Connect new lead. You have a customer lead and you can view all the details by logging in to the Clipsal Connect mobile app. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , androidList);
						}
					}else if(slaType==1){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Clipsal Connect reminder", "You have a customer lead pending a response within"+slaStep1 +" You can view all the details by logging in to the Clipsal Connect mobile app.  Thanks, Clipsal and Schneider Electric Consumer Support Team.", iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Clipsal Connect reminder", "You have a customer lead pending a response within"+slaStep1+"You can view all the details by logging in to the Clipsal Connect mobile app.  Thanks, Clipsal and Schneider Electric Consumer Support Team", androidList);
						}
						
					}else if(slaType==2){
						
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Second Reminder ", "Opportunity Assigned to you "+slaStep2+"hrs Ago" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Second Reminder", "Opportunity Assigned to you "+slaStep2+"hrs Ago" , androidList);
						}
					}else if(slaType==3){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Third Reminder", "Opportunity Assigned to you "+slaStep3+"hrs Ago, You have lost the opportunity" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Third Reminder", "Opportunity Assigned to you "+slaStep3+"hrs Ago, You have lost the opportunity" , androidList);
						}
					}
					
					
					

				}catch(Exception e){
					e.printStackTrace();
				}
			}


			
		}


	}


	private List<PartnerDevice> getDeviceIdsForPartnes(List<String> partnerIds) throws AppException {
		// TODO Auto-generated method stub

		return  partnerDAO.listPartnerDeviceByIds(partnerIds);


	}






	public ObjectNode pushNotificationToAndroid(String messageTitle, String message,List<String> deviceList) {

		ObjectNode response = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.getJsonFactory().configure(Feature.ESCAPE_NON_ASCII, false);
			response = mapper.getNodeFactory().objectNode();
			//List<PartnerDevice> deviceList = opportunityDAO.listPartnerDevice("Android");

			if(deviceList == null || (deviceList != null && deviceList.size() == 0)){
				response.put("errorMsg", "Android | No devices found for sending push notification");
				response.put("status", "error");
				LOGGER.debug("Android | No devices found for sending push notification");
				return response;
			}

			ObjectNode requestJSON = mapper.getNodeFactory().objectNode();
			ObjectNode dataNode = mapper.getNodeFactory().objectNode();
			dataNode.put("title", messageTitle);
			dataNode.put("message", message);
			requestJSON.put("data", dataNode);
			ArrayNode deviceIdArray = mapper.getNodeFactory().arrayNode();
			for(String device : deviceList){
				deviceIdArray.add(device);
			}
			requestJSON.put("registration_ids", deviceIdArray);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", "key="+"AIzaSyBkUP-vVMXxRveUWci2RGn8mOy8auW0INY");

			HttpEntity<String> entityRequest = new HttpEntity<String>(requestJSON.toString(), headers);

			RestTemplate restTemplate = new RestTemplate();
			//add proxy if required
			//	SimpleClientHttpRequestFactory factory =   ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory());
			//	Proxy proxy= new Proxy(Type.HTTP, new InetSocketAddress("proxy-ip", proxy-port));
			//	factory.setProxy(proxy);
			//	restTemplate = new RestTemplate(factory);

			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			
			JSONObject restResponse = restTemplate.postForObject("https://android.googleapis.com/gcm/send", entityRequest, JSONObject.class);
			
			response = (ObjectNode) mapper.readTree(restResponse.toJSONString());
			
			
			
			LOGGER.debug("Android push notification response = " + response);

		} catch (Exception e) {
			e.printStackTrace();
			response.put("errorMsg", "Error while sending Android push notification");
			response.put("status", "error");
			LOGGER.error("Error while sending Android push notification", e);
		}

		return response;

	}



	public ObjectNode pushNotificationToIOS(String messageTitle, String message,List<String> deviceList) {
		/*
		 *Reference :
		 *https://github.com/notnoop/java-apns
		 *http://developement.sebastian-bothe.de/?page_id=40
		 *and
		 *http://blog.synyx.de/2010/07/sending-apple-push-notifications-with-notnoops-java-apns-library/
		 */		
		//iosCertificatePassword="SCHNEIDER";
		ApnsService service = null;
		InputStream certStream = null;
		ObjectNode response = null;
		boolean hasError = false;
		boolean checkPortAvailable=false;
		boolean sendnotification=false;
		//if(sendnotification)
		{

			checkPortAvailable = checkPortAvailable(2195);		
			LOGGER.info("checkPortAvailable:::"+checkPortAvailable);
			if(checkPortAvailable==true)
			{
				try {
					ObjectMapper mapper = new ObjectMapper();
					mapper.getJsonFactory().configure(Feature.ESCAPE_NON_ASCII, false);
					response = mapper.getNodeFactory().objectNode();
	
					//List<PartnerDevice> deviceList = opportunityDAO.listPartnerDevice("iOS");
					if(deviceList == null || (deviceList != null && deviceList.size() == 0)){
						response.put("errorMsg", "iOS | No devices found for sending push notification");
						response.put("status", "error");
						LOGGER.debug("iOS | No devices found for sending push notification");
						return response;
					}
	
					//certStream = this.getClass().getClassLoader().getResourceAsStream("ck4.p12");
	
					certStream = this.getClass().getClassLoader().getResourceAsStream("ClosedLoopAU.p12");
	
	
					//For production environment
					//Add proxy if required
	
					LOGGER.info("pushNotificationToIOS:Proxy:::"+solution30Proxy);
	
					if(solution30Proxy!=null && solution30Proxy.equalsIgnoreCase("1"))
					{	
						Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("205.167.7.126", 80));
						service = APNS.newService()
								.withCert(certStream, iosCertificatePassword)
								.withProductionDestination()
								.withProxy(proxy)
								.build();
	
					}
					else
					{
	
						service = APNS.newService()
								.withCert(certStream, iosCertificatePassword)
								.withProductionDestination()
								.build();
					}
					/*boolean testconnection =false;
					try 
					{
					
						service.testConnection();
						testconnection=true;
						
					}catch(NetworkIOException e)
					{
	                 LOGGER.info("oops network exception");
	                 testconnection=false;
					}*/
					
					     LOGGER.info("inside test connection if it is success then start the service");
		                 service.start();
					
						for (String device : deviceList)
						{
							try {
								//badge : The number of updates to display.
								Thread.sleep(3000);
								int badge = 1;
								PayloadBuilder payloadBuilder = APNS.newPayload();
								payloadBuilder = payloadBuilder.badge(badge).alertBody(message);
		
								//check if the message is too long, shrink it if so.
								if (payloadBuilder.isTooLong()) {
									LOGGER.debug("Push message is too long, shrinking the content to guarantee the delivery.");
									payloadBuilder = payloadBuilder.shrinkBody();
								}
								String payload = payloadBuilder.build();
								String token = device;
								service.push(token, payload);
							} catch (Exception e) {
								hasError = true;
								response.put("status", "error");
								response.put("message", "iOS | Error while sending push notification");
								LOGGER.error("iOS | Error while sending push notification", e);
							}
						}
					
				} catch (Exception ex) {
	
					if(!hasError){
						hasError = true;
						response.put("status", "error");
						response.put("message", "iOS | Error while starting the push notification service");
					}
	
					LOGGER.error("iOS | Error while starting the push notification service", ex);
				} finally {
	
					if (service != null) {
						//service.stop();
					}
	
					if(certStream != null)
						try {
							certStream.close();
						} catch (IOException e) {}
	
				}
	
				if(!hasError){
					response.put("status", "success");
					response.put("message", "iOS | Push notification sent successfully");
				}
	
			}

		}
		return response;
	}


	public boolean checkPortAvailable(int port)
	{
		boolean portExists=false;
		ServerSocket serverSocket=null;
		try {
			serverSocket = new ServerSocket(port);
			portExists=true;
		} catch (IOException e) {
			System.out.println("Could not listen on port: " + port);
			portExists=false;
			// ...
		}

		return portExists;
	}



	@Override
	public void sendMail() {
		// TODO Auto-generated method stub
		iEmailService.sendApprovedMail("sudhir", "sudhir.marni@gmail.com", "test", BigInteger.valueOf(123),"sdsad" , "sdfsfds");

	}




	@Override
	public void sendMailToConsumer(String consumerEmail,String oopId,String partnerName,String projectName,String consumerName,String zipCode,String budget,String templateId,String date,String custNumber,String custMail,List<BOM> boms,String address,String city) {
		// TODO Auto-generated method stub
		List<String> templateList=new ArrayList<String>();
		if (templateId.equalsIgnoreCase("NewOpp")){
			templateList = EmailTemplates.sendEmailtoPartnerForNewRequest(
					consumerEmail,oopId, partnerName, projectName, consumerName, zipCode,
					budget,date,custNumber,custMail);
		}else{
			templateList = EmailTemplates.sendEmailtoPartnerForNewRequest(
					consumerEmail,oopId, partnerName, projectName, consumerName, zipCode,
					budget,date,custNumber,custMail);
			
		}
		
		try {
		   // iEmailService.sendMail(consumerEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
			iEmailService.sendMail("sunke.arun@gmail.com", templateList.get(1), templateList.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	public void sendMailNoPartnerAccepted(String country){
		
		try{
			List<Opportunity> opps=partnerFactory.getPartnerManger("AU").getOppWhenNoPartnerAccepted(country);
			List<String> templateList=new ArrayList<String>();
			if (opps!=null && opps.size()>0){
				
				for(Opportunity opp:opps)
				{
					
					
					templateList = EmailTemplates
							.sendEmailtoSEAdminWithNoPartnerAnswered(seAdminEmail,
									opp.getId(), opp.getCustomerDetails().getFirstName(), opp.getAddress().getZip(), opp.getBudget());		
				
					try {
						//iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
						iEmailService.sendMail("sunke.arun@gmail.com", templateList.get(1), templateList.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					try {
						Opportunity oppSend=new Opportunity();
						oppSend.setId(opp.getId());
						oppSend.setStatus(OppStatus.PARKED.getValue());
						opportunityManager.updateOpportunity(oppSend, null, null, 0, null, UpdSource.ACCDECLINED,false);
						
						//opportunityDAO.updateOppStatus(opp.getId(), OppStatus.PARKED);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						
					}
			}
		}catch(Exception e ){
			e.printStackTrace();
		}
	}

	@Override
	public void sendMailAllPartnerDeclined(String country) {
		
		try{
			List<Opportunity> opps=partnerFactory.getPartnerManger("AU").getOppWhenNoPartnerAccepted(country);
			List<String> templateList=new ArrayList<String>();
			if (opps!=null && opps.size()>0){
				
				for(Opportunity opp:opps){
					templateList = EmailTemplates
							.sendEmailtoSEAdminWithAllPartnerDeclined(seAdminEmail,
									opp.getId(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
				
					try {
						//iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
						//iEmailService.sendMail("richard.chaplin@schneider-electric.com", templateList.get(1), templateList.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					
					//set the opp to parked state
					
					try {
						Opportunity oppSend=new Opportunity();
						oppSend.setId(opp.getId());
						oppSend.setStatus(OppStatus.PARKED.getValue());
						opportunityManager.updateOpportunity(oppSend, null, null, 0, null, UpdSource.ACCDECLINED,false);
						
						//opportunityDAO.updateOppStatus(opp.getId(), OppStatus.PARKED);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						
					}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void sendNewOppToBFO() {
	
		try{
		Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequest();
		
		//if(reqs!=null && reqs.size()>0)
		{	
			String token=httpsClient.getToken();
		
		
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
		
			for (Entry entry : entrySet) {
			
				Map<String,String> responseMap =httpsClient.sendCompositRequestToBFO(entry.getValue().toString(), token);
				
				bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatus(responseMap, entry.getKey().toString());
			}
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	
	@Override
	public void sendNewOppToIF() {
	
		try{
		Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequestIF();
		
		if(reqs!=null && reqs.size()>0)
		{
			String bearerToken=httpsClient.getIFWBarerToken();
			
			Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) 
			{
			
				Map<String,String> responseMap =httpsClient.sendOppCreateRequestToIF(entry.getValue(),bearerToken);
				
				bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatusIF(responseMap, entry.getKey().toString());
			}
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void sendAcceptRejectToBFO(String oppStatus) {
		// TODO Auto-generated method stub
		
		try{
			Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityStatusRequests(oppStatus);
			
			String token=httpsClient.getToken();
			
			
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) {
			
				boolean responseMap =httpsClient.sendUpdateRequestToBFO(entry.getValue().toString(), token,"");
				if (responseMap){
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppAcceptRejectStatus(responseMap, entry.getKey().toString());
				}
				//foManager.updateBFOOppStatus(responseMap, entry.getKey().toString());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	public void sendAcceptRejectToBFOIF(String oppStatus) {
		// TODO Auto-generated method stub
		boolean responseMap=false;

		try{
			Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityStatusRequestsIF(oppStatus);
			
			if(reqs!=null && reqs.size()>0)
			{	
				String bearerToken=httpsClient.getIFWBarerToken();
				Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
				for (Entry entry : entrySet)
				{
					
					String bfoOppId=""; 
					
					bfoOppId = bfoManagerFactory.getBFOManager(bfoManagerName).getBfoOppId(entry.getKey().toString(),"OPPLINK");
					
					String responseData =httpsClient.sendAcceptRejectUpdateRequestToIF(entry.getValue(),oppStatus,bearerToken,bfoOppId);
					if (responseData!=null && responseData.length()>0)					
					{
						responseMap=true;
					}	
					
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppAcceptRejectStatusIF(responseMap, entry.getKey().toString(),responseData,bfoOppId);
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}	
	

	@Override
	public void sendStatusHistToBFO(String statusId) {
		// TODO Auto-generated method stub
		try{
			Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityHistStatusRequests(statusId);
			
			String token=httpsClient.getToken();
			
			
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) {
			
				boolean responseMap =httpsClient.sendUpdateRequestToBFO(entry.getValue().toString(), token,statusId);
				if(responseMap){
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHist(responseMap, entry.getKey().toString());
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void sendStatusHistToIF(String statusId) {
		// TODO Auto-generated method stub
		try{

			Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityHistStatusRequestsIF(statusId);
			
			if(reqs!=null && reqs.size()>0)
			{	
				String bearerToken=httpsClient.getIFWBarerToken();
				Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
			
				for (Entry entry : entrySet)
				{
				
					String bfoOppId=""; 
					boolean responseMap=false;
					String custVisitId="";
					bfoOppId = bfoManagerFactory.getBFOManager(bfoManagerName).getBfoOppId(entry.getKey().toString(),"OPPHISTORY");
					
					if(statusId!=null && !statusId.equalsIgnoreCase("INSTALLATION"))
					{
						responseMap =httpsClient.sendUpdateRequestToIF(entry.getValue(),statusId,bearerToken,bfoOppId);
	
						if(responseMap)
						{
							bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHistIF(responseMap, entry.getKey().toString());
						}
					}else
					{
						custVisitId = httpsClient.sendUpdateRequestToIFInstallation(entry.getValue(),statusId,bearerToken,bfoOppId);
						if(custVisitId!=null && !custVisitId.equalsIgnoreCase("") && custVisitId.length()>0)
						{
							responseMap=true;
							bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHistInstallation(responseMap, entry.getKey().toString(),custVisitId);
						}
	
					}
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
		
	private File getPDfStream(String base64Str){
		
		File of =null;
    try {
      /*  Base64 b = new Base64();
       
        byte[] imageBytes = b.decode(base64Str); 
         fos = new FileOutputStream("bomPDF.pdf");
        fos.write(imageBytes);
        fos.close();*/
        if (base64Str!=null && base64Str.length()>100){
	        byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(base64Str);  
	         of = new File("bomPDF");  
	        FileOutputStream osf = new FileOutputStream(of);  
	        osf.write(btDataFile);  
	        osf.flush();  
        }else{
        	return null;
        }
        
        
    } catch (Exception e) {

        System.out.println("Error :::" + e);
        e.printStackTrace();

    }
    
    	return of;
    
	}
	
	private  File generateCsvFile(List<BOM> boms) {
		try {
			 	File   file = new File("bomCSV");
					FileWriter writer = new FileWriter(file);
					writer.append("Product Id");
					writer.append(',');
					writer.append("Category");
					writer.append(',');
					writer.append("Description");
					writer.append(',');
					writer.append("Color");
					writer.append(',');
					writer.append("Quantity");
					writer.append(',');
					writer.append("Total Cost");
					
			for(BOM bom :boms){
				
					
					writer.append('\n');
		
					writer.append(bom.getProductId());
					writer.append(',');
					writer.append(bom.getCategory());
					writer.append(',');
					writer.append(bom.getDescription());
					writer.append(',');
					writer.append(bom.getColor());
					writer.append(',');
					writer.append(""+bom.getQuantity());
					writer.append(',');
					writer.append(""+bom.getUnitCost());

			
			}
			writer.flush();
			writer.close();
			
			return file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void sendBOM(String oppId, String userId, String supplierEmail) {
		// TODO Auto-generated method stub
		
		
		List<String> template=new ArrayList<String>();
		Opportunity opp=opportunityDAO.getOportunity("Distributor", oppId);

		//get opportunity boms
		List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(oppId));
		
		template = EmailTemplates.sendEmailtoPartnerForNewRequest(
				supplierEmail, opp.getId(),"Distributor", opp.getTitle(), opp
						.getCustomerDetails().getFirstName(), opp
						.getAddress().getZip(), opp.getBudget(),opp.getOpportunityDate(),opp.getCustomerDetails().getPhone(),opp.getCustomerDetails().getEmail());
		
		try {
			
			
			FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
			FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
			Thread.sleep(3000);
			//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
			iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.AUSTRALIA.getValue(),0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void sendMailToConsumerPartnerAccepted(String countryCode,boolean sendSms) {
		
List<CustomerPartner> custList=consumerDAO.getConsumerWithPartnersForAcceptedOpps(countryCode);
		
		List<String> template=new ArrayList<String>();
		for(CustomerPartner cust:custList){
			
			//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
			
			
			template=EmailTemplates.sendEmailtoConsumerFollowBy("richard.chaplin@schneider-electric.com", cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone(),cust.getPartnerLastName());
			
			try {
				//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
				iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
				Thread.sleep(2000);
				consumerDAO.updateOppSLA(cust.getOppId(),"NOTI1");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		   //once send update the customer SLA to 1 in opportunity SLA table
		   //same loop
		
			
			
		}
		
	}

	@Override
	public void sendMailToConsumerOppReceived(String countryCode) {
		// TODO Auto-generated method stub
		List<CustomerPartner> custList=consumerDAO.getCustomersWhoSendsTheNewOpps(countryCode);
		
		List<String> template=new ArrayList<String>();
		for(CustomerPartner cust:custList){
			
			//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
			
			template=EmailTemplates.sendEmailtoConsumerValidate(cust.getCustomerEmail(), cust.getCustomerName(),cust.getPartnerLastName());
			//template=FREmailTemplates.sendEmailtoConsumerFollowBy(cust.getCustomerEmail(), cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone());
			
			try {
				//iEmailService.sendMail("richard.chaplin@schneider-electric.com", template.get(1), template.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
				iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
				Thread.sleep(2000);
				consumerDAO.updateOppSLA(cust.getOppId(),"NEWOPP");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
				//once send update the customer SLA to 1 in opportunity SLA table
		//same loop
		
			
			
		}
	}
	
	public void notifyPartnerForN4toN6(int hrs,String nType,String country){
		Map<String,Object> returnMap=opportunityDAO.getPartnerDeviceForNotifications(hrs,nType,country);
		List<Integer> oppIdList=new ArrayList<Integer>();
		
		for(Map.Entry<String, Object> entry:returnMap.entrySet())
		{
			if(entry.getKey().equalsIgnoreCase("OPPLIST"))
			{			
				oppIdList=(List<Integer>)entry.getValue();
			}

//		returnMap.put("PARTNERCUST", partCustMap);Rappel
//		returnMap.put("DEVICELIST", partnerDeviceList);
		if (returnMap!=null && !returnMap.isEmpty()){
			
			
			try {
				if(nType.equalsIgnoreCase("N4"))
				{
				notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N4");
				//update sla table
				consumerDAO.updateOppNOTI(oppIdList, "N4");
				
				}
				else if(nType.equalsIgnoreCase("N5"))
				{
				notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N5");
				consumerDAO.updateOppNOTI(oppIdList, "N5");
				}
				else if(nType.equalsIgnoreCase("N6"))
				{
				notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N6");
				consumerDAO.updateOppNOTI(oppIdList, "N6");
				}
			} catch (AppException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		}
			
	}
	
	private void notifyPartner(List<PartnerDevice> deviceList,Map<String,String> partnerCust,String noteType) throws AppException{

		

		 if (deviceList!=null && deviceList.size()>0){
				try{	
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
					for(PartnerDevice partnerDevice:deviceList){
						String custName=partnerCust.get(partnerDevice.getPartnerId());
						
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS")){
							iosList.add(partnerDevice.getDeviceId());
					        if(noteType.equalsIgnoreCase("N4")){
					        	pushNotificationToIOS("Clipsal Connect second reminder","You have accepted a customer lead for " + custName +"Please complete the quotation within 2 days. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , iosList);
					        }
					        if(noteType.equalsIgnoreCase("N5")){
					        	pushNotificationToIOS("Clipsal Connect second reminder","You have accepted a customer lead for " + custName +"Please complete the quotation within 7 days"
					        			+ ". Thanks, Clipsal and Schneider Electric Consumer Support Team. " , iosList);
					        }
					        if(noteType.equalsIgnoreCase("N6")){
					        	pushNotificationToIOS("Clipsal Connect second reminder","You have accepted a customer lead for " + custName +"Please complete the quotation within 3 weeks. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , iosList);
					        }
						}else{
							androidList.add(partnerDevice.getDeviceId());
							
							if(noteType.equalsIgnoreCase("N4")){
								pushNotificationToAndroid("Clipsal Connect second reminder","You have accepted a customer lead for " + custName +"Please complete the quotation within 2 days. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , androidList);
							}
							if(noteType.equalsIgnoreCase("N5")){
								pushNotificationToAndroid("Clipsal Connect second reminder","You have accepted a customer lead for " + custName +"Please complete the quotation within 7 days. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , androidList);
							}
							if(noteType.equalsIgnoreCase("N6")){
								pushNotificationToAndroid("Clipsal Connect second reminder","You have accepted a customer lead for " + custName +"Please complete the quotation within 3 weeks. Thanks, Clipsal and Schneider Electric Consumer Support Team. " , androidList);
							}
						
						}
					
					}
						
				}catch(Exception e){
					e.printStackTrace();
				}
		
		}


	}
	
	public static void main(String args[])
	{
		
		AUNotificationManagerImpl auNotificationManager = new AUNotificationManagerImpl();
		String messageTitle="IOS PN";
		String message = "SENS PN TO IOS";
		List<String> deviceList = new ArrayList<String>();
		deviceList.add("fad11117cb8528d32984e00a08f5be82f83d9a80312973819ffc61726734bdfb");
		auNotificationManager.pushNotificationToIOS(messageTitle, message, deviceList); 
		
         
		
	}

	@Override
	public void notifyPartnerForCancel(String country, String nType)
			throws AppException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendSmsTopartnerwhenConsCancelled(String countryCode,
			boolean sendSms,String notifyType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendSmsToConsumerWhenMeetingBooked(String countryCode,
			boolean sendSms, String notifyType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMailToConsumerWhenPartnerBookedMeeting(String countryCode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMailToAminForInvoice(String countryCode, String status) {
		// TODO Auto-generated method stub
		
	}
	
	

	
}

package com.se.cl.partner.manager.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ServerSocket;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;







import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.codec.binary.Hex;
import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.notnoop.exceptions.NetworkIOException;
import com.se.cl.constants.CountryCode;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.ConsumerDAO;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerPartner;
import com.se.cl.model.Opportunity;
import com.se.cl.model.PartnerDevice;
import com.se.cl.partner.manager.NotificationManager;
import com.se.cl.partner.manager.PartnerManagerFactory;
import com.se.cl.util.PropertyLoader;
import com.se.clm.bfo.client.HttpsClient;
import com.se.clm.bfo.manager.BFOManagerFactory;
import com.se.clm.mail.FREmailTemplates;
import com.se.clm.mail.IEmailService;
@Component("FranceNotification")
public class FRNotificationManagerImpl implements NotificationManager{

	@Autowired
	PartnerDAO partnerDAO;
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	PartnerManagerFactory partnerFactory;
	
	@Autowired
	IEmailService iEmailService;
	
	@Autowired
	BFOManagerFactory bfoManagerFactory;

	@Autowired
	HttpsClient httpsClient;

	
	@Autowired
	ConsumerDAO consumerDAO;

	@Autowired
	OpportunityManager opportunityManager;


	private @Value("${fr.se.admin.email}") String seAdminEmail;
	private @Value("${fr.accept.sla.step1}") Long slaStep1;
	private @Value("${fr.accept.sla.step2}") Long slaStep2;
	private @Value("${fr.accept.sla.step3}") Long slaStep3;

	

	//private @Value("${partner-count}") String propertyField;

	private @Value("${google.api.key}") String googleApiKey;
	private @Value("${google.push.notification.url}") String googlePushNotificationURL;
	private @Value("${ios.certificate.password}") String iosCertificatePassword;

	private @Value("${solution30.proxy}") String solution30Proxy;

	
	private static final Logger LOGGER = LoggerFactory.getLogger(FRNotificationManagerImpl.class);
    private static final String bfoManagerName="FRB";


	@Override
	public void notifyPartnerForSALRemider(int slaStep,String country) throws AppException {
		
	
		try{
			HashMap<String,List<String>>  map=partnerDAO.getPartnerListForSLAReminder(slaStep,country);
			
			notifyPartner(map,"SLA",slaStep);
			sendEmailsToPartners(map,"SLA",slaStep);
			//
		}catch(Exception e){
			e.printStackTrace();
		}
		//send emails


	}

 private void sendEmailsToPartners(HashMap<String,List<String>> map,String noteType,int slaStep) throws InterruptedException{
	 
	 
	 
		
		if (map.get("OPP-PARTNER")!=null && map.get("OPP-PARTNER").size()>0){
			
			List<String> partnerIds=map.get("OPP-PARTNER");
			List<String> template=new ArrayList<String>();
			
			if (slaStep==0){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);

					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					
					template = FREmailTemplates.sendEmailtoPartnerForNewRequest(
							opp.getPartner().getEmail(), opp.getPartner()
									.getName(), opp.getTitle(), opp
									.getCustomerDetails().getFirstName(), opp
									.getAddress().getZip(), opp.getBudget());
					
					try {
						Thread.sleep(1000);
					
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
						iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			
			if (slaStep==1){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = FREmailTemplates
							.sendEmailtoPartnerForRemainderOne(opp.getPartner()
									.getEmail(), opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
					

					try {
						Thread.sleep(1000);
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
						iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
					} catch (Exception e) {
						// TODO Auto-generated catch block seAdminEmail
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			if (slaStep==2){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = FREmailTemplates
							.sendEmailtoPartnerFOrRemainderTwo(opp.getPartner()
									.getEmail(), opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
					

					try {
						Thread.sleep(1000);
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
						iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
					} catch (Exception e) {
						// TODO Auto-generated catch block seAdminEmail
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			Set<String> sol30OppIdList=null;
			if (slaStep==3)
			{
				//get opportunityByPartner Id
				HashMap<String,String> sendsol30Map = new HashMap<String,String>();
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = FREmailTemplates
							.sendEmailtoPartnerFOrRemainderThree(opp.getPartner()
									.getEmail(), opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
					

					try {
						Thread.sleep(1000);
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
						iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),3);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
					//send to solution 30
					
					//LOGGER.info("After SLA-3 opportunity is sending to sol30.......oppId:::"+parts[0]);
					
					/*if(sendsol30Map.get(parts[0])!=null && !sendsol30Map.get(parts[0]).equalsIgnoreCase("1"))
					{
						partnerFactory.getPartnerManger("FR").sendOppTOSol30( parts[0], " ");
					}
					
					sendsol30Map.put(parts[0] ,"1");
					
					sol30OppIdList = new HashSet<String>();
					sol30OppIdList.add(parts[0]);
					//remove assignment for other partners , who are already assigned to this opportunity
					opportunityDAO.unassignOpportunityFromPartner(parts[0]);
					*/
					sol30OppIdList = new HashSet<String>();
					sol30OppIdList.add(parts[0]);
				
				}
				
			     
				if(sol30OppIdList!=null && sol30OppIdList.size()>0)
				{	
				    for(String oppId:sol30OppIdList) 
				    {
				    	LOGGER.info("After SLA-3 opportunity is sending to sol30.......oppId:::"+oppId);
				    	LOGGER.info("sol30OppIdList:oppId:::"+oppId);
				    	partnerFactory.getPartnerManger("FR").sendOppTOSol30(oppId," ");
				    	LOGGER.info("After SLA-3 opportunity is sending to sol30..And UnAssign the Opp from Partner");
				    	opportunityDAO.unassignOpportunityFromPartner(oppId);
	
				    } 
				}				
				
			
			}
			
			//update opp with sla trigger status
			
			
			if(map.get("OPPORTUNITY")!=null && map.get("OPPORTUNITY").size()>0){
				//for frace if sla step 3 send opportunity to solution 30
				
				if (slaStep!=3){
					try {
						opportunityManager.updateOpportunity(null,map.get("OPPORTUNITY"),noteType,slaStep, null, UpdSource.SLA,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				//opportunityDAO.updateOppNotiStatus(map.get("OPPORTUNITY") ,noteType,slaStep);
				}
			}


		}
		
		
 }


	public void notifyPartnerForNewOpportunity(String country) throws AppException {

		try{
			HashMap<String,List<String>>  map=partnerDAO.getPartnerListForNewOpps(country);
			slaStep1=Long.valueOf(PropertyLoader.getSparQlProperty("fr.accept.sla.step1"));
			
			notifyPartner(map,"NEWOPP",0);
			sendEmailsToPartners(map,"NEWOPP",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		//return null;
	}




	private void notifyPartner(HashMap<String,List<String>> map,String noteType,int slaType) throws AppException{

		if (map.get("PARTNER")!=null && map.get("PARTNER").size()>0){

			List<PartnerDevice> deviceList=getDeviceIdsForPartnes( map.get("PARTNER"));

			if (deviceList!=null && deviceList.size()>0){
				try{	
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
					for(PartnerDevice partnerDevice:deviceList){
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS")){
							iosList.add(partnerDevice.getDeviceId());
						}else{
							androidList.add(partnerDevice.getDeviceId());
						}
					}

					//below hardcoded for now .. needs to be computed from prop file
					if(slaType==0){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Une nouvelle demande de vous pour", "Alerte E-Partner"+"\n"+"Bonne nouvelle! Vous avez reçu une nouvelle demande de devis" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Une nouvelle demande de vous pour", "Alerte E-Partner"+"\n"+"Bonne nouvelle! Vous avez reçu une nouvelle demande de devis" , androidList);
						}
					}else if(slaType==1){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Première Rappel", "Connectez-vous vite pour  Répondre a cette nouvelle demande de devisgo" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Première Rappel", "Connectez-vous vite pour  Répondre a cette nouvelle demande de devisgo" , androidList);
						}
						
					}else if(slaType==2){
						
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Deuxième Rappel ", "Connectez-vous vite pour  Répondre a cette nouvelle demande de devisgo" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Deuxième Rappel", "Connectez-vous vite pour  Répondre a cette nouvelle demande de devisgo" , androidList);
						}
					}else if(slaType==3){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Troisième Rappel", "Connectez-vous vite pour  Répondre a cette nouvelle demande de devisgo" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Troisième Rappel", "Connectez-vous vite pour  Répondre a cette nouvelle demande de devisgo" , androidList);
						}
					}
					
					
					

				}catch(Exception e){
					e.printStackTrace();
				}
			}


			
		}


	}
	
	
	private void notifyPartner(List<PartnerDevice> deviceList,Map<String,String> partnerCust,String noteType) throws AppException{

		

		 if (deviceList!=null && deviceList.size()>0){
				try{	
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
					for(PartnerDevice partnerDevice:deviceList){
						String custName=partnerCust.get(partnerDevice.getPartnerId());
						
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS")){
							iosList.add(partnerDevice.getDeviceId());
					        if(noteType.equalsIgnoreCase("N4")){
					        	pushNotificationToIOS("Rappel","N’oubliez pas d’envoyer votre devis a" + custName +" et de mettre a jour votre appli mobile Partenaire SE" , iosList);
					        }
					        if(noteType.equalsIgnoreCase("N5")){
					        	pushNotificationToIOS("Rappel"," Urgent - N’oubliez d’envoyer votre devis a "+custName+"  et de mettre a jour votre appli mobile Partenaire SE" , iosList);
					        }
					        if(noteType.equalsIgnoreCase("N6")){
					        	pushNotificationToIOS("Rappel","Avez-vous des nouvelles du devis de "+custName+"? N’oubliez pas de mettre a jour votre appli mobile Partenaire SE", iosList);
					        }
						}else{
							androidList.add(partnerDevice.getDeviceId());
							
							if(noteType.equalsIgnoreCase("N4")){
								pushNotificationToAndroid("Rappel","N’oubliez pas d’envoyer votre devis a"+custName+" et de mettre a jour votre appli mobile Partenaire SE" , androidList);
							}
							if(noteType.equalsIgnoreCase("N5")){
								pushNotificationToAndroid("Rappel","Urgent - N’oubliez d’envoyer votre devis a  "+custName+"  et de mettre a jour votre appli mobile Partenaire SE" , androidList);
							}
							if(noteType.equalsIgnoreCase("N6")){
								pushNotificationToAndroid("Rappel","Avez-vous des nouvelles du devis de "+custName+"? N’oubliez pas de mettre a jour votre appli mobile Partenaire SE" , androidList);
							}
						
						}
					
					}
						
				}catch(Exception e){
					e.printStackTrace();
				}
		


			
		}


	}


	private List<PartnerDevice> getDeviceIdsForPartnes(List<String> partnerIds) throws AppException {
		// TODO Auto-generated method stub

		return  partnerDAO.listPartnerDeviceByIds(partnerIds);


	}






	public ObjectNode pushNotificationToAndroid(String messageTitle, String message,List<String> deviceList) {

		ObjectNode response = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.getJsonFactory().configure(Feature.ESCAPE_NON_ASCII, false);
			response = mapper.getNodeFactory().objectNode();
			//List<PartnerDevice> deviceList = opportunityDAO.listPartnerDevice("Android");

			if(deviceList == null || (deviceList != null && deviceList.size() == 0)){
				response.put("errorMsg", "Android | No devices found for sending push notification");
				response.put("status", "error");
				LOGGER.debug("Android | No devices found for sending push notification");
				return response;
			}

			ObjectNode requestJSON = mapper.getNodeFactory().objectNode();
			ObjectNode dataNode = mapper.getNodeFactory().objectNode();
			dataNode.put("title", messageTitle);
			dataNode.put("message", message);
			requestJSON.put("data", dataNode);
			ArrayNode deviceIdArray = mapper.getNodeFactory().arrayNode();
			for(String device : deviceList){
				deviceIdArray.add(device);
			}
			requestJSON.put("registration_ids", deviceIdArray);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", "key="+"AIzaSyBkUP-vVMXxRveUWci2RGn8mOy8auW0INY");

			HttpEntity<String> entityRequest = new HttpEntity<String>(requestJSON.toString(), headers);

			RestTemplate restTemplate = new RestTemplate();
			//add proxy if required
			//	SimpleClientHttpRequestFactory factory =   ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory());
			//	Proxy proxy= new Proxy(Type.HTTP, new InetSocketAddress("proxy-ip", proxy-port));
			//	factory.setProxy(proxy);
			//	restTemplate = new RestTemplate(factory);

			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			
			JSONObject restResponse = restTemplate.postForObject("https://android.googleapis.com/gcm/send", entityRequest, JSONObject.class);
			
			response = (ObjectNode) mapper.readTree(restResponse.toJSONString());
			
			
			
			LOGGER.debug("Android push notification response = " + response);

		} catch (Exception e) {
			e.printStackTrace();
			response.put("errorMsg", "Error while sending Android push notification");
			response.put("status", "error");
			LOGGER.error("Error while sending Android push notification", e);
		}

		return response;

	}



	public ObjectNode pushNotificationToIOS(String messageTitle, String message,List<String> deviceList) 
	{
		/*
		 *Reference :
		 *https://github.com/notnoop/java-apns
		 *http://developement.sebastian-bothe.de/?page_id=40
		 *and
		 *http://blog.synyx.de/2010/07/sending-apple-push-notifications-with-notnoops-java-apns-library/
		 */		
         boolean sendnotification =false;
         
		ApnsService service = null;
		InputStream certStream = null;
		ObjectNode response = null;
		boolean hasError = false;
		//iosCertificatePassword="P@ssword0";
          
		boolean checkPortAvailable=false;
		
		if(sendnotification)
		{
					checkPortAvailable = checkPortAvailable(2195);
					LOGGER.info("checkPortAvailable:::"+checkPortAvailable);
					if(checkPortAvailable==true)
					{
			
						try {
							ObjectMapper mapper = new ObjectMapper();
							mapper.getJsonFactory().configure(Feature.ESCAPE_NON_ASCII, false);
							response = mapper.getNodeFactory().objectNode();
			
							//List<PartnerDevice> deviceList = opportunityDAO.listPartnerDevice("iOS");
							if(deviceList == null || (deviceList != null && deviceList.size() == 0)){
								response.put("errorMsg", "iOS | No devices found for sending push notification");
								response.put("status", "error");
								LOGGER.debug("iOS | No devices found for sending push notification");
								return response;
							}
			
			
							//certStream = this.getClass().getClassLoader().getResourceAsStream("ck4.p12");
			
							certStream = this.getClass().getClassLoader().getResourceAsStream("ePartnerFR.p12");
			
			
							//For production environment
							//Add proxy if required
							//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy-host, proxy-port));
							//LOGGER.info("iosCertificatePassword:::::"+iosCertificatePassword);
			
							//LOGGER.info("certStream:::::"+certStream);
			
							if(solution30Proxy!=null && solution30Proxy.equalsIgnoreCase("1"))
							{	
								Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("205.167.7.126", 80));
								service = APNS.newService()
										.withCert(certStream, iosCertificatePassword)
										.withProductionDestination()
										.withProxy(proxy)
										.build();
			
							}
							else
							{
			
								service = APNS.newService()
										.withCert(certStream, iosCertificatePassword)
										.withProductionDestination()
										.build();
							}
			
							  	service.start();
								for (String device : deviceList) {
									try {
										//badge : The number of updates to display.
										Thread.sleep(1000);
										int badge = 1;
										PayloadBuilder payloadBuilder = APNS.newPayload();
										payloadBuilder = payloadBuilder.badge(badge).alertBody(message);
				
										//check if the message is too long, shrink it if so.
										if (payloadBuilder.isTooLong()) {
											LOGGER.debug("Push message is too long, shrinking the content to guarantee the delivery.");
											payloadBuilder = payloadBuilder.shrinkBody();
										}
										String payload = payloadBuilder.build();
										String token = device;
										service.push(token, payload);
									} catch (Exception e) {
										hasError = true;
										response.put("status", "error");
										response.put("message", "iOS | Error while sending push notification");
										LOGGER.error("iOS | Error while sending push notification", e);
									}
								}
							
						} catch (Exception ex) {
			
							if(!hasError){
								hasError = true;
								response.put("status", "error");
								response.put("message", "iOS | Error while starting the push notification service");
							}
			
							LOGGER.error("iOS | Error while starting the push notification service", ex);
						} finally {
			
							if (service != null) {
								//service.stop();
							}
			
							if(certStream != null)
								try {
									certStream.close();
								} catch (IOException e) {}
			
						}
			
						if(!hasError){
							response.put("status", "success");
							response.put("message", "iOS | Push notification sent successfully");
						}
					}		
		}
		return response;

	}

		public boolean checkPortAvailable(int port)
		  {
			  boolean portExists=false;
			  ServerSocket serverSocket=null;
			  try {
				    serverSocket = new ServerSocket(port);
				    portExists=true;
				} catch (IOException e) {
				    System.out.println("Could not listen on port: " + port);
				    portExists=false;
				    // ...
				}
			  
			  return portExists;
		  }


	@Override
	public void sendMail() {
		// TODO Auto-generated method stub
		iEmailService.sendApprovedMail("sudhir", "sudhir.marni@gmail.com", "test", BigInteger.valueOf(123),"sdsad" , "sdfsfds");

	}




/*
	@Override
	public void sendMailToConsumer(String consumerEmail,String oppId,String partnerName,String projectName,String consumerName,String zipCode,String budget,String templateId) {
		// TODO Auto-generated method stub
		
	}
	*/
	
	
	
	public void sendMailNoPartnerAccepted(String country){
		
		try{
			List<Opportunity> opps=partnerFactory.getPartnerManger("FR").getOppWhenNoPartnerAccepted(country);
			List<String> templateList=new ArrayList<String>();
			if (opps!=null && opps.size()>0){
				
				for(Opportunity opp:opps){
					
					
					templateList = FREmailTemplates
							.sendEmailtoSEAdminWithNoPartnerAnswered(seAdminEmail,
									opp.getId(), opp.getCustomerDetails().getFirstName(), opp.getAddress().getZip(), opp.getBudget());		
				
					try {
						iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.FRANCE.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//for france send opportunity to Solution 30
					
					try {
						//send to solution 30
						partnerFactory.getPartnerManger("FR").sendOppTOSol30( opp.getId(), " ");
						
						//remove assignment for other partners , who are already assigned to this opportunity
						opportunityDAO.unassignOpportunityFromPartner(opp.getId());
						
						//opportunityDAO.updateOppStatus(opp.getId(), OppStatus.PARKED);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						
					}
			}
		}catch(Exception e ){
			e.printStackTrace();
		}
	}

	@Override
	public void sendMailAllPartnerDeclined(String country) {
		
		try{
			List<Opportunity> opps=partnerFactory.getPartnerManger("FR").getOppWhenNoPartnerAccepted(country);
			List<String> templateList=new ArrayList<String>();
			if (opps!=null && opps.size()>0){
				
				for(Opportunity opp:opps){
					templateList = FREmailTemplates
							.sendEmailtoSEAdminWithAllPartnerDeclined(seAdminEmail,
									opp.getId(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
				
					try {
						iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.FRANCE.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					
					//for france send opportunity to Solution 30
					
					try {
						//send to solution 30
						partnerFactory.getPartnerManger("FR").sendOppTOSol30( opp.getId(), " ");
						
						//remove assignment for other partners , who are already assigned to this opportunity
						opportunityDAO.unassignOpportunityFromPartner(opp.getId());
						
						//opportunityDAO.updateOppStatus(opp.getId(), OppStatus.PARKED);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						
					}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void sendNewOppToBFO() {
	
		try{
		Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequest();
		
		String token=httpsClient.getToken();
		
		
		Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
		
		for (Entry entry : entrySet) {
		
			Map<String,String> responseMap =httpsClient.sendCompositRequestToBFO(entry.getValue().toString(), token);
			
			bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatus(responseMap, entry.getKey().toString());
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	
	/*@Override
	public void sendNewOppToIF() {
	
		try{
		Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequestIF();
		
		String bearerToken=httpsClient.getIFWBarerToken();

		Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
		
		for (Entry entry : entrySet) {
		
			Map<String,String> responseMap =httpsClient.sendOppCreateRequestToIF(entry.getValue(),bearerToken);			
			bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatus(responseMap, entry.getKey().toString());
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}*/
	
	@Override
	public void sendNewOppToIF() {
	
		try{
		Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequestIF();
		if(reqs!=null && reqs.size()>0)
		{
			String bearerToken=httpsClient.getIFWBarerToken();
			
			Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) 
			{
			
				Map<String,String> responseMap =httpsClient.sendOppCreateRequestToIF(entry.getValue(),bearerToken);
				
				bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatusIF(responseMap, entry.getKey().toString());
			}
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void sendAcceptRejectToBFO(String oppStatus) {
		// TODO Auto-generated method stub
		
		try{
			Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityStatusRequests(oppStatus);
			
			String token=httpsClient.getToken();			
			
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) {
			
				boolean responseMap =httpsClient.sendUpdateRequestToBFO(entry.getValue().toString(), token,"");
				if (responseMap){
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppAcceptRejectStatus(responseMap, entry.getKey().toString());
				}
				//foManager.updateBFOOppStatus(responseMap, entry.getKey().toString());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	
	@Override
	public void sendAcceptRejectToBFOIF(String oppStatus) {
		// TODO Auto-generated method stub
		boolean responseMap=false;

		try{
			Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityStatusRequestsIF(oppStatus);
			if(reqs!=null && reqs.size()>0)
			{	
				String bearerToken=httpsClient.getIFWBarerToken();
				Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
				for (Entry entry : entrySet)
				{
					
					String bfoOppId=""; 
					
					bfoOppId = bfoManagerFactory.getBFOManager(bfoManagerName).getBfoOppId(entry.getKey().toString(),"OPPLINK");
					
					String responseData =httpsClient.sendAcceptRejectUpdateRequestToIF(entry.getValue(),oppStatus,bearerToken,bfoOppId);
					if (responseData!=null && responseData.length()>0)					
					{
						responseMap=true;
					}	
					
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppAcceptRejectStatusIF(responseMap, entry.getKey().toString(),responseData,bfoOppId);
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}	

	@Override
	public void sendStatusHistToBFO(String statusId) {
		// TODO Auto-generated method stub
		try{
			Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityHistStatusRequests(statusId);
			
			String token=httpsClient.getToken();
			
			
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) {
			
				boolean responseMap =httpsClient.sendUpdateRequestToBFO(entry.getValue().toString(), token,statusId);
				if(responseMap){
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHist(responseMap, entry.getKey().toString());
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void sendStatusHistToIF(String statusId) {
		// TODO Auto-generated method stub
		try{

			Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityHistStatusRequestsIF(statusId);
			if(reqs!=null && reqs.size()>0)
			{	
				String bearerToken=httpsClient.getIFWBarerToken();
				
				Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
				
				for (Entry entry : entrySet)
				{
				
					String bfoOppId=""; 
					boolean responseMap=false;
					String custVisitId="";
					bfoOppId = bfoManagerFactory.getBFOManager(bfoManagerName).getBfoOppId(entry.getKey().toString(),"OPPHISTORY");
					
					if(statusId!=null && statusId.equalsIgnoreCase("INSTALLATION"))
					{
						LOGGER.info("sendStatusHistToIF:::statusId:::"+statusId);
						custVisitId = httpsClient.sendUpdateRequestToIFInstallation(entry.getValue(),statusId,bearerToken,bfoOppId);
						LOGGER.info("sendStatusHistToIF:::custVisitId:::"+custVisitId);
	
						if(custVisitId!=null && !custVisitId.equalsIgnoreCase("") && custVisitId.length()>0)
						{
							responseMap=true;
							bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHistInstallation(responseMap, entry.getKey().toString(),custVisitId);
						}
						
						
					}else
					{
						responseMap =httpsClient.sendUpdateRequestToIF(entry.getValue(),statusId,bearerToken,bfoOppId);
	
						if(responseMap)
						{
							bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHistIF(responseMap, entry.getKey().toString());
						}
	
					}
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}	
	private File getPDfStream(String base64Str){
		
		File of =null;
    try {
      /*  Base64 b = new Base64();
       
        byte[] imageBytes = b.decode(base64Str); 
         fos = new FileOutputStream("bomPDF.pdf");
        fos.write(imageBytes);
        fos.close();*/
        
        byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(base64Str);  
         of = new File("bomPDF.pdf");  
        FileOutputStream osf = new FileOutputStream(of);  
        osf.write(btDataFile);  
        osf.flush();  
        
        
    } catch (Exception e) {

        System.out.println("Error :::" + e);
        e.printStackTrace();

    }
    
    	return of;
    
	}
	
	private  File generateCsvFile(List<BOM> boms) {
		try {
			 	File   file = new File("bomCSV.csv");
					FileWriter writer = new FileWriter(file);
					writer.append("Product Id");
					writer.append(',');
					writer.append("Category");
					writer.append(',');
					writer.append("Description");
					writer.append(',');
					writer.append("Color");
					writer.append(',');
					writer.append("Quantity");
					writer.append(',');
					writer.append("Total Cost");
					
			for(BOM bom :boms){
				
					
					writer.append('\n');
		
					writer.append(bom.getProductId());
					writer.append(',');
					writer.append(bom.getCategory());
					writer.append(',');
					writer.append(bom.getDescription());
					writer.append(',');
					writer.append(bom.getColor());
					writer.append(',');
					writer.append(""+bom.getQuantity());
					writer.append(',');
					writer.append(""+bom.getUnitCost());

			
			}
			writer.flush();
			writer.close();
			
			return file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void sendBOM(String oppId, String userId, String supplierEmail) {
		List<String> template=new ArrayList<String>();
		Opportunity opp=opportunityDAO.getOportunity("Distributor", oppId);

		//get opportunity boms
		List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(oppId));
		
		template = FREmailTemplates.sendEmailtoPartnerForNewRequest(
				supplierEmail, "Distributor", opp.getTitle(), opp
						.getCustomerDetails().getFirstName(),opp
						.getAddress().getZip(), opp.getBudget());
		
		try {
			Thread.sleep(1000);
		
			FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
			FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
			
			iEmailService.sendMail(supplierEmail, template.get(1), template.get(0),fsr,fsrcsv,CountryCode.FRANCE.getValue(),0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
/**
 * this method to send the email to consumer that partner accepted the opportunity
 */
	
	@Override
	public void sendMailToConsumerPartnerAccepted(String countryCode,boolean sendSms) {
		
		List<CustomerPartner> custList=consumerDAO.getConsumerWithPartnersForAcceptedOpps(countryCode);
		
		List<String> template=new ArrayList<String>();
		for(CustomerPartner cust:custList){
			
			//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
			
			
			template=FREmailTemplates.sendEmailtoConsumerFollowBy(cust.getCustomerEmail(), cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone(),cust.getPartnerLastName());
			
			try {
				//iEmailService.sendMail(cust.getCustomerEmail(), template.get(1), template.get(0),null,null,CountryCode.FRANCE.getValue(),1);
				iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),null,null,CountryCode.FRANCE.getValue(),1);
				Thread.sleep(2000);
				consumerDAO.updateOppSLA(cust.getOppId(),"NOTI1");
			} catch (Exception e) {
				// TODO Auto-generated catch block seAdminEmail
				e.printStackTrace();
			}
			
			
		}
		   //once send update the customer SLA to 1 in opportunity SLA table
		   //same loop
		
			
			
		}
		
	}

@Override
public void sendMailToConsumerOppReceived(String countryCode) {
	
	List<CustomerPartner> custList=consumerDAO.getCustomersWhoSendsTheNewOpps(countryCode);
	
	List<String> template=new ArrayList<String>();
	for(CustomerPartner cust:custList){
		
		//send mails to customer/consumers if the list is not null
	if(cust!=null)
	{
		
		template=FREmailTemplates.sendEmailtoConsumerValidate(cust.getCustomerEmail(), cust.getCustomerName(),cust.getPartnerLastName());
		//template=FREmailTemplates.sendEmailtoConsumerFollowBy(cust.getCustomerEmail(), cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone());
		
		try {
			//iEmailService.sendMail(cust.getCustomerEmail(), template.get(1), template.get(0),null,null,CountryCode.FRANCE.getValue(),1);
			iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),null,null,CountryCode.FRANCE.getValue(),1);
			Thread.sleep(2000);
			consumerDAO.updateOppSLA(cust.getOppId(),"NEWOPP");
		} catch (Exception e) {
			// TODO Auto-generated catch block seAdminEmail
			e.printStackTrace();
		}
		
		
	}
			//once send update the customer SLA to 1 in opportunity SLA table
	//same loop
	
		
		
	}
	
}

/**
 * notify partner for N4 N5 N6
 */

@SuppressWarnings("unchecked")
public void notifyPartnerForN4toN6(int hrs,String nType,String country){
	
	
	Map<String,Object> returnMap=opportunityDAO.getPartnerDeviceForNotifications(hrs,nType,country);
	List<Integer> oppIdList=new ArrayList<Integer>();
	
	for(Map.Entry<String, Object> entry:returnMap.entrySet())
	{
		if(entry.getKey().equalsIgnoreCase("OPPLIST"))
		{			
			oppIdList=(List<Integer>) entry.getValue();
			
		}

//	returnMap.put("PARTNERCUST", partCustMap);Rappel
//	returnMap.put("DEVICELIST", partnerDeviceList);
	if (returnMap!=null && !returnMap.isEmpty()){
		
		
		try {
			if(nType.equalsIgnoreCase("N4"))
			{
			notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N4");
			//update sla table
			consumerDAO.updateOppNOTI(oppIdList, "N4");
			
			}
			else if(nType.equalsIgnoreCase("N5"))
			{
			notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N5");
			consumerDAO.updateOppNOTI(oppIdList, "N5");
			}
			else if(nType.equalsIgnoreCase("N6"))
			{
			notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N6");
			consumerDAO.updateOppNOTI(oppIdList, "N6");
			}
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	}
}

public static void main(String args[])
{
	
   	String messageTitle="hai";
   	String message="aaaaaaaaaa";
   	List<String> deviceList=null;
   	deviceList = new ArrayList<String>();
   	deviceList.add("28c5b5f4f80ddf9fe8ba98dbc8a7b607137a74f5cecbae62aa643c5bd0ad8abc");  	
   	
   	FRNotificationManagerImpl fr = new FRNotificationManagerImpl();
	fr.pushNotificationToIOS(messageTitle, message,deviceList);
	
   	//fr.sendPN();
	
	
}

public  void sendPN()
{
    
	 String token = "28c5b5f4f80ddf9fe8ba98dbc8a7b607137a74f5cecbae62aa643c5bd0ad8abc";
	 String host = "gateway.push.apple.com";
	 int port = 2195;

	 String payload = "{\"aps\":{\"alert\":\"Message from Java o_O\"}}";

	    try {
	        KeyStore keyStore = KeyStore.getInstance("PKCS12");

	        keyStore.load(getClass().getResourceAsStream("PushCertificats.p12"), "Schneider".toCharArray());
	        KeyManagerFactory keyMgrFactory = KeyManagerFactory.getInstance("SunX509");
	        keyMgrFactory.init(keyStore, "Schneider".toCharArray());

	        SSLContext sslContext = SSLContext.getInstance("TLS");
	        sslContext.init(keyMgrFactory.getKeyManagers(), null, null);
	        SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

	        SSLSocket sslSocket = (SSLSocket) sslSocketFactory.createSocket(host, port);
	        String[] cipherSuites = sslSocket.getSupportedCipherSuites();
	        sslSocket.setEnabledCipherSuites(cipherSuites);
	        sslSocket.startHandshake();

	        char[] t = token.toCharArray();
	        byte[] b = Hex.decodeHex(t);

	        OutputStream outputstream = sslSocket.getOutputStream();

	        outputstream.write(0);
	        outputstream.write(0);
	        outputstream.write(32);
	        outputstream.write(b);
	        outputstream.write(0);
	        outputstream.write(payload.length());
	        outputstream.write(payload.getBytes());

	        outputstream.flush();
	        outputstream.close();

	    } catch (Exception exception) {
	        exception.printStackTrace();
	    }
	
}

@Override
public void sendMailToConsumer(String consumerEmail, String oopId,String partnerName, String projectName, String consumerName,String zipCode, String budget, String templateId, String date,
		String custNumber,String custMail,List<BOM> boms,String address,String city) 

{
	List<String> templateList=new ArrayList<String>();
	if (templateId!=null && templateId.equalsIgnoreCase("NewOpp"))
	{
		templateList = FREmailTemplates.sendEmailtoPartnerForNewRequest(
				consumerEmail, partnerName, projectName, consumerName, zipCode,
				budget);
	}else
	{
		templateList = FREmailTemplates.sendEmailtoPartnerForNewRequest(
				consumerEmail, partnerName, projectName, consumerName, zipCode,
				budget);
		
	}
	
	try {
		//iEmailService.sendMail(consumerEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.FRANCE.getValue(),1);
		iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.FRANCE.getValue(),1);
	} catch (Exception e) {
		// TODO Auto-generated catch block seAdminEmail
		e.printStackTrace();
	}	
	
}
@Override
public void notifyPartnerForCancel(String country, String nType)
		throws AppException {
	// TODO Auto-generated method stub
	
}

@Override
public void sendSmsTopartnerwhenConsCancelled(String countryCode,
		boolean sendSms,String notifyType) {
	// TODO Auto-generated method stub
	
}

@Override
public void sendSmsToConsumerWhenMeetingBooked(String countryCode,
		boolean sendSms, String notifyType) {
	// TODO Auto-generated method stub
	
}

@Override
public void sendMailToConsumerWhenPartnerBookedMeeting(String countryCode) {
	// TODO Auto-generated method stub
	
}

@Override
public void sendMailToAminForInvoice(String countryCode, String status) {
	// TODO Auto-generated method stub
	
}
	
}

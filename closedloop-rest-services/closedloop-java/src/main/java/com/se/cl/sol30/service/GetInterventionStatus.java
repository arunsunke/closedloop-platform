
package com.se.cl.sol30.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInterventionStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getInterventionStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="schneiderRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInterventionStatus", propOrder = {
    "schneiderRef"
})
public class GetInterventionStatus {

    @XmlElement(required = true)
    protected String schneiderRef;

    /**
     * Gets the value of the schneiderRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchneiderRef() {
        return schneiderRef;
    }

    /**
     * Sets the value of the schneiderRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchneiderRef(String value) {
        this.schneiderRef = value;
    }

}

package com.se.cl.sol30.service.impl;

import javax.xml.ws.Holder;

import com.se.cl.model.Opportunity;
import com.se.cl.model.Solution30StatusResponse;
import com.se.cl.sol30.service.ActivityReturn;

public interface Solution30Service {

	public Holder<ActivityReturn> sendOpportunityToSol30(Opportunity opportunity);
	public Solution30StatusResponse getOpportunityStatus(String oppId);
	
	public void updateOpportunityHistory();
	
	
}

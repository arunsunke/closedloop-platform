
package com.se.cl.sol30.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for confirmMeetingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="confirmMeetingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="executionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="executionMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activityReturn" type="{http://ws.schneider.pc30.fr/}ActivityReturn" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmMeetingResponse", propOrder = {
    "executionCode",
    "executionMessage",
    "activityReturn"
})
public class ConfirmMeetingResponse {

    protected String executionCode;
    protected String executionMessage;
    protected ActivityReturn activityReturn;

    /**
     * Gets the value of the executionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionCode() {
        return executionCode;
    }

    /**
     * Sets the value of the executionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionCode(String value) {
        this.executionCode = value;
    }

    /**
     * Gets the value of the executionMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionMessage() {
        return executionMessage;
    }

    /**
     * Sets the value of the executionMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionMessage(String value) {
        this.executionMessage = value;
    }

    /**
     * Gets the value of the activityReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityReturn }
     *     
     */
    public ActivityReturn getActivityReturn() {
        return activityReturn;
    }

    /**
     * Sets the value of the activityReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityReturn }
     *     
     */
    public void setActivityReturn(ActivityReturn value) {
        this.activityReturn = value;
    }

}

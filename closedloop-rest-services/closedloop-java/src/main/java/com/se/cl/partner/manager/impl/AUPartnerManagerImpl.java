package com.se.cl.partner.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.Opportunity;
import com.se.cl.model.Partner;
import com.se.cl.partner.manager.PartnerManager;
import com.se.cl.util.GetDistanceUsingGDMatrix;
import com.se.clm.mail.EmailTemplates;
import com.se.clm.mail.IEmailService;

@Component("Australia")
public class AUPartnerManagerImpl implements PartnerManager {

	private static final Logger logger = LoggerFactory.getLogger(AUPartnerManagerImpl.class);

	@Autowired
	PartnerDAO partnerDAO;
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	OpportunityManager opportunityManager;
	
	@Autowired 
	IEmailService iEmailService;

	@Autowired 
	GetDistanceUsingGDMatrix gdMetrix;
	
	
	private @Value("${partner-distance}") Long distance;
	private @Value("${partner-count}") Long noOfPartners;
	private @Value("${se.admin.email}") String seAdminEmail;
	private @Value("${route.to.admin}") String routeToAdmin;
	
	

	private @Value("${google.api.key}") String googleApiKey;
	private @Value("${google.push.notification.url}") String googlePushNotificationURL;
	private @Value("${ios.certificate.password}") String iosCertificatePassword;

   

	public List<String> getNearByPartners(String originZip,String byType) {

		Hashtable<String,String> partnerAndZip=partnerDAO.getPartnerZips(CountryCode.AUSTRALIA.getValue());
		//make list 
		Set<Map.Entry<String, String>> entrySet = partnerAndZip.entrySet();
		List<String> partnerList=new ArrayList<String>();
		List<String> zipsList=new ArrayList<String>();
		for (Entry<String, String> entry : entrySet) {
			System.out.println("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
			partnerList.add((String)entry.getKey());
			zipsList.add((String)entry.getValue());
		}
		List<String> originsList=new ArrayList<String>();
		originsList.add(originZip);
		//GetDistanceUsingGDMatrix getdistance=new GetDistanceUsingGDMatrix();
		LinkedHashMap<String,Long> zipAndDistance=	gdMetrix.getZipAndDistance(originsList, zipsList);


		//zip-distance 
		//attach distance to partner
		HashMap<String,Long> partnerDistance=new HashMap<String,Long>();
		for (Entry<String, String> entry : entrySet) {
			logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
			//get partner id after ,
			String zipCode = entry.getValue().toString().substring( entry.getValue().toString().indexOf(",")+1);
			
			partnerList.add((String)entry.getKey());
			zipsList.add(zipCode);

			if (zipAndDistance.get(entry.getValue().toString())!=null){
				partnerDistance.put(entry.getKey().toString(), zipAndDistance.get(entry.getValue().toString()));
			}

		}

		//filter partners bytype

		logger.debug("partnerDistance :"+partnerDistance);

		Set<Map.Entry<String, Long>> partnerEntrySet = partnerDistance.entrySet();

		List<String> filteredPartnerList=new ArrayList<String>();
		for (Entry<String, Long> entry : partnerEntrySet) {
			logger.debug("partner ID: " + entry.getKey() + " distance: " + entry.getValue());
			if (Long.valueOf((entry.getValue().toString()))<=distance){
				filteredPartnerList.add(entry.getKey().toString());
			}
		}

		return filteredPartnerList;
	}

	/**
	 * @smarni for now this is duplicate method as of above in future we need to refactor
	 * @param originZip
	 * @param byType
	 * @return
	 */
	private List<String> getNearByPartners(String originZip,String byType,Long inputdistance) {

		Hashtable<String,String> partnerAndZip=partnerDAO.getPartnerZips(CountryCode.AUSTRALIA.getValue());
		//make list 
		Set<Map.Entry<String, String>> entrySet = partnerAndZip.entrySet();
		List<String> partnerList=new ArrayList<String>();
		List<String> zipsList=new ArrayList<String>();
		for (Entry<String, String> entry : entrySet) {
			System.out.println("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
			partnerList.add((String)entry.getKey());
			zipsList.add((String)entry.getValue());
		}
		List<String> originsList=new ArrayList<String>();
		originsList.add(originZip);
		//GetDistanceUsingGDMatrix getdistance=new GetDistanceUsingGDMatrix();
		LinkedHashMap<String,Long> zipAndDistance=	gdMetrix.getZipAndDistance(originsList, zipsList);


		//zip-distance 
		//attach distance to partner
		HashMap<String,Long> partnerDistance=new HashMap<String,Long>();
		for (Entry<String, String> entry : entrySet) {
			logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
			//get partner id after ,
			String zipCode = entry.getValue().toString().substring( entry.getValue().toString().indexOf(",")+1);
			
			partnerList.add((String)entry.getKey());
			zipsList.add(zipCode);

			if (zipAndDistance.get(entry.getValue().toString())!=null){
				partnerDistance.put(entry.getKey().toString(), zipAndDistance.get(entry.getValue().toString()));
			}

		}

		//filter partners bytype

		logger.debug("partnerDistance :"+partnerDistance);

		Set<Map.Entry<String, Long>> partnerEntrySet = partnerDistance.entrySet();

		List<String> filteredPartnerList=new ArrayList<String>();
		for (Entry<String, Long> entry : partnerEntrySet) {
			logger.debug("partner ID: " + entry.getKey() + " distance: " + entry.getValue());
			if (Long.valueOf((entry.getValue().toString()))<=inputdistance){
				filteredPartnerList.add(entry.getKey().toString());
			}
		}

		return filteredPartnerList;
	}

	public void assignPartnerToOpportunity() {

		
		//route to admin 
		/**
		 * if we have admin user in stakeholder and route.to.admin is true in partner properties send all opportunities to admin user 
		 * 
		 */
		List<Partner> partners=partnerDAO.getPartner("AU");
		Partner admin=adminfound(partners);
		if (routeToAdmin.equalsIgnoreCase("true") && admin!=null ){
			//need to refactor below code when time permits 
			HashMap<String,String> oppMap=opportunityDAO.getOpportunityIdAndZip(OppStatus.LOADED,"AU");
			
			if (oppMap!=null && oppMap.size()>0){
				Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
				List<String> partnerList=new ArrayList<String>();
				partnerList.add(admin.getId());
				for (Entry<String, String> entry : entrySet) 
				{
					logger.info("assignPartnerToOpportunity admin:" + admin.getId());
					HashMap<String,List<String>> oppPartnerList=new HashMap<String,List<String>>();
					oppPartnerList.put(entry.getKey().toString(), partnerList);
					partnerDAO.assignOpportunityToPartner(oppPartnerList);
					Opportunity opp=new Opportunity();
					opp.setStatus(OppStatus.INITIAL.getValue());
					List<String> oppIds=new ArrayList<String>();
					oppIds.addAll(oppPartnerList.keySet());
					try {
						opportunityManager.updateOpportunity(opp, oppIds, null, 0, null, UpdSource.LISTUPD,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		}else{
		
		
				HashMap<String,String> oppMap=opportunityDAO.getOpportunityIdAndZip(OppStatus.LOADED,"AU");
		
				if (oppMap!=null && oppMap.size()>0){
		
					Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
		
					for (Entry<String, String> entry : entrySet) 
					{
						System.out.println("OPP ID: " + entry.getKey() + " ZIP: " + entry.getValue());
		
						List<String> partnerList=getNearByPartners( entry.getValue().toString(), null);
						//List<String> partnerList = new ArrayList<String>();
						//partnerList.add("100400");
						if (partnerList!=null && partnerList.size()>0)
						{
							
							logger.info("assignPartnerToOpportunity:partnerList.size:" + partnerList.size());
		
							HashMap<String,List<String>> oppPartnerList=new HashMap<String,List<String>>();
							oppPartnerList.put(entry.getKey().toString(), partnerList);
							partnerDAO.assignOpportunityToPartner(oppPartnerList);
							
							Opportunity opp=new Opportunity();
						
							opp.setStatus(OppStatus.INITIAL.getValue());
							List<String> oppIds=new ArrayList<String>();
							oppIds.addAll(oppPartnerList.keySet());
							try {
								opportunityManager.updateOpportunity(opp, oppIds, null, 0, null, UpdSource.LISTUPD,false);
							} catch (AppException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}else{
							//no partner foound for the opportunity set the opportunity in PARKED state
							Opportunity oppSend =new Opportunity();
							oppSend.setId(entry.getKey().toString());
							try {
								opportunityManager.updateOpportunity(oppSend, null,null,0, null, UpdSource.AUPARKED,false);
							} catch (AppException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							//opportunityDAO.updateOppStatusSystem( entry.getKey().toString(), OppStatus.PARKED.name(),"No Partners Found for this opportuity");
							
							//send email to se admin that no partner found
							List<String> template=new ArrayList<String>();
							Opportunity opp=opportunityDAO.getOportunity(entry.getKey().toString());
							
							template = EmailTemplates
									.sendEmailtoSEAdminWithNoPartnerFound(seAdminEmail,
											entry.getKey().toString(), opp
													.getCustomerDetails()
													.getFirstName(), entry.getValue()
													.toString(), opp.getBudget());
							;
							try {
								iEmailService.sendMail(seAdminEmail, template.get(1), template.get(0),null,null,CountryCode.AUSTRALIA.getValue(),0);
							} catch (MessagingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					
					}
		
				}
		
		}	
		

	}



	private Partner adminfound(List<Partner> partners) {
		// TODO Auto-generated method stub
		for(Partner partner:partners){
			if(partner.isAdmin()){
				return partner;
			}
		}
		
		return null;
	}

	@Override
	public List<Opportunity> getOppWhenNoPartnerAccepted(String country) {
		// TODO Auto-generated method stub
		
		List<String> oppsIdList=partnerDAO.getOppWhenNoPartnerAccepted(country);
		List<Opportunity> oppsList=new ArrayList<Opportunity>();
		if(oppsIdList!=null && oppsIdList.size()>0){
			
			for(String oppId:oppsIdList){
				Opportunity opp=opportunityDAO.getOportunity(oppId);
				oppsList.add(opp);
				//set opportunity to PARKED
				/*try {
					opportunityDAO.updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
			
		}
		
		
		
		
		return oppsList;
		
	}



	@Override
	public List<Opportunity> getOppWhenAllPartnerDeclined(String country) {
		

		List<String> oppsIdList=partnerDAO.getOppWhenAllPartnerDeclined(country);
		List<Opportunity> oppsList=new ArrayList<Opportunity>();
		if(oppsIdList!=null && oppsIdList.size()>0){
			
			for(String oppId:oppsIdList){
				Opportunity opp=opportunityDAO.getOportunity(oppId);
				oppsList.add(opp);
				//set opportunity to PARKED
				try {
					Opportunity oppSend=new Opportunity();
					oppSend.setId(oppId);
					oppSend.setStatus(OppStatus.PARKED.getValue());
					opportunityManager.updateOpportunity(oppSend, null, null, 0, null, UpdSource.ACCDECLINED,false);
					//opportunityDAO.updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
		return oppsList;
		
	}



	@Override
	public List<Partner> getPartner(String countryCode,String zipCode,String distance) throws AppException {
		// TODO Auto-generated method stub
		List<Partner> returnPartnerList=new ArrayList<Partner>();
		if (zipCode!=null ){
			List<Partner> partnerList=partnerDAO.getPartner(countryCode);
			List<String> filteredPartnerList=new ArrayList<String>();
			if (distance!=null){
				filteredPartnerList=getNearByPartners(countryCode+","+zipCode,null,Long.parseLong(distance));
			}else{
				filteredPartnerList=getNearByPartners(countryCode+","+zipCode,null);
			}
			
			for(Partner partner:partnerList){
				if (filteredPartnerList.contains(partner.getId())){
					returnPartnerList.add(partner);
				}
			}
			return returnPartnerList;
		}
		
		return partnerDAO.getPartner(countryCode);
	}



	@Override
	public void sendOppTOSol30(String oppId, String zip) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public boolean sendOppTOSol30(String oppId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void assignFolderPermissionsToPartners(String countryCode) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void unAssignFolderPermissionsToPartners(String countryCode) {
		// TODO Auto-generated method stub
		
	}



	



}

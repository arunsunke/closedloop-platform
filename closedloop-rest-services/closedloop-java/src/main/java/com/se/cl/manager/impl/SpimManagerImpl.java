package com.se.cl.manager.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.se.cl.dao.OpportunityDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.SpimManager;
import com.se.cl.model.OpportunityImages;
import com.se.cl.partner.manager.PartnerManagerFactory;
import com.se.cl.spim.client.SpimHandler;

@Component
public class SpimManagerImpl implements SpimManager{

	private static Logger LOGGER = LoggerFactory.getLogger(SpimManagerImpl.class);
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	PartnerManagerFactory partnerfactFactory;
	
	@Autowired
	SpimHandler spimHandler;
		
    @Async   
	@Override
	public void uploadImages(String imsToken,String refreshToken ,String userType,
			String oppTitle,List<String> imgData, List<String> imageTitle,String oppId,String userId) throws AppException 
	{
		List<OpportunityImages> oppImages=new ArrayList<OpportunityImages>();
		boolean uploadSuccess=false;
		try
		{
		// TODO Auto-generated method stub
		oppImages=spimHandler.uploadConsumerImages(imsToken,refreshToken,userType,oppTitle,imgData,imageTitle,oppId,userId);
		if(oppImages!=null && oppImages.size()>0)
		{
			opportunityDAO.udateOpportunityConsumerImages(oppTitle, oppImages);
			uploadSuccess=true;
		}
		
		}catch(Exception e)
		{
			LOGGER.error("Exception :::"+e.getMessage());
		}
		
		//return uploadSuccess ;
	}
	
	
	@Override
	public List<Map<String,String>> getImageIdList(String imsToken, String userType,
			String oppTitle,String oppId,String userId) throws AppException {
		// TODO Auto-generated method stub
		List<Map<String,String>> imageObjIdList = new ArrayList<Map<String ,String>>();
		List<OpportunityImages> oppImagesList = new ArrayList<OpportunityImages>();
		Map<String ,String> imageMapList = null; 
		oppImagesList = opportunityDAO.getOportunityImages(oppId,oppTitle,userId);
		LOGGER.info("oppImagesList"+oppImagesList);
		if(oppImagesList!=null && oppImagesList.size()>0)
			LOGGER.info("oppImagesList Size is:::"+oppImagesList.size());

		//oppConsumerImagesList = opportunityDAO.getOportunityImages(oppId,oppTitle,userId);
		
		
		for(OpportunityImages oppImages : oppImagesList)
		{ 
			imageMapList = new HashMap<String,String>();
			imageMapList.put("objectId"+"",oppImages.getArtifactObjectId());
			imageMapList.put("title",oppImages.getArtifactName());
			imageMapList.put("userType",oppImages.getUserType());
			imageObjIdList.add(imageMapList);
		}	
		return imageObjIdList;
	}
	
	
	@Override
	public Map<String,Object> getImageContent(String imsToken,String refreshToken, String userType,
			String opportunityTitle, String imageObjectId) throws AppException {
		// TODO Auto-generated method stub
		Map<String,Object> responseImageData = new HashMap<String ,Object>();
        responseImageData = spimHandler.getImagesData(imsToken, refreshToken,userType,opportunityTitle,imageObjectId);
		return responseImageData;
	}


    @Async   
	@Override
	public void uploadPartnerImages(String imsToken, String refreshToken,
			String userType, String oppTitle, String imgData,
			String imageTitle, String oppId, String userId) throws AppException {
		
		// TODO Auto-generated method stub
		
		OpportunityImages oppImages=null;
		boolean uploadSuccess=false;

		try
		{
		// TODO Auto-generated method stub
		oppImages = new OpportunityImages();
		oppImages = spimHandler.uploadPartnerImages(imsToken,refreshToken,userType,oppTitle,imgData,imageTitle,oppId,userId);
		if(oppImages!=null && oppImages.getArtifactObjectId().length()>0)
		{
			
			opportunityDAO.udateOpportunityImages(oppTitle, oppImages);
			uploadSuccess=true;
		}
		
		}catch(Exception e)
		{
			LOGGER.error("Exception :::"+e.getMessage());
		}
		
		//return uploadSuccess ;
		
	}

}

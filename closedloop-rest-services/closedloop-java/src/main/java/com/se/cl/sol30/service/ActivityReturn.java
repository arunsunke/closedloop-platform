
package com.se.cl.sol30.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityReturn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityReturn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSol30" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="schneiderRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityReturn", propOrder = {
    "idSol30",
    "schneiderRef"
})
public class ActivityReturn {

    @XmlElement(required = true)
    protected String idSol30;
    @XmlElement(required = true)
    protected String schneiderRef;

    /**
     * Gets the value of the idSol30 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSol30() {
        return idSol30;
    }

    /**
     * Sets the value of the idSol30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSol30(String value) {
        this.idSol30 = value;
    }

    /**
     * Gets the value of the schneiderRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchneiderRef() {
        return schneiderRef;
    }

    /**
     * Sets the value of the schneiderRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchneiderRef(String value) {
        this.schneiderRef = value;
    }

}

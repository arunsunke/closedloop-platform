package com.se.cl.sol30.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import com.se.cl.sol30.service.ISchneider;
import com.se.cl.sol30.service.SchneiderService;

public class TestSol30 {

	public static void main(String[] args) {
		
		/*SchneiderService service=new SchneiderService();

		ISchneider sc=service.getSchneiderPort();
		Holder<List<String>> opportunities=new Holder<List<String>>();
		Holder<String> executionCode=new Holder<String>();
		Holder<String> executionMessage=new Holder<String>();
			
		sc.getSchneiderOpportunity(opportunities, executionCode, executionMessage);
		*/
		TestSol30 testsol30 = new TestSol30();
		
		testsol30.executeSolution30();
		
		
		//System.out.println("test");
	}
	
	public void executeSolution30()
	{
		
		SchneiderService service=new SchneiderService();

		ISchneider sc=service.getSchneiderPort();
		
   	   // ((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 1000);
		/*Map<String, Object> requestContext = ((BindingProvider) sc).getRequestContext();

	    requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 1000); //10 secs

	    requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, 2 * 60 * 1000); //2 min
*/
		
   	    ((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 100);
		
		//((BindingProvider)sc).getRequestContext().put("javax.xml.ws.client.connectionTimeout", 100); // 10 seconds  
		//((BindingProvider)sc).getRequestContext().put("javax.xml.ws.client.receiveTimeout", 1*60*1000); // 1 minute

		Holder<List<String>> opportunities=new Holder<List<String>>();
		Holder<String> executionCode=new Holder<String>();
		Holder<String> executionMessage=new Holder<String>();
			
		sc.getSchneiderOpportunity(opportunities, executionCode, executionMessage);
		
		System.out.println("test");

		
	}
	
	
}

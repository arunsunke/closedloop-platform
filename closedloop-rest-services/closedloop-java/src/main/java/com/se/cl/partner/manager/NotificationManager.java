package com.se.cl.partner.manager;

import java.util.List;

import com.se.cl.exception.AppException;
import com.se.cl.model.BOM;

public interface NotificationManager {
	
	
	public void notifyPartnerForNewOpportunity(String country)throws AppException ;
	
	public void notifyPartnerForCancel(String country,String nType) throws AppException;
	
	public void  sendSmsTopartnerwhenConsCancelled(String countryCode,boolean sendSms,String notifyType);
	
	public void  sendSmsToConsumerWhenMeetingBooked(String countryCode,boolean sendSms,String notifyType);

	
	public void notifyPartnerForSALRemider(int slaStep, String country) throws AppException;
	
	public void sendMail();
	
	//public void  sendMailToConsumer(String consumerEmail,String partnerName,String projectName,String consumerName,String zipCode,String budget,   String templateId);

	public void sendMailNoPartnerAccepted(String country);
	
	public void sendMailAllPartnerDeclined(String country);
	
	public void sendNewOppToBFO();
	
	public void sendNewOppToIF();
	
	public void sendAcceptRejectToBFO(String oppStatus);
	
	public void sendStatusHistToBFO(String statusId);
	
	public void sendBOM(String oppId,String userId,String supplierEmail);
	
	public void sendMailToConsumerOppReceived(String countryCode);
	
	//public void sendMailToConsumerPartnerAccepted(String countryCode);
	
	public void notifyPartnerForN4toN6(int hrs,String nType,String country);

	public void sendAcceptRejectToBFOIF(String oppStatus);

	public void sendStatusHistToIF(String statusId);

	public void sendMailToConsumerPartnerAccepted(String countryCode, boolean sendSms);

	public void sendMailToConsumer(String consumerEmail,String oopId, String partnerName,String projectName, String consumerName, String zipCode,String budget, String templateId, String date, String custNumber,String custMail,List<BOM> boms,String address,String city);

	public void sendMailToConsumerWhenPartnerBookedMeeting(String countryCode);

	public void sendMailToAminForInvoice(String countryCode,String status);


}

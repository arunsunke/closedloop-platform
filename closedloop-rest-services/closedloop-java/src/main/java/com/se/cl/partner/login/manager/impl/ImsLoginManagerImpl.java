package com.se.cl.partner.login.manager.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.se.cl.model.IMS;
import com.se.cl.partner.login.manager.ImsLoginManager;
import com.sun.jersey.core.util.Base64;


public class ImsLoginManagerImpl implements ImsLoginManager
{
	private static final Logger logger = LoggerFactory.getLogger(ImsLoginManagerImpl.class);
	
	//SQE_SIGNIN_URL=https://login-sqe.pace.schneider-electric.com/api/v1/token
	//PREPROD_SIGNIN_URL=https://login-ppr.pace.schneider-electric.com/api/v1/token
	//PROD_SIGNIN_URL=https://login.dces.schneider-electric.com/api/v1/token

	private  @Value("${imsUrl}") String imsUrl;
	private  @Value("${imsUserUrl}") String imsUserUrl;
	private  @Value("${imsClientId}") String imsClientId;
	private  @Value("${imsClientSecret}") String imsClientSecret;
	
	/*
	String imsUrl="https://login-ppr.pace.schneider-electric.com/api/v1/token";
	String imsUserUrl="https://login-ppr.pace.schneider-electric.com/api/v1/user";
	String imsClientId="closed-loop";
	String imsClientSecret="qQ@*$46b2mBRANF";
	*/
	
	
	public static void main(String[] args)
	  {
	     try {
			//System.out.println(new HttpsClientImpl().getToken());
	    	 String User="schneiderclims@gmail.com";
	    	 String Password="Mphasis123";
	 		//System.out.println(new ImsLoginManagerImpl().AuthenticateUser(User, Password));
	    	// System.out.println(new ImsLoginManagerImpl().getAuthenticateUserToken(User,Password));
	    	 IMS ims=null;
	    	 ims = new ImsLoginManagerImpl().getAuthenticateUserToken(User,Password);
	    	 //new ImsLoginManagerImpl().getAccessTokenFromRefreshToken(ims.getRefreshToken());
	    	 //System.out.println(new ImsLoginManagerImpl().getImsUserId("c3756550-ab38-444b-9ac9-4a5329326787"));
             
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	public   boolean AuthenticateUser(String User,String Password)
	{
		/*
		 
		  SQE_SIGNIN_URL=https://login-sqe.pace.schneider-electric.com/api/v1/token
		  PREPROD_SIGNIN_URL=https://login-ppr.pace.schneider-electric.com/api/v1/token
		  PROD_SIGNIN_URL=https://login.dces.schneider-electric.com/api/v1/token

		 */
		
		//String imsUrl="https://login-ppr.pace.schneider-electric.com/api/v1/token";
		
		logger.info("AuthenticateUser:User:"+User);
		logger.info("AuthenticateUser:Password:"+Password);
		URL url;
	    boolean response=false;
		try
		{
							
				logger.debug("AuthenticateUser:imsUrl::::"+imsUrl);
			    url = new URL(imsUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("POST");
			    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			    String postData="grant_type=password&username="+User+"&password="+Password+"";
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(postData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();

		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        response = parseImsResponseJsonData(strBuffer.toString());
		        logger.info("response is"+response);
		        return response;
				 } catch (MalformedURLException e) {
						e.printStackTrace();
					 } catch (Exception e) {
						e.printStackTrace();
					 }
				
				
				return response;
	}
	
	
	public   IMS getAuthenticateUserToken(String User,String Password)
	{
		/*
		 
		  SQE_SIGNIN_URL=https://login-sqe.pace.schneider-electric.com/api/v1/token
		  PREPROD_SIGNIN_URL=https://login-ppr.pace.schneider-electric.com/api/v1/token
		  PROD_SIGNIN_URL=https://login.dces.schneider-electric.com/api/v1/token

		 */
		
	
		
		
		logger.info("getAuthenticateUserToken:User:"+User);
		//logger.debug("getAuthenticateUserToken:Password:"+Password);
		URL url;
		IMS ims=new IMS(); 
	    try
		{
							
				logger.debug("getAuthenticateUserToken:imsUrl::::"+imsUrl);
			    url = new URL(imsUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("POST");
			    con.setRequestProperty("Authorization", "Basic "+new String(Base64.encode(imsClientId+":"+imsClientSecret)));
			    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			    String postData="grant_type=password&username="+User+"&password="+Password+"";
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(postData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();

		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        String jsonStr = strBuffer.toString();
		        ObjectMapper mapper = new ObjectMapper();
				if (jsonStr!=null && jsonStr.length()>0)
				{
					try {
						org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
						String access_token=rootNode.path("access_token").getTextValue();
						String expires_in=Long.toString(rootNode.path("expires_in").asLong());
						String refresh_token=rootNode.path("refresh_token").getTextValue();
						logger.info("getAuthenticateUserToken:::access_token:::"+access_token);
						logger.info("getAuthenticateUserToken:::expires_in:::"+expires_in);
						logger.info("getAuthenticateUserToken:::refresh_token:::"+refresh_token);
						ims.setAccessToken(access_token);
						ims.setExpiresIn(expires_in);
						ims.setRefreshToken(refresh_token);
						

						/*if(access_token!=null && access_token.length()>0)
						{
							response=access_token;
						}*/

					} catch (Exception e) 
					{
						logger.error("Error occurred while Parsing the Values from JSON  "+ e);
						e.printStackTrace();

					}
				}

		        //logger.info("response is"+response);
		        return ims;
				 } catch (MalformedURLException e) {
						e.printStackTrace();
					 } catch (Exception e) {
						e.printStackTrace();
					 }
				
				
				return ims;
	}
	

	private boolean parseImsResponseJsonData(String jsonStr) 
	{

		boolean updateResult=false;
		logger.info("parseImsResponseJsonData:::jsonStr:::"+jsonStr);
		ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0)
		{
			try {
				org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
				String access_token=rootNode.path("access_token").getTextValue();
				logger.info("parseImsResponseJsonData:::access_token:::"+access_token);
				if(access_token!=null && access_token.length()>0)
				{
					updateResult=true;
				}

			} catch (Exception e) 
			{
				logger.error("Error occurred while Parsing the Values from JSON  "+ e);
				e.printStackTrace();

			}
		}
		return updateResult;
		// TODO Auto-generated method stub
		//return null;
	}
	@Override
	public IMS getAccessTokenFromRefreshToken(String refeshToken) 
	{
		
		logger.info("getAccessTokenFromRefreshToken:refeshToken:"+refeshToken);
		URL url;
	    String response="";
		IMS ims = new IMS(); 
	    try
		{
							
				logger.debug("getAccessTokenFromRefreshToken:imsUrl::::"+imsUrl);
			    url = new URL(imsUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("POST");
			    con.setRequestProperty("Authorization", "Basic "+new String(Base64.encode(imsClientId+":"+imsClientSecret)));
			    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			    String postData="grant_type=refresh_token&refresh_token="+refeshToken+"";
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(postData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();

		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        String jsonStr = strBuffer.toString();
		        ObjectMapper mapper = new ObjectMapper();
				if (jsonStr!=null && jsonStr.length()>0)
				{
					try {
						org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
						String access_token=rootNode.path("access_token").getTextValue();
						String expires_in=Long.toString(rootNode.path("expires_in").asLong());
						String refresh_token=rootNode.path("refresh_token").getTextValue();
						logger.info("getAccessTokenFromRefreshToken:::access_token:::"+access_token);
						logger.info("getAccessTokenFromRefreshToken:::expires_in:::"+expires_in);
						logger.info("getAccessTokenFromRefreshToken:::refresh_token:::"+refresh_token);
						ims.setAccessToken(access_token);
						ims.setExpiresIn(expires_in);
						ims.setRefreshToken(refresh_token);
						
					} catch (Exception e) 
					{
						logger.error("getAccessTokenFromRefreshToken:Error occurred while Parsing the Values from JSON  "+ e);
						e.printStackTrace();

					}
				}

		        //logger.info("response is"+response);
		        return ims;
				 } catch (MalformedURLException e) {
						e.printStackTrace();
					 } catch (Exception e) {
						e.printStackTrace();
					 }
				
				
				return ims;	
				
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 * @see com.se.cl.partner.login.manager.ImsLoginManager#getImsUserId(java.lang.String)
	 */
	public String getImsUserId(String imsToken)
	{
		logger.info("getImsUserId:imsToken:"+imsToken);
		URL url;
		String imsUserId="";
		BufferedReader reader = null;
		StringBuilder stringBuilder=null;
	    try
		{
							
				logger.debug("getImsUserId:imsUserUrl::::"+imsUserUrl);
			    url = new URL(imsUserUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("GET");
			    con.setRequestProperty("Authorization",imsToken);
			    con.setRequestProperty("Accept","Application/json");
			    con.setDoInput(true);		   
			    // give it 10 seconds to respond
			    con.setReadTimeout(10*1000);
			    con.connect();
			    int responseCode = con.getResponseCode();
			    if(responseCode==401)
			    	return imsUserId="unauthorized";
			    // read the output from the server
			    reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			    stringBuilder = new StringBuilder();
			    String line = null;
			    while ((line = reader.readLine()) != null)
			     {
			        stringBuilder.append(line + "\n");
			     }
			    logger.info("Response data  :"+ stringBuilder.toString());
		        String jsonStr = stringBuilder.toString();
		        ObjectMapper mapper = new ObjectMapper();
				if (jsonStr!=null && jsonStr.length()>0)
				{
					try 
					{
						org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
						imsUserId=rootNode.path("id").getTextValue();
						
					} catch (Exception e) 
					{
						logger.error("Error occurred while Parsing the Values from JSON  "+ e);
						e.printStackTrace();

					}
				}
		        logger.debug("imsUserId is:::"+imsUserId);
		         return imsUserId;
				 } catch (MalformedURLException e) 
	             {
						e.printStackTrace();
				 } catch (Exception e)
	             {
						e.printStackTrace();
				 }
				
				
				return imsUserId;	
	}


}

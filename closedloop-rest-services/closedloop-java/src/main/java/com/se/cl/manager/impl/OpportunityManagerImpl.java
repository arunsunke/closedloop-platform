package com.se.cl.manager.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.se.cl.constants.BFOStatus;
import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.OpportunityBFODAO;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.Solution30DAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.BOM;
import com.se.cl.model.Distributor;
import com.se.cl.model.Opportunity;
import com.se.cl.model.Partner;
import com.se.cl.model.PartnerDevice;
import com.se.cl.model.User;
import com.se.cl.partner.manager.NotificationManagerFactory;
import com.se.cl.partner.manager.PartnerManagerFactory;
import com.se.cl.util.EncryptDecryptUtil;


public class OpportunityManagerImpl implements OpportunityManager{

	private static Logger LOGGER = LoggerFactory.getLogger(OpportunityManagerImpl.class);
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	Solution30DAO solution30dao;
	@Autowired 
	OpportunityBFODAO opportunityBFODAO;
	
	@Autowired
	NotificationManagerFactory notificationManagerFactory;
	
	@Autowired
	PartnerManagerFactory partnerfactFactory;
	
	
	
	@Override
	public synchronized void  updateOpportunityStatus(String partnerId, String oppId,
			String statusId, String status,boolean isAdminAssigned,String countryCode) throws AppException{
	
		if (status.equalsIgnoreCase("READ")){
			//commented for testing TODO: remove below comment later
			opportunityDAO.updateOppReadStatus(oppId, partnerId, 1);
			
		}else{
			
			LOGGER.info("updateOpportunityStatus:isAdminAssigned:::::::"+isAdminAssigned);
			
			//Opportunity opp=opportunityDAO.updateOppStatus(partnerId, oppId, status);
			Opportunity oppSend=new Opportunity();
			oppSend.setId(oppId);
			Partner partner=new Partner();
			partner.setId(partnerId);
			oppSend.setPartner(partner);
			oppSend.setStatus(status);
			Opportunity opp =updateOpportunity(oppSend, null,null,0,null,UpdSource.APPACCEPT,isAdminAssigned);
			LOGGER.info("updateOpportunityStatus:oppId:::::::"+oppId);

			List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(oppId));
			
			//send email to consumer if opp accepted and no errors
			//TODO : for now AU hardcoded get country from opportunity
			/*if ( opp!=null)
			{
				if (status.equalsIgnoreCase(OppStatus.ACCEPTED.name()))
				{
					notificationManagerFactory.getNotificationManager(opp.getAddress().getCountryCode()+"N").sendMailToConsumer(opp.getCustomerDetails().getEmail(),oppId ,opp.getPartner().getName(),
							opp.getTitle(), opp.getCustomerDetails().getFirstName(), opp.getAddress().getZip(), opp.getBudget(),"NewOpp",opp.getOpportunityDate(),opp.getCustomerDetails().getPhone(),opp.getCustomerDetails().getEmail(),boms,opp.getAddress().getSt_name(),opp.getAddress().getCity());
					
							  
				
				}
			}*/
			
			if(countryCode!=null && countryCode.equalsIgnoreCase(CountryCode.SWEDEN.getValue()))
			{
				
				String fromDate = getTimeForTZ("CET");
			    String toDate =   getTimeForTZ("CET");	
			    LOGGER.debug("updateOpportunityStatus:fromDate:"+fromDate);
			    LOGGER.debug("updateOpportunityStatus:toDate:"+toDate);
				try
				{
				
					updateOpportunityHistory(partnerId,oppId, BFOStatus.MEETING.name(),"Meeting Scheduled", "Meeting Scheduled",fromDate, toDate,"status","CL");
					updateOpportunityHistory(partnerId,oppId, BFOStatus.QUOTATION.name(),"Quotation Sent", "Quotation Sent",fromDate, toDate,"status","CL");
					updateOpportunityHistory(partnerId,oppId, BFOStatus.WON.name(),"Opportunity Won", "Opportunity Won",fromDate, toDate,"status","CL");
				
				}catch(Exception e)
				{
				     LOGGER.debug("Exception in updateOpportunityStatus:::"+e.getMessage());   
				}
							
			}	
			
			}
	
		
		
	}


	@Override
	public int addDevicetoPartner(PartnerDevice device) throws AppException{
		return opportunityDAO.addDevicetoPartner(device);

	}


	@Override
	public synchronized void updateOpportunityHistory(String partnerId, String oppId,
			String statusId,String status,String reason,String startTime,String endTime,String type,String source) throws AppException {
		    //String timeZone="CET";
			//try {
				opportunityDAO.updateOppHistory(partnerId, oppId, statusId,status,reason,startTime,endTime,type,source);
		/*	} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
*/			
	
	}
	
	public String getTimeForTZ(String timeZone) {

		Date currDate = new Date();
		Date dateCet = getDateInTimeZone(currDate,(timeZone!=null&&timeZone.length()>0?timeZone:"CET"));
		
	  	 DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
	  	String formatted = DATE_FORMAT.format(dateCet);
	  //	System.out.println(formatted);
	  	 
	  	 
	      return formatted;
	   }
		public String getTimeForTZ(String timeZone,String date) throws ParseException {
		//parse to simple date format	
		DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
			  	
		Date datefrom = DATE_FORMAT.parse(date);
		Date dateCet = getDateInTimeZone(datefrom,(timeZone!=null&&timeZone.length()>0?timeZone:"CET"));
		
	  	String formatted = DATE_FORMAT.format(dateCet);
	  //	System.out.println(formatted);
	  	 
	  	 
	      return formatted;
	   }
	


	

	public  Date getDateInTimeZone(Date currentDate, String timeZoneId) {
	       
	       TimeZone tz = TimeZone.getTimeZone(timeZoneId);
	       Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
	       mbCal.setTimeInMillis(currentDate.getTime());
	       Calendar cal = Calendar.getInstance();
	       cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
	       cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
	       cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
	       cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
	       cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
	       cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
	       cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));
	       return cal.getTime();
	   }
	


	
	@Override
	public Map<String, Object> addOpportunity(Opportunity opportunity) throws AppException {

		Map<String, Object> resposeMap = null;		
		try { 
			
			//validate opportunity
			resposeMap = validateOpportunity(opportunity);
			
			if(resposeMap.get("status") != null && "success".equals((String)resposeMap.get("status"))) {
				
				if (opportunity.getSource()!=null ){
					
					opportunity.setOpportunityDate(getTimeForTZ(opportunity.getSource().getTimeZone()!=null?opportunity.getSource().getTimeZone():"CET"));
				
				}
				long opportunityId = opportunityDAO.addOpportunity(opportunity);
				if (opportunityId > 0)
				{
					//Assign opportunity when source value is not CL
					LOGGER.info("Opportunity Created : ID :"+opportunityId  + " Name: "+ opportunity.getTitle() );
					if (opportunity.getSource() != null
							&& opportunity.getSource().getId() != null
							&& !opportunity.getSource().getId().equalsIgnoreCase("CL")) {

						try {

							String partnerId=opportunityDAO.getPartnerId(opportunity.getSource().getId());
							
							opportunityDAO.assignOpportunityToPartner(opportunityId, Integer.parseInt(partnerId));
							
							Opportunity opp=new Opportunity();
							opp.setId(String.valueOf(opportunityId));
							opp.setStatus(OppStatus.ACCEPTED.getValue());
							updateOpportunity(opp, null, null, 0,null, UpdSource.ACCDECLINED,false);
							if(opportunity!=null && opportunity.getAddress().getCountryCode().equalsIgnoreCase(CountryCode.SWEDEN.getValue()))
							{
								
								String fromDate = getTimeForTZ("CET");
							    String toDate =   getTimeForTZ("CET");
							    LOGGER.debug("addOpportunity:fromDate:"+fromDate);
							    LOGGER.debug("addOpportunity:toDate:"+toDate);
								
								try
								{
								
									updateOpportunityHistory(partnerId,String.valueOf(opportunityId), BFOStatus.MEETING.name(),"Meeting Scheduled", "Meeting Scheduled",fromDate, toDate,"status","CL");
									updateOpportunityHistory(partnerId,String.valueOf(opportunityId), BFOStatus.QUOTATION.name(),"Quotation Sent", "Quotation Sent",fromDate, toDate,"status","CL");
									updateOpportunityHistory(partnerId,String.valueOf(opportunityId), BFOStatus.WON.name(),"Opportunity Won", "Opportunity Won",fromDate, toDate,"status","CL");
								
								}catch(Exception e)
								{
								     LOGGER.debug("Exception in addOpportunity:::"+e.getMessage());   
								}
											
							}	
							
						} catch (Exception e) {
							e.printStackTrace();
							LOGGER.error("Not able to assign opportunity to partner from source");
						}
					}
					
					if(opportunity!=null && opportunity.getAddress().getCountryCode().equalsIgnoreCase(CountryCode.SWEDEN.getValue()))
					{
						String oppId = EncryptDecryptUtil.encrypt(String.valueOf(opportunityId));
						resposeMap.put("status", "success");
						resposeMap.put("message", "Opportunity saved successfully");
						resposeMap.put("opportunityId", oppId);
					
					}else
					{	
						resposeMap.put("status", "success");
						resposeMap.put("message", "Opportunity saved successfully");
						resposeMap.put("opportunityId", opportunityId);
					}
				/**
				 * for DEMO clipspec opps auto assign
				 * 
				 */
					/*if (opportunity.getSource()!=null && opportunity.getSource().getId()!=null && opportunity.getSource().getId().equalsIgnoreCase("CL")){
						
						try{
							
							LOGGER.info("assign opportunity started");
							partnerfactFactory.getPartnerManger("FR").assignPartnerToOpportunity();	
							LOGGER.info("assign opportunity end");

							
							
						}catch(Exception e){
							e.printStackTrace();
							LOGGER.error("Not able to assign opportunity to partner from source");
						}
					}
				
					
					try {
						LOGGER.info("New opportunity notification started");
						notificationManagerFactory.getNotificationManager("FRN").notifyPartnerForNewOpportunity("FR");
						LOGGER.info("New opportunity notification end");
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					*/
			/**
			 * end demo 		
			 */
					
					
				} else {
					resposeMap.put("status", "error");
					resposeMap.put("message", "Error while saving opportunity");
				}
			}

		} catch (Exception e) { 

			LOGGER.error("Error while saving opportunity", e);
			resposeMap = new HashMap<String, Object>();
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Error while saving opportunity");

		} 

		return resposeMap;
	}


	private Map<String, Object> validateOpportunity(Opportunity opportunity) {
		Map<String, Object> resposeMap = new HashMap<String, Object>();	

		if(opportunity == null){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "opportunity is null");
		}else if(opportunity.getTitle() == null || "".equals(opportunity.getTitle())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invalid opportunity title");
		}else if(opportunity.getDescription() == null || "".equals(opportunity.getDescription())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invalid opportunity description");
		}else if(opportunity.getBudget() == null || "".equals(opportunity.getBudget())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity budget");
		}/*else if(opportunity.getBmPDF() == null || "".equals(opportunity.getBmPDF())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity bmPDF");
		}*//*else if(opportunity.getOpportunityDate() == null || "".equals(opportunity.getOpportunityDate())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity date");
		}*//*else if(opportunity.getOpportunityExpDate() == null || "".equals(opportunity.getOpportunityExpDate())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity expiry date");
		}*/else if(opportunity.getAddress() == null){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity address");
		}else if(opportunity.getAddress().getCity() == null || "".equals(opportunity.getAddress().getCity())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity address -> city");
		}else if(opportunity.getAddress().getSt_name() == null || "".equals(opportunity.getAddress().getSt_name())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity address -> st-name");
		}else if(opportunity.getAddress().getZip() == null || "".equals(opportunity.getAddress().getZip())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity address -> zip");
		}else if(opportunity.getAddress().getCountryCode() == null || "".equals(opportunity.getAddress().getCountryCode() ) || !CountryCode.contains(opportunity.getAddress().getCountryCode())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild opportunity address -> Country Code");
		}else if(opportunity.getCustomerDetails() == null){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "No customerDetails found in opportunity");
		}else if(opportunity.getCustomerDetails().getPhone() == null || "".equals(opportunity.getCustomerDetails().getPhone())){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "Invaild customer phone");
		}else if(opportunity.getCustomerDetails().getAddress() == null){
			resposeMap.put("status", "error");
			resposeMap.put("errorMessage", "No address found in customerDetails");
		}else if(opportunity.getBoms() == null)
		{
			float unitCost=134.12f;
			String category = "Plugs";
			String productId= "EER50000";
			String productName="multi pin plug";
			String description="Night Glow Switches";
			String color= "White";
			int quantity= 5;
			BOM bom = new BOM();
			List<BOM> bomList = new ArrayList<BOM>();
			bom.setCategory(category);
			bom.setProductName(productName);
			bom.setProductId(productId);
			bom.setUnitCost(unitCost);
			bom.setColor(color);
			bom.setDescription(description);
		  
			if(opportunity.getAddress().getCountryCode() != null && CountryCode.FRANCE.getValue().equals(opportunity.getAddress().getCountryCode() ) )
		    {
		    	bom.setCurrencyCode("EUR");
		    	
		    }else if(opportunity.getAddress().getCountryCode() != null && CountryCode.AUSTRALIA.getValue().equals(opportunity.getAddress().getCountryCode() ) )
		    {
		    	bom.setCurrencyCode("AUD");

		    }else if(opportunity.getAddress().getCountryCode() != null && CountryCode.SWEDEN.getValue().equals(opportunity.getAddress().getCountryCode() ) )
		    {
		    	bom.setCurrencyCode("SEK");

		    }
			
			bom.setQuantity(quantity);
			bomList.add(bom);
			opportunity.setBoms(bomList);
			resposeMap.put("status", "success");
			resposeMap.put("errorMessage", null);
			
			
		}else{
			for(BOM bom : opportunity.getBoms())
			{
				if(bom == null){
					resposeMap.put("status", "error");
					resposeMap.put("errorMessage", "Invalid bom found in opportunity");
				}else if(bom.getCategory() == null || "".equals(bom.getCategory())){
					resposeMap.put("status", "error");
					resposeMap.put("errorMessage", "Invalid bom category");
				}else if(bom.getProductId() == null || "".equals(bom.getProductId())){
					resposeMap.put("status", "error");
					resposeMap.put("errorMessage", "Invalid bom productId");
				}else if(bom.getUnitCost() <= 0.0F){
					resposeMap.put("status", "error");
					resposeMap.put("errorMessage", "Invalid bom unitCost");
				}else{
					resposeMap.put("status", "success");
					resposeMap.put("errorMessage", null);
				}
			}
		}

		return resposeMap;
	}


	@Override
	public Opportunity loadOpportunityById(long opportunityId)
			throws AppException {
		return opportunityDAO.loadOpportunityById(opportunityId);
	}
	
	
	@Override
	public User validateUser(User user) {
		// TODO Auto-generated method stub
		return opportunityDAO.getUser(user);
	}


	@Override
	public int addDistributorToPartner(Distributor distributor) {
		// TODO Auto-generated method stub
		return opportunityDAO.addDistributorToPartener(distributor);
	}


	@Override
	public List<Distributor> getDistList(String partnerId) {
		// TODO Auto-generated method stub
		return 	 opportunityDAO.getDistList(partnerId);

	}


	@Override
	public int updateDistributor(Distributor distributor,String shId,String distId) {
		
	 return	 opportunityDAO.updateDistributor(distributor,shId,distId);
	}


	@Override
	public int deleteDistributor(String distId,String shId) {
		// TODO Auto-generated method stub
		return opportunityDAO.deleteDistributor(distId,shId);
	}


	@Override
	public synchronized Opportunity updateOpportunity(Opportunity opportunity,List<String> oppIds,String noteType,int slaStep,Map<String,String> bfoResponse ,UpdSource source,boolean isAdminAssigned) throws AppException {
      
        LOGGER.info("updateOpportunity:::source"+source);
		Opportunity oppResponse=new Opportunity();
		if (UpdSource.APPACCEPT.equals(source))
		{
			oppResponse=opportunityDAO.updateOppStatus(opportunity.getPartner().getId(), opportunity.getId(), opportunity.getStatus(),isAdminAssigned);
		}
		//Blocked parked state for Australia as per richard/Anthony Request
		/*if(UpdSource.AUPARKED.equals(source)){
			opportunityDAO.updateOppStatusSystem( opportunity.getId(), OppStatus.PARKED.name(),"No Partners Found for this opportuity");
		}*/
		if(UpdSource.SLA.equals(source))
		{
			opportunityDAO.updateOppNotiStatus(oppIds ,noteType,slaStep);
			
			// after 3rd sla removing the link from partner opportunity
			//Blocked parked state for Australia as per richard/Anthony Request
			/*if (slaStep == 3) {
				try {
					Opportunity opp=new Opportunity();
					opp.setStatus(OppStatus.PARKED.getValue());
					updateOpportunity(opp, oppIds, null, 0,null, UpdSource.LISTUPD,false);
					//updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}*/

			
		}
		if(UpdSource.ACCDECLINED.equals(source))
		{
			List<String> oppIdsList=new ArrayList<String>();
			oppIdsList.add(opportunity.getId());
			opportunityDAO.updateOppStatus(oppIdsList, OppStatus.valueOf(opportunity.getStatus()));
		}
		
		if(UpdSource.LISTUPD.equals(source)){
			opportunityDAO.updateOppStatus(oppIds, OppStatus.valueOf(opportunity.getStatus()));
			
		}
		
		if(UpdSource.SOL30.equals(source))
		{
		     LOGGER.info("updateOpportunity:source::::::"+source);
		     LOGGER.info("updateOpportunity:opportunity.getId()::::::"+opportunity.getId());
		     LOGGER.info("updateOpportunity:noteType::::::"+noteType);
		     
			//noteType is return value from sol30
			solution30dao.updateOpportunity(opportunity.getId(),noteType);
		}
		if(UpdSource.BFO!=null && UpdSource.BFO.equals(source))
		{
			opportunityBFODAO.updateBFOOppStatus(bfoResponse, opportunity.getId());
		}
		
		if(UpdSource.BFOIF!=null && UpdSource.BFOIF.equals(source))
		{
			opportunityBFODAO.updateBFOOppStatusIF(bfoResponse, opportunity.getId());
		}
		
		
		
		return oppResponse;
	}


	@Override
	public boolean sendOpptoSol30(String oppId, String zip,String countryCode) 
	{
		// TODO Auto-generated method stub
		return partnerfactFactory.getPartnerManger(countryCode).sendOppTOSol30(oppId);		
	}


	@Override
	public String getOpportunityStatus(String oppId) throws AppException 
	{
	
		// TODO Auto-generated method stub
				return opportunityDAO.getOpportunityStatus(oppId);
	
	
	}


	@Override
	public List<BOM> getOpportunitySBomArray(String oppId) throws AppException {
		// TODO Auto-generated method stub
		return opportunityDAO.getBoms(Long.parseLong(oppId));		

	}


	@Override
	public void updateOpportunityHistoryCancel(String partnerId,
			String opportunityId, String statusId, String status,
			String reason, String startTime, String endTime, String type,
			String source) {
		// TODO Auto-generated method stub
		try {
			opportunityDAO.updateOppHistoryCancel(partnerId, opportunityId, statusId,status,reason,startTime,endTime,type,source);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	@Override
	public String getOpportunityMeetingDate(String oppId) throws AppException 
	{
		// TODO Auto-generated method stub
		return opportunityDAO.getOpportunityMeetingDate(oppId);
	}


	@Override
	public String getConsumerName(String oppId) 
	{
		// TODO Auto-generated method stub
		
		return opportunityDAO.getOpportunityConsumerName(oppId);
	}


	@Override
	public void updateOpportunityTitle(String oppId)
	{
		// TODO Auto-generated method stub
		
	     opportunityDAO.updateOpportunityTitle(oppId);

		
	}


	

	
		
		
		
	
	

}

package com.se.cl.spim.client;

import java.util.List;
import java.util.Map;

import com.se.cl.model.OpportunityImages;

public interface SpimHandler 
{
	
	public String CreateProject(String projetctName,String imsToken,String spimUrl);
	public String CreateSubFolder(String subFolderName,String imsToken,String rootFolderObjectId,String createSubFolderUrl);
	public String uploadImages(String subFolderObjectId,String imageName,String imsToken,String uploadImageUrl,String base64ContentStream);
	public Map<String,Object> FetchProjectFolder(String imsToken,String spimUrl);
	public Map<String,Object> FetchProjectSubFolder(String rootFolderObjectId,String imsToken,String subFolderSpimUrl);
	public List<Map<String,String>> FetchProjectSubFolderImageObjectIds(String subFolderObjectId,String imsToken,String ImageSpimUrl,String userType);
	public Map<String,Object> FetchProjectSubFolderImageContent(String imageObjectId,String imsToken,String refreshToken,String ImageDownloadUrl);
	public String CheckProjectFolderExists(String imsToken,String spimUrl,String rootFolderTitle);
	public String CheckProjectSubFolderExists(String imsToken,String spimSubFolderUrl,String rootFolderObjId,String subFolderName);
	public boolean ShareFolderPermissions(String adminimsToken,List<String> userImsTokens,List<String> useremailId,String ObjectId,String sharePermissionsUrl);
	public boolean RevokeFolderPermissions(String imsToken,List<String> userImsTokens,String ObjectId,String sharePermissionsUrl);
    public OpportunityImages uploadPartnerImages(String imsToken,String refreshToken,String userType,String opportunityTitle,String base64ContentStream,String imageTitle,String oppId,String userId);
	
    public List<OpportunityImages> uploadConsumerImages(String imsToken,String refreshToken,String userType,String opportunityTitle,List<String> base64ContentStream,List<String> imageTitle,String oppId,String userId);

    public List<Map<String,String>> getListOfUploadedImages(String imsToken,String userType,String opportunityTitle);
	public Map<String,Object> getImagesData(String imsToken,String refreshToken,String userType,String opportunityTitle,String imageObjectId);
	public List<Map<String,String>> getListOfConsumerUploadedImages(String imsToken,String userType,String opportunityTitle);
	public List<Map<String,String>> FetchConsumerProjectSubFolderImageObjectIds(String subFolderObjectId,String imsToken,String ImageSpimUrl,String userType);
	
	public String getPartnerImsFederatedID(String partnerId);
	
	public List<String> getPartnerImsFederatedIDsByCountry(String countryCode);
	
			
}
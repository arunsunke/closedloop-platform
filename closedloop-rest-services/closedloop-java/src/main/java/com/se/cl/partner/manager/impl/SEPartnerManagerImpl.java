package com.se.cl.partner.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.MessagingException;
import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.IMS;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityImages;
import com.se.cl.model.Partner;
import com.se.cl.partner.login.manager.ImsLoginManager;
import com.se.cl.partner.manager.PartnerManager;
import com.se.cl.sol30.service.ActivityReturn;
import com.se.cl.sol30.service.impl.Solution30Service;
import com.se.cl.spim.client.SpimHandler;
import com.se.cl.util.GetDistanceUsingGDMatrix;
import com.se.clm.mail.EmailTemplates;
import com.se.clm.mail.IEmailService;

@Component("Sweden")
public class SEPartnerManagerImpl implements PartnerManager {


	private static final Logger logger = LoggerFactory.getLogger(FRPartnerManagerImpl.class);

	@Autowired
	PartnerDAO partnerDAO;
	@Autowired
	OpportunityDAO opportunityDAO;
	
	
	@Autowired 
	GetDistanceUsingGDMatrix gdMetrix;
	
	@Autowired
	Solution30Service solution30Service;
	

	
	@Autowired
	OpportunityManager opportunityManager;
	
	@Autowired
	SpimHandler spimHandler;
	
	@Autowired
	ImsLoginManager imsLoginManager;
	
	private @Value("${fr.partner.distance}") Long distance;
	private @Value("${fr.partner.count}") Long noOfPartners;
	private @Value("${fr.se.admin.email}") String seAdminEmail;
	//private @Value("${partner-count}") String propertyField;

	private @Value("${google.api.key}") String googleApiKey;
	private @Value("${google.push.notification.url}") String googlePushNotificationURL;
	private @Value("${ios.certificate.password}") String iosCertificatePassword;

	private  @Value("${sharePermissionsUrl}") String sharePermissionsUrl;
	
	private  @Value("${AuthorizationBearerToken}") String AuthorizationBearerToken;
	
	private  @Value("${spim.adminUser}") String spimAdminUser;
	private  @Value("${spim.adminPassword}") String spimAdminPassword;
	private  @Value("${spim.adminUserImsFederatedId}") String spimAdminUserImsFederatedId;
	private  @Value("${spim.adminUserEmailId}") String spimAdminUserEmailId;



	public List<String> getNearByPartners(String originZip,String byType) {
		List<String> filteredPartnerList=new ArrayList<String>();
		Hashtable<String,String> partnerAndZip=partnerDAO.getPartnerZips(CountryCode.SWEDEN.getValue());
		//make list 
		if(partnerAndZip !=null && !partnerAndZip.isEmpty()){
			Set<Map.Entry<String, String>> entrySet = partnerAndZip.entrySet();
			List<String> partnerList=new ArrayList<String>();
			List<String> zipsList=new ArrayList<String>();
			for (Entry entry : entrySet) {
				logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
				partnerList.add((String)entry.getKey());
				zipsList.add((String)entry.getValue());
			}
			List<String> originsList=new ArrayList<String>();
			originsList.add(originZip);
			//GetDistanceUsingGDMatrix getdistance=new GetDistanceUsingGDMatrix();
			LinkedHashMap<String,Long> zipAndDistance=	gdMetrix.getZipAndDistance(originsList, zipsList);
	
	
			//zip-distance 
			//attach distance to partner
			HashMap<String,Long> partnerDistance=new HashMap<String,Long>();
			for (Entry entry : entrySet) {
				logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
				//get partner id after ,
				String zipCode = entry.getValue().toString().substring( entry.getValue().toString().indexOf(",")+1);
				
				partnerList.add((String)entry.getKey());
				zipsList.add(zipCode);
	
				if (zipAndDistance.get(entry.getValue().toString())!=null){
					partnerDistance.put(entry.getKey().toString(), zipAndDistance.get(entry.getValue().toString()));
				}
	
			}
	
			//filter partners bytype
	
			logger.debug("partnerDistance :"+partnerDistance);
	
			Set<Map.Entry<String, Long>> partnerEntrySet = partnerDistance.entrySet();
	
			
			for (Entry entry : partnerEntrySet) {
				logger.debug("partner ID: " + entry.getKey() + " distance: " + entry.getValue());
				if (Long.valueOf((entry.getValue().toString()))<=distance){
					filteredPartnerList.add(entry.getKey().toString());
				}
			}
		}
		return filteredPartnerList;
	}
    
	public List<String> getPartnersByCountry(String originZip,String byType) {
		Hashtable<String,String> partnerAndZip=partnerDAO.getPartnerZips(CountryCode.SWEDEN.getValue());
		List<String> partnerList=null;
		//make list 
		if(partnerAndZip !=null && !partnerAndZip.isEmpty())
		{
			Set<Map.Entry<String, String>> entrySet = partnerAndZip.entrySet();
			partnerList=new ArrayList<String>();
			for (Entry entry : entrySet)
			{
				logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
				if(entry.getKey()!=null && !entry.getKey().toString().equals("88888"))
				{	
						partnerList.add((String)entry.getKey());
				}
			}
			
		}
		return partnerList;
	}


/**
 * send 50% to solution 30 and 50% to partner matching
 * 
 */
	public void assignPartnerToOpportunity() {
		
		int currentExecution=Integer.parseInt(partnerDAO.getCLconfig().get("CL_FR_PART_OR_SOL30")); //if value is 1 it is partner assignment if it is 0 send to solution 30
		
		
		HashMap<String,String> oppMap=opportunityDAO.getOpportunityIdAndZip(OppStatus.LOADED,"SW");

		if (oppMap!=null && oppMap.size()>0){
			
			
			if (oppMap.size()>1){

				Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
				
				int i=0;
				for (Entry entry : entrySet) {
					logger.debug("OPP ID: " + entry.getKey() + " ZIP: " + entry.getValue());
					
					if(currentExecution==0){
						if(i%2==0){
							assignToPartner(entry.getKey().toString(),entry.getValue().toString());
							
							
						}else{
							//sendOppTOSol30(entry.getKey().toString(),entry.getValue().toString());
							//logger.debug("Sent solution 30");
							assignToPartner(entry.getKey().toString(),entry.getValue().toString());

							
						}
					}
					else{
						
						if(i%2==0){
							//sendOppTOSol30(entry.getKey().toString(),entry.getValue().toString());
							//logger.debug("Sent solution 30");
							assignToPartner(entry.getKey().toString(),entry.getValue().toString());

								
						}else{
							
							assignToPartner(entry.getKey().toString(),entry.getValue().toString());
						}
						
					}
				
					i++;
				
				}
			
				if (currentExecution==0 && ((i-1)%2==0)){
					partnerDAO.updateCLconfig("1");
					//update current execution 1 for solution 30
					
				}else{
					
					//update current execution 0 for partnermatch
					partnerDAO.updateCLconfig("0");
				}
			
			}else{

				Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
				for (Entry entry : entrySet) {
					System.out.println("OPP ID: " + entry.getKey() + " ZIP: " + entry.getValue());
					if(currentExecution==0){
						assignToPartner(entry.getKey().toString(),entry.getValue().toString());
					}else{
							//sendOppTOSol30(entry.getKey().toString(),entry.getValue().toString());
							//logger.debug("Sent solution 30");
						    assignToPartner(entry.getKey().toString(),entry.getValue().toString());

						}
					}
				
				if (currentExecution==0 ){
					partnerDAO.updateCLconfig("1");
					//update current execution 0 for partnermatch
				}else{
					//update current execution 1 for solution 30
					partnerDAO.updateCLconfig("0");
				}
			}
		}
		
		
		

	}
	
	@Override
	public void sendOppTOSol30(String oppId, String zip)  {
	
	
		try {
				Opportunity opp = opportunityDAO.loadOpportunityById(Long.parseLong(oppId));
				
				Holder<ActivityReturn> activityReturn=solution30Service.sendOpportunityToSol30(opp);
				Opportunity oppSend=new Opportunity();
				oppSend.setId(oppId);
				if (activityReturn!=null){
					//updated success status to opportunity table
					
				
					opportunityManager.updateOpportunity(oppSend, null, activityReturn.value.getIdSol30(), 0, null, UpdSource.SOL30,false);
					//solution30dao.updateOpportunity(oppId,activityReturn.value.getIdSol30());
					
				}else{
					//update status so that it will retry for failed to send earlier
					opportunityManager.updateOpportunity(oppSend, null, "999111999", 0, null, UpdSource.SOL30,false);

					//solution30dao.updateOpportunity(oppId, "999111999");
				}
		
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	
	}


	private void assignToPartner(String oppId,String zip){

				

				//List<String> partnerList=getNearByPartners( zip, null);
		           //List<String> partnerList= new ArrayList<String>();
		           //partnerList.add("100412");
		           //partnerList.add("100413");
					List<String> partnerList = getPartnersByCountry(zip,null);          
		
				if (partnerList!=null && partnerList.size()>0){
					HashMap<String,List<String>> oppPartnerList=new HashMap<String,List<String>>();
					oppPartnerList.put(oppId, partnerList);
					partnerDAO.assignOpportunityToPartner(oppPartnerList);
					Opportunity opp=new Opportunity();
					
					opp.setStatus(OppStatus.INITIAL.getValue());
					List<String> oppIds=new ArrayList<String>();
					oppIds.addAll(oppPartnerList.keySet());
					try {
						opportunityManager.updateOpportunity(opp, oppIds, null, 0, null, UpdSource.LISTUPD,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}/*else
				{
					
					
					logger.info("No Partner Found for this opportunity , Sending opportunity to Solution 30");
					
					 *  Commented on 28/06/2015 As per Sweden Use cases All opportunites should be assigned to Partner
					 *  If no partner found we need to discuss with business logic
					 
					//sendOppTOSol30(oppId, zip);
				
				}
			*/
			

		
	}

	@Override
	public List<Opportunity> getOppWhenNoPartnerAccepted(String country) {
		// TODO Auto-generated method stub
		
		List<String> oppsIdList=partnerDAO.getOppWhenNoPartnerAccepted(country);
		List<Opportunity> oppsList=new ArrayList<Opportunity>();
		if(oppsIdList!=null && oppsIdList.size()>0){
			
			for(String oppId:oppsIdList){
				Opportunity opp=opportunityDAO.getOportunity(oppId);
				oppsList.add(opp);
				//set opportunity to PARKED
				/*try {
					opportunityDAO.updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
			
		}
		
		
		
		
		return oppsList;
		
	}



	@Override
	public List<Opportunity> getOppWhenAllPartnerDeclined(String country) {
		

		List<String> oppsIdList=partnerDAO.getOppWhenAllPartnerDeclined(country);
		List<Opportunity> oppsList=new ArrayList<Opportunity>();
		if(oppsIdList!=null && oppsIdList.size()>0){
			
			for(String oppId:oppsIdList){
				Opportunity opp=opportunityDAO.getOportunity(oppId);
				oppsList.add(opp);
				//set opportunity to PARKED
				try {
					Opportunity oppSend=new Opportunity();
					oppSend.setId(oppId);
					oppSend.setStatus(OppStatus.PARKED.getValue());
					opportunityManager.updateOpportunity(oppSend, null, null, 0, null, UpdSource.ACCDECLINED,false);
					//opportunityDAO.updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
		return oppsList;
		
	}



	@Override
	public List<Partner> getPartner(String countryCode,String zipCode,String distance) {
		// TODO Auto-generated method stub
		return partnerDAO.getPartner(countryCode);
	}


	@Override
	public boolean sendOppTOSol30(String oppId) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void assignFolderPermissionsToPartners(String countryCode) 
	{
		// TODO Auto-generated method stub
        
		IMS ims=null;
		String adminUser="schneiderclims@gmail.com"; 
		String adminPassword="Mphasis123";		
		String adminImsToken="";
		ims = imsLoginManager.getAuthenticateUserToken(spimAdminUser, spimAdminPassword);	
		//ims = imsLoginManager.getAuthenticateUserToken(adminUser, adminPassword);	
		if(ims!=null)
		{
			adminImsToken = ims.getAccessToken();
		}
			
		List<Map<String,String>>  listPartnersOpportunities =  partnerDAO.assignOpportunityImagesToPartners(countryCode);
        for(Map<String,String> oppMap:listPartnersOpportunities)
        {
        	String oppId="";
        	String partnerId="";
       
        	oppId = oppMap.get("oppId").toString();
        	partnerId= oppMap.get("partnerId").toString();
        	
        	logger.info("partnerId::::"+partnerId);
        	logger.info("oppId:::::::"+oppId);
        	
        	sharePermissionsToPartners(partnerId,oppId,adminImsToken);
        }
		
	}

	@Override
	public void unAssignFolderPermissionsToPartners(String countryCode) 
	{
		// TODO Auto-generated method stub
		
		 partnerDAO.unAssignOpportunityImagesToPartners(countryCode);
		
	}
	
	public void sharePermissionsToPartners(String partnerId,String oppId,String adminImsToken)

	{
		//String imsToken="";

		List<String> partnerImsTokensList = new ArrayList<String>();
		List<String> partnerImsEmailIdList = new ArrayList<String>();    	
		try 
		{


			Map<String,String> partnerImsIdEmailId = new HashMap<String,String>(); 

			partnerImsIdEmailId = partnerDAO.getPartnerImsID(partnerId);

			String partnerImsId = partnerImsIdEmailId.get("partnerImsId");

			String partnerEmailId = partnerImsIdEmailId.get("userId");

			boolean sharePermissions=false;

			partnerImsTokensList.add("3805bb22-aa40-4223-b296-4f6acb380ef1");

			partnerImsEmailIdList.add("schneiderclims@gmail.com");

			partnerImsTokensList.add(partnerImsId);

			partnerImsEmailIdList.add("schneiderclims@gmail.com");

			String adminUser="schneiderclims@gmail.com"; 
			String adminPassword="Mphasis123";		

			String User="bhargavi.p@mphasis.com";
			String Password="Mphasis1";

			String User1 = "epartner.closedloop1@gmail.com";
			String Password1="Mphasis123";

			String User2 = "epartner.closedloop2@gmail.com";
			String Password2="Mphasis123";

			List<OpportunityImages> oppImageObjectIds=new ArrayList<OpportunityImages>(); 

 			IMS ims=null;
			
 			if(adminImsToken!=null && adminImsToken.length()>0)
 			{
 				adminImsToken= adminImsToken;
 			}
 			else
 			{	
	 			ims = imsLoginManager.getAuthenticateUserToken(spimAdminUser, spimAdminPassword);	
				if(ims!=null)
				{
					adminImsToken = ims.getAccessToken();
				}
 			}
			oppImageObjectIds =  partnerDAO.getPartnerImageDetails(oppId);

			String imageObjId="";

			if(oppImageObjectIds!=null)
				logger.debug("size of the oppImageObjectIds:::"+oppImageObjectIds.size());

			for(OpportunityImages imgIds:oppImageObjectIds)
			{
				imageObjId = imgIds.getArtifactObjectId();

				logger.info("sharePermissionsToPartners:imsToken::::"+adminImsToken);
				logger.info("sharePermissionsToPartners:imageObjId::::"+imageObjId);
				sharePermissions = spimHandler.ShareFolderPermissions(adminImsToken,partnerImsTokensList,partnerImsEmailIdList,imageObjId,sharePermissionsUrl);

			}


		}catch(Exception e)
		{

			logger.debug("Exception in sharePermissionsToPartners:"+e.getMessage());
			e.printStackTrace();
		}

	}

}

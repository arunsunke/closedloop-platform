
package com.se.cl.sol30.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInterventionStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getInterventionStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="expertFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expertLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expertId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="picturesUrl" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="executionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="executionMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInterventionStatusResponse", propOrder = {
    "status",
    "date",
    "expertFirstName",
    "expertLastName",
    "expertId",
    "picturesUrl",
    "executionCode",
    "executionMessage"
})
public class GetInterventionStatusResponse {

    protected String status;
    protected Object date;
    protected String expertFirstName;
    protected String expertLastName;
    protected String expertId;
    protected List<String> picturesUrl;
    protected String executionCode;
    protected String executionMessage;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setDate(Object value) {
        this.date = value;
    }

    /**
     * Gets the value of the expertFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpertFirstName() {
        return expertFirstName;
    }

    /**
     * Sets the value of the expertFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpertFirstName(String value) {
        this.expertFirstName = value;
    }

    /**
     * Gets the value of the expertLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpertLastName() {
        return expertLastName;
    }

    /**
     * Sets the value of the expertLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpertLastName(String value) {
        this.expertLastName = value;
    }

    /**
     * Gets the value of the expertId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpertId() {
        return expertId;
    }

    /**
     * Sets the value of the expertId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpertId(String value) {
        this.expertId = value;
    }

    /**
     * Gets the value of the picturesUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the picturesUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPicturesUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPicturesUrl() {
        if (picturesUrl == null) {
            picturesUrl = new ArrayList<String>();
        }
        return this.picturesUrl;
    }

    /**
     * Gets the value of the executionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionCode() {
        return executionCode;
    }

    /**
     * Sets the value of the executionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionCode(String value) {
        this.executionCode = value;
    }

    /**
     * Gets the value of the executionMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionMessage() {
        return executionMessage;
    }

    /**
     * Sets the value of the executionMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionMessage(String value) {
        this.executionMessage = value;
    }

}

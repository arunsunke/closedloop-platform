package com.se.cl.partner.login.manager.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.se.cl.partner.login.manager.KenticoLoginManager;

public class KenticoLoginManagerImpl implements KenticoLoginManager
{
	
	
	private static final Logger logger = LoggerFactory.getLogger(KenticoLoginManagerImpl.class);
	private  @Value("${kenticoTokenUrl}") String kenticoTokenUrl;
	private  @Value("${kenticoClientId}") String kenticoClientId;
	private  @Value("${kenticoRedirectionUrl}") String kenticoRedirectionUrl;
	private  @Value("${kenticoResponseType}") String kenticoResponseType;
	
	//private String kenticoTokenUrl="https://auth.kinvey.com/oauth/auth";
	//private String kenticoClientId="kid_VeYj_jr0hi";
	//private String kenticoRedirectionUrl="http://52.17.97.196:80/";
	//private String kenticoResponseType="code";
	
	public static void main(String[] args)
	
	  {
	     try {
	    	 String User="";
	    	 String Password="";
	    	 User="admin@closedloop.com";
	    	 Password = "admin123";
	    	 String tokenUrl ="";
	    	 tokenUrl = new KenticoLoginManagerImpl().getTokenUrl();
	    	 logger.info("tokenUrl:::::"+tokenUrl);
	 		System.out.println(new KenticoLoginManagerImpl().AuthenticateUser(User, Password,tokenUrl));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	
	public   String getTokenUrl()
	{
		//String tokenUrl="https://auth.kinvey.com/oauth/auth";
		logger.info("getToken:tokenUrl:"+kenticoTokenUrl);
		URL url;
	    boolean response=false;
	    String jsonStringData="";
	    String authenticatetokenUrl="";
		try
		{
							
			    url = new URL(kenticoTokenUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("POST");
			    HashMap<String,String> postJsonContentMap = new HashMap<String,String>();
			    postJsonContentMap.put("client_id", kenticoClientId);
			    postJsonContentMap.put("redirect_uri", kenticoRedirectionUrl);
			    postJsonContentMap.put("response_type", kenticoResponseType);
			    ObjectMapper objMapper = new ObjectMapper();
			    jsonStringData =  objMapper.writeValueAsString(postJsonContentMap);
		    	logger.info("getTokenUrl:::jsonStringData:::::::::::"+jsonStringData);
		    	con.setRequestProperty("Content-Type", "application/json");
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(jsonStringData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();

		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        
		        JsonNode rootNode = objMapper.readTree(strBuffer.toString());
				authenticatetokenUrl=rootNode.path("temp_login_uri").textValue();
				logger.info("getTokenUrl:::authenticatetokenUrl:::"+authenticatetokenUrl);
		        return authenticatetokenUrl;
				 } catch (MalformedURLException e) {
						e.printStackTrace();
					 } catch (Exception e) {
						e.printStackTrace();
					 }
				
				
				return authenticatetokenUrl;
	}

	public   boolean AuthenticateUser(String UserName,String Password,String authenticateUrl)
	{
		logger.info("AuthenticateUser:UserName:"+UserName);
		//logger.info("AuthenticateUser:Password:"+Password);
		logger.info("AuthenticateUser:authenticateUrl:"+authenticateUrl);
		URL url;
	    boolean response=false;
	    String jsonStringData="";
		try
		{
							
			    HashMap<String,String> postJsonContentMap = new HashMap<String,String>();
			    postJsonContentMap.put("client_id", kenticoClientId);
			    postJsonContentMap.put("redirect_uri", kenticoRedirectionUrl);
			    postJsonContentMap.put("response_type",kenticoResponseType);
			    postJsonContentMap.put("username" , UserName);
			    postJsonContentMap.put("password" , Password);
			    ObjectMapper objMapper = new ObjectMapper();
			    jsonStringData =  objMapper.writeValueAsString(postJsonContentMap);
		    	//logger.info("AuthenticateUser:jsonStringData:::::::::::"+jsonStringData);
			    url = new URL(authenticateUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    con.setDoOutput(true);
			    con.setRequestMethod("POST");
			    con.setRequestProperty("Content-Type", "application/json");
			    con.setDoInput(true);		   
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			    outputStreamWriter.write(jsonStringData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    con.setReadTimeout(1000);
			    StringBuffer strBuffer = new StringBuffer();
		        InputStream inputStream = con.getInputStream();

		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        
		        if(!strBuffer.toString().equalsIgnoreCase("Not Found") || !strBuffer.toString().equalsIgnoreCase("Unauthorized") )
		        {
		        	response=true;
		        }
		        logger.info("response is"+response);
		        return response;
				 } catch (MalformedURLException e) {
						e.printStackTrace();
					 } catch (Exception e) {
						e.printStackTrace();
					 }
				
				
				return response;
	}
	
}

package com.se.cl.partner.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.Opportunity;
import com.se.cl.model.Partner;
import com.se.cl.partner.manager.PartnerManager;
import com.se.cl.sol30.service.ActivityReturn;
import com.se.cl.sol30.service.impl.Solution30Service;
import com.se.cl.util.GetDistanceUsingGDMatrix;


@Component("France")
public class FRPartnerManagerImpl implements PartnerManager {

	private static final Logger logger = LoggerFactory.getLogger(FRPartnerManagerImpl.class);

	@Autowired
	PartnerDAO partnerDAO;
	@Autowired
	OpportunityDAO opportunityDAO;
	
	
	@Autowired 
	GetDistanceUsingGDMatrix gdMetrix;
	
	@Autowired
	Solution30Service solution30Service;
	

	
	@Autowired
	OpportunityManager opportunityManager;
	
	
	private @Value("${fr.partner.distance}") Long distance;
	private @Value("${fr.partner.count}") Long noOfPartners;
	private @Value("${fr.se.admin.email}") String seAdminEmail;
	//private @Value("${partner-count}") String propertyField;

	private @Value("${google.api.key}") String googleApiKey;
	private @Value("${google.push.notification.url}") String googlePushNotificationURL;
	private @Value("${ios.certificate.password}") String iosCertificatePassword;

	private @Value("${fr.route.to.admin}") String routeToAdmin;
	
	
	

	public List<String> getNearByPartners(String originZip,String byType) {
		List<String> filteredPartnerList=new ArrayList<String>();
		Hashtable<String,String> partnerAndZip=partnerDAO.getPartnerZips(CountryCode.FRANCE.getValue());
		//make list 
		if(partnerAndZip !=null && !partnerAndZip.isEmpty()){
			Set<Map.Entry<String, String>> entrySet = partnerAndZip.entrySet();
			List<String> partnerList=new ArrayList<String>();
			List<String> zipsList=new ArrayList<String>();
			for (Entry entry : entrySet) {
				logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
				partnerList.add((String)entry.getKey());
				zipsList.add((String)entry.getValue());
			}
			List<String> originsList=new ArrayList<String>();
			originsList.add(originZip);
			//GetDistanceUsingGDMatrix getdistance=new GetDistanceUsingGDMatrix();
			LinkedHashMap<String,Long> zipAndDistance=	gdMetrix.getZipAndDistance(originsList, zipsList);
	
	
			//zip-distance 
			//attach distance to partner
			HashMap<String,Long> partnerDistance=new HashMap<String,Long>();
			for (Entry entry : entrySet) {
				logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
				//get partner id after ,
				String zipCode = entry.getValue().toString().substring( entry.getValue().toString().indexOf(",")+1);
				
				partnerList.add((String)entry.getKey());
				zipsList.add(zipCode);
	
				if (zipAndDistance.get(entry.getValue().toString())!=null){
					partnerDistance.put(entry.getKey().toString(), zipAndDistance.get(entry.getValue().toString()));
				}
	
			}
	
			//filter partners bytype
	
			logger.debug("partnerDistance :"+partnerDistance);
	
			Set<Map.Entry<String, Long>> partnerEntrySet = partnerDistance.entrySet();
	
			
			for (Entry entry : partnerEntrySet) {
				logger.debug("partner ID: " + entry.getKey() + " distance: " + entry.getValue());
				if (Long.valueOf((entry.getValue().toString()))<=distance){
					filteredPartnerList.add(entry.getKey().toString());
				}
			}
		}
		return filteredPartnerList;
	}
	
	/**
	 * @smarni for now this is duplicate method as of above in future we need to refactor
	 * @param originZip
	 * @param byType
	 * @return
	 */
	private List<String> getNearByPartners(String originZip,String byType,Long inputdistance) {

		Hashtable<String,String> partnerAndZip=partnerDAO.getPartnerZips(CountryCode.FRANCE.getValue());
		//make list 
		Set<Map.Entry<String, String>> entrySet = partnerAndZip.entrySet();
		List<String> partnerList=new ArrayList<String>();
		List<String> zipsList=new ArrayList<String>();
		for (Entry<String, String> entry : entrySet) {
			System.out.println("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
			partnerList.add((String)entry.getKey());
			zipsList.add((String)entry.getValue());
		}
		List<String> originsList=new ArrayList<String>();
		originsList.add(originZip);
		//GetDistanceUsingGDMatrix getdistance=new GetDistanceUsingGDMatrix();
		LinkedHashMap<String,Long> zipAndDistance=	gdMetrix.getZipAndDistance(originsList, zipsList);


		//zip-distance 
		//attach distance to partner
		HashMap<String,Long> partnerDistance=new HashMap<String,Long>();
		for (Entry<String, String> entry : entrySet) {
			logger.debug("partner ID: " + entry.getKey() + " ZIP: " + entry.getValue());
			//get partner id after ,
			String zipCode = entry.getValue().toString().substring( entry.getValue().toString().indexOf(",")+1);
			
			partnerList.add((String)entry.getKey());
			zipsList.add(zipCode);

			if (zipAndDistance.get(entry.getValue().toString())!=null){
				partnerDistance.put(entry.getKey().toString(), zipAndDistance.get(entry.getValue().toString()));
			}

		}

		//filter partners bytype

		logger.debug("partnerDistance :"+partnerDistance);

		Set<Map.Entry<String, Long>> partnerEntrySet = partnerDistance.entrySet();

		List<String> filteredPartnerList=new ArrayList<String>();
		for (Entry<String, Long> entry : partnerEntrySet) {
			logger.debug("partner ID: " + entry.getKey() + " distance: " + entry.getValue());
			if (Long.valueOf((entry.getValue().toString()))<=inputdistance){
				filteredPartnerList.add(entry.getKey().toString());
			}
		}

		return filteredPartnerList;
	}


/**
 * send 50% to solution 30 and 50% to partner matching
 * @throws InterruptedException 
 * 
 */
	public void assignPartnerToOpportunity()  {
		
		int currentExecution=Integer.parseInt(partnerDAO.getCLconfig().get("CL_FR_PART_OR_SOL30")); //if value is 1 it is partner assignment if it is 0 send to solution 30
		
		
		HashMap<String,String> oppMap=opportunityDAO.getOpportunityIdAndZip(OppStatus.LOADED,"FR");

		if (oppMap!=null && oppMap.size()>0){
			
			
			if (oppMap.size()>1){

				Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
				
				int i=0;
				for (Entry entry : entrySet) {
					logger.debug("OPP ID: " + entry.getKey() + " ZIP: " + entry.getValue());
					
					if(currentExecution==0){
						if(i%2==0){
							assignToPartner(entry.getKey().toString(),entry.getValue().toString());
							
							
						}else{
							//sendOppTOSol30(entry.getKey().toString(),entry.getValue().toString());
							logger.debug("Sent solution 30");
							
						}
					}
					else{
						
						if(i%2==0){
							//sendOppTOSol30(entry.getKey().toString(),entry.getValue().toString());
							logger.debug("Sent solution 30");
							
								
						}else{
							
							assignToPartner(entry.getKey().toString(),entry.getValue().toString());
						}
						
					}
				
					i++;
				
				}
			
				if (currentExecution==0 && ((i-1)%2==0)){
					partnerDAO.updateCLconfig("1");
					//update current execution 1 for solution 30
					
				}else{
					
					//update current execution 0 for partnermatch
					partnerDAO.updateCLconfig("0");
				}
			
			}else{

				Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
				for (Entry entry : entrySet) {
					System.out.println("OPP ID: " + entry.getKey() + " ZIP: " + entry.getValue());
					if(currentExecution==0){
						assignToPartner(entry.getKey().toString(),entry.getValue().toString());
					}else{
							//sendOppTOSol30(entry.getKey().toString(),entry.getValue().toString());
							logger.debug("Sent solution 30");
						}
					}
				
				if (currentExecution==0 ){
					partnerDAO.updateCLconfig("1");
					//update current execution 0 for partnermatch
				}else{
					//update current execution 1 for solution 30
					partnerDAO.updateCLconfig("0");
				}
			}
		}
		
		
		

	}
	
	@Override
	public void sendOppTOSol30(String oppId, String zip)  {
	
	
		try {
				Opportunity opp = opportunityDAO.loadOpportunityById(Long.parseLong(oppId));
				
				Holder<ActivityReturn> activityReturn=solution30Service.sendOpportunityToSol30(opp);
				Opportunity oppSend=new Opportunity();
				if (activityReturn!=null)
				{		
					logger.info("FRPartnerManagerImpl:sendOppTOSol30:activityReturn.value.getIdSol30()"+activityReturn.value.getIdSol30());
		        }
				oppSend.setId(oppId);
				if (activityReturn!=null){
					//updated success status to opportunity table
					
				    logger.info("sendOppTOSol30:::Calling before opportunityManager.updateOpportunity...");
				
				    opportunityManager.updateOpportunity(oppSend, null, activityReturn.value.getIdSol30(), 0, null, UpdSource.SOL30,false);
					
				    logger.info("sendOppTOSol30:::Calling after opportunityManager.updateOpportunity...");

					//solution30dao.updateOpportunity(oppId,activityReturn.value.getIdSol30());
					
				}else{
					//update status so that it will retry for failed to send earlier
					opportunityManager.updateOpportunity(oppSend, null, "999111999", 0, null, UpdSource.SOL30,false);

					//solution30dao.updateOpportunity(oppId, "999111999");
				}
		
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
	}

	@Override
	public boolean sendOppTOSol30(String oppId)
	{
	
       boolean result=false;	
		try {
				Opportunity opp = opportunityDAO.loadOpportunityById(Long.parseLong(oppId));
				
				Holder<ActivityReturn> activityReturn=solution30Service.sendOpportunityToSol30(opp);
				Opportunity oppSend=new Opportunity();
				oppSend.setId(oppId);
				if (activityReturn!=null){
					//updated success status to opportunity table
					
					result=true;
					opportunityManager.updateOpportunity(oppSend, null, activityReturn.value.getIdSol30(), 0, null, UpdSource.SOL30,false);
					//solution30dao.updateOpportunity(oppId,activityReturn.value.getIdSol30());
					
				}else{
					//update status so that it will retry for failed to send earlier
					opportunityManager.updateOpportunity(oppSend, null, "999111999", 0, null, UpdSource.SOL30,false);
					result=false;
					//solution30dao.updateOpportunity(oppId, "999111999");
				}
		
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	
	}

	public void assignToPartner(String oppId,String zip) {


		//route to admin 
		/**
		 * if we have admin user in stakeholder and route.to.admin is true in partner properties send all opportunities to admin user 
		 * 
		 */
		List<Partner> partners=partnerDAO.getPartner("FR");
		Partner admin=adminfound(partners);
		if (routeToAdmin.equalsIgnoreCase("true") && admin!=null ){
			//need to refactor below code when time permits 
			HashMap<String,String> oppMap=opportunityDAO.getOpportunityIdAndZip(OppStatus.LOADED,"FR");
			if (oppMap!=null && oppMap.size()>0){
				Set<Map.Entry<String, String>> entrySet = oppMap.entrySet();
				List<String> partnerList=new ArrayList<String>();
				partnerList.add(admin.getId());
				for (Entry<String, String> entry : entrySet) 
				{
					logger.info("assignPartnerToOpportunity admin:" + admin.getId());
					HashMap<String,List<String>> oppPartnerList=new HashMap<String,List<String>>();
					oppPartnerList.put(entry.getKey().toString(), partnerList);
					partnerDAO.assignOpportunityToPartner(oppPartnerList);
					Opportunity opp=new Opportunity();
					opp.setStatus(OppStatus.INITIAL.getValue());
					List<String> oppIds=new ArrayList<String>();
					oppIds.addAll(oppPartnerList.keySet());
					try {
						opportunityManager.updateOpportunity(opp, oppIds, null, 0, null, UpdSource.LISTUPD,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}else
		{


			List<String> partnerList=getNearByPartners( zip, null);

			if (partnerList!=null && partnerList.size()>0){
				HashMap<String,List<String>> oppPartnerList=new HashMap<String,List<String>>();
				oppPartnerList.put(oppId, partnerList);
				partnerDAO.assignOpportunityToPartner(oppPartnerList);
				Opportunity opp=new Opportunity();

				opp.setStatus(OppStatus.INITIAL.getValue());
				List<String> oppIds=new ArrayList<String>();
				oppIds.addAll(oppPartnerList.keySet());
				try {
					opportunityManager.updateOpportunity(opp, oppIds, null, 0, null, UpdSource.LISTUPD,false);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}else{

				logger.info("No Partner Found for this opportunity , Sending opportunity to Solution 30");
				sendOppTOSol30(oppId, zip);

			}

		}

	}

			
		

	
	/*private void assignToPartner(String oppId,String zip) {

				

				List<String> partnerList=getNearByPartners( zip, null);

				if (partnerList!=null && partnerList.size()>0){
					HashMap<String,List<String>> oppPartnerList=new HashMap<String,List<String>>();
					oppPartnerList.put(oppId, partnerList);
					partnerDAO.assignOpportunityToPartner(oppPartnerList);
					Opportunity opp=new Opportunity();
					
					opp.setStatus(OppStatus.INITIAL.getValue());
					List<String> oppIds=new ArrayList<String>();
					oppIds.addAll(oppPartnerList.keySet());
					try {
						opportunityManager.updateOpportunity(opp, oppIds, null, 0, null, UpdSource.LISTUPD,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}else{
					
					
					logger.info("No Partner Found for this opportunity , Sending opportunity to Solution 30");
					sendOppTOSol30(oppId, zip);
				
				}
			
			

		
	}*/
	
	private Partner adminfound(List<Partner> partners) {
		// TODO Auto-generated method stub
		for(Partner partner:partners){
			if(partner.isAdmin()){
				return partner;
			}
		}
		
		return null;
	}
	
	
	

	@Override
	public List<Opportunity> getOppWhenNoPartnerAccepted(String country) {
		// TODO Auto-generated method stub
		
		List<String> oppsIdList=partnerDAO.getOppWhenNoPartnerAccepted(country);
		List<Opportunity> oppsList=new ArrayList<Opportunity>();
		if(oppsIdList!=null && oppsIdList.size()>0){
			
			for(String oppId:oppsIdList){
				Opportunity opp=opportunityDAO.getOportunity(oppId);
				oppsList.add(opp);
				//set opportunity to PARKED
				/*try {
					opportunityDAO.updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
			
		}
		
		
		
		
		return oppsList;
		
	}



	@Override
	public List<Opportunity> getOppWhenAllPartnerDeclined(String country) {
		

		List<String> oppsIdList=partnerDAO.getOppWhenAllPartnerDeclined(country);
		List<Opportunity> oppsList=new ArrayList<Opportunity>();
		if(oppsIdList!=null && oppsIdList.size()>0){
			
			for(String oppId:oppsIdList){
				Opportunity opp=opportunityDAO.getOportunity(oppId);
				oppsList.add(opp);
				//set opportunity to PARKED
				try {
					Opportunity oppSend=new Opportunity();
					oppSend.setId(oppId);
					oppSend.setStatus(OppStatus.PARKED.getValue());
					opportunityManager.updateOpportunity(oppSend, null, null, 0, null, UpdSource.ACCDECLINED,false);
					//opportunityDAO.updateOppStatus(oppId, OppStatus.PARKED);
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
		return oppsList;
		
	}



	@Override
	public List<Partner> getPartner(String countryCode,String zipCode,String distance) throws AppException {
		// TODO Auto-generated method stub
		
		//if zip code and distance is given then it will return filtered  partners 
		//else all partners
		
		List<Partner> returnPartnerList=new ArrayList<Partner>();
		if (zipCode!=null )
		{
			List<Partner> partnerList=partnerDAO.getPartner(countryCode);
			List<String> filteredPartnerList=new ArrayList<String>();
			if (distance!=null){
				filteredPartnerList=getNearByPartners(countryCode+","+zipCode,null,Long.parseLong(distance));
			}else{
				filteredPartnerList=getNearByPartners(countryCode+","+zipCode,null);
			}
			
			for(Partner partner:partnerList){
				if (filteredPartnerList.contains(partner.getId())){
					returnPartnerList.add(partner);
				}
			}
			return returnPartnerList;
		}
		
		return partnerDAO.getPartner(countryCode);
	}

	@Override
	public void assignFolderPermissionsToPartners(String countryCode) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void unAssignFolderPermissionsToPartners(String countryCode) {
		// TODO Auto-generated method stub
		
	}




}


package com.se.cl.sol30.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.se.cl.sol30.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetInterventionStatusResponse_QNAME = new QName("http://ws.schneider.pc30.fr/", "getInterventionStatusResponse");
    private final static QName _CancelMeetingResponse_QNAME = new QName("http://ws.schneider.pc30.fr/", "cancelMeetingResponse");
    private final static QName _ConfirmMeetingResponse_QNAME = new QName("http://ws.schneider.pc30.fr/", "confirmMeetingResponse");
    private final static QName _ConfirmMeeting_QNAME = new QName("http://ws.schneider.pc30.fr/", "confirmMeeting");
    private final static QName _CancelMeeting_QNAME = new QName("http://ws.schneider.pc30.fr/", "cancelMeeting");
    private final static QName _GetSchneiderOpportunityResponse_QNAME = new QName("http://ws.schneider.pc30.fr/", "getSchneiderOpportunityResponse");
    private final static QName _GetInterventionStatus_QNAME = new QName("http://ws.schneider.pc30.fr/", "getInterventionStatus");
    private final static QName _GetSchneiderOpportunity_QNAME = new QName("http://ws.schneider.pc30.fr/", "getSchneiderOpportunity");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.se.cl.sol30.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivityData }
     * 
     */
    public ActivityData createActivityData() {
        return new ActivityData();
    }

    /**
     * Create an instance of {@link GetInterventionStatusResponse }
     * 
     */
    public GetInterventionStatusResponse createGetInterventionStatusResponse() {
        return new GetInterventionStatusResponse();
    }

    /**
     * Create an instance of {@link ConfirmMeetingResponse }
     * 
     */
    public ConfirmMeetingResponse createConfirmMeetingResponse() {
        return new ConfirmMeetingResponse();
    }

    /**
     * Create an instance of {@link CancelMeetingResponse }
     * 
     */
    public CancelMeetingResponse createCancelMeetingResponse() {
        return new CancelMeetingResponse();
    }

    /**
     * Create an instance of {@link ConfirmMeeting }
     * 
     */
    public ConfirmMeeting createConfirmMeeting() {
        return new ConfirmMeeting();
    }

    /**
     * Create an instance of {@link CancelMeeting }
     * 
     */
    public CancelMeeting createCancelMeeting() {
        return new CancelMeeting();
    }

    /**
     * Create an instance of {@link GetSchneiderOpportunity }
     * 
     */
    public GetSchneiderOpportunity createGetSchneiderOpportunity() {
        return new GetSchneiderOpportunity();
    }

    /**
     * Create an instance of {@link GetInterventionStatus }
     * 
     */
    public GetInterventionStatus createGetInterventionStatus() {
        return new GetInterventionStatus();
    }

    /**
     * Create an instance of {@link GetSchneiderOpportunityResponse }
     * 
     */
    public GetSchneiderOpportunityResponse createGetSchneiderOpportunityResponse() {
        return new GetSchneiderOpportunityResponse();
    }

    /**
     * Create an instance of {@link ActivityDesciption }
     * 
     */
    public ActivityDesciption createActivityDesciption() {
        return new ActivityDesciption();
    }

    /**
     * Create an instance of {@link Equipment }
     * 
     */
    public Equipment createEquipment() {
        return new Equipment();
    }

    /**
     * Create an instance of {@link ClientData }
     * 
     */
    public ClientData createClientData() {
        return new ClientData();
    }

    /**
     * Create an instance of {@link ActivityReturn }
     * 
     */
    public ActivityReturn createActivityReturn() {
        return new ActivityReturn();
    }

    /**
     * Create an instance of {@link ActivityData.ActivityDescriptions }
     * 
     */
    public ActivityData.ActivityDescriptions createActivityDataActivityDescriptions() {
        return new ActivityData.ActivityDescriptions();
    }

    /**
     * Create an instance of {@link ActivityData.Equipments }
     * 
     */
    public ActivityData.Equipments createActivityDataEquipments() {
        return new ActivityData.Equipments();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInterventionStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "getInterventionStatusResponse")
    public JAXBElement<GetInterventionStatusResponse> createGetInterventionStatusResponse(GetInterventionStatusResponse value) {
        return new JAXBElement<GetInterventionStatusResponse>(_GetInterventionStatusResponse_QNAME, GetInterventionStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelMeetingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "cancelMeetingResponse")
    public JAXBElement<CancelMeetingResponse> createCancelMeetingResponse(CancelMeetingResponse value) {
        return new JAXBElement<CancelMeetingResponse>(_CancelMeetingResponse_QNAME, CancelMeetingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmMeetingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "confirmMeetingResponse")
    public JAXBElement<ConfirmMeetingResponse> createConfirmMeetingResponse(ConfirmMeetingResponse value) {
        return new JAXBElement<ConfirmMeetingResponse>(_ConfirmMeetingResponse_QNAME, ConfirmMeetingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmMeeting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "confirmMeeting")
    public JAXBElement<ConfirmMeeting> createConfirmMeeting(ConfirmMeeting value) {
        return new JAXBElement<ConfirmMeeting>(_ConfirmMeeting_QNAME, ConfirmMeeting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelMeeting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "cancelMeeting")
    public JAXBElement<CancelMeeting> createCancelMeeting(CancelMeeting value) {
        return new JAXBElement<CancelMeeting>(_CancelMeeting_QNAME, CancelMeeting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSchneiderOpportunityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "getSchneiderOpportunityResponse")
    public JAXBElement<GetSchneiderOpportunityResponse> createGetSchneiderOpportunityResponse(GetSchneiderOpportunityResponse value) {
        return new JAXBElement<GetSchneiderOpportunityResponse>(_GetSchneiderOpportunityResponse_QNAME, GetSchneiderOpportunityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInterventionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "getInterventionStatus")
    public JAXBElement<GetInterventionStatus> createGetInterventionStatus(GetInterventionStatus value) {
        return new JAXBElement<GetInterventionStatus>(_GetInterventionStatus_QNAME, GetInterventionStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSchneiderOpportunity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.schneider.pc30.fr/", name = "getSchneiderOpportunity")
    public JAXBElement<GetSchneiderOpportunity> createGetSchneiderOpportunity(GetSchneiderOpportunity value) {
        return new JAXBElement<GetSchneiderOpportunity>(_GetSchneiderOpportunity_QNAME, GetSchneiderOpportunity.class, null, value);
    }

}

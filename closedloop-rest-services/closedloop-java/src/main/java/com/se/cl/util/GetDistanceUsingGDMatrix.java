package com.se.cl.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixRow;

public class GetDistanceUsingGDMatrix {
	
	private static final Logger logger = LoggerFactory.getLogger(GetDistanceUsingGDMatrix.class);
	
	@Value("${partner.match.google.key}")
	private String googleKey;
	
	private  DistanceMatrix getDistance(List<String> originsList,List<String> destinationsList ){
		
		//googleKey= "AIzaSyDk8ExEg2sa6uFsfuJcjrH3wNMewSxs4sQ";
		//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyDk8ExEg2sa6uFsfuJcjrH3wNMewSxs4sQ");//sudhir.marni
		//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyD6ktR0G4g2lkJox5VxV_SDPX7o5Qc0RwU");//closedlooptest@gmail.com
		GeoApiContext context = new GeoApiContext().setApiKey(googleKey);//closedlooptest@gmail.com
		//GeoApiContext context = new GeoApiContext().setApiKey("9S5bjrIw7wYTTHIIjOZLNH6CHKo=");
		//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyDK9gdYV5hxkuT2-_4j0i7CjiZw72LGVag");//smarni2014
		DistanceMatrix results=null;
		
		
		String[] origins = originsList.toArray(new String[originsList.size()]);
		String[] destinations = destinationsList.toArray(new String[destinationsList.size()]);
		
		try {
			results=DistanceMatrixApi.getDistanceMatrix(context, origins, destinations).await();
			logger.debug("result :" +results);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return results;
	}
	
	public  LinkedHashMap<String,Long> getZipAndDistance(List<String> originsList,List<String> destinationsList ){
		
		List<String> destationtemp=new ArrayList<String>();
		LinkedHashMap<String,Long> zipAndDistanceMap=new LinkedHashMap<String,Long>();
		LinkedHashMap<String,Long> zipAndDistanceFinalMap=new LinkedHashMap<String,Long>();
		int i=0;
		if (destinationsList.size()>25){
			int k=0;
			
			
		for (String destination :destinationsList){
			destationtemp.add(destination);
			if(i==24){
				DistanceMatrix response =	getDistance(originsList,destationtemp);
				zipAndDistanceMap=calculateDistance(response,destationtemp);
				zipAndDistanceFinalMap.putAll(zipAndDistanceMap);
				i=0;
				destationtemp=new ArrayList<String>();
			}
			
			i++;
			k++;
			//when it is not multiples of 25
			if (i!=24 && k==destinationsList.size()){
				DistanceMatrix response =	getDistance(originsList,destationtemp);
				zipAndDistanceMap=calculateDistance(response,destationtemp);
				zipAndDistanceFinalMap.putAll(zipAndDistanceMap);
				i=0;
				destationtemp=new ArrayList<String>();
			}
			
			
		}
		}else{
			DistanceMatrix response =	getDistance(originsList,destinationsList);
			zipAndDistanceMap=calculateDistance(response,destinationsList);
			zipAndDistanceFinalMap.putAll(zipAndDistanceMap);
			i=0;
			destationtemp=new ArrayList<String>();
		}
		
		
		
		return zipAndDistanceFinalMap;
	}
	
	
	
	
	private static LinkedHashMap<String,Long> calculateDistance(DistanceMatrix response,List<String> destinationsList) {
	
		LinkedHashMap<String, Long> zipAndDistanceMap=new LinkedHashMap<String, Long>();
		
		logger.debug("response"+ response);
		DistanceMatrixRow[] dmat=response.rows;
		for (int i=0;i<dmat.length;i++){
			DistanceMatrixElement[] dmatEle=dmat[i].elements;
			 for(int j=0;j<dmatEle.length;j++){
				 DistanceMatrixElement datelement=dmatEle[j];
				 if (datelement.status.equalsIgnoreCase("OK")){
				 zipAndDistanceMap.put(destinationsList.get(j), Long.valueOf(datelement.distance.inMeters));
				 }
				 
			 }
			
		}
		
		return zipAndDistanceMap;
		
	}

	public static void main(String a[]){
		GetDistanceUsingGDMatrix gm=new GetDistanceUsingGDMatrix();
		
		List<String> originsList =new ArrayList<String>();
				List<String> destinationsList =new ArrayList<String>();
				originsList.add("AU,3163");
				destinationsList.add("AU,3163");
				destinationsList.add("AU,3057");
				destinationsList.add("AU,3585");
				destinationsList.add("AU,3084");
				destinationsList.add("AU,3564");
		gm.getZipAndDistance(originsList,destinationsList);
		
	}

}

package com.se.cl.manager;

import java.util.List;
import java.util.Map;

import com.se.cl.exception.AppException;

public interface SpimManager
{
	
	public void uploadImages(String imsToken,String refreshToken,String userType,String oppTitle,List<String> imgData,List<String> imageTitle,String oppId,String userId) throws AppException;
	
	public void uploadPartnerImages(String imsToken,String refreshToken,String userType,String oppTitle,String imgData,String imageTitle,String oppId,String userId) throws AppException;
	
	public List<Map<String,String>> getImageIdList(String imsToken,String userType,String oppTitle,String oppId,String userId) throws AppException;

	public Map<String,Object> getImageContent(String imsToken,String refreshToken,String userType,String opportunityTitle,String imageObjectId) throws AppException;
	
}

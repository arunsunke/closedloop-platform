package com.se.cl.manager.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.commons.httpclient.util.DateUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class Test {
	
	public static String getTimeForTZ() {

	      // create a calendar
	      Calendar cal = Calendar.getInstance();

	      // print current time zone
	      String name=cal.getTimeZone().getDisplayName();
	      System.out.println("Current Time Zone:" + name );
	      TimeZone tz = TimeZone.getTimeZone("UTC+1");

	      // set the time zone with the given time zone value 
	      // and print it
	      cal.setTimeZone(tz);
	      System.out.println(cal.getTime());
	      
	  	 DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
	  	String formatted = DATE_FORMAT.format(cal.getTime());
	  	System.out.println(formatted);
	  	DateTime dt = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("IST"))).withDate(cal.YEAR, cal.MONTH, cal.DAY_OF_MONTH);
	  	 System.out.println("TZ date :"+dt.toString()); 
	      return formatted;
	   }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(Test.getTimeForTZ());
		
		
	   /* DateTime dt = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("IST"))).withDate(2013, 12, 24).withTime(2, 0, 0, 00);  
        System.out.println(dt.toString());  
          
        DateTime systemDate = dt.withZone(DateTimeZone.forTimeZone(Calendar.getInstance().getTimeZone()));  
        System.out.println(systemDate.toString());*/
	
	/*	Date currDate = new Date();
		Date dateIndia = getDateInTimeZone(currDate,"CET");
		System.out.println("India date      :"+dateIndia);
		System.out.println("India time unix :"+dateIndia.getTime());
		 
		Date dateUtc = getDateInTimeZone(currDate,"UTC");
		System.out.println("Utc date :"+dateUtc);
		System.out.println("Utc time unix :"+dateUtc.getTime());*/
		
		
		String tt=getTimeForTZ("CET","23-03-2015 10:10:10 AM");
		System.out.println(tt);
		
	}
	
	public static Date getDateInTimeZone(Date currentDate, String timeZoneId) {
	       
	       TimeZone tz = TimeZone.getTimeZone(timeZoneId);
	       Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
	       mbCal.setTimeInMillis(currentDate.getTime());
	       Calendar cal = Calendar.getInstance();
	       cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
	       cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
	       cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
	       cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
	       cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
	       cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
	       cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));
	       return cal.getTime();
	       
	       
	       
	       
	   }
	
	public static String getTimeForTZ(String timeZone,String date) {
		//parse to simple date format	
		try{
		DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
			  	
		Date datefrom = DATE_FORMAT.parse(date);
		Date dateCet = getDateInTimeZone(datefrom,(timeZone!=null&&timeZone.length()>0?timeZone:"CET"));
		
	  	String formatted = DATE_FORMAT.format(dateCet);
	  //	System.out.println(formatted);
	  	 
	  	 
	      return formatted;
		}catch(Exception e){
			e.printStackTrace();
		
		}
		return null;
	   }
	
	
	

}

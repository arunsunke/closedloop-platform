package com.se.cl.manager;

import java.util.List;
import java.util.Map;

import com.se.cl.constants.UpdSource;
import com.se.cl.exception.AppException;
import com.se.cl.model.BOM;
import com.se.cl.model.Distributor;
import com.se.cl.model.Opportunity;
import com.se.cl.model.PartnerDevice;
import com.se.cl.model.User;

public interface OpportunityManager {

	public void updateOpportunityStatus(String partnerId,String oppId,String statusId,String status,boolean isAdminAssigned,String countryCode) throws AppException;
	public void updateOpportunityHistory(String partnerId,String oppId,String statusId,String status,String reason,String startTime,String endTime,String type,String source) throws AppException;
	public int addDevicetoPartner(PartnerDevice device) throws AppException;	
	public Map<String, Object> addOpportunity(Opportunity opportunity) throws AppException;
	public Opportunity loadOpportunityById(long opportunityId) throws AppException;
	public User validateUser(User user);
	public int addDistributorToPartner(Distributor distributor);
	public List<Distributor> getDistList(String partnerId);
	
	public int deleteDistributor(String distId,String shId);
	
	public int updateDistributor(Distributor distributor, String shid,	String distId);
	
	public Opportunity updateOpportunity(Opportunity opportunity ,List<String> oppIds,String noteType,int slaStep,Map<String,String> bfoResponse ,UpdSource source,boolean isAdminAssigned)  throws AppException;
	
	public boolean sendOpptoSol30(String oppId, String zip, String countryCode) ;
	
	public String getOpportunityStatus(String oppId) throws AppException;
	
	public List<BOM> getOpportunitySBomArray(String oppId) throws AppException;
	
	public void updateOpportunityHistoryCancel(String partnerId,String opportunityId, String statusId, String status,String reason, String startTime, String endTime, String type,
			String string) throws AppException;
	
	
	public String getOpportunityMeetingDate(String oppId) throws AppException;
	
	public String getConsumerName(String oppId);
	
	public void updateOpportunityTitle(String oppId);

	
}

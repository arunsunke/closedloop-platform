package com.se.cl.partner.manager;

import java.util.List;

import com.se.cl.exception.AppException;
import com.se.cl.model.Opportunity;
import com.se.cl.model.Partner;

public interface PartnerManager {
	
	public List<String> getNearByPartners(String originZip,String byType);
	public void assignPartnerToOpportunity();
	
	public List<Opportunity> getOppWhenNoPartnerAccepted(String country);
	public List<Opportunity> getOppWhenAllPartnerDeclined(String country);
	
	public List<Partner> getPartner(String countryCode,String zipCode,String distance) throws AppException;
	
	public void sendOppTOSol30(String oppId, String zip);
	
	public boolean sendOppTOSol30(String oppId);
	
	public void assignFolderPermissionsToPartners(String countryCode);
	
	public void unAssignFolderPermissionsToPartners(String countryCode);
		

}

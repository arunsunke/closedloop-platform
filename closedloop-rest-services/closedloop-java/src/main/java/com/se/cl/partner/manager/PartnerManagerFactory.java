package com.se.cl.partner.manager;

public interface PartnerManagerFactory {
	
	PartnerManager getPartnerManger(String managerName);

}

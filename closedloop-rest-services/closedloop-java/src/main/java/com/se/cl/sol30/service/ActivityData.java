
package com.se.cl.sol30.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="refInter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="activityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="activityDescriptions" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="activityDescription" type="{http://ws.schneider.pc30.fr/}ActivityDesciption" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="equipments" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="equipment" type="{http://ws.schneider.pc30.fr/}Equipment" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityData", propOrder = {
    "refInter",
    "activityType",
    "activityDescriptions",
    "equipments"
})
public class ActivityData {

    @XmlElement(required = true)
    protected String refInter;
    @XmlElement(required = true)
    protected String activityType;
    protected ActivityData.ActivityDescriptions activityDescriptions;
    protected ActivityData.Equipments equipments;

    /**
     * Gets the value of the refInter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefInter() {
        return refInter;
    }

    /**
     * Sets the value of the refInter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefInter(String value) {
        this.refInter = value;
    }

    /**
     * Gets the value of the activityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * Sets the value of the activityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityType(String value) {
        this.activityType = value;
    }

    /**
     * Gets the value of the activityDescriptions property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityData.ActivityDescriptions }
     *     
     */
    public ActivityData.ActivityDescriptions getActivityDescriptions() {
        return activityDescriptions;
    }

    /**
     * Sets the value of the activityDescriptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityData.ActivityDescriptions }
     *     
     */
    public void setActivityDescriptions(ActivityData.ActivityDescriptions value) {
        this.activityDescriptions = value;
    }

    /**
     * Gets the value of the equipments property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityData.Equipments }
     *     
     */
    public ActivityData.Equipments getEquipments() {
        return equipments;
    }

    /**
     * Sets the value of the equipments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityData.Equipments }
     *     
     */
    public void setEquipments(ActivityData.Equipments value) {
        this.equipments = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="activityDescription" type="{http://ws.schneider.pc30.fr/}ActivityDesciption" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activityDescription"
    })
    public static class ActivityDescriptions {

        protected List<ActivityDesciption> activityDescription;

        /**
         * Gets the value of the activityDescription property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the activityDescription property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getActivityDescription().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ActivityDesciption }
         * 
         * 
         */
        public List<ActivityDesciption> getActivityDescription() {
            if (activityDescription == null) {
                activityDescription = new ArrayList<ActivityDesciption>();
            }
            return this.activityDescription;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="equipment" type="{http://ws.schneider.pc30.fr/}Equipment" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "equipment"
    })
    public static class Equipments {

        @XmlElement(required = true)
        protected List<Equipment> equipment;

        /**
         * Gets the value of the equipment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the equipment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEquipment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Equipment }
         * 
         * 
         */
        public List<Equipment> getEquipment() {
            if (equipment == null) {
                equipment = new ArrayList<Equipment>();
            }
            return this.equipment;
        }

    }

}

package com.se.cl.partner.manager.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.notnoop.exceptions.NetworkIOException;
import com.se.cl.constants.CountryCode;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.ConsumerDAO;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerPartner;
import com.se.cl.model.Opportunity;
import com.se.cl.model.PartnerDevice;
import com.se.cl.partner.manager.NotificationManager;
import com.se.cl.partner.manager.PartnerManagerFactory;
import com.se.cl.util.PropertyLoader;
import com.se.clm.bfo.client.HttpsClient;
import com.se.clm.bfo.manager.BFOManagerFactory;
import com.se.clm.mail.SWEmailTemplates;
import com.se.clm.mail.IEmailService;
import com.se.clm.mail.SWEmailTemplates;
@Component("SwedenNotification")
public class SENotificationManagerImpl implements NotificationManager{

	@Autowired
	PartnerDAO partnerDAO;
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	PartnerManagerFactory partnerFactory;
	
	@Autowired
	IEmailService iEmailService;
	
	@Autowired
	BFOManagerFactory bfoManagerFactory;

	@Autowired
	HttpsClient httpsClient;

	
	@Autowired
	ConsumerDAO consumerDAO;

	@Autowired
	OpportunityManager opportunityManager;


	private @Value("${sw.se.admin.email}") String seAdminEmail;
	private @Value("${fr.accept.sla.step1}") Long slaStep1;
	private @Value("${fr.accept.sla.step2}") Long slaStep2;
	private @Value("${fr.accept.sla.step3}") Long slaStep3;
	private @Value("${fr.phcountrycode}") String phCountryCode;
	
	//private @Value("${partner-count}") String propertyField;

	
	private @Value("${google.api.key}") String googleApiKey;
	private @Value("${google.push.notification.url}") String googlePushNotificationURL;
	private @Value("${ios.certificate.password}") String iosCertificatePassword;

	private @Value("${solution30.proxy}") String solution30Proxy;

	
	private static final Logger LOGGER = LoggerFactory.getLogger(SENotificationManagerImpl.class);
    private static final String bfoManagerName="FRB";
    
	private @Value("${sweden.admin.email}") String swedenAdminEmail;


	@Override
	public void notifyPartnerForSALRemider(int slaStep,String country) throws AppException {
		
	
		try{
			HashMap<String,List<String>>  map=partnerDAO.getPartnerListForSLAReminder(slaStep,country);
			
			notifyPartner(map,"SLA",slaStep);
			sendEmailsToPartners(map,"SLA",slaStep);
			//
		}catch(Exception e){
			e.printStackTrace();
		}
		//send emails
	
		


	}

 private void sendEmailsToPartners(HashMap<String,List<String>> map,String noteType,int slaStep){
	 
	 
	 
		
		if (map.get("OPP-PARTNER")!=null && map.get("OPP-PARTNER").size()>0){
			
			List<String> partnerIds=map.get("OPP-PARTNER");
			List<String> template=new ArrayList<String>();
			
			if (slaStep==0){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);

					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					
					template = SWEmailTemplates.sendEmailtoPartnerForNewRequest(
							opp.getPartner().getEmail(), opp.getPartner()
									.getName(), opp.getTitle(), opp
									.getCustomerDetails().getFirstName(), opp
									.getAddress().getZip(), opp.getBudget(),opp.getId(),boms,opp.getAddress().getSt_name(),opp.getAddress().getCity());
					
					
					try {
						Thread.sleep(1000);
					
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						LOGGER.debug("sendEmailsToPartners:opp.getPartner().getEmail():"+opp.getPartner().getEmail());
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.SWEDEN.getValue(),4);

						iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),fsr,fsrcsv,CountryCode.SWEDEN.getValue(),4);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			
			if (slaStep==1){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = SWEmailTemplates
							.sendEmailtoPartnerForRemainderOne(opp.getPartner()
									.getEmail(), opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
					

					try {
						Thread.sleep(1000);
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.SWEDEN.getValue(),4);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			if (slaStep==2){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = SWEmailTemplates
							.sendEmailtoPartnerFOrRemainderTwo(opp.getPartner()
									.getEmail(), opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
					

					try {
						Thread.sleep(1000);
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.SWEDEN.getValue(),4);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
				}
				
			}
			
			Set<String> sol30OppIdList=null;
			if (slaStep==4){
				//get opportunityByPartner Id
				for(String ids:partnerIds){
					String[] parts = ids.split("\\|");
					Opportunity opp=opportunityDAO.getOportunity(parts[1], parts[0]);
					//get opportunity boms
					List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(parts[0]));
					template = SWEmailTemplates
							.sendEmailtoPartnerFOrRemainderThree(opp.getPartner()
									.getEmail(), opp.getPartner().getName(),
									opp.getTitle(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
					

					try {
						Thread.sleep(1000);
						FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
						FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
						//iEmailService.sendMail(opp.getPartner().getEmail(), template.get(1), template.get(0),fsr,fsrcsv,CountryCode.SWEDEN.getValue(),4);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//update opportunity Status
				
					//send to solution 30
					//partnerFactory.getPartnerManger("SW").sendOppTOSol30( parts[0], " ");
					
					//remove assignment for other partners , who are already assigned to this opportunity
					//opportunityDAO.unassignOpportunityFromPartner(parts[0]);
					
					sol30OppIdList = new HashSet<String>();
					sol30OppIdList.add(parts[0]);
				}
				
			    //At present I have commented to send data to sol30 because for sweden there is no sol30
				/*if(sol30OppIdList!=null && sol30OppIdList.size()>0)
				{	
				    for(String oppId:sol30OppIdList) 
				    {
				    	LOGGER.info("After SLA-3 opportunity is sending to sol30.......oppId:::"+oppId);
				    	LOGGER.info("sol30OppIdList:oppId:::"+oppId);
				    	partnerFactory.getPartnerManger("FR").sendOppTOSol30(oppId," ");
				    	LOGGER.info("After SLA-3 opportunity is sending to sol30..And UnAssign the Opp from Partner");
				    	opportunityDAO.unassignOpportunityFromPartner(oppId);
	
				    } 
				}				*/
				
				
			
			}
			
			//update opp with sla trigger status
			
			
			if(map.get("OPPORTUNITY")!=null && map.get("OPPORTUNITY").size()>0){
				//for frace if sla step 3 send opportunity to solution 30
				
				if (slaStep!=3){
					try {
						opportunityManager.updateOpportunity(null,map.get("OPPORTUNITY"),noteType,slaStep, null, UpdSource.SLA,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				//opportunityDAO.updateOppNotiStatus(map.get("OPPORTUNITY") ,noteType,slaStep);
				}
			}


		}
		
		
 }


	public void notifyPartnerForNewOpportunity(String country) throws AppException {

		try{
			HashMap<String,List<String>>  map=partnerDAO.getPartnerListForNewOpps(country);
			slaStep1=Long.valueOf(PropertyLoader.getSparQlProperty("fr.accept.sla.step1"));
			
			notifyPartner(map,"NEWOPP",0);
			sendEmailsToPartners(map,"NEWOPP",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		//return null;
	}

	public void notifyPartnerForCancel(String country,String nType) throws AppException {

	Map<String,Object> returnMap=opportunityDAO.getPartnerDeviceForNotifications(0,nType,country);
	List<Integer> oppIdList=new ArrayList<Integer>();
	
	for(Map.Entry<String, Object> entry:returnMap.entrySet())
	{
		if(entry.getKey().equalsIgnoreCase("OPPLIST"))
		{			
			oppIdList=(List<Integer>) entry.getValue();
			
		}	
		
	}	
	if (returnMap!=null && !returnMap.isEmpty())
	{
		
		
		try {
			if(nType.equalsIgnoreCase("CANCEL")) 
			{
				
				notifyPartnerForOpportunityCancel((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"),(Map<String,String>)returnMap.get("PARTNEROPP"), "CANCEL",(Map<String,String>)returnMap.get("PARTNEROPPID"),(Map<String,String>)returnMap.get("PARTNERDATE"));
				consumerDAO.updateOppNOTI(oppIdList, "CANCEL");
			}
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	}	
		
	//}

	private void notifyPartner(HashMap<String,List<String>> map,String noteType,int slaType) throws AppException{

		if (map.get("PARTNER")!=null && map.get("PARTNER").size()>0)
		{

			List<PartnerDevice> deviceList=getDeviceIdsForPartnes( map.get("PARTNER"));

			if (deviceList!=null && deviceList.size()>0){
				try{	
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
					for(PartnerDevice partnerDevice:deviceList){
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS")){
							iosList.add(partnerDevice.getDeviceId());
						}else{
							androidList.add(partnerDevice.getDeviceId());
						}
					}

					//below hardcoded for now .. needs to be computed from prop file
					if(slaType==0){
						if (iosList!=null && iosList.size()>0){
							//pushNotificationToIOS("Une nouvelle demande de vous pour", "Alerte E-Partner"+"\n"+"Bonne nouvelle! Vous avez reçu une nouvelle demande de devis" , iosList);
							pushNotificationToIOS("Ny order!","Du har en ny order som avvakter en installatör" , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							//pushNotificationToAndroid("Une nouvelle demande de vous pour", "Alerte E-Partner"+"\n"+"Bonne nouvelle! Vous avez reçu une nouvelle demande de devis" , androidList);
							pushNotificationToAndroid("Ny order!", "\n"+"Du har en ny order som avvakter en installatör" , androidList);
						}
					}else if(slaType==4){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Ny order (1:a påminnelse)!", "Du har en ny order som avvakter en installatör." , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Ny order (1:a påminnelse)!", "Du har en ny order som avvakter en installatör." , androidList);
						}
						
					}else if(slaType==1){
						
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Ny order (2:a påminnelse)!", "Du har en ny order som avvakter en installatör." , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Ny order (2:a påminnelse)!", "Du har en ny order som avvakter en installatör." , androidList);
						}
					}else if(slaType==2){
						if (iosList!=null && iosList.size()>0){
							pushNotificationToIOS("Ny order (3:a påminnelse)!", "Du har en ny order som avvakter en installatör." , iosList);
						}
	
						if (androidList!=null && androidList.size()>0){
							pushNotificationToAndroid("Ny order (3:a påminnelse)!", "Du har en ny order som avvakter en installatör." , androidList);
						}
					}
	
				}catch(Exception e){
					e.printStackTrace();
				}
			}


			
		}


	}
	
	
	private void notifyPartner(List<PartnerDevice> deviceList,Map<String,String> partnerCust,String noteType) throws AppException{

		

		 if (deviceList!=null && deviceList.size()>0){
				try{	
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
					for(PartnerDevice partnerDevice:deviceList){
						String custName=partnerCust.get(partnerDevice.getPartnerId());
						
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS")){
							iosList.add(partnerDevice.getDeviceId());
					        if(noteType.equalsIgnoreCase("N4")){
					        	pushNotificationToIOS("Rappel","N’oubliez pas d’envoyer votre devis a" + custName +" et de mettre a jour votre appli mobile Partenaire SW" , iosList);
					        }
					        if(noteType.equalsIgnoreCase("N5")){
					        	pushNotificationToIOS("Rappel"," Urgent - N’oubliez d’envoyer votre devis a "+custName+"  et de mettre a jour votre appli mobile Partenaire SW" , iosList);
					        }
					        if(noteType.equalsIgnoreCase("N6")){
					        	pushNotificationToIOS("Rappel","Avez-vous des nouvelles du devis de "+custName+"? N’oubliez pas de mettre a jour votre appli mobile Partenaire SW", iosList);
					        }
					        
						}else{
							androidList.add(partnerDevice.getDeviceId());
							
							if(noteType.equalsIgnoreCase("N4")){
								pushNotificationToAndroid("Rappel","N’oubliez pas d’envoyer votre devis a"+custName+" et de mettre a jour votre appli mobile Partenaire SW" , androidList);
							}
							if(noteType.equalsIgnoreCase("N5")){
								pushNotificationToAndroid("Rappel","Urgent - N’oubliez d’envoyer votre devis a  "+custName+"  et de mettre a jour votre appli mobile Partenaire SW" , androidList);
							}
							if(noteType.equalsIgnoreCase("N6")){
								pushNotificationToAndroid("Rappel","Avez-vous des nouvelles du devis de "+custName+"? N’oubliez pas de mettre a jour votre appli mobile Partenaire SW" , androidList);
							}
							
						
						}
					
					}
						
				}catch(Exception e){
					e.printStackTrace();
				}
		


			
		}


	}
	
	public void notifyPartnerForOpportunityCancel(List<PartnerDevice> deviceList,Map<String,String> partnerCust,Map<String,String> partnerOpp,String noteType,Map<String,String> OppId,Map<String,String> cancelDate) throws AppException{

		 if (deviceList!=null && deviceList.size()>0)
		 {
				try{	
					
					LOGGER.info("notifyPartnerForOpportunityCancel:::deviceList.size():"+deviceList.size());
					//get andriodList
					List<String> iosList=new ArrayList<String>();
					List<String> androidList=new ArrayList<String>();
                    String oppIdValue ="";
					String canceldate="";
					String partnerIdValue="";
					for(PartnerDevice partnerDevice:deviceList)
					{
					
						LOGGER.debug("partner ID:"+partnerDevice.getPartnerId());
						if (partnerDevice.getDeviceType().equalsIgnoreCase("IOS"))
						{
							iosList.add(partnerDevice.getDeviceId());
							
						
						}else{
							androidList.add(partnerDevice.getDeviceId());
						
						}
						
					
					    Iterator it = OppId.entrySet().iterator();
					
					    Iterator it1 = cancelDate.entrySet().iterator();
					    
					    while (it.hasNext()) 
					    {
					        Map.Entry pair = (Map.Entry)it.next();
                            String partnerId = pair.getKey().toString();
					        System.out.println(pair.getKey() + " = " + pair.getValue());
					        
					        if(partnerDevice.getPartnerId()==Integer.parseInt(partnerId))
					        {
	                            oppIdValue = pair.getValue().toString();
					        }
					        
					        LOGGER.debug("oppIdValue:::"+oppIdValue);
						
					    }
					    
					    while (it1.hasNext()) 
					    {
					        Map.Entry pair = (Map.Entry)it1.next();
                            String partnerId1 = pair.getKey().toString();
					        partnerIdValue = String.valueOf(partnerDevice.getPartnerId());
                            System.out.println(pair.getKey() + " = " + pair.getValue());
					        
					        if(partnerDevice.getPartnerId()==Integer.parseInt(partnerId1))
					        {
	                            canceldate = pair.getValue().toString();
					        }
					        LOGGER.debug("canceldate:::"+canceldate);
						 }		
					}
					
                    String InstallationDate="";					
                    InstallationDate = opportunityDAO.checkInstallationExists(String.valueOf(oppIdValue),partnerIdValue);
                    LOGGER.debug("InstallationDate:::"+InstallationDate); 
                    if(iosList!=null && iosList.size()>0)
					{	
						
			        	if(InstallationDate!=null && InstallationDate.length()>0)
			        	  pushNotificationToIOS("Order avbokad!","Order "+oppIdValue+ " avbokad och kommer tas bort från dina aktuella ordrar."+"\n"+"Kom ihåg att ta bort din kalendarbokning "+InstallationDate, iosList);
			        	else
			        	  pushNotificationToIOS("Order avbokad!","Order "+oppIdValue+ " avbokad och kommer tas bort från dina aktuella ordrar.", iosList);
			        	
					}
					if(androidList!=null && androidList.size()>0)
					{
			        	if(InstallationDate!=null && InstallationDate.length()>0)
			        		pushNotificationToAndroid("Order avbokad!","Order "+ oppIdValue+" avbokad och kommer tas bort från dina aktuella ordrar."+"\n"+"Kom ihåg att ta bort din kalendarbokning "+InstallationDate, androidList);
			        	else
			        		pushNotificationToAndroid("Order avbokad!","Order "+ oppIdValue+" avbokad och kommer tas bort från dina aktuella ordrar.", androidList);
					}
						
				}catch(Exception e){
					e.printStackTrace();
				}
		
			
		}


	}



	private List<PartnerDevice> getDeviceIdsForPartnes(List<String> partnerIds) throws AppException {
		// TODO Auto-generated method stub

		return  partnerDAO.listPartnerDeviceByIds(partnerIds);


	}






	public ObjectNode pushNotificationToAndroid(String messageTitle, String message,List<String> deviceList) {

		ObjectNode response = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.getJsonFactory().configure(Feature.ESCAPE_NON_ASCII, false);
			response = mapper.getNodeFactory().objectNode();
			//List<PartnerDevice> deviceList = opportunityDAO.listPartnerDevice("Android");

			if(deviceList == null || (deviceList != null && deviceList.size() == 0)){
				response.put("errorMsg", "Android | No devices found for sending push notification");
				response.put("status", "error");
				LOGGER.debug("Android | No devices found for sending push notification");
				return response;
			}

			ObjectNode requestJSON = mapper.getNodeFactory().objectNode();
			ObjectNode dataNode = mapper.getNodeFactory().objectNode();
			dataNode.put("title", messageTitle);
			dataNode.put("message", message);
			requestJSON.put("data", dataNode);
			ArrayNode deviceIdArray = mapper.getNodeFactory().arrayNode();
			for(String device : deviceList){
				deviceIdArray.add(device);
			}
			requestJSON.put("registration_ids", deviceIdArray);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("Authorization", "key="+"AIzaSyBkUP-vVMXxRveUWci2RGn8mOy8auW0INY");

			HttpEntity<String> entityRequest = new HttpEntity<String>(requestJSON.toString(), headers);

			RestTemplate restTemplate = new RestTemplate();
			//add proxy if required
			//	SimpleClientHttpRequestFactory factory =   ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory());
			//	Proxy proxy= new Proxy(Type.HTTP, new InetSocketAddress("proxy-ip", proxy-port));
			//	factory.setProxy(proxy);
			//	restTemplate = new RestTemplate(factory);

			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			
			JSONObject restResponse = restTemplate.postForObject("https://android.googleapis.com/gcm/send", entityRequest, JSONObject.class);
			
			response = (ObjectNode) mapper.readTree(restResponse.toJSONString());
			
			
			
			LOGGER.debug("Android push notification response = " + response);

		} catch (Exception e) {
			e.printStackTrace();
			response.put("errorMsg", "Error while sending Android push notification");
			response.put("status", "error");
			LOGGER.error("Error while sending Android push notification", e);
		}

		return response;

	}



	public ObjectNode pushNotificationToIOS(String messageTitle, String message,List<String> deviceList) {
		/*
		 *Reference :
		 *https://github.com/notnoop/java-apns
		 *http://developement.sebastian-bothe.de/?page_id=40
		 *and
		 *http://blog.synyx.de/2010/07/sending-apple-push-notifications-with-notnoops-java-apns-library/
		 */		
        
		
		ApnsService service = null;
		InputStream certStream = null;
		ObjectNode response = null;
		boolean hasError = false;
		boolean checkPortAvailable=false;
		boolean sendnotification=false;
		if(sendnotification)
		{
			checkPortAvailable = checkPortAvailable(2195);
			LOGGER.info("checkPortAvailable:::"+checkPortAvailable);
			if(checkPortAvailable==true)
			{
			LOGGER.info("iosCertificatePassword:::::"+iosCertificatePassword);
	   
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.getJsonFactory().configure(Feature.ESCAPE_NON_ASCII, false);
				response = mapper.getNodeFactory().objectNode();
	
				//List<PartnerDevice> deviceList = opportunityDAO.listPartnerDevice("iOS");
				if(deviceList == null || (deviceList != null && deviceList.size() == 0)){
					response.put("errorMsg", "iOS | No devices found for sending push notification");
					response.put("status", "error");
					LOGGER.debug("iOS | No devices found for sending push notification");
					return response;
				}
	
				//certStream = this.getClass().getClassLoader().getResourceAsStream("ck4.p12");
				
				certStream = this.getClass().getClassLoader().getResourceAsStream("ePartnerSW.p12");
	
				
				LOGGER.debug("certStream"+certStream);
	
				//For production environment
				//Add proxy if required
				//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy-host, proxy-port));
				/*service = APNS.newService()
						.withCert(certStream, iosCertificatePassword)
						.withProductionDestination()
						//.withProxy(proxy)
						.build();*/
	
				// For dev environment
				
				
			LOGGER.info("iosCertificatePassword:::::"+iosCertificatePassword);
	
			if(solution30Proxy!=null && solution30Proxy.equalsIgnoreCase("1"))
			{	
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("205.167.7.126", 80));
				service = APNS.newService()
						.withCert(certStream, iosCertificatePassword)
						.withProductionDestination()
						.withProxy(proxy)
						.build();
	
			}
			else
			{
			
					service = APNS.newService()
					.withCert(certStream, iosCertificatePassword)
					.withProductionDestination()
					.build();
			}
			
			
				
	            //LOGGER.info("inside test connection if it is success then start the service");
	             service.start();
			
				for (String device : deviceList) {
					try {
						//badge : The number of updates to display.
						Thread.sleep(1000);
						int badge = 1;
						PayloadBuilder payloadBuilder = APNS.newPayload();
						payloadBuilder = payloadBuilder.badge(badge).alertBody(message);
	
						//check if the message is too long, shrink it if so.
						if (payloadBuilder.isTooLong()) {
							LOGGER.debug("Push message is too long, shrinking the content to guarantee the delivery.");
							payloadBuilder = payloadBuilder.shrinkBody();
						}
						String payload = payloadBuilder.build();
						String token = device;
						service.push(token, payload);
					} catch (Exception e) 
					{
						hasError = true;
						response.put("status", "error");
						response.put("message", "iOS | Error while sending push notification");
						LOGGER.error("iOS | Error while sending push notification", e);
					}
				}
			
			} catch (Exception ex) {
	
				if(!hasError){
					hasError = true;
					response.put("status", "error");
					response.put("message", "iOS | Error while starting the push notification service");
				}
	
				LOGGER.error("iOS | Error while starting the push notification service", ex);
			} finally {
	
				if (service != null) {
					//service.stop();
				}
	
				if(certStream != null)
					try {
						certStream.close();
					} catch (IOException e) {}
	
			}
	
			if(!hasError){
				response.put("status", "success");
				response.put("message", "iOS | Push notification sent successfully");
			}
			return response;
			}else
			{
				LOGGER.info("port not available for sending PN for IOS...");
				response.put("status", "failure");
				response.put("message", "iOS | Push notification sending failed");
				return  response;
			}
		}
		
		return response;
	
	}




	@Override
	public void sendMail() {
		// TODO Auto-generated method stub
		iEmailService.sendApprovedMail("sudhir", "sudhir.marni@gmail.com", "test", BigInteger.valueOf(123),"sdsad" , "sdfsfds");

	}




	/*
	public void sendMailToConsumer(String consumerEmail,String oppId,String partnerName,String projectName,String consumerName,String zipCode,String budget,String templateId) {
		// TODO Auto-generated method stub
		
	}*/

	
	
	
	
	public void sendMailNoPartnerAccepted(String country){
		
		try{
			List<Opportunity> opps=partnerFactory.getPartnerManger("SW").getOppWhenNoPartnerAccepted(country);
			List<String> templateList=new ArrayList<String>();
			if (opps!=null && opps.size()>0){
				
				for(Opportunity opp:opps){
					
					
					templateList = SWEmailTemplates
							.sendEmailtoSEAdminWithNoPartnerAnswered(seAdminEmail,
									opp.getId(), opp.getCustomerDetails().getFirstName(), opp.getAddress().getZip(), opp.getBudget());		
				
					try {
						//iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.SWEDEN.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//for france send opportunity to Solution 30
					
					try {
						//send to solution 30
						partnerFactory.getPartnerManger("SW").sendOppTOSol30( opp.getId(), " ");
						
						//remove assignment for other partners , who are already assigned to this opportunity
						opportunityDAO.unassignOpportunityFromPartner(opp.getId());
						
						//opportunityDAO.updateOppStatus(opp.getId(), OppStatus.PARKED);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						
					}
			}
		}catch(Exception e ){
			e.printStackTrace();
		}
	}

	@Override
	public void sendMailAllPartnerDeclined(String country) {
		
		try{
			List<Opportunity> opps=partnerFactory.getPartnerManger("SW").getOppWhenNoPartnerAccepted(country);
			List<String> templateList=new ArrayList<String>();
			if (opps!=null && opps.size()>0){
				
				for(Opportunity opp:opps){
					templateList = SWEmailTemplates
							.sendEmailtoSEAdminWithAllPartnerDeclined(seAdminEmail,
									opp.getId(), opp.getCustomerDetails()
											.getFirstName(), opp.getAddress()
											.getZip(), opp.getBudget());
				
					try {
						//iEmailService.sendMail(seAdminEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.SWEDEN.getValue(),0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					
					//for Sweden for due course we are not sending opportunity to Solution 30
					
					try {
						//send to solution 30
						/*partnerFactory.getPartnerManger("SW").sendOppTOSol30( opp.getId(), " ");
						
						//remove assignment for other partners , who are already assigned to this opportunity
						opportunityDAO.unassignOpportunityFromPartner(opp.getId());
						*/
						//opportunityDAO.updateOppStatus(opp.getId(), OppStatus.PARKED);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
						
					}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void sendNewOppToBFO() {
	
		try{
		Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequest();
		
		String token=httpsClient.getToken();
		
		
		Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
		
		for (Entry entry : entrySet) {
		
			Map<String,String> responseMap =httpsClient.sendCompositRequestToBFO(entry.getValue().toString(), token);
			
			bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatus(responseMap, entry.getKey().toString());
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	
	/*@Override
	public void sendNewOppToIF() {
	
		try{
		Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequestIF();
		
		String bearerToken=httpsClient.getIFWBarerToken();
		
		Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
		
		for (Entry entry : entrySet) {
		
			Map<String,String> responseMap =httpsClient.sendOppCreateRequestToIF(entry.getValue(),bearerToken);
			
			bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatus(responseMap, entry.getKey().toString());
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}*/
	
	@Override
	public void sendNewOppToIF() {
	
		try{
		Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityRequestIF();
		if(reqs!=null && reqs.size()>0)
		{
			String bearerToken=httpsClient.getIFWBarerToken();
			Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) 
			{
			
				Map<String,String> responseMap =httpsClient.sendOppCreateRequestToIF(entry.getValue(),bearerToken);
				
				bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppStatusIF(responseMap, entry.getKey().toString());
			}
		}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void sendAcceptRejectToBFO(String oppStatus) {
		// TODO Auto-generated method stub
		
		try{
			Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityStatusRequests(oppStatus);
			
			String token=httpsClient.getToken();
			
			
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) {
			
				boolean responseMap =httpsClient.sendUpdateRequestToBFO(entry.getValue().toString(), token,"");
				if (responseMap){
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppAcceptRejectStatus(responseMap, entry.getKey().toString());
				}
				//foManager.updateBFOOppStatus(responseMap, entry.getKey().toString());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	
	@Override
	public void sendAcceptRejectToBFOIF(String oppStatus) {
		// TODO Auto-generated method stub
		boolean responseMap=false;

		try{
			Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityStatusRequestsIF(oppStatus);
			if(reqs!=null && reqs.size()>0)
			{
				String bearerToken=httpsClient.getIFWBarerToken();
				Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
				for (Entry entry : entrySet)
				{
					
					String bfoOppId=""; 
					
					bfoOppId = bfoManagerFactory.getBFOManager(bfoManagerName).getBfoOppId(entry.getKey().toString(),"OPPLINK");
					
					String responseData =httpsClient.sendAcceptRejectUpdateRequestToIF(entry.getValue(),oppStatus,bearerToken,bfoOppId);
					if (responseData!=null && responseData.length()>0)					
					{
						responseMap=true;
					}	
					
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppAcceptRejectStatusIF(responseMap, entry.getKey().toString(),responseData,bfoOppId);
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}	

		

	@Override
	public void sendStatusHistToBFO(String statusId) {
		// TODO Auto-generated method stub
		try{
			Map<String,String> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityHistStatusRequests(statusId);
			
			String token=httpsClient.getToken();
			
			
			Set<Map.Entry<String, String>> entrySet = reqs.entrySet();
			
			for (Entry entry : entrySet) {
			
				boolean responseMap =httpsClient.sendUpdateRequestToBFO(entry.getValue().toString(), token,statusId);
				if(responseMap){
					bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHist(responseMap, entry.getKey().toString());
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void sendStatusHistToIF(String statusId) {
		// TODO Auto-generated method stub
		try{

			Map<String,Object> reqs=bfoManagerFactory.getBFOManager(bfoManagerName).getOpportunityHistStatusRequestsIF(statusId);
			if(reqs!=null && reqs.size()>0)
			{
		
				String bearerToken=httpsClient.getIFWBarerToken();
				Set<Map.Entry<String, Object>> entrySet = reqs.entrySet();
				for (Entry entry : entrySet)
				{
				
					String bfoOppId=""; 
					boolean responseMap=false;
					String custVisitId="";
					bfoOppId = bfoManagerFactory.getBFOManager(bfoManagerName).getBfoOppId(entry.getKey().toString(),"OPPHISTORY");
					
					if(statusId!=null && !statusId.equalsIgnoreCase("INSTALLATION"))
					{
						responseMap =httpsClient.sendUpdateRequestToIF(entry.getValue(),statusId,bearerToken,bfoOppId);
	
						if(responseMap)
						{
							bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHist(responseMap, entry.getKey().toString());
						}
						
					}else
					{
						custVisitId = httpsClient.sendUpdateRequestToIFInstallation(entry.getValue(),statusId,bearerToken,bfoOppId);
						if(custVisitId!=null && !custVisitId.equalsIgnoreCase("") && custVisitId.length()>0)
						{
							responseMap=true;
							bfoManagerFactory.getBFOManager(bfoManagerName).updateBFOOppHistInstallation(responseMap, entry.getKey().toString(),custVisitId);
						}
	
					}
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}	
	private File getPDfStream(String base64Str){
		
		File of =null;
    try {
      /*  Base64 b = new Base64();
       
        byte[] imageBytes = b.decode(base64Str); 
         fos = new FileOutputStream("bomPDF.pdf");
        fos.write(imageBytes);
        fos.close();*/
        
        byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(base64Str);  
         of = new File("bomPDF.pdf");  
        FileOutputStream osf = new FileOutputStream(of);  
        osf.write(btDataFile);  
        osf.flush();  
        
        
    } catch (Exception e) {

        System.out.println("Error :::" + e);
        e.printStackTrace();

    }
    
    	return of;
    
	}
	
	private  File generateCsvFile(List<BOM> boms) {
		try {
			 	File   file = new File("bomCSV.csv");
					FileWriter writer = new FileWriter(file);
					writer.append("Product Id");
					writer.append(',');
					writer.append("Category");
					writer.append(',');
					writer.append("Description");
					writer.append(',');
					writer.append("Color");
					writer.append(',');
					writer.append("Quantity");
					writer.append(',');
					writer.append("Total Cost");
					
			for(BOM bom :boms){
				
					
					writer.append('\n');
		
					writer.append(bom.getProductId());
					writer.append(',');
					writer.append(bom.getCategory());
					writer.append(',');
					writer.append(bom.getDescription());
					writer.append(',');
					writer.append(bom.getColor());
					writer.append(',');
					writer.append(""+bom.getQuantity());
					writer.append(',');
					writer.append(""+bom.getUnitCost());

			
			}
			writer.flush();
			writer.close();
			
			return file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void sendBOM(String oppId, String userId, String supplierEmail) {
		List<String> template=new ArrayList<String>();
		Opportunity opp=opportunityDAO.getOportunity("Distributor", oppId);

		//get opportunity boms
		List<BOM> boms=opportunityDAO.getBoms(Long.parseLong(oppId));
		
		template = SWEmailTemplates.sendEmailtoPartnerForNewRequest(
				supplierEmail, "Distributor", opp.getTitle(), opp
						.getCustomerDetails().getFirstName(),opp
						.getAddress().getZip(), opp.getBudget(),opp.getId(),boms,opp.getCustomerDetails().getAddress().getSt_name(),opp.getAddress().getCity());
		
		try {
			Thread.sleep(1000);
		
			FileSystemResource fsr=new FileSystemResource(getPDfStream(opp.getBmPDF()));
			FileSystemResource fsrcsv=new FileSystemResource(generateCsvFile(boms));
			
			iEmailService.sendMail(supplierEmail, template.get(1), template.get(0),fsr,fsrcsv,CountryCode.SWEDEN.getValue(),0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
/**
 * this method to send the email to consumer that partner accepted the opportunity
 */
	
	@Override
	public void sendMailToConsumerPartnerAccepted(String countryCode,boolean sendSms) {
		
		List<CustomerPartner> custList=consumerDAO.getConsumerWithPartnersForAcceptedOpps(countryCode);
		
		List<String> template=new ArrayList<String>();
		for(CustomerPartner cust:custList){
			
			//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
			
			
			//template=SWEmailTemplates.sendEmailtoConsumerFollowBy(cust.getCustomerEmail(), cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone(),cust.getPartnerLastName(),String.valueOf(cust.getOppId()));
			template=SWEmailTemplates.sendEmailtoConsumerFollowBy("sunke.arun@gmail.com", cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone(),cust.getPartnerLastName(),String.valueOf(cust.getOppId()));

			
			try {
				iEmailService.sendMail(cust.getCustomerEmail(), template.get(1), template.get(0),null,null,CountryCode.SWEDEN.getValue(),5);
				//Method to send SMS to consumer when partner Accepts the Opportunity
				if(sendSms)
				{	
					httpsClient.sendSMSToConsumer(cust.getCustomerPhone(), cust.getCustomerName(), cust.getPartnerFirstName(),cust.getPartnerPhone(),phCountryCode,cust.getOppId());
				}
				Thread.sleep(3000);				
				consumerDAO.updateOppSLA(cust.getOppId(),"NOTI1");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
				//once send update the customer SLA to 1 in opportunity SLA table
		//same loop
		
			
			
		}
		
		}
	
public void sendSmsTopartnerwhenConsCancelled(String countryCode,boolean sendSms,String notifyType) {
		
		List<CustomerPartner> custList=consumerDAO.getpartnerwhenCancelledOpp(countryCode);
		
		String InstallationExists = "";
		
		
		List<String> template=new ArrayList<String>();
		for(CustomerPartner cust:custList)
		{
			
		LOGGER.debug("Opportunity Name::::"+cust.getOppName());
		LOGGER.debug("Opportunity Id::::"+cust.getOppId());

		//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
			
			try {
				if(sendSms)
				{	
					
					InstallationExists = opportunityDAO.checkInstallationExists(String.valueOf(cust.getOppId()), cust.getPartnerId());
					LOGGER.debug("sendSmsTopartnerwhenConsCancelled:::InstallationExists::"+InstallationExists);
					httpsClient.sendSMSToPartner(cust.getPartnerPhone(),cust.getCustomerName(), cust.getPartnerFirstName(),cust.getOppName(), phCountryCode,cust.getOppId(),cust.getMeetingDate(),InstallationExists);
					consumerDAO.updateOppSLA(cust.getOppId(),"CANCEL");

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
				//once send update the customer SLA to 1 in opportunity SLA table
		//same loop
		
			
			
		}
		
	}

public static void main(String[] args) {
	SENotificationManagerImpl impl=new SENotificationManagerImpl();
	
	impl.sendSmsTopartnerwhenConsCancelled("SW", true,"CANCEL");
}

@Override
public void sendMailToConsumerOppReceived(String countryCode) {
	
	List<CustomerPartner> custList=consumerDAO.getCustomersWhoSendsTheNewOpps(countryCode);
	
	List<BOM> boms = new ArrayList<BOM>();
	
	List<String> template=new ArrayList<String>();
	
	for(CustomerPartner cust:custList)
	{
		
			//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
			
			boms=opportunityDAO.getBoms(cust.getOppId());	
	
			template=SWEmailTemplates.sendEmailtoConsumerValidate(cust.getCustomerEmail(), cust.getCustomerName(),cust.getPartnerLastName(),cust.getCustomerZip(),boms,cust.getCustomerAddress(),Integer.toString(cust.getOppId()),cust.getCustomerCity());
			//template=SWEmailTemplates.sendEmailtoConsumerFollowBy(cust.getCustomerEmail(), cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone());
			
			try {
				//iEmailService.sendMail(cust.getCustomerEmail(), template.get(1), template.get(0),null,null,CountryCode.SWEDEN.getValue(),4);
				iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),null,null,CountryCode.SWEDEN.getValue(),4);

				Thread.sleep(2000);
				consumerDAO.updateOppSLA(cust.getOppId(),"NEWOPP");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
			//once send update the customer SLA to 1 in opportunity SLA table
	//same loop
		
		
	}
	
}

/**
 * notify partner for N4 N5 N6
 */

@SuppressWarnings("unchecked")
public void notifyPartnerForN4toN6(int hrs,String nType,String country){
	
	
	Map<String,Object> returnMap=opportunityDAO.getPartnerDeviceForNotifications(hrs,nType,country);
	List<Integer> oppIdList=new ArrayList<Integer>();
	
	for(Map.Entry<String, Object> entry:returnMap.entrySet())
	{
		
		if(entry.getKey().equalsIgnoreCase("OPPLIST"))
		{			
			oppIdList=(List<Integer>) entry.getValue();
			
		}

//	returnMap.put("PARTNERCUST", partCustMap);Rappel
//	returnMap.put("DEVICELIST", partnerDeviceList);
	if (returnMap!=null && !returnMap.isEmpty()){
		
		
		try {
			if(nType.equalsIgnoreCase("N4"))
			{
			notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N4");
			//update sla table
			consumerDAO.updateOppNOTI(oppIdList, "N4");
			
			}
			else if(nType.equalsIgnoreCase("N5"))
			{
			notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N5");
			consumerDAO.updateOppNOTI(oppIdList, "N5");
			}
			else if(nType.equalsIgnoreCase("N6"))
			{
			notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "N6");
			consumerDAO.updateOppNOTI(oppIdList, "N6");
			}
			else if(nType.equalsIgnoreCase("CANCEL"))
			{
				//notifyPartner((List<PartnerDevice>)returnMap.get("DEVICELIST"), (Map<String,String>)returnMap.get("PARTNERCUST"), "CANCEL");
				//consumerDAO.updateOppNOTI(oppIdList, "CANCEL");
			}
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	}
}

@Override
public void sendMailToConsumer(String consumerEmail, String oopId,
		String partnerName, String projectName, String consumerName,
		String zipCode, String budget, String templateId, String date,
		String custNumber,String custMail,List<BOM> boms,String address,String city) {
	List<String> templateList=new ArrayList<String>();
	if (templateId.equalsIgnoreCase("NewOpp")){
		templateList = SWEmailTemplates.sendEmailtoPartnerForNewRequest(
				consumerEmail, partnerName, projectName, consumerName, zipCode,
				budget,oopId,boms,address,city);
	}else{
		templateList = SWEmailTemplates.sendEmailtoPartnerForNewRequest(
				consumerEmail, partnerName, projectName, consumerName, zipCode,
				budget,oopId,boms,address,city);
		
	}
	
	try {
		iEmailService.sendMail(consumerEmail, templateList.get(1), templateList.get(0),null,null,CountryCode.SWEDEN.getValue(),0);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	
}

/*@Override
public void sendSmsToConsumerWhenMeetingBooked(String countryCode,
		boolean sendSms, String notifyType) {
	// TODO Auto-generated method stub
	
}*/

@Override

public void sendSmsToConsumerWhenMeetingBooked(String countryCode,boolean sendSms,String notifyType) {
	
	List<CustomerPartner> custList=consumerDAO.getConsumerWhenMeetingBooked(countryCode,"SMS");
	
	List<String> template=new ArrayList<String>();
	for(CustomerPartner cust:custList)
	{
		//send mails to customer/consumers if the list is not null
		if(cust!=null)
		{
	
			try {
				if(sendSms)
				{	
					LOGGER.debug("customer phone:::"+cust.getCustomerPhone());
					httpsClient.sendSMSToConsumerWhenMeetingbooked(cust.getCustomerPhone(),cust.getCustomerName(), cust.getPartnerFirstName(),cust.getOppName(),cust.getCustomerZip(),cust.getCustomerCity(),cust.getCustomerAddress(),cust.getMeetingDate(), phCountryCode);
					consumerDAO.updateOppSLA(cust.getOppId(),notifyType);
	
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		
	}
	
}

/**
 * this method to send the email to consumer that partner booked the meeting for installation
 */
	
	@Override
	public void sendMailToConsumerWhenPartnerBookedMeeting(String countryCode) {
		
		List<CustomerPartner> custList=consumerDAO.getConsumerWhenMeetingBooked(countryCode,"EMAIL");
		
		List<String> template=new ArrayList<String>();
		for(CustomerPartner cust:custList){
			
			//send mails to customer/consumers if the list is not null
		if(custList!=null && custList.size()>0)
		{
			
			LOGGER.info("sendMailToConsumerWhenPartnerBookedMeeting:opp_id:::"+cust.getOppId());
			template=SWEmailTemplates.sendEmailtoConsumerWhenPartnerBookedMeeting(cust.getCustomerEmail(), cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone(),cust.getPartnerLastName(),cust.getBomList(),cust.getOppId(),cust.getCustomerAddress(),cust.getCustomerZip(),cust.getCustomerCity(),cust.getMeetingDate());
			
			try {
				//iEmailService.sendMail(cust.getCustomerEmail(), template.get(1), template.get(0),null,null,CountryCode.SWEDEN.getValue(),5);
				iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),null,null,CountryCode.SWEDEN.getValue(),5);

				Thread.sleep(3000);				
				consumerDAO.updateOppSLA(cust.getOppId(),"MEETING_EMAIL");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
				//once send update the customer SLA to 1 in opportunity SLA table
		//same loop
		
			
			
		}
		
		}
	
	
	
	/**
	 * This method to send the Invoice email to Admin when Installed Status 
	 */
		
		@Override
		public void sendMailToAminForInvoice(String countryCode,String status) {
			
			List<CustomerPartner> custList=consumerDAO.getConsumerWhenPartnerInstalled(countryCode,"EMAIL");
			
			List<String> template=new ArrayList<String>();
			for(CustomerPartner cust:custList){
				
				//send mails to customer/consumers if the list is not null
			if(custList!=null && custList.size()>0)
			{
				
				LOGGER.debug("sendMailToAminForInvoice:::swedenAdminEmail:::"+swedenAdminEmail);
				template=SWEmailTemplates.sendInvoicetoAdminWhenPartnerInstalled("sunke.arun@gmail.com", cust.getPartnerFirstName(),cust.getCompany(),cust.getCustomerName(),cust.getPartnerPhone(),cust.getPartnerLastName(),cust.getBomList(),cust.getOppId(),cust.getCustomerAddress(),cust.getCustomerZip(),cust.getCustomerCity(),cust.getMeetingDate());
				if(template!=null)
				{	
					LOGGER.debug("invooice template"+template.get(0));
					LOGGER.debug("invooice template"+template.get(1));
				}
				try {
					iEmailService.sendMail("sunke.arun@gmail.com", template.get(1), template.get(0),null,null,CountryCode.SWEDEN.getValue(),6);
					Thread.sleep(5000);				
					consumerDAO.updateOppSLA(cust.getOppId(),"INSTALLED_EMAIL");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
					//once send update the customer SLA to 1 in opportunity SLA table
			//same loop
				
			}
			
			}

/*@Override
public void sendMailToConsumer(String consumerEmail, String partnerName,
		String projectName, String consumerName, String zipCode, String budget,
		String templateId, String date, String custNumber) {
	// TODO Auto-generated method stub
	
}*/
		
		
  public boolean checkPortAvailable(int port)
  {
	  boolean portExists=false;
	  ServerSocket serverSocket=null;
	  try {
		    serverSocket = new ServerSocket(port);
		    portExists=true;
		} catch (IOException e) {
		    System.out.println("Could not listen on port: " + port);
		    portExists=false;
		    // ...
		}
	  
	  return portExists;
  }
  
	
}

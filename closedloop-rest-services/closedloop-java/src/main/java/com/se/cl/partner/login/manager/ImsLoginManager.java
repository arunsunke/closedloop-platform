package com.se.cl.partner.login.manager;

import com.se.cl.model.IMS;

public interface ImsLoginManager
{
	public boolean AuthenticateUser(String User,String Password);
	public IMS getAuthenticateUserToken(String User,String Password);
	public IMS getAccessTokenFromRefreshToken(String refeshToken);
	public String  getImsUserId(String imsToken);

}

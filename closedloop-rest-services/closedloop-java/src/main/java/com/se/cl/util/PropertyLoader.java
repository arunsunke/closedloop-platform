package com.se.cl.util;



import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.NoSuchMessageException;


public class PropertyLoader implements ApplicationContextAware {
	
	private static ApplicationContext appCtx;
//	private static Properties errorsProperties;
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
			appCtx = applicationContext;
//		errorsProperties = (Properties) appCtx.getBean("errorsProperties");
	}
	
	/**
	 * This method returns the value for the key in the sparql.properties file
	 * 
	 * @param key
	 * @return String
	 */
	public static String getSparQlProperty(String key,Object...args){
		String propertyVal = "";
		
				try {
			propertyVal = appCtx.getMessage(key, args, null);
		} catch(NoSuchMessageException noSuchMessageException) {
			try{
				propertyVal = appCtx.getMessage(key, args, null);
			}catch(NoSuchMessageException noSuchMsgException){
				
			}
		}
		return propertyVal;
	}
	


	
}

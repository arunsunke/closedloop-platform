package com.se.cl.sol30.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.se.cl.constants.BFOStatus;
import com.se.cl.constants.OppStatus;
import com.se.cl.constants.UpdSource;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.Solution30DAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.BOM;
import com.se.cl.model.Opportunity;
import com.se.cl.model.Solution30StatusResponse;
import com.se.cl.sol30.service.ActivityData;
import com.se.cl.sol30.service.ActivityData.Equipments;
import com.se.cl.sol30.service.ActivityReturn;
import com.se.cl.sol30.service.ClientData;
import com.se.cl.sol30.service.Equipment;
import com.se.cl.sol30.service.ISchneider;
import com.se.cl.sol30.service.SchneiderService;

public class Solution30ServiceImpl implements Solution30Service{

	private static final Logger logger = LoggerFactory.getLogger(Solution30ServiceImpl.class);
	
	@Autowired
	Solution30DAO solution30DAO;
	
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	OpportunityManager OpportunityManager;
	
	private @Value("${solution30.proxy}") String solution30Proxy;

	
	@Override
	public Holder<ActivityReturn> sendOpportunityToSol30(Opportunity opportunity) {
		
		logger.info("sendOpportunityToSol30:::Calling Sol30 Activity Install service");
		
		logger.info("sendOpportunityToSol30:solution30Proxy:::"+solution30Proxy);
		
		if(solution30Proxy!=null && solution30Proxy.equalsIgnoreCase("1"))
		{	
			System.setProperty("http.proxyHost", "205.167.7.126");
			System.setProperty("http.proxyPort", "80");
		}
		
		SchneiderService service=new SchneiderService();
			
		ISchneider sc=service.getSchneiderPort();
		
		//((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 1000);
		//Map<String, Object> requestContext = ((BindingProvider) sc).getRequestContext();

	    //requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 1000); //10 secs

	    //requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, 1 * 60 * 1000); //1 min

		//((BindingProvider)sc).getRequestContext().put("javax.xml.ws.client.connectionTimeout", 10*1000); // 10 seconds  
		//((BindingProvider)sc).getRequestContext().put("javax.xml.ws.client.receiveTimeout", 1*60*1000); // 1 minute

   	    ((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 10000); // 10 seconds

		
		Holder<String> executionCode=new Holder<String>();
		Holder<String> executionMessage=new Holder<String>();
		Holder<ActivityReturn> activityReturn=new Holder<ActivityReturn>();
		ActivityData activityData=getActivityData(opportunity);
		ClientData clientData=getClientData(opportunity);
		sc.confirmMeeting(clientData, activityData, executionCode, executionMessage, activityReturn);
		
		if (executionCode.value.equals("OK")){
			logger.info("Success");	
			logger.info("sol30 Id :"+activityReturn.value.getIdSol30());
			logger.info("Schneider Ref Id :"+activityReturn.value.getSchneiderRef());
		    return activityReturn;
		 }
		
		else{
			logger.error("Error While calling service :"+ executionMessage.value );
			
			return null;
		}
		
	}
	
	@Override
	public Solution30StatusResponse getOpportunityStatus(String oppId){
		
		logger.info("getOpportunityStatus:::Calling Sol30 InterventionStatus service for oppId:"+oppId);
		
		logger.info("getOpportunityStatus:solution30Proxy:::"+solution30Proxy);
		
		if(solution30Proxy!=null && solution30Proxy.equalsIgnoreCase("1"))
		{	
			System.setProperty("http.proxyHost", "205.167.7.126");
			System.setProperty("http.proxyPort", "80");
		}

		SchneiderService service=new SchneiderService();
        
		ISchneider sc=service.getSchneiderPort();
		
		//Map<String, Object> requestContext = ((BindingProvider) sc).getRequestContext();

	    //requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, 10 * 1000); //10 secs

	    //requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, 1 * 60 * 1000); //1 min

		
   	    //((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 10*1000);
   	    
   	    //((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.connection.timeout", 1*60*1000);


		//((BindingProvider)sc).getRequestContext().put("javax.xml.ws.client.connectionTimeout", 10*1000); // 10 seconds  
		//((BindingProvider)sc).getRequestContext().put("javax.xml.ws.client.receiveTimeout", 1*60*1000); // 1 minute
  
   	    ((BindingProvider) sc).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", 10000); //10 seconds

		
		Holder<java.lang.String> status=new Holder<java.lang.String>();
		Holder<java.lang.Object> date=new Holder<java.lang.Object>();
		Holder<java.lang.String> expertFirstName=new Holder<java.lang.String>();
		Holder<java.lang.String> expertLastName=new Holder<java.lang.String>();
		Holder<java.lang.String> expertId=new Holder<java.lang.String>();
		Holder<java.util.List<java.lang.String>> picturesUrl=new Holder<java.util.List<java.lang.String>>();
		
		Holder<java.lang.String> executionCode =new Holder<java.lang.String>();
		Holder<java.lang.String> executionMessage=new Holder<java.lang.String>();
		
	    
		sc.getInterventionStatus(oppId, status, date, expertFirstName, expertLastName, expertId, picturesUrl, executionCode, executionMessage);
		
	
		
		if (executionCode.value.equals("OK")){
			
			//logger.info("Success");	
			//logger.info("Status Id :"+status.value);
			
			Solution30StatusResponse response =new Solution30StatusResponse();
			
			response.setStatusId(status.value);
			response.setExpertId(expertId!=null?expertId.value:"");
			response.setExpertFirstName(expertFirstName!=null?expertFirstName.value:"");
			response.setExpertLastName(expertLastName!=null?expertLastName.value:"");
			//need to write stupid tightly coupled object for this response , if the response changes you need to change the object
			
		    return response;
		 }
		
		else{
			logger.error("Error While calling service :"+ executionMessage.value );
			
			return null;
		}
		
		
		
	}
	
	
	
	
	@Override
	public void updateOpportunityHistory() {
		
		
		//get opportunities which are sent to solution30
		List<String> opps=solution30DAO.getOpportunities();
		
		if (opps!=null && opps.size()>0){
			
			for(String oppId:opps){
			
				Solution30StatusResponse response=getOpportunityStatus(oppId);
				if(response!=null){
					//update history
					updateOppHistory(response,oppId);
				}
			}
			
		}
		
		
	}
	//smarni
	//Infuture if we have any issued with transactions we need to enable below line and test
	//@Transactional(propagation=Propagation.REQUIRED)
	private void updateOppHistory(Solution30StatusResponse response,String oppId) {

		String statusId="";
		String status="";
		String reason="";
		boolean isStepOne=false;
		if (response.getStatusId().equalsIgnoreCase("1")){
			
			System.out.println("STEP 1");
			try{
			statusId=BFOStatus.ACCEPTED.getValue();
			 status="sol 30 accept";
			 reason="";
			 //check record already exists or not 
			 if(!opportunityDAO.checkLinkExists(oppId, "99999")){
				 System.out.println("opp not found");
				 opportunityDAO.assignOpportunityToPartner(Long.valueOf(oppId), 99999);
				 Opportunity opp=new Opportunity();
				 opp.setId(oppId);
				 opp.setStatus(OppStatus.ACCEPTED.getValue());
				 OpportunityManager.updateOpportunity(opp, null, null, 0, null, UpdSource.ACCDECLINED,false);
				 
				 
			 }
			}catch(Exception e){
				e.printStackTrace();//TODO ignore for now .. revisit to handle duplicate key 
			}
			isStepOne=true;
			 
		}else if (response.getStatusId().equalsIgnoreCase("2")){
			
			 statusId=BFOStatus.MEETING.getValue();
			 status="sol 30 meeting";
			 reason="";
						
		}else if (response.getStatusId().equalsIgnoreCase("3")){
			
			 statusId="HOLD";
			 status="sol 30 hold";
			 reason="";
			
		
		}else if (response.getStatusId().equalsIgnoreCase("4")){
			
			 statusId=BFOStatus.INSTALLATION.getValue();
			 status="sol 30 Installation";
			 reason="";
			
		}else if (response.getStatusId().equalsIgnoreCase("5")){
					
			 statusId=BFOStatus.CANCEL.getValue();
			 status="sol 30 Cancel";
			 reason="";
			
		}else if (response.getStatusId().equalsIgnoreCase("6")){
			 statusId="COMPLETE";
			 status="sol 30 Cancel";
			 reason="";
		}
		
		
		if(!isStepOne){
			
			if (response.getStatusId().equalsIgnoreCase("4")){
				try {
					opportunityDAO.updateOppHistory("99999", oppId, BFOStatus.QUOTATION.getValue(), "Sol 30 Quote", reason, null, null, "status","SOL30");
					opportunityDAO.updateOppHistory("99999", oppId, BFOStatus.WON.getValue(), "Sol 30 WON", reason, null, null, "status","SOL30");
					opportunityDAO.updateOppHistory("99999", oppId, statusId, status, reason, null, null, "status","SOL30");
				
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				try {
					opportunityDAO.updateOppHistory("99999", oppId, statusId, status, reason, null, null, "status","SOL30");
				} catch (AppException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			
		}
	
	}

	private ClientData getClientData(Opportunity opportunity) {
		/*  <clientData>
        <refClient>X11</refClient>
        <!--Optional:-->
        <city>paris</city>
        <zip>21124</zip>
        <!--Optional:-->
        <addressLine1>1 test dr</addressLine1>
        <!--Optional:-->
        <addressLine2/>
        <country>SR</country>
        <firstName>Test</firstName>
        <lastName>Tester</lastName>
        <gender>M</gender>
        <!--Optional:-->
        <phone>1231234567</phone>
        <!--Optional:-->
        <mobile>1234231234</mobile>
        <email>test@closeloop.com</email>
        <!--Optional:-->
        <companyMame>ABC</companyMame>
     </clientData>*/
		
		
		
		ClientData clientData=new ClientData();
		
		clientData.setRefClient("CL11");
		clientData.setCity(opportunity.getAddress().getCity());
		clientData.setZip(opportunity.getAddress().getZip());
		clientData.setAddressLine1("test dr");
		clientData.setCountry(opportunity.getAddress().getCountryCode());
		
		clientData.setFirstName(opportunity.getCustomerDetails().getFirstName());
		clientData.setLastName(opportunity.getCustomerDetails().getLastName());
		clientData.setPhone(opportunity.getCustomerDetails().getPhone());
		clientData.setEmail(opportunity.getCustomerDetails().getEmail());
		//clientData.setCompanyMame(opportunity.getCustomerDetails().get);
		
		
		return clientData;
	}

	private ActivityData getActivityData(Opportunity opportunity) {
		/*
		 *   <activityData>
            <refInter>OP123-5</refInter>
            <activityType>INSTALL</activityType>
            <!--Optional:-->
            <activityDescriptions>
               <!--Zero or more repetitions:-->
               <activityDescription>
                  <!--Optional:-->
                  <key></key>
                  <!--Optional:-->
                  <value></value>
               </activityDescription>
            </activityDescriptions>
            <!--Optional:-->
            <equipments>
               <!--1 or more repetitions:-->
               <equipment>
                  <!--Optional:-->
                  <ref>EER50000</ref>
                  <!--Optional:-->
                  <qt>10</qt>
                  <!--Optional:-->
                  <price>101.23</price>
               </equipment>
            </equipments>
         </activityData>

		 * 
		 */
		
		ActivityData adata=new ActivityData();
		adata.setRefInter(opportunity.getId());
		adata.setActivityType("INSTALL");
		//adata.setActivityDescriptions(value);
		
		Equipments equipments=new Equipments();
		List<Equipment> equpis=new ArrayList<Equipment>();
		for(BOM bom:opportunity.getBoms()){
			
			Equipment equip=new Equipment();
			equip.setPrice(Double.valueOf(bom.getUnitCost()));
			equip.setQt(Float.valueOf(bom.getQuantity()).longValue());
			equip.setRef(bom.getProductId());
			equpis.add(equip);
			
			
		}
		
		equipments.getEquipment().addAll(equpis);
		
		adata.setEquipments(equipments);
		
		
		return adata;
	}

	
	
	
	
	
	

}

package com.se.clm.bfo.util;

import java.util.List;

import com.se.cl.model.BOM;
import com.se.cl.model.Question;

public class OppInputXMLDTO {
	
	private String prodList=null;
	private String salutation=null;
	private String firstName=null;
	private String lastName=null;
	private String cityC=null;
	private String countryC=null;
	private String zipCodeC=null;
	private String streetC=null;
	private String personalEMail=null;
	private String phone;
	
	private String ownerId=null;
	private String oppName=null;
	private String includedInForecastc=null;
	private String currencyIsoCode=null;
	private String closeDate=null;
	private String leadingBusinessc=null;
	private String countryOfDestinationc=null;
	private String opportunitySourcec=null;
	private String recordTypeId=null;
	private String opOwnerId=null;
	
	private StringBuilder sObjectsSB=null;
	private String techCommercialReferencec=null;
	private String quantityc=null;
	private String amountc=null;
	private String step=null; 
	private List<BOM> bomsList;
	private List<Question> questions;
	
	/*private String salutation="Ms";
	private String firstName="PanimozhiTestAccount";
	private String lastName="Sep182014";
	private String cityC="Bangalore";
	private String countryC="IN";
	private String zipCodeC="787767";
	private String streetC="rv";
	private String personalEMail="pavaithiru@gmail.com";
	private String ownerId="005A0000001Qe7t";
	private String oppName="Testingpavi";
	private String includedInForecastc="Yes";
	private String currencyIsoCode="INR";
	private String closeDate="2015-08-18";
	private String leadingBusinessc="GS";
	private String countryOfDestinationc="IN";
	private String opportunitySourcec="Sales Rep";
	private String recordTypeId="012A0000000nYNW";
	private String opOwnerId="005A0000001Qe7t";
	private String step="1";
	
	
	
	
	private String techCommercialReferencec="EER21001";
	private String quantityc="1";
	private String amountc="10";*/
	
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public List<BOM> getBomsList() {
		return bomsList;
	}
	public void setBomsList(List<BOM> bomsList) {
		this.bomsList = bomsList;
	}
	
	
	
	/*public String getProdList() {
		return prodList;
	}
	public void setProdList(String prodList) {
		this.prodList = prodList;
	}*/
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCityC() {
		return cityC;
	}
	public void setCityC(String cityC) {
		this.cityC = cityC;
	}
	public String getCountryC() {
		return countryC;
	}
	public void setCountryC(String countryC) {
		this.countryC = countryC;
	}
	public String getZipCodeC() {
		return zipCodeC;
	}
	public void setZipCodeC(String zipCodeC) {
		this.zipCodeC = zipCodeC;
	}
	public String getStreetC() {
		return streetC;
	}
	public void setStreetC(String streetC) {
		this.streetC = streetC;
	}
	public String getPersonalEMail() {
		return personalEMail;
	}
	public void setPersonalEMail(String personalEMail) {
		this.personalEMail = personalEMail;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getOppName() {
		return oppName;
	}
	public void setOppName(String oppName) {
		this.oppName = oppName;
	}
	public String getIncludedInForecastc() {
		return includedInForecastc;
	}
	public void setIncludedInForecastc(String includedInForecastc) {
		this.includedInForecastc = includedInForecastc;
	}
	public String getCurrencyIsoCode() {
		return currencyIsoCode;
	}
	public void setCurrencyIsoCode(String currencyIsoCode) {
		this.currencyIsoCode = currencyIsoCode;
	}
	public String getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	public String getLeadingBusinessc() {
		return leadingBusinessc;
	}
	public void setLeadingBusinessc(String leadingBusinessc) {
		this.leadingBusinessc = leadingBusinessc;
	}
	public String getCountryOfDestinationc() {
		return countryOfDestinationc;
	}
	public void setCountryOfDestinationc(String countryOfDestinationc) {
		this.countryOfDestinationc = countryOfDestinationc;
	}
	public String getOpportunitySourcec() {
		return opportunitySourcec;
	}
	public void setOpportunitySourcec(String opportunitySourcec) {
		this.opportunitySourcec = opportunitySourcec;
	}
	public String getRecordTypeId() {
		return recordTypeId;
	}
	public void setRecordTypeId(String recordTypeId) {
		this.recordTypeId = recordTypeId;
	}
	public String getOpOwnerId() {
		return opOwnerId;
	}
	public void setOpOwnerId(String opOwnerId) {
		this.opOwnerId = opOwnerId;
	}
	public String getTechCommercialReferencec() {
		return techCommercialReferencec;
	}
	public void setTechCommercialReferencec(String techCommercialReferencec) {
		this.techCommercialReferencec = techCommercialReferencec;
	}
	public String getQuantityc() {
		return quantityc;
	}
	public void setQuantityc(String quantityc) {
		this.quantityc = quantityc;
	}
	public String getAmountc() {
		return amountc;
	}
	public void setAmountc(String amountc) {
		this.amountc = amountc;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	
	

	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	

}

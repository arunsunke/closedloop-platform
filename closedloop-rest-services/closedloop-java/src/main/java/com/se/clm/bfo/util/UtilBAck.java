package com.se.clm.bfo.util;

import com.se.cl.model.BOM;

public class UtilBAck {
	
	String namespace="\"http://schemas.cordys.com/default\"";
	String updateNS="\"http://schemas.cordys.com/bfoservices\"";
	
	public String getOppInputXML(OppInputXMLDTO oppInputXMLDTO){		
		
		StringBuilder OPPINPUTXML=new StringBuilder();
		OPPINPUTXML.append("<create_update_Acc_Opp_Proc xmlns=");
		OPPINPUTXML.append(namespace+">");
		OPPINPUTXML.append("<OPPINPXML xmlns="+namespace+"><AccountDetails><Salutation>");
		OPPINPUTXML.append(oppInputXMLDTO.getSalutation());
		OPPINPUTXML.append("</Salutation><FirstName>");
		OPPINPUTXML.append(oppInputXMLDTO.getFirstName());
		OPPINPUTXML.append("</FirstName><LastName>");
		OPPINPUTXML.append(oppInputXMLDTO.getLastName());
		OPPINPUTXML.append("</LastName><City__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getCityC());
		OPPINPUTXML.append("</City__c><Country__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getCountryC());
		OPPINPUTXML.append("</Country__c><ZipCode__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getZipCodeC());
		OPPINPUTXML.append("</ZipCode__c><Street__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getStreetC());
		OPPINPUTXML.append("</Street__c><PersonEmail>");
		OPPINPUTXML.append(oppInputXMLDTO.getPersonalEMail());
		OPPINPUTXML.append("</PersonEmail><OwnerId>");
		OPPINPUTXML.append(oppInputXMLDTO.getOwnerId());
		OPPINPUTXML.append("</OwnerId></AccountDetails><OppDetails><Name>");
		OPPINPUTXML.append(oppInputXMLDTO.getOppName());
		OPPINPUTXML.append("</Name><IncludedInForecast__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getIncludedInForecastc());
		OPPINPUTXML.append("</IncludedInForecast__c><CurrencyIsoCode>");
		OPPINPUTXML.append(oppInputXMLDTO.getCurrencyIsoCode());
		OPPINPUTXML.append("</CurrencyIsoCode><CloseDate>");
		OPPINPUTXML.append(oppInputXMLDTO.getCloseDate());
		OPPINPUTXML.append("</CloseDate><LeadingBusiness__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getLeadingBusinessc());
		OPPINPUTXML.append("</LeadingBusiness__c><CountryOfDestination__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getCountryOfDestinationc());
		OPPINPUTXML.append("</CountryOfDestination__c><OpportunitySource__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getOpportunitySourcec());
		OPPINPUTXML.append("</OpportunitySource__c><RecordTypeId>");
		OPPINPUTXML.append(oppInputXMLDTO.getRecordTypeId());
		OPPINPUTXML.append("</RecordTypeId><OwnerId>");
		OPPINPUTXML.append(oppInputXMLDTO.getOwnerId());
		OPPINPUTXML.append("</OwnerId>");
		//OPPINPUTXML.append("<ProductLine>");
		String produlineObjects=getproductLineObjects(oppInputXMLDTO);
		OPPINPUTXML.append(produlineObjects);
		OPPINPUTXML.append("</OppDetails></OPPINPXML>");
		OPPINPUTXML.append("<STEP xmlns="+namespace+">");
		OPPINPUTXML.append(oppInputXMLDTO.getStep());
		OPPINPUTXML.append("</STEP>");
		OPPINPUTXML.append("</create_update_Acc_Opp_Proc>");
		
		
		String oppInputXML=OPPINPUTXML.toString();
		System.out.println("Final OppInputXML Is:"+oppInputXML);
		return OPPINPUTXML.toString(); 
	}
	
	
	public String getproductLineObjects(OppInputXMLDTO oppInputXMLDTO){
		
		StringBuilder sObjectsSB=null;
		//String techCommercialReferencec=null;
		//String quantityc=null;
		//String amountc=null;
		sObjectsSB=new StringBuilder();
		sObjectsSB.append("<ProductLine>");
		//for(int i=0;i<2;i++){
		/*List<BOM> bomList=new ArrayList<BOM>();
		BOM bom2=new BOM();
		BOM bom1=new BOM();
		bom2.setTotQty(1);
		bom2.setUnitCost(10);
		bom2.setProductId("EER21001");
		bom1.setTotQty(1);
		bom1.setUnitCost(10);
		bom1.setProductId("EER21001");
		bomList.add(bom1);
		bomList.add(bom2);
		for(BOM bom:bomList){*/
		for(BOM bom:oppInputXMLDTO.getBomsList()){
			
			/*techCommercialReferencec="EER21001";
			quantityc="1";
			amountc="10";*/
			//System.out.println(sObject);
			sObjectsSB.append("<sObjects><TECH_CommercialReference__c>");
			sObjectsSB.append(bom.getProductId());
			sObjectsSB.append("</TECH_CommercialReference__c><Quantity__c>");
			sObjectsSB.append(bom.getQuantity());
			sObjectsSB.append("</Quantity__c><Amount__c>");
			sObjectsSB.append(bom.getUnitCost());
			sObjectsSB.append("</Amount__c></sObjects>");
			
		}
		
		sObjectsSB.append("</ProductLine>");
		
		return sObjectsSB.toString();
	}
	
	public String updateInputXML(UpdateOppInputXMLDTO updateOppInputXMLDTO){
		StringBuilder UPDATEOPPINPUTXML=new StringBuilder();
		UPDATEOPPINPUTXML.append("<UpdateOpportunityV1  xmlns=");
		UPDATEOPPINPUTXML.append(updateNS+">");
		UPDATEOPPINPUTXML.append("OPPINPXML xmlns=");
		UPDATEOPPINPUTXML.append(namespace+">");
		UPDATEOPPINPUTXML.append("<id>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getId());
		UPDATEOPPINPUTXML.append("</id>");
		UPDATEOPPINPUTXML.append("</OPPINPXML>");
		UPDATEOPPINPUTXML.append("<STEP xmlns=");
		UPDATEOPPINPUTXML.append(namespace+">");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getStepValue());
		UPDATEOPPINPUTXML.append("</STEP>");
		UPDATEOPPINPUTXML.append("<Competitors xmlns=");
		if(updateOppInputXMLDTO.getCompititorsValue()!=null){
			UPDATEOPPINPUTXML.append(namespace+">");
			UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getCompititorsValue());
			UPDATEOPPINPUTXML.append("</Competitors>");
		}else{
		UPDATEOPPINPUTXML.append(namespace+"/>");
		}
		UPDATEOPPINPUTXML.append("<Events xmlns=");
		if(updateOppInputXMLDTO.getEvents()!=null){
			UPDATEOPPINPUTXML.append(namespace+">");
			UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getEvents());
			UPDATEOPPINPUTXML.append("</Events>");
		}else{
		UPDATEOPPINPUTXML.append(namespace+"/>");
		}
		UPDATEOPPINPUTXML.append("<ValueChainPlayer xmlns=");
		UPDATEOPPINPUTXML.append(namespace+">");
		UPDATEOPPINPUTXML.append("<Account__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getAcctC());
		UPDATEOPPINPUTXML.append("</Account__c>");
		UPDATEOPPINPUTXML.append("<AccountRole__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getAcctRoleC());
		UPDATEOPPINPUTXML.append("</AccountRole__c>");
		UPDATEOPPINPUTXML.append("<Contact__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getContactC());
		UPDATEOPPINPUTXML.append("</Contact__c>");
		UPDATEOPPINPUTXML.append("<ContactRole__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getContactRoleC());
		UPDATEOPPINPUTXML.append("</ContactRole__c>");
		UPDATEOPPINPUTXML.append("</ValueChainPlayer>");
		UPDATEOPPINPUTXML.append("<ProductLine xmlns=");
		if(updateOppInputXMLDTO.getProductLine()!=null){
			UPDATEOPPINPUTXML.append(namespace+">");
			UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getProductLine());
			UPDATEOPPINPUTXML.append("</ProductLine>");
		}else{
		UPDATEOPPINPUTXML.append(namespace+"/>");
		}
		UPDATEOPPINPUTXML.append("</UpdateOpportunityV1>");		
		System.out.println(UPDATEOPPINPUTXML.toString());
		
		return UPDATEOPPINPUTXML.toString(); 
	}

}

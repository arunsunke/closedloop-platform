package com.se.clm.bfo.util;

public class UpdateOppInputXMLDTO {
	
	private String id;
	private String stepValue;
	private String acctC;
	private String acctRoleC;
	private String contactC;
	private String contactRoleC;
	private String compititorsValue;
	private String events;
	private String productLine;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStepValue() {
		return stepValue;
	}
	public void setStepValue(String stepValue) {
		this.stepValue = stepValue;
	}
	public String getAcctC() {
		return acctC;
	}
	public void setAcctC(String acctC) {
		this.acctC = acctC;
	}
	public String getAcctRoleC() {
		return acctRoleC;
	}
	public void setAcctRoleC(String acctRoleC) {
		this.acctRoleC = acctRoleC;
	}
	public String getContactC() {
		return contactC;
	}
	public void setContactC(String contactC) {
		this.contactC = contactC;
	}
	public String getContactRoleC() {
		return contactRoleC;
	}
	public void setContactRoleC(String contactRoleC) {
		this.contactRoleC = contactRoleC;
	}
	public String getCompititorsValue() {
		return compititorsValue;
	}
	public void setCompititorsValue(String compititorsValue) {
		this.compititorsValue = compititorsValue;
	}
	public String getEvents() {
		return events;
	}
	public void setEvents(String events) {
		this.events = events;
	}
	public String getProductLine() {
		return productLine;
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	
	

}

package com.se.clm.mail;

public class SWTemplate4 {

	public static String getTemplate(String secA, String secB) {
		StringBuilder body = new StringBuilder();
		body.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		body.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		body.append("<head>");
		body.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
		body.append("<title>Untitled Document</title>");
		body.append("<style type=\"text/css\"> ");
		body.append(".Header {");
		body.append("font-family: Arial, Helvetica, sans-serif;");
		body.append("font-size: 24px;");
		body.append("}");
		body.append(".reference {");
		body.append("color: #3C0;");
		body.append("font-family: Arial, Helvetica, sans-serif;");
		body.append("font-size: 10px;");
		body.append("}");
		body.append(".reference {");
		body.append("font-family: Arial, Helvetica, sans-serif;");
		body.append("color: #3DCD58;");
		body.append("}");
		body.append("#Header span {");
		body.append("font-family: Arial, Helvetica, sans-serif;");
		body.append("}");
		body.append("#Main tr td table tr td table tr td table tr td table tr td table tr td {");
		body.append("font-family: Arial, Helvetica, sans-serif;");
		body.append("font-size: 14px;");
		body.append("color: #000000;");
		body.append("}");
		body.append("#border {");
		body.append("border-bottom-width: thick;");
		body.append("border-bottom-style: solid;");
		body.append("border-bottom-color: #32ad3c;");
		body.append("}");
		body.append("#Main tr td table tr td table tr td p {");
		body.append("font-family: Arial, Helvetica, sans-serif;");
		body.append("}");
		body.append("#Main tr td table tr td table tr td p {");
		body.append("font-size: 10px;");
		body.append("}");
		body.append("#Main tr td table tr td table tr td p {");
		body.append("font-size: 12px;");
		body.append("}");
		body.append("#Main tr td table tr td table tr td p {");
		body.append("font-size: 10px;");
		body.append("}");
		body.append("#Main tr td table tr td table tr td p span {");
		body.append("font-style: italic;");
		body.append("}");
		body.append(".link {");
		body.append("font-weight: bold;");
		body.append("}");
		body.append(".abutton {  font: bold 11px Arial;  text-decoration: none;  padding: 2px 6px 2px 6px; }");
		body.append(".wh{background-color:white;  border-top: 1px solid #3DCD58;  border-right: 1px solid #3DCD58;  border-bottom: 1px solid #3DCD58;  border-left: 1px solid #3DCD58;color:#3DCD58;padding: 6px 12px;}");
		body.append("</style>");
		body.append("</head>");
		body.append("<body>");
		body.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"Main\" style='font-family: Arial, Helvetica, sans-serif !important;'>");
		body.append("<tr>");
		body.append("<td align=\"left\" valign=\"top\" style=\"background-color: #3dcd58;\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append("<tr>");
		body.append("<td><table width=\"100%\" border=\"0\" style=\"\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #3dcd58;\">");
		body.append(" <tr>");
		body.append("<td>&nbsp;</td>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append("</tr>");
		body.append("<tr align=\"left\" valign=\"bottom\">");
		body.append("<td height=\"100\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#3DCD58\">");
		body.append("<tr align=\"left\" valign=\"center\">");
		body.append("<td style=\"border-bottom-color:#32ad3c;border-bottom-style:solid;border-bottom-width:thick\">&nbsp;</td>");
		body.append("<td width=\"600\" height=\"85\" bgcolor=\"#FFFFFF\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append("<tr>");
		body.append("<td width=\"20\">&nbsp;</td>");
		body.append("<td width=\"120\" height=\"85\"><img width=\"100px\" style=\"position: relative; top: -18px;\" src=\"cid:identifier103\" alt=\"\" border=\"0\" /></td>");
		body.append("<td width=\"20\">&nbsp;  </td>");
		body.append("<td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append("<tr>");
		body.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append(" <tr>");
		body.append("<td style=\"font-size: 24px\"><br />");
		body.append("faktura</td>");
		body.append("<td valign=\"bottom\" style=\"color: #3DCD58;\">Ordernummer:"+ secA + "<br /></td>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append("</tr>");
		body.append("<tr>");
		body.append("<td height=\"50\"><hr />");
		body.append("<p>&nbsp;</p>");
		body.append("<p>&nbsp;</p></td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append("<td style=\"border-bottom-color:#32ad3c;border-bottom-style:solid;border-bottom-width:thick\">&nbsp;</td>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append("</tr>");
		body.append("<tr>");
		body.append("<td height=\"200\" align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append("<tr align=\"left\" valign=\"bottom\">");
		body.append("<td>&nbsp;</td>");
		body.append("<td width=\"600\" height=\"400\" valign=\"top\" bgcolor=\"#FFFFFF\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append("<tr>");
		body.append("<td width=\"20\">&nbsp;</td>");
		body.append("<td>");
		body.append(secB);
		body.append("<br>");
		body.append("<p><a style=\"color: #3DCD58;\">Med v&auml;nliga h&auml;lsningar</a></p>");
		body.append("<p>&nbsp;</p>");
		body.append("<p><span style=\"color: #3DCD58; font-style: italic; font-size: 12px; font-family: Arial, Helvetica, sans-serif;\"><a  href=\"http://www.wiseup.se/\" style=\"color: rgb(0,0,255);\" text-decoration:none;>WiseUp</a></span></p>");
		body.append("<p>&nbsp;</p></td>");
		body.append(" <td width=\"20\">&nbsp;</td>");
		body.append(" </tr>");
		body.append("<tr>");
		body.append(" <td height=\"120\" bgcolor=\"#F1F1F1\">&nbsp;</td>");
		body.append(" <td height=\"80\" align=\"left\" valign=\"top\" bgcolor=\"#F1F1F1\"><p><br />");
		body.append("<span style=\"font-size: 10px; font-weight: bold;\"><u>Allm&auml;nna aff&auml;rsvillkor</u></span></p>");
		body.append("<p>De vid tiden f&ouml;r din best&auml;llning g&auml;llande allm&auml;nna aff&auml;rsvillkoren hittar du under f&ouml;ljande <a href=\"http://www.wiseup.se/affarsvillkor\">l&auml;nk</a></p>");
		body.append("<td height=\"120\" bgcolor=\"#F1F1F1\">&nbsp;</td>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append("<td>&nbsp;</td>");
		body.append("</tr>");
		body.append("</table></td>");
		body.append(" </tr>");
		body.append("<tr>");
		body.append(" <td height=\"200\" align=\"left\" valign=\"top\" style=\"background-color:#3DCD58\" width=\"100%\"><p>&nbsp;</p>");
		body.append("<table width=\"100%\" style=\"width:100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		body.append("<tr>");
		body.append("<td width=\"15%\"></td>");
		body.append(" <td width=\"30%\"><img width=\"100px\" src=\"cid:identifier102\" alt=\"\" border=\"0\" /></td>");
		body.append("<td width=\"15%\"></td>");
		body.append("<td width=\"20%\" style=\"color: #FFF; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">Legal</td>");
		body.append("<td width=\"20%\" style=\"color: #FFF; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\"><img width=\"20px\" src=\"cid:identifier101\" alt=\"\" border=\"0\" /></td>");
		body.append("<td>&nbsp;</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("<p>&nbsp;</p></td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</body>");
		body.append("</html>");

		return body.toString();

	}

	public static void main(String[] args) {
		SWTemplate4 template = new SWTemplate4();
		StringBuilder secB = new StringBuilder();
		secB.append("Hej" + " " + "Arun");
		secB.append("<br><br>Stort tack f&ouml;r din best&auml;llning " + "FIN"
				+ 101 + " " + "p&aring; wiseup.se.");
		secB.append("<br><br>Du kan se aktuell status f&ouml;r din best&auml;llning p&aring; WiseUp.se. En av v&aring;ra certifieradeelektriker kommer kontakta dig enligt dina &ouml;nskem&aring;l f&ouml;r att boka in en tid f&ouml;r installation.");
		secB.append("<br><br>N&auml;r ni har kommit &ouml;verens om ett installationsdatum kommer vi att skicka en orderbekr&auml;ftelse.</b> ");
		secB.append("<br><br>Best&auml;llningen &auml;r registrerad f&ouml;r");
		secB.append("<br><br><b>Kundens namn:</b>");
		secB.append("Arun");
		secB.append("<br><b>Adress:");
		secB.append("India");
		secB.append("<br><b>Postnummer:</b>" + "1000");
		secB.append("<br><b>stad</b>:" + "Bang");
		secB.append("<br>Din best&auml;llnings&ouml;versikt");
		/*
		 * for(BOM bom:boms) { secB.append("<br> Artikel:" +
		 * boms.get(0).getProductName()); secB.append("<br> Antal:" +
		 * boms.get(0).getQuantity()); secB.append("<br> summa:" +
		 * boms.get(0).getUnitCost()); }
		 */
		secB.append("<br> <br> Med v&auml;nliga h&auml;slningar");
		System.out.println(template.getTemplate("101", secB.toString()));

	}
}

package com.se.clm.bfo.util;

import java.util.ArrayList;
import java.util.List;

import com.se.cl.model.BOM;

public class TestClient {

	public static void main(String[] args) {/*
		// TODO Auto-generated method stub
		DocumentBuilderFactory builderFactory =
		        DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
		    builder = builderFactory.newDocumentBuilder();
		    
		    
		    Document doc=builder.newDocument();
		    Element root=doc.createElement("create_update_Acc_Opp_Proc");
		    System.out.println(doc.getFirstChild());
		    System.out.println(root.getTagName());
		    Node newChild=doc.createElement("OPPINPXML");;
			root.appendChild(newChild);
		    System.out.println(doc.toString());
		
		
		} catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}
		String OPPINPUTXML1="<OPPINPXML><AccountDetails><Salutation>Ms</Salutation><FirstName>PanimozhiTestAccount</FirstName><LastName>Sep182014</LastName>"+
				"<City__c>Bangalore</City__c><Country__c>IN</Country__c><ZipCode__c>787767</ZipCode__c><Street__c>rv</Street__c><PersonEmail>pavaithiru@gmail.com</PersonEmail>"+
				"<OwnerId>005A0000001Qe7t</OwnerId></AccountDetails><OppDetails><Name>Testingpavi</Name><IncludedInForecast__c>Yes</IncludedInForecast__c><CurrencyIsoCode>INR</CurrencyIsoCode>"+
				"<CloseDate>2015-08-18</CloseDate><LeadingBusiness__c>GS</LeadingBusiness__c><CountryOfDestination__c>IN</CountryOfDestination__c><OpportunitySource__c>Sales Rep</OpportunitySource__c>"+
				"<RecordTypeId>012A0000000nYNW</RecordTypeId><OwnerId>005A0000001Qe7t</OwnerId><ProductLine><sObjects><TECH_CommercialReference__c>EER21001</TECH_CommercialReference__c><Quantity__c>1</Quantity__c>"+
				"<Amount__c>10</Amount__c></sObjects><sObjects><TECH_CommercialReference__c>EER21001</TECH_CommercialReference__c><Quantity__c>1</Quantity__c><Amount__c>10</Amount__c></sObjects></ProductLine>"+
				"</OppDetails></OPPINPXML>";
		
		String prodList=null;
		String salutation=null;
		String firstName=null;
		String lastName=null;
		String cityC=null;
		String countryC=null;
		String zipCodeC=null;
		String streetC=null;
		String personalEMail=null;
		String ownerId=null;
		String oppName=null;
		String includedInForecastc=null;
		String currencyIsoCode=null;
		String closeDate=null;
		String leadingBusinessc=null;
		String countryOfDestinationc=null;
		String opportunitySourcec=null;
		String recordTypeId=null;
		String opOwnerId=null;
		
		salutation="Ms";
		firstName="PanimozhiTestAccount";
		lastName="Sep182014";
		cityC="Bangalore";
		countryC="IN";
		zipCodeC="787767";
		streetC="rv";
		personalEMail="pavaithiru@gmail.com";
		ownerId="005A0000001Qe7t";
		oppName="Testingpavi";
		includedInForecastc="Yes";
		currencyIsoCode="INR";
		closeDate="2015-08-18";
		leadingBusinessc="GS";
		countryOfDestinationc="IN";
		opportunitySourcec="Sales Rep";
		recordTypeId="012A0000000nYNW";
		opOwnerId="005A0000001Qe7t";
		
		
		StringBuilder OPPINPUTXML=new StringBuilder();
		OPPINPUTXML.append("<OPPINPXML><AccountDetails><Salutation>");
		OPPINPUTXML.append(salutation);
		OPPINPUTXML.append("</Salutation><FirstName>");
		OPPINPUTXML.append(firstName);
		OPPINPUTXML.append("</FirstName><LastName>");
		OPPINPUTXML.append(lastName);
		OPPINPUTXML.append("</LastName><City__c>");
		OPPINPUTXML.append(cityC);
		OPPINPUTXML.append("</City__c><Country__c>");
		OPPINPUTXML.append(countryC);
		OPPINPUTXML.append("</Country__c><ZipCode__c>");
		OPPINPUTXML.append(zipCodeC);
		OPPINPUTXML.append("</ZipCode__c><Street__c>");
		OPPINPUTXML.append(streetC);
		OPPINPUTXML.append("</Street__c><PersonEmail>");
		OPPINPUTXML.append(personalEMail);
		OPPINPUTXML.append("</PersonEmail><OwnerId>");
		OPPINPUTXML.append(ownerId);
		OPPINPUTXML.append("</OwnerId></AccountDetails><OppDetails><Name>");
		OPPINPUTXML.append(oppName);
		OPPINPUTXML.append("</Name><IncludedInForecast__c>");
		OPPINPUTXML.append(includedInForecastc);
		OPPINPUTXML.append("</IncludedInForecast__c><CurrencyIsoCode>");
		OPPINPUTXML.append(currencyIsoCode);
		OPPINPUTXML.append("</CurrencyIsoCode><CloseDate>");
		OPPINPUTXML.append(closeDate);
		OPPINPUTXML.append("</CloseDate><LeadingBusiness__c>");
		OPPINPUTXML.append(leadingBusinessc);
		OPPINPUTXML.append("</LeadingBusiness__c><CountryOfDestination__c>");
		OPPINPUTXML.append(countryOfDestinationc);
		OPPINPUTXML.append("</CountryOfDestination__c><OpportunitySource__c>");
		OPPINPUTXML.append(opportunitySourcec);
		OPPINPUTXML.append("</OpportunitySource__c><RecordTypeId>");
		OPPINPUTXML.append(recordTypeId);
		OPPINPUTXML.append("</RecordTypeId><OwnerId>");
		OPPINPUTXML.append(opOwnerId);
		OPPINPUTXML.append("</OwnerId>");
		OPPINPUTXML.append("<ProductLine>");
		
		System.out.println(OPPINPUTXML.toString());
		//List<String> produlineList=new ArrayList<String>();
		//List<String> sObjects=new ArrayList<String>();
		StringBuilder sObjectsSB=null;
		String techCommercialReferencec=null;
		String quantityc=null;
		String amountc=null;
		
		//for(String sObject:sObjects){
		for(int i=0;i<2;i++){
			sObjectsSB=new StringBuilder();
			techCommercialReferencec="EER21001";
			quantityc="1";
			amountc="10";
			//System.out.println(sObject);
			sObjectsSB.append("<sObjects><TECH_CommercialReference__c>");
			sObjectsSB.append(techCommercialReferencec);
			sObjectsSB.append("</TECH_CommercialReference__c><Quantity__c>");
			sObjectsSB.append(quantityc);
			sObjectsSB.append("</Quantity__c><Amount__c>");
			sObjectsSB.append(amountc);
			sObjectsSB.append("</Amount__c></sObjects>");
			
		}
		
		
		
		
		prodList=sObjectsSB.toString();
		OPPINPUTXML.append(prodList);
		OPPINPUTXML.append("</ProductLine>");
		OPPINPUTXML.append("</OppDetails></OPPINPXML>");
		
		String oppInputXML=OPPINPUTXML.toString();
		
		System.out.println(oppInputXML);
		
		
		
		
	*/
		
		 String salutation="Ms";
		 String firstName="PanimozhiTestAccount";
		 String lastName="Sep182014";
		 String cityC="Bangalore";
		 String countryC="IN";
		 String zipCodeC="787767";
		 String streetC="rv";
		 String personalEMail="pavaithiru@gmail.com";
		 String ownerId="005A0000001Qe7t";
		 String oppName="Testingpavi";
		 String includedInForecastc="Yes";
		 String currencyIsoCode="INR";
		 String closeDate="2015-08-18";
		 String leadingBusinessc="GS";
		 String countryOfDestinationc="IN";
		 String opportunitySourcec="Sales Rep";
		 String recordTypeId="012A0000000nYNW";
		 String opOwnerId="005A0000001Qe7t";
		 String step="1";
		
		 List<BOM> bomsList=new ArrayList<BOM>();
			BOM bom2=new BOM();
			BOM bom1=new BOM();
			bom2.setQuantity(1);;
			bom2.setUnitCost(10);
			bom2.setProductId("EER21001");
			bom1.setQuantity(1);
			bom1.setUnitCost(10);
			bom1.setProductId("EER21001");
			bomsList.add(bom1);
			bomsList.add(bom2);
		
		
		/* String techCommercialReferencec="EER21001";
		 String quantityc="1";
		 String amountc="10";*/
		 
		 
		 UtilBAck util=new UtilBAck();
			OppInputXMLDTO oppInputXMLDTO=new OppInputXMLDTO();
			oppInputXMLDTO.setSalutation(salutation);
			oppInputXMLDTO.setFirstName(firstName);
			oppInputXMLDTO.setLastName(lastName);
			oppInputXMLDTO.setCityC(cityC);
			oppInputXMLDTO.setStreetC(streetC);
			oppInputXMLDTO.setCountryC(countryC);
			oppInputXMLDTO.setZipCodeC(zipCodeC);
			oppInputXMLDTO.setPersonalEMail(personalEMail);
			oppInputXMLDTO.setOwnerId(ownerId);
			oppInputXMLDTO.setOppName(oppName);
			oppInputXMLDTO.setIncludedInForecastc(includedInForecastc);
			oppInputXMLDTO.setCurrencyIsoCode(currencyIsoCode);
			oppInputXMLDTO.setCloseDate(closeDate);
			oppInputXMLDTO.setLeadingBusinessc(leadingBusinessc);
			oppInputXMLDTO.setCountryOfDestinationc(countryOfDestinationc);
			oppInputXMLDTO.setOpportunitySourcec(opportunitySourcec);
			oppInputXMLDTO.setRecordTypeId(recordTypeId);
			oppInputXMLDTO.setOpOwnerId(opOwnerId);
			oppInputXMLDTO.setStep(step);
			oppInputXMLDTO.setBomsList(bomsList);
			
			String OppInputXML=util.getOppInputXML(oppInputXMLDTO);
			System.out.println(OppInputXML);
		
	}

}

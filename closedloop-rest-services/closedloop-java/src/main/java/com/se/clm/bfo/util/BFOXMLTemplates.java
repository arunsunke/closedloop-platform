package com.se.clm.bfo.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.se.cl.model.BOM;
import com.se.cl.model.Question;

public  class BFOXMLTemplates {


	private static Logger logger = LoggerFactory.getLogger(BFOXMLTemplates.class);	
	static String namespace="\"http://schemas.cordys.com/default\"";
	static String updateNS="\"http://schemas.cordys.com/bfoservices\"";
	
	public static  String getOppInputXML(OppInputXMLDTO oppInputXMLDTO){		
		
		StringBuilder OPPINPUTXML=new StringBuilder();
		OPPINPUTXML.append("<create_update_Acc_Opp_Proc xmlns=");
		OPPINPUTXML.append(namespace+">");
		OPPINPUTXML.append("<OPPINPXML xmlns="+namespace+"><AccountDetails><Salutation>");
		OPPINPUTXML.append(oppInputXMLDTO.getSalutation());
		OPPINPUTXML.append("</Salutation><FirstName>");
		OPPINPUTXML.append(oppInputXMLDTO.getFirstName());
		OPPINPUTXML.append("</FirstName><LastName>");
		OPPINPUTXML.append(oppInputXMLDTO.getLastName());
		OPPINPUTXML.append("</LastName><City__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getCityC());
		OPPINPUTXML.append("</City__c><Country__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getCountryC());
		OPPINPUTXML.append("</Country__c><ZipCode__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getZipCodeC());
		OPPINPUTXML.append("</ZipCode__c><Street__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getStreetC());
		OPPINPUTXML.append("</Street__c><PersonEmail>");
		OPPINPUTXML.append(oppInputXMLDTO.getPersonalEMail());
		OPPINPUTXML.append("</PersonEmail>");
		OPPINPUTXML.append("<PersonMobilePhone>");
		OPPINPUTXML.append(oppInputXMLDTO.getPhone());
		OPPINPUTXML.append("</PersonMobilePhone>");
		OPPINPUTXML.append("<OwnerId>");
		OPPINPUTXML.append(oppInputXMLDTO.getOwnerId());
		OPPINPUTXML.append("</OwnerId></AccountDetails><OppDetails><Name>");
		OPPINPUTXML.append(oppInputXMLDTO.getOppName());
		OPPINPUTXML.append("</Name><IncludedInForecast__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getIncludedInForecastc());
		OPPINPUTXML.append("</IncludedInForecast__c><CurrencyIsoCode>");
		OPPINPUTXML.append(oppInputXMLDTO.getCurrencyIsoCode());
		OPPINPUTXML.append("</CurrencyIsoCode><CloseDate>");
		OPPINPUTXML.append(oppInputXMLDTO.getCloseDate());
		OPPINPUTXML.append("</CloseDate><LeadingBusiness__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getLeadingBusinessc());
		OPPINPUTXML.append("</LeadingBusiness__c><CountryOfDestination__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getCountryOfDestinationc());
		OPPINPUTXML.append("</CountryOfDestination__c><OpportunitySource__c>");
		OPPINPUTXML.append(oppInputXMLDTO.getOpportunitySourcec());
		OPPINPUTXML.append("</OpportunitySource__c>");
		//04172015 as per Anh removed the record type id 
//		OPPINPUTXML.append("<RecordTypeId>");
//		OPPINPUTXML.append(oppInputXMLDTO.getRecordTypeId());
//		OPPINPUTXML.append("</RecordTypeId>");
		
		OPPINPUTXML.append("<OwnerId>");
		OPPINPUTXML.append(oppInputXMLDTO.getOwnerId());
		OPPINPUTXML.append("</OwnerId>");
		OPPINPUTXML.append("<MarketSegment__c>Residential</MarketSegment__c>");
		OPPINPUTXML.append("<ChannelType__c>Indirect</ChannelType__c>");
		
		//OPPINPUTXML.append("<ProductLine>");
		String produlineObjects=getproductLineObjects(oppInputXMLDTO);
		OPPINPUTXML.append(produlineObjects);
		
		//add Assessment questionnaire
		if(oppInputXMLDTO.getQuestions()!=null && oppInputXMLDTO.getQuestions().size()>0){
			String assessmentObj=getAssessmentObject(oppInputXMLDTO.getQuestions());
			OPPINPUTXML.append(assessmentObj);
		}
		OPPINPUTXML.append("</OppDetails></OPPINPXML>");
		OPPINPUTXML.append("<STEP xmlns="+namespace+">");
		OPPINPUTXML.append(oppInputXMLDTO.getStep());
		OPPINPUTXML.append("</STEP>");
		OPPINPUTXML.append("</create_update_Acc_Opp_Proc>");
		
		
		String oppInputXML=OPPINPUTXML.toString();
		logger.info("Final OppInputXML Is:"+oppInputXML);
		return OPPINPUTXML.toString(); 
	}
	
	
	/*private static String getAssessmentObject(List<Question> questions) {
		// TODO Auto-generated method stub
		<Assessment>
		<Assessment__c>a4lA0000000L7Lh</Assessment__c>
		<AssessmentResponse>
		  <sObjects xmlns="http://schemas.cordys.com/default">
		  <Question__c>a50A00000008PAK</Question__c>
		  <ResponseText__c>350euros</ResponseText__c>
		  <AnswerType>T</AnswerType>
		  </sObjects>
		<sObjects xmlns="http://schemas.cordys.com/default">
		  <Question__c>a50A00000008PAM</Question__c>
		  <ResponseText__c>3chambres </ResponseText__c>
		  <AnswerType>T</AnswerType>
		  </sObjects>
		 </AssessmentResponse>
	  </Assessment> 
		  
		StringBuilder sObjectsSB=null;
		sObjectsSB=new StringBuilder();
		sObjectsSB.append("<Assessment>");
		sObjectsSB.append("<Assessment__c>a4lA0000000L7Lh</Assessment__c>");
		sObjectsSB.append("<AssessmentResponse>");
		
		for(Question question:questions){
			
			sObjectsSB.append("<sObjects xmlns=\"http://schemas.cordys.com/default\">");
			sObjectsSB.append("<Question__c>"+question.getId()+"</Question__c>");
			sObjectsSB.append("<ResponseText__c>"+question.getValue()+"</ResponseText__c>");
			sObjectsSB.append("<AnswerType>T</AnswerType>");
			sObjectsSB.append("</sObjects>");
			
		}
		sObjectsSB.append("</AssessmentResponse>");
		sObjectsSB.append("</Assessment>");
	
		
		
		
		return sObjectsSB.toString();
	}*/
	
	
	private static String getAssessmentObject(List<Question> questions) {
		// TODO Auto-generated method stub
		/*<Assessment>
		<Assessment__c>a4lA0000000L7Lh</Assessment__c>
		<AssessmentResponse>
		  <sObjects xmlns="http://schemas.cordys.com/default">
		  <Question__c>a50A00000008PAK</Question__c>
		  <ResponseText__c>350euros</ResponseText__c>
		  <AnswerType>T</AnswerType>
		  </sObjects>
		<sObjects xmlns="http://schemas.cordys.com/default">
		  <Question__c>a50A00000008PAM</Question__c>
		  <ResponseText__c>3chambres </ResponseText__c>
		  <AnswerType>T</AnswerType>
		  </sObjects>
		 </AssessmentResponse>
	  </Assessment> 
		  */
		
		/*<Assessment__c>a4lA0000000L7Lh</Assessment__c> 
        <AssessmentResponse> 
          <sObjects xmlns="http://schemas.cordys.com/default"> 
          <Question__c>a50A00000008PAK</Question__c> 
            <ResponseText__c>350 </ResponseText__c> 
          <AnswerType>T</AnswerType> 
          </sObjects> 
        <sObjects xmlns="http://schemas.cordys.com/default"> 
          <Question__c>a50A00000008PAL</Question__c> 
                 <Answer__c>a4zA00000004Dea</Answer__c> 
          <AnswerType>NT</AnswerType> 
          </sObjects> 

        </AssessmentResponse> 
  </Assessment> 


	=> if answer is a free text, you only need to send the bFO ID of the question: 
	<Question__c>a50A00000008PAK</Question__c> 
	            <ResponseText__c>350 </ResponseText__c> 
	          <AnswerType>T</AnswerType> 
	
	=>If the answer is a picklist value, you will need to send the bFO ID of the question and of the answer: 
	<Question__c>a50A00000008PAL</Question__c> 
	                 <Answer__c>a4zA00000004Dea</Answer__c> 
	          <AnswerType>NT</AnswerType> 

		*/
		
		StringBuilder sObjectsSB=null;
		sObjectsSB=new StringBuilder();
		sObjectsSB.append("<Assessment>");
		sObjectsSB.append("<Assessment__c>a4lA0000000L7Lh</Assessment__c>");
		sObjectsSB.append("<AssessmentResponse>");
		
		for(Question question:questions)
		{
			
			if(question.getAnsType()!=null && question.getAnsType().equalsIgnoreCase("T"))
			{	
				sObjectsSB.append("<sObjects xmlns=\"http://schemas.cordys.com/default\">");
				sObjectsSB.append("<Question__c>"+question.getId()+"</Question__c>");
				sObjectsSB.append("<ResponseText__c>"+question.getValue()+"</ResponseText__c>");
				sObjectsSB.append("<AnswerType>"+question.getAnsType()+"</AnswerType>");
				sObjectsSB.append("</sObjects>");
			}else if(question.getAnsType()!=null && question.getAnsType().equalsIgnoreCase("NT"))
			{
				sObjectsSB.append("<sObjects xmlns=\"http://schemas.cordys.com/default\">");
				sObjectsSB.append("<Question__c>"+question.getId()+"</Question__c>");
				sObjectsSB.append("<Answer__c>"+question.getAnsId()+"</Answer__c>");
				sObjectsSB.append("<AnswerType>"+question.getAnsType()+"</AnswerType>");
				sObjectsSB.append("</sObjects>");				
			}
			
		}
		sObjectsSB.append("</AssessmentResponse>");
		sObjectsSB.append("</Assessment>");
		
		return sObjectsSB.toString();
	}

	public static String getproductLineObjects(OppInputXMLDTO oppInputXMLDTO){
		
		StringBuilder sObjectsSB=null;
		sObjectsSB=new StringBuilder();
		sObjectsSB.append("<ProductLine>");
	
		for(BOM bom:oppInputXMLDTO.getBomsList()){
			
		
			sObjectsSB.append("<sObjects><TECH_CommercialReference__c>");
			sObjectsSB.append(bom.getProductId());
			sObjectsSB.append("</TECH_CommercialReference__c><Quantity__c>");
			sObjectsSB.append(bom.getQuantity());
			sObjectsSB.append("</Quantity__c><Amount__c>");
			sObjectsSB.append(bom.getUnitCost());
			sObjectsSB.append("</Amount__c></sObjects>");
			
		}
		
		sObjectsSB.append("</ProductLine>");
		
		return sObjectsSB.toString();
	}
	
	public static String updateInputXML(UpdateOppInputXMLDTO updateOppInputXMLDTO){
		StringBuilder UPDATEOPPINPUTXML=new StringBuilder();
		UPDATEOPPINPUTXML.append("<UpdateOpportunityV1  xmlns=");
		UPDATEOPPINPUTXML.append(updateNS+">");
		UPDATEOPPINPUTXML.append("OPPINPXML xmlns=");
		UPDATEOPPINPUTXML.append(namespace+">");
		UPDATEOPPINPUTXML.append("<id>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getId());
		UPDATEOPPINPUTXML.append("</id>");
		UPDATEOPPINPUTXML.append("</OPPINPXML>");
		UPDATEOPPINPUTXML.append("<STEP xmlns=");
		UPDATEOPPINPUTXML.append(namespace+">");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getStepValue());
		UPDATEOPPINPUTXML.append("</STEP>");
		UPDATEOPPINPUTXML.append("<Competitors xmlns=");
		if(updateOppInputXMLDTO.getCompititorsValue()!=null){
			UPDATEOPPINPUTXML.append(namespace+">");
			UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getCompititorsValue());
			UPDATEOPPINPUTXML.append("</Competitors>");
		}else{
		UPDATEOPPINPUTXML.append(namespace+"/>");
		}
		UPDATEOPPINPUTXML.append("<Events xmlns=");
		if(updateOppInputXMLDTO.getEvents()!=null){
			UPDATEOPPINPUTXML.append(namespace+">");
			UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getEvents());
			UPDATEOPPINPUTXML.append("</Events>");
		}else{
		UPDATEOPPINPUTXML.append(namespace+"/>");
		}
		UPDATEOPPINPUTXML.append("<ValueChainPlayer xmlns=");
		UPDATEOPPINPUTXML.append(namespace+">");
		UPDATEOPPINPUTXML.append("<Account__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getAcctC());
		UPDATEOPPINPUTXML.append("</Account__c>");
		UPDATEOPPINPUTXML.append("<AccountRole__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getAcctRoleC());
		UPDATEOPPINPUTXML.append("</AccountRole__c>");
		UPDATEOPPINPUTXML.append("<Contact__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getContactC());
		UPDATEOPPINPUTXML.append("</Contact__c>");
		UPDATEOPPINPUTXML.append("<ContactRole__c>");
		UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getContactRoleC());
		UPDATEOPPINPUTXML.append("</ContactRole__c>");
		UPDATEOPPINPUTXML.append("</ValueChainPlayer>");
		UPDATEOPPINPUTXML.append("<ProductLine xmlns=");
		if(updateOppInputXMLDTO.getProductLine()!=null){
			UPDATEOPPINPUTXML.append(namespace+">");
			UPDATEOPPINPUTXML.append(updateOppInputXMLDTO.getProductLine());
			UPDATEOPPINPUTXML.append("</ProductLine>");
		}else{
		UPDATEOPPINPUTXML.append(namespace+"/>");
		}
		UPDATEOPPINPUTXML.append("</UpdateOpportunityV1>");		
		logger.info("BFO update opportunity : "+UPDATEOPPINPUTXML.toString());
		
		return UPDATEOPPINPUTXML.toString(); 
	}
	
	public static String stepOneUpdateResXML(String id,String AcctC,String AcctRoleC,String contactC,String contactRolec){
		StringBuilder stepOneUpdateResXML=new StringBuilder();
		
		String NS="\"http://schemas.cordys.com/bfoservices\"";
		String defaultNS="\"http://schemas.cordys.com/default\"";
		String stepOneID="1";
		stepOneUpdateResXML.append("<UpdateOpportunityV1  xmlns=");
		stepOneUpdateResXML.append(NS+">");
		stepOneUpdateResXML.append("<OPPINPXML xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		if(id!=null){
		stepOneUpdateResXML.append("<id>");
		stepOneUpdateResXML.append(id);
		stepOneUpdateResXML.append("</id>");
		}else{
			stepOneUpdateResXML.append("<id/>");
		}
		stepOneUpdateResXML.append("</OPPINPXML>");
		stepOneUpdateResXML.append("<STEP xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append(stepOneID);
		stepOneUpdateResXML.append("</STEP>");
		stepOneUpdateResXML.append("<ValueChainPlayer xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append("<Account__c>");
		stepOneUpdateResXML.append(AcctC);
		stepOneUpdateResXML.append("</Account__c>");
		stepOneUpdateResXML.append("<AccountRole__c>");
		stepOneUpdateResXML.append(AcctRoleC);
		stepOneUpdateResXML.append("</AccountRole__c>");
		stepOneUpdateResXML.append("<Contact__c>");
		stepOneUpdateResXML.append(contactC);
		stepOneUpdateResXML.append("</Contact__c>");
		stepOneUpdateResXML.append("<ContactRole__c>");
		stepOneUpdateResXML.append(contactRolec);
		stepOneUpdateResXML.append("</ContactRole__c>");
		stepOneUpdateResXML.append("</ValueChainPlayer>");
		stepOneUpdateResXML.append("</UpdateOpportunityV1>");
		
		
		logger.info("BFO step1 :"+stepOneUpdateResXML.toString());
		
		return stepOneUpdateResXML.toString();
	}
	
	public static Object stepOneUpdateResJSON(String id,String AcctC,String AcctRoleC,String contactC,String contactRolec){
		
		Map<String,Object> stepOneUpdateJson=new HashMap<String,Object>();	
		try 
		{
			HashMap<String,Object> opportunitydata = new HashMap<String,Object>();
			HashMap<String,Object> opportunity = new HashMap<String,Object>();

			HashMap<String,String>  newValueChainPlayer = new HashMap<String,String>();
			newValueChainPlayer.put("seAccountId",AcctC);  //AcctC	
			newValueChainPlayer.put("accountRole",AcctRoleC);
			newValueChainPlayer.put("seContactId",contactC);  //contactC
			newValueChainPlayer.put("contactRole",contactRolec);
			newValueChainPlayer.put("isLeadingPartner","true");		
			HashMap<String,HashMap<String,String>> valueChainPlayers = new  HashMap<String ,HashMap<String,String>>();		
			valueChainPlayers.put("newValueChainPlayer", newValueChainPlayer)	;
			opportunitydata.put("stage", "4 - Influence & Develop");
			opportunitydata.put("valueChainPlayers",valueChainPlayers);
			opportunity.put("opportunity",opportunitydata);
			stepOneUpdateJson.put("closedLoopOpportunity",opportunity);			
			
		}catch(Exception e) 
		{
			logger.info("Exception :"+e.getMessage());
		}
		return stepOneUpdateJson;
			
		
	}
	
	public static String stepTwoUpdateResXML(String id,String ownerID,String subject,XMLGregorianCalendar startDtTime,XMLGregorianCalendar endDtTime,String whoID){
		StringBuilder stepOneUpdateResXML=new StringBuilder();
		
		String NS="\"http://schemas.cordys.com/bfoservices\"";
		String defaultNS="\"http://schemas.cordys.com/default\"";
		String stepTwoID="2";
		stepOneUpdateResXML.append("<UpdateOpportunityV1  xmlns=");
		stepOneUpdateResXML.append(NS+">");
		stepOneUpdateResXML.append("<OPPINPXML xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		if(id!=null){
		stepOneUpdateResXML.append("<id>");
		stepOneUpdateResXML.append(id);
		stepOneUpdateResXML.append("</id>");
		}else{
			stepOneUpdateResXML.append("<id/>");
		}
		stepOneUpdateResXML.append("</OPPINPXML>");
		stepOneUpdateResXML.append("<STEP xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append(stepTwoID);
		stepOneUpdateResXML.append("</STEP>");
		stepOneUpdateResXML.append("<Events xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append("<OwnerId>");
		stepOneUpdateResXML.append(ownerID);
		stepOneUpdateResXML.append("</OwnerId>");
		stepOneUpdateResXML.append("<Subject>");
		stepOneUpdateResXML.append(subject);
		stepOneUpdateResXML.append("</Subject>");
		stepOneUpdateResXML.append("<StartDateTime>");
		stepOneUpdateResXML.append(startDtTime);
		stepOneUpdateResXML.append("</StartDateTime>");
		stepOneUpdateResXML.append("<EndDateTime>");
		stepOneUpdateResXML.append(endDtTime);
		stepOneUpdateResXML.append("</EndDateTime>");
		stepOneUpdateResXML.append("<WhoId>");
		stepOneUpdateResXML.append(whoID);
		stepOneUpdateResXML.append("</WhoId>");
		stepOneUpdateResXML.append("<Status__c>planned</Status__c>");
		stepOneUpdateResXML.append("</Events>");
		stepOneUpdateResXML.append("</UpdateOpportunityV1>");
		logger.info("BFO Step two :"+stepOneUpdateResXML.toString());
		
		return stepOneUpdateResXML.toString();
	}
	
	//public static Object stepTwoUpdateResJSON(String id,String ownerID,String subject,XMLGregorianCalendar startDtTime,XMLGregorianCalendar endDtTime,String whoID){
	public static Object stepTwoUpdateResJSON(String id,String ownerID,String subject,String startDtTime,String endDtTime,String whoID){

		//ownerID="SESA337713";
		Map<String,Object> stepTwoUpdateJson=new HashMap<String,Object>();	
		try 
		{
			HashMap<String,Object> opportunity = new HashMap<String,Object>();
			HashMap<String,Object> opportunitydata = new HashMap<String,Object>();
			HashMap<String,Object>  newCustomerVisit = new HashMap<String,Object>();
			newCustomerVisit.put("assignedTo",ownerID);	
			newCustomerVisit.put("subject",subject);
			newCustomerVisit.put("startDate",startDtTime);
			newCustomerVisit.put("endDate",endDtTime);
			newCustomerVisit.put("status","Planned");		
			newCustomerVisit.put("seContactId",whoID);		

			HashMap<String,HashMap<String,Object>> customerVisit = new  HashMap<String ,HashMap<String,Object>>();		
			customerVisit.put("newCustomerVisit", newCustomerVisit)	;
			//opportunitydata.put("stage", "3 - Identify & Qualify");
			opportunitydata.put("customerVisit",customerVisit);
			opportunity.put("opportunity", opportunitydata);
			stepTwoUpdateJson.put("closedLoopOpportunity",opportunity);			
			
		}catch(Exception e) 
		{
			logger.info("Exception in stepTwoUpdateResJSON :"+e.getMessage());
		}
		return stepTwoUpdateJson;
			
	}
	
	public static Object stepTwoUpdateResJSONInstallation(String id,String ownerID,String subject,String startDtTime,String endDtTime,String whoID,int recCount,String bfoCustomerAccId,String bfoCustVisitId)
	{

		//ownerID="SESA337713";
		Map<String,Object> stepTwoUpdateJson=new HashMap<String,Object>();	
		try 
		{
			HashMap<String,Object> opportunity = new HashMap<String,Object>();
			HashMap<String,Object> opportunitydata = new HashMap<String,Object>();
			HashMap<String,Object>  newCustomerVisit = new HashMap<String,Object>();
			HashMap<String,HashMap<String,Object>> customerVisit = new  HashMap<String ,HashMap<String,Object>>();		
			HashMap<String,Object>  updateCustomerVisit = new HashMap<String,Object>();

			if(recCount==1)
			{	
				newCustomerVisit.put("assignedTo",ownerID);	
				newCustomerVisit.put("subject",subject);
				newCustomerVisit.put("startDate",startDtTime);
				newCustomerVisit.put("endDate",endDtTime);
				newCustomerVisit.put("status","Planned");		
				newCustomerVisit.put("seContactId",whoID);		
				customerVisit.put("newCustomerVisit", newCustomerVisit)	;
			}else if(recCount>1)	
			{
				updateCustomerVisit.put("id",bfoCustVisitId);	
				updateCustomerVisit.put("assignedTo",ownerID);	
				updateCustomerVisit.put("subject",subject);
				updateCustomerVisit.put("startDate",startDtTime);
				updateCustomerVisit.put("endDate",endDtTime);
				updateCustomerVisit.put("status","Rescheduled");		
				updateCustomerVisit.put("seContactId",whoID);		
				customerVisit.put("updateCustomerVisit", updateCustomerVisit);
				
			}
			
			opportunitydata.put("customerVisit",customerVisit);
			opportunity.put("opportunity", opportunitydata);
			stepTwoUpdateJson.put("closedLoopOpportunity",opportunity);			
			
		}catch(Exception e) 
		{
			logger.info("Exception in stepTwoUpdateResJSONInstallation :"+e.getMessage());
		}
		return stepTwoUpdateJson;
			
	}
	
	public static String stepThreeUpdateResXML(String id,String quoteSubmissionDtC){
		StringBuilder stepOneUpdateResXML=new StringBuilder();
		
		String NS="\"http://schemas.cordys.com/bfoservices\"";
		String defaultNS="\"http://schemas.cordys.com/default\"";
		String stepthreeID="3";
		stepOneUpdateResXML.append("<UpdateOpportunityV1  xmlns=");
		stepOneUpdateResXML.append(NS+">");
		stepOneUpdateResXML.append("<OPPINPXML xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		if(id!=null){
		stepOneUpdateResXML.append("<id>");
		stepOneUpdateResXML.append(id);
		stepOneUpdateResXML.append("</id>");
		}else{
			stepOneUpdateResXML.append("<id/>");
		}
		
		stepOneUpdateResXML.append("<QuoteSubmissionDate__c>");
		stepOneUpdateResXML.append(quoteSubmissionDtC);
		stepOneUpdateResXML.append("</QuoteSubmissionDate__c>");		
		
		stepOneUpdateResXML.append("</OPPINPXML>");
		stepOneUpdateResXML.append("<STEP xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append(stepthreeID);
		stepOneUpdateResXML.append("</STEP>");
		stepOneUpdateResXML.append("</UpdateOpportunityV1>");
		
		
		logger.info("BFO Step 3:"+stepOneUpdateResXML.toString());
		
		return stepOneUpdateResXML.toString();
	}
	
	
	public static Object stepThreeUpdateResJSON(String id,String quoteSubmissionDtC)
	{	
		Map<String,Object> stepThreeUpdateJson=new HashMap<String,Object>();	
		try
		{		
			HashMap<String,Object> opportunity = new HashMap<String,Object>();
			HashMap<String,String> opportunitydata = new HashMap<String,String>();
			opportunitydata.put("stage","6 - Negotiate & Win");
			opportunitydata.put("quotationSubmissionDate",quoteSubmissionDtC );
			opportunity.put("opportunity", opportunitydata);
			stepThreeUpdateJson.put("closedLoopOpportunity",opportunity);			
		}catch(Exception e)
		{
			logger.info("Exception in stepThreeUpdateResJSON :"+e.getMessage());
		}
		
		return stepThreeUpdateJson;
	}
	
	
	public static String stepFourUpdateResXML(String id,String reasonC,String currencyISOCode,String techCompititorsNameC){
		StringBuilder stepOneUpdateResXML=new StringBuilder();
		
		String NS="\"http://schemas.cordys.com/bfoservices\"";
		String defaultNS="\"http://schemas.cordys.com/default\"";
		String stepFourID="4";
		stepOneUpdateResXML.append("<UpdateOpportunityV1  xmlns=");
		stepOneUpdateResXML.append(NS+">");
		stepOneUpdateResXML.append("<OPPINPXML xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		if(id!=null){
		stepOneUpdateResXML.append("<id>");
		stepOneUpdateResXML.append(id);
		stepOneUpdateResXML.append("</id>");
		}else{
			stepOneUpdateResXML.append("<id/>");
		}
		
		stepOneUpdateResXML.append("<Reason__c>");
		stepOneUpdateResXML.append(reasonC);
		stepOneUpdateResXML.append("</Reason__c>");		
		
		stepOneUpdateResXML.append("</OPPINPXML>");
		stepOneUpdateResXML.append("<STEP xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append(stepFourID);
		stepOneUpdateResXML.append("</STEP>");
		stepOneUpdateResXML.append("<CurrencyIsoCode>");
		stepOneUpdateResXML.append(currencyISOCode);
		stepOneUpdateResXML.append("</CurrencyIsoCode>");
		stepOneUpdateResXML.append("<Competitors xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append("<TECH_CompetitorName__c>");
		stepOneUpdateResXML.append(techCompititorsNameC);
		stepOneUpdateResXML.append("</TECH_CompetitorName__c>");
		stepOneUpdateResXML.append("<OtherName__c>Other</OtherName__c>");
		stepOneUpdateResXML.append("</Competitors>");		
		stepOneUpdateResXML.append("</UpdateOpportunityV1>");
				
		logger.info("BFO Step four:"+stepOneUpdateResXML.toString());
		
		return stepOneUpdateResXML.toString();
	}
	
	
	public static Object stepFourUpdateResJSON(String id,String reasonC,String currencyISOCode,String techCompititorsNameC)
	{	
		
		Map<String,Object> stepFourUpdateJson=new HashMap<String,Object>();	
		try 
		{
			HashMap<String,Object> opportunitydata= new HashMap<String,Object>();
			HashMap<String,Object> opportunity= new HashMap<String,Object>();
			HashMap<String,String> competitor= new HashMap<String,String>();
			competitor.put("name","others");	
			competitor.put("winner","true");			
			opportunitydata.put("stage", "0 - Closed");
			opportunitydata.put("reason", reasonC);
			opportunitydata.put("status", "Lost");
			opportunitydata.put("competitor",competitor);			
			opportunity.put("opportunity",opportunitydata);			
			stepFourUpdateJson.put("closedLoopOpportunity",opportunity);			
			
		}catch(Exception e) 
		{
			logger.info("Exception in stepFourUpdateResJSON :"+e.getMessage());
		}
		return stepFourUpdateJson ;
		
	}
	
	public static String stepFiveUpdateResXML(String id){
		StringBuilder stepOneUpdateResXML=new StringBuilder();
		
		String NS="\"http://schemas.cordys.com/bfoservices\"";
		String defaultNS="\"http://schemas.cordys.com/default\"";
		String stepFiveID="5";
		stepOneUpdateResXML.append("<UpdateOpportunityV1  xmlns=");
		stepOneUpdateResXML.append(NS+">");
		stepOneUpdateResXML.append("<OPPINPXML xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		if(id!=null){
		stepOneUpdateResXML.append("<id>");
		stepOneUpdateResXML.append(id);
		stepOneUpdateResXML.append("</id>");
		}else{
			stepOneUpdateResXML.append("<id/>");
		}
		
		stepOneUpdateResXML.append("</OPPINPXML>");
		stepOneUpdateResXML.append("<STEP xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append(stepFiveID);
		stepOneUpdateResXML.append("</STEP>");
		stepOneUpdateResXML.append("</UpdateOpportunityV1>");
		
		
		logger.info("BFO Step 5:"+stepOneUpdateResXML.toString());
		
		return stepOneUpdateResXML.toString();
	}
	
	public static Object stepFiveUpdateResJSON(String id,String bfoValueChainPlayer){
		Map<String,Object> stepFiveUpdateJson=new HashMap<String,Object>();	
		try 
		{
			HashMap<String,Object> opportunity= new HashMap<String,Object>();
			HashMap<String,Object> valueChainPlayer= new HashMap<String,Object>();
			HashMap<String,String> winningValueChainPlayer= new HashMap<String,String>();
			HashMap<String,Object> opportunitydata= new HashMap<String,Object>();
			winningValueChainPlayer.put("seReference", bfoValueChainPlayer);
			valueChainPlayer.put("winningValueChainPlayer", winningValueChainPlayer);
			opportunitydata.put("valueChainPlayer", valueChainPlayer);
			opportunitydata.put("stage", "7 - Deliver & Validate");			
			opportunitydata.put("status","Won - Deliver & Validate by Partner");			
			opportunity.put("opportunity", opportunitydata);
			stepFiveUpdateJson.put("closedLoopOpportunity",opportunity);			
			
		}catch(Exception e) 
		{
			logger.info("Exception in stepFiveUpdateJson :"+e.getMessage());
		}
		return stepFiveUpdateJson;
		
	}
	
	public static String stepSixUpdateResXML(String id,String reason){
		StringBuilder stepOneUpdateResXML=new StringBuilder();
		
		String NS="\"http://schemas.cordys.com/bfoservices\"";
		String defaultNS="\"http://schemas.cordys.com/default\"";
		String stepSixID="6";
		stepOneUpdateResXML.append("<UpdateOpportunityV1  xmlns=");
		stepOneUpdateResXML.append(NS+">");
		stepOneUpdateResXML.append("<OPPINPXML xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		if(id!=null){
			stepOneUpdateResXML.append("<id>");
			stepOneUpdateResXML.append(id);
			stepOneUpdateResXML.append("</id>");
		}else{
			stepOneUpdateResXML.append("<id/>");
		}
		if(reason!=null && reason.length()>0){
			stepOneUpdateResXML.append("<Reason__c>");
			stepOneUpdateResXML.append(reason);
			stepOneUpdateResXML.append("</Reason__c>");
		}else{
			stepOneUpdateResXML.append("<Reason__c>");
			stepOneUpdateResXML.append("Administration - Customer changed mind and project cancelled");
			stepOneUpdateResXML.append("</Reason__c>");
		}
		 
		stepOneUpdateResXML.append("</OPPINPXML>");
		stepOneUpdateResXML.append("<STEP xmlns=");
		stepOneUpdateResXML.append(defaultNS+">");
		stepOneUpdateResXML.append(stepSixID);
		stepOneUpdateResXML.append("</STEP>");
		stepOneUpdateResXML.append("</UpdateOpportunityV1>");
		
		
		logger.info("BFO Step 6: "+stepOneUpdateResXML.toString());
		
		return stepOneUpdateResXML.toString();
	}
	
	
	public static Object stepSixUpdateResJSON(String id,String reason){
		Map<String,Object> stepSixUpdateJson=new HashMap<String,Object>();	
		try 
		{
			HashMap<String,Object> opportunity= new HashMap<String,Object>();
			HashMap<String,String> opportunitydata= new HashMap<String,String>();
			opportunitydata.put("stage", "0 - Closed");
			if(reason!=null && reason.length()>0)
			{	
				opportunitydata.put("reason",reason);
			}
			else
			{
				opportunitydata.put("reason","Administration - Customer changed mind and project cancelled");
			}
			opportunitydata.put("status","Cancelled by Customer");
			opportunity.put("opportunity", opportunitydata);
			stepSixUpdateJson.put("closedLoopOpportunity",opportunity);
		}catch(Exception e)
		{
			logger.info("Exception in stepSixUpdateResJson :"+e.getMessage());

		}
		
		return stepSixUpdateJson;
	}


}

package com.se.clm.bfo.manager.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.se.cl.constants.BFOStatus;
import com.se.cl.constants.OppStatus;
import com.se.cl.dao.OpportunityBFODAO;
import com.se.cl.model.Account;
import com.se.cl.model.AccountDetails;
import com.se.cl.model.Assessment;
import com.se.cl.model.BFOOpportunity;
import com.se.cl.model.BOM;
import com.se.cl.model.ClosedLoopOpportunity;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityBFOHistory;
import com.se.cl.model.OpportunityLine;
import com.se.cl.model.Owner;
import com.se.cl.model.PhysicalAddress;
import com.se.cl.model.Question;
import com.se.cl.model.Response;
import com.se.clm.bfo.manager.BFOManager;
import com.se.clm.bfo.util.BFOXMLTemplates;
import com.se.clm.bfo.util.OppInputXMLDTO;
@Component("SwedenBFO")
public class SEBFOManagerImpl implements BFOManager{

	@Autowired
	OpportunityBFODAO opportunityBFODAO;
	
	private @Value("${bfo.fr.ownerId}") String ownerId;
	private @Value("${compititor.id}") String compititorId;
	private @Value("${bfo.whoId}") String whoId;
	private @Value("${bfo.rec.type.id}") String recordTypeId;
	
	private @Value("${ifw.fr.ownerId}") String ifwownerId;
		
	private static Logger logger = LoggerFactory.getLogger(SEBFOManagerImpl.class);		
	private static final String COUNTRY_CODE="SW";
	
	@Override
	public Map<String,String> getOpportunityRequest() {
		
		List<Opportunity> oppList=opportunityBFODAO.getPendinOpportunities(COUNTRY_CODE);
		Map<String,String> requestOppXMLs=new HashMap<String,String>();
		for(Opportunity opp:oppList){
			String reqXML=getRequestXML(opp);
			if (reqXML!=null && reqXML.length()>0){
				requestOppXMLs.put(opp.getId(),reqXML);
			}
		}
		
		return requestOppXMLs;
	}
	
	@Override
	public Map<String,Object> getOpportunityRequestIF() {
		
		List<Opportunity> oppList=opportunityBFODAO.getPendinOpportunitiesIF(COUNTRY_CODE);
		Map<String,Object> requestOppJson=new HashMap<String,Object>();
		for(Opportunity opp:oppList)
		{
			Object reqJson=getBFORequestJson(opp);
			if (reqJson!=null)			
			{
				logger.info("Opp Id:::"+ opp.getId() +"reqJson Data:"+reqJson);
				requestOppJson.put(opp.getId(),reqJson);
			}
		}
		
		return requestOppJson;
	}
	
	
	
	private String getRequestXML(Opportunity opp){
		String OppInputXML="";
		try{
			 	
				OppInputXMLDTO oppInputXMLDTO=new OppInputXMLDTO();
				oppInputXMLDTO.setSalutation("Mr");
				oppInputXMLDTO.setFirstName(opp.getCustomerDetails().getFirstName());
				oppInputXMLDTO.setLastName(opp.getCustomerDetails().getLastName());
				oppInputXMLDTO.setCityC(opp.getCustomerDetails().getAddress().getCity());
				oppInputXMLDTO.setStreetC(opp.getCustomerDetails().getAddress().getSt_name());
				oppInputXMLDTO.setCountryC(opp.getCustomerDetails().getAddress().getCountryCode());
				oppInputXMLDTO.setZipCodeC(opp.getCustomerDetails().getAddress().getZip());
				oppInputXMLDTO.setPersonalEMail(opp.getCustomerDetails().getEmail());
				oppInputXMLDTO.setPhone(opp.getCustomerDetails().getPhone());
				oppInputXMLDTO.setOwnerId(ownerId); //hard coded
				oppInputXMLDTO.setOppName(opp.getTitle());
				oppInputXMLDTO.setIncludedInForecastc("Yes");
				oppInputXMLDTO.setCurrencyIsoCode(opp.getBoms().get(0).getCurrencyCode());
				 SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			     Date date = dt.parse(opp.getOpportunityDate());
			     SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
			     //adding 30days to opportunity as discussion Anh
			     Calendar c = Calendar.getInstance();
			     c.setTime(date); 
			     c.add(Calendar.DATE, 30); 
			    oppInputXMLDTO.setCloseDate(dt1.format(c.getTime()));
				oppInputXMLDTO.setLeadingBusinessc("Partner");
				oppInputXMLDTO.setCountryOfDestinationc(opp.getAddress().getCountryCode());
				oppInputXMLDTO.setOpportunitySourcec("Website");
				oppInputXMLDTO.setRecordTypeId(recordTypeId);
				oppInputXMLDTO.setOpOwnerId(ownerId);
				oppInputXMLDTO.setStep("0");
				oppInputXMLDTO.setBomsList(opp.getBoms());
				oppInputXMLDTO.setQuestions(opp.getQuestions());
				
				 OppInputXML=BFOXMLTemplates.getOppInputXML(oppInputXMLDTO);
				 logger.info("BFO Opp Create XML:"+OppInputXML);
		
			}catch(Exception e){
				e.printStackTrace();
			}
			
		return OppInputXML;
	}
	
	
	
	private HashMap<String,Object> getBFORequestJson(Opportunity opp){

    	ClosedLoopOpportunity closedLoopOpportunity = new ClosedLoopOpportunity();
    	HashMap<String,Object> responseData = new   HashMap<String,Object>();
    	    	
	try{
		 	
		    Account account = new Account();
		    AccountDetails accountDetails = new AccountDetails();
		    PhysicalAddress physicalAddress = new PhysicalAddress();
		    Owner owner = new Owner();
		    
		    //BFOOpportunity opportunity = new BFOOpportunity();
		    HashMap<String ,Object> opportunitydetails = new HashMap<String,Object>();
		    accountDetails.setSalutation("Mr.");
		    accountDetails.setFirstName(opp.getCustomerDetails().getFirstName());
		    accountDetails.setLastName(opp.getCustomerDetails().getLastName());
		    accountDetails.setMobilePhone(opp.getCustomerDetails().getPhone());
		    owner.setSesaId(ifwownerId);
		    accountDetails.setOwner(owner);
		    physicalAddress.setCity(opp.getCustomerDetails().getAddress().getCity());
		    physicalAddress.setZipCode(opp.getCustomerDetails().getAddress().getZip());
		    physicalAddress.setStreet(opp.getCustomerDetails().getAddress().getSt_name());
		    physicalAddress.setCountryCode(opp.getCustomerDetails().getAddress().getCountryCode());
		    accountDetails.setPhysicalAddress(physicalAddress);
		    account.setAccountDetails(accountDetails);
		    
		    //opportunity.setName(opp.getTitle());
		    opportunitydetails.put("name", opp.getTitle());
		    //opportunity.setOwner(owner);
		    opportunitydetails.put("owner", owner);
		    
		    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    Date date = dt.parse(opp.getOpportunityDate());
		    SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		     //adding 30days to opportunity as discussion Anh
		     Calendar c = Calendar.getInstance();
		     c.setTime(date); 
		     c.add(Calendar.DATE, 30); 
		    //opportunity.setCloseDate(dt1.format(c.getTime()));
		    opportunitydetails.put("closeDate",dt1.format(c.getTime()));
		    //opportunity.setCountryCode(opp.getCustomerDetails().getAddress().getCountryCode());
		    opportunitydetails.put("countryCode",opp.getCustomerDetails().getAddress().getCountryCode());
		    //opportunity.setLeadingBusiness("Partner");
		    opportunitydetails.put("leadingBusiness","Partner");
		    //opportunity.setMarketSegment("Residential");
		    opportunitydetails.put("marketSegment","Residential");
		    //opportunity.setChannel("Indirect");
		    opportunitydetails.put("channel","Indirect");
		    //opportunity.setCurrencyCode("EUR");
		    opportunitydetails.put("currencyCode","SEK");
		    //opportunity.setSource("Website");
		    opportunitydetails.put("source","Website");
		    Assessment assesment = new Assessment();
		    List<Question> AssessmentList =  opp.getQuestions();
		    //logger.debug("AssessmentList.size()"+AssessmentList.size());
		    
		    if(AssessmentList!=null && AssessmentList.size()>0)
		    {	
			    assesment.setName("Wiser Configurator");
		    	List<HashMap<String,String>> responseArrayList = new ArrayList<HashMap<String,String>>();
		    	HashMap<String,String> response =null ; //new HashMap<String,String>();
		    	for(Question assessList: AssessmentList)
			    {	                //Response response = new Response();
			    	if(assessList.getAnsType()!=null && assessList.getAnsType().equalsIgnoreCase("NT"))
		            {
		                	if((assessList.getId()!=null && !assessList.getId().equalsIgnoreCase("null")) && (assessList.getAnsId()!=null && !assessList.getAnsId().equalsIgnoreCase("null")))
		                	{		
		    			    	response = new HashMap<String,String>();
		                		response.put("question", assessList.getId());
		                		response.put("answerOption",assessList.getAnsId());
		                		logger.info("question"+assessList.getId());
			                	logger.info("answerOption"+assessList.getAnsId());	                	
		                	}
		            
		             }else
		                {
		                	if(assessList.getId()!=null && !assessList.getId().equalsIgnoreCase("null"))
		                	{	
		    			    	response = new HashMap<String,String>();
		                		response.put("question", assessList.getId());
		                		response.put("answerText",assessList.getValue());
		                		logger.info("question"+assessList.getId());
			                	logger.info("answerText"+assessList.getValue());	               	
	 	                		
		               	}

		             }
			    	responseArrayList.add(response);
			    }
			    assesment.setResponse(responseArrayList);			    
			    opportunitydetails.put("assessment", assesment);
		    }
		    
		    if(opp.getOppSourceType()!=null && opp.getOppSourceType().equalsIgnoreCase("CL-MOB"))
		    {
			    
    			List<HashMap<String,String>> bomsArrayListProductLine = new ArrayList<HashMap<String,String>>();
			    HashMap<String,String> ProductLineMap = new HashMap<String,String>();
				ProductLineMap.put("productLine", "PTLRC - LIGHT AND ROOM CONTROL");
		    	ProductLineMap.put("unitAmount", "1.0");
		    	ProductLineMap.put("quantity", "1");
			    bomsArrayListProductLine.add(ProductLineMap);
				opportunitydetails.put("opportunityLine",bomsArrayListProductLine);
			 }else
			 {
		    	List<BOM> bomList  = new ArrayList<BOM>();			    
		 		bomList = opp.getBoms();	
			    List<OpportunityLine> bomsArrayList = new ArrayList<OpportunityLine>();
	            if(bomList!=null && bomList.size()>0)
	            {	
				    for(BOM bomlist : bomList)
				    {
					    OpportunityLine oppLine = new OpportunityLine();
				    	oppLine.setCommercialReference(bomlist.getProductId());
				    	oppLine.setQuantity(bomlist.getQuantity());
				    	oppLine.setUnitAmount(bomlist.getUnitCost());
				    	bomsArrayList.add(oppLine);
				    }
				    opportunitydetails.put("opportunityLine",bomsArrayList);
	            }
			  }
		    closedLoopOpportunity.setAccount(account);
		    //logger.info("account.getAccountDetails().getFirstName() :::"+ account.getAccountDetails().getFirstName());
		    closedLoopOpportunity.setOpportunity(opportunitydetails);
		    //logger.info("opportunity.getName():::"+opportunity.getName());
		    responseData.put("closedLoopOpportunity",closedLoopOpportunity);
		    
		}catch(Exception e){
			e.printStackTrace();
		}
		
	return responseData;
}

	
	@Override
	public void updateBFOOppStatus(Map<String, String> bfoResponse, String oppId) {
		// TODO Auto-generated method stub
		
		opportunityBFODAO.updateBFOOppStatus(bfoResponse, oppId);
		
	}
	
	@Override
	public void updateBFOOppStatusIF(Map<String, String> bfoResponse, String oppId) {
		// TODO Auto-generated method stub
		
		opportunityBFODAO.updateBFOOppStatusIF(bfoResponse, oppId);
		
	}

	@Override
	public void updateBFOOppAcceptRejectStatus(boolean response, 
			String seqId) {
		// TODO Auto-generated method stub
		opportunityBFODAO.updateBFOOppAcceptRejectStatus(response, null, seqId);
		
	}
	
	@Override
	public void updateBFOOppAcceptRejectStatusIF(boolean response, 
			String seqId,String responseData,String oppId) {
		// TODO Auto-generated method stub
		opportunityBFODAO.updateBFOOppAcceptRejectStatusIF(response, oppId, seqId,responseData);
		
	}
	
	
	@Override
	public void updateBFOOppHist(boolean bfoResponse, String oppId) {
		// TODO Auto-generated method stub
		opportunityBFODAO.updateBFOOppHist(bfoResponse, oppId);
	}
	
	@Override
	public void updateBFOOppHistIF(boolean bfoResponse, String oppId) 
	{
		// TODO Auto-generated method stub
		opportunityBFODAO.updateBFOOppHistIF(bfoResponse, oppId);
	}
	
	
	@Override
	public void updateBFOOppHistInstallation(boolean bfoResponse, String oppId,String custVisitId) {
		// TODO Auto-generated method stub
		opportunityBFODAO.updateBFOOppHistInstallation(bfoResponse, oppId,custVisitId);
	}


	@Override
	public Map<String, String> getOpportunityStatusRequests(String oppStatus) {
		// TODO Auto-generated method stub
		List<OpportunityBFOHistory> oppList=opportunityBFODAO.getPendingOppStatus(oppStatus,COUNTRY_CODE);
		Map<String,String> requestOppXMLs=new HashMap<String,String>();
		for(OpportunityBFOHistory oppHist:oppList){
			String reqXML=getUpdateRequestXML(oppHist);
			if (reqXML!=null && reqXML.length()>0){
				requestOppXMLs.put(oppHist.getHistSeqId(),reqXML);
			}
		}
		
		return requestOppXMLs;
	}
	
	@Override
	public Map<String, Object> getOpportunityStatusRequestsIF(String oppStatus) {
		// TODO Auto-generated method stub
		List<OpportunityBFOHistory> oppList=opportunityBFODAO.getPendingOppStatusIF(oppStatus,COUNTRY_CODE);
		Map<String,Object> requestOppJSONData=new HashMap<String,Object>();
		//Map<String,Object> jsonMap = new HashMap<String,Object>(); 
		for(OpportunityBFOHistory oppHist:oppList){
			Object reqJson=getUpdateRequestJSON(oppHist);
			if (reqJson!=null)
			{
				//reqJson = reqJson+"^"+oppHist.getBfoOppId();
				//jsonMap.put(oppHist.getBfoOppId(),reqJson);
				requestOppJSONData.put(oppHist.getHistSeqId(),reqJson);
			}
		}		
		return requestOppJSONData;
		
	}
	
	private String getUpdateRequestXML(OpportunityBFOHistory oppHist){
		
		String responseXML="";
		if (oppHist.getOppStatus().equalsIgnoreCase(OppStatus.ACCEPTED.name())){
			
			responseXML=BFOXMLTemplates.stepOneUpdateResXML(oppHist.getBfoOppId(), oppHist.getBfoAccountCd(), "Electrical Contractor", oppHist.getBfoContactCd(), "Primary customer contact");
			
		}/*else if (oppHist.getOppStatus().equalsIgnoreCase(OppStatus.DECLINED.name())){
			responseXML=BFOXMLTemplates.stepFourUpdateResXML(oppHist.getBfoOppId(), "Price - Too high compared to customer budget", "AUD", compititorId);
		}*/
		
		
		return responseXML;
	}
	
	
private Object getUpdateRequestJSON(OpportunityBFOHistory oppHist){
		
		Object responseJson="";
		if (oppHist.getOppStatus().equalsIgnoreCase(OppStatus.ACCEPTED.name())){
			
			responseJson=BFOXMLTemplates.stepOneUpdateResJSON(oppHist.getBfoOppId(), oppHist.getBfoAccountCd(), "Electrical Contractor", oppHist.getBfoContactCd(), "Primary customer contact");
			
		}/*else if (oppHist.getOppStatus().equalsIgnoreCase(OppStatus.DECLINED.name())){
			responseJson=BFOXMLTemplates.stepFourUpdateResXML(oppHist.getBfoOppId(), "Price - Too high compared to customer budget", "AUD", compititorId);
		}*/
		return responseJson;
	}
	
	@Override
	public Map<String, String> getOpportunityHistStatusRequests(String statusId) {
		//List<OpportunityBFOHistory> oppList=opportunityBFODAO.getPendingOppHistoryStatus(statusId);
		List<OpportunityBFOHistory> oppList=null;
		if(statusId!=null && statusId.equalsIgnoreCase("INSTALLATION"))
		{	
		
			oppList=opportunityBFODAO.getPendingOppHistoryStatusForInstallation(statusId,COUNTRY_CODE);
			
		}else
		{
			oppList=opportunityBFODAO.getPendingOppHistoryStatus(statusId,COUNTRY_CODE);

		}
		Map<String,String> requestOppXMLs=new HashMap<String,String>();
		for(OpportunityBFOHistory oppHist:oppList){
			String reqXML=getUpdateHistRequestXML(oppHist,statusId);
			
			if (reqXML!=null && reqXML.length()>0){
				requestOppXMLs.put(oppHist.getHistSeqId(),reqXML);
			}
		}
		
		return requestOppXMLs;
	}
	
	@Override
	public Map<String, Object> getOpportunityHistStatusRequestsIF(String statusId) {
		//List<OpportunityBFOHistory> oppList=opportunityBFODAO.getPendingOppHistoryStatus(statusId);
		List<OpportunityBFOHistory> oppList=null;
		if(statusId!=null && statusId.equalsIgnoreCase("INSTALLATION"))
		{	
		
			oppList=opportunityBFODAO.getPendingOppHistoryStatusForInstallation(statusId,COUNTRY_CODE);
			
		}else
		{
			oppList=opportunityBFODAO.getPendingOppHistoryStatusIF(statusId,COUNTRY_CODE);

		}
		Map<String,Object> requestOppJSON=new HashMap<String,Object>();
		//Map<String,Object> jsonMap = new HashMap<String,Object>(); 
		for(OpportunityBFOHistory oppHist:oppList){
			Object reqJSON=getUpdateHistRequestJSON(oppHist,statusId);
			if (reqJSON!=null)
			{				
				//jsonMap.put(oppHist.getBfoOppId(),reqJSON);
				requestOppJSON.put(oppHist.getHistSeqId(),reqJSON);
			}
		}
		
		return requestOppJSON;
	}
	
	
	
private String getUpdateHistRequestXML(OpportunityBFOHistory oppHist,String statusId){
		
		String responseXML="";
		//String ownerId="005A0000001plXT";
		//String whoID="003A000001KlIhh";
		//String techCompititorsNameC="a2ZA00000004Odt";
		//convert start date and end date greg cal
		 final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 XMLGregorianCalendar startDate=null;
		 XMLGregorianCalendar endDate=null;
		 try{
		 if (oppHist.getStartDate()!=null){
			GregorianCalendar gc=new GregorianCalendar();
			Date date =DATE_FORMAT.parse(oppHist.getStartDate());
			//Date date=new Date(System.currentTimeMillis());
			gc.setTime(date);
			 startDate=DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		}
		 if (oppHist.getEndDate()!=null){
				GregorianCalendar gc=new GregorianCalendar();
				Date date =DATE_FORMAT.parse(oppHist.getEndDate());
				//Date date=new Date(System.currentTimeMillis());
				gc.setTime(date);
				 endDate=DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			}
		 
		 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
		if (statusId.equalsIgnoreCase(BFOStatus.MEETING.name())){
			responseXML=BFOXMLTemplates.stepTwoUpdateResXML(oppHist.getBfoOppId(), ownerId, "Project study", startDate, endDate, oppHist.getBfoContactCd());			
		}else if (statusId.equalsIgnoreCase(BFOStatus.INSTALLATION.name())){
			responseXML=BFOXMLTemplates.stepTwoUpdateResXML(oppHist.getBfoOppId(), ownerId, "Installation", startDate, endDate, oppHist.getBfoContactCd());
		}else if (statusId.equalsIgnoreCase(BFOStatus.WON.name())){
			responseXML=BFOXMLTemplates.stepFiveUpdateResXML(oppHist.getBfoOppId());
		}else if (statusId.equalsIgnoreCase(BFOStatus.LOST.name())){
			responseXML=BFOXMLTemplates.stepFourUpdateResXML(oppHist.getBfoOppId(), oppHist.getReason(), "EUR", compititorId);
		}else if (statusId.equalsIgnoreCase(BFOStatus.QUOTATION.name())){
			responseXML=BFOXMLTemplates.stepThreeUpdateResXML(oppHist.getBfoOppId(), oppHist.getStartDate());
		}else if (statusId.equalsIgnoreCase(BFOStatus.CANCEL.name())){
			responseXML=BFOXMLTemplates.stepSixUpdateResXML(oppHist.getBfoOppId(),oppHist.getReason());
		}
		
		
		return responseXML;
	}


private Object getUpdateHistRequestJSON(OpportunityBFOHistory oppHist,String statusId){
	
	Object responseJSON="";
	//convert start date and end date greg cal
	 final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 XMLGregorianCalendar startDate=null;
	 XMLGregorianCalendar endDate=null;
	 String quotationDate="";
	 try{
	 if (oppHist.getStartDate()!=null){
		GregorianCalendar gc=new GregorianCalendar();
		Date date =DATE_FORMAT.parse(oppHist.getStartDate());
		//Date date=new Date(System.currentTimeMillis());
		gc.setTime(date);
		 startDate=DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}
	 if (oppHist.getEndDate()!=null){
			GregorianCalendar gc=new GregorianCalendar();
			Date date =DATE_FORMAT.parse(oppHist.getEndDate());
			//Date date=new Date(System.currentTimeMillis());
			gc.setTime(date);
			 endDate=DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		}
	 
	 if (oppHist.getStartDate()!=null)
	 {
		    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    Date date = dt.parse(oppHist.getStartDate());
		    SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		     //adding 30days to opportunity as discussion Anh
		     Calendar c = Calendar.getInstance();
		     c.setTime(date); 
		     //c.add(Calendar.DATE, 30); 
		     logger.info(dt1.format(c.getTime()));
		     quotationDate = dt1.format(c.getTime()).toString();
		     logger.info("quotationDate"+quotationDate);
	 }
	 
	 
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	
	if (statusId.equalsIgnoreCase(BFOStatus.MEETING.name())){
		//Project study replaced with Meeting 
		responseJSON=BFOXMLTemplates.stepTwoUpdateResJSON(oppHist.getBfoOppId(), ifwownerId, "Project study", startDate.toString(), endDate.toString(), oppHist.getBfoContactCd());			
	}else if (statusId.equalsIgnoreCase(BFOStatus.INSTALLATION.name()))
	{
		//responseJSON=BFOXMLTemplates.stepTwoUpdateResJSON(oppHist.getBfoOppId(), ifwownerId, "Installation", startDate.toString(), endDate.toString(), oppHist.getBfoContactCd());
		responseJSON=BFOXMLTemplates.stepTwoUpdateResJSONInstallation(oppHist.getBfoOppId(), ifwownerId, "Installation", startDate.toString(), endDate.toString(), oppHist.getBfoContactCd(),oppHist.getRecordCount(),oppHist.getBfoCustAccountId(),oppHist.getBfoCustVisitId());

	}else if (statusId.equalsIgnoreCase(BFOStatus.WON.name())){
		responseJSON=BFOXMLTemplates.stepFiveUpdateResJSON(oppHist.getBfoOppId(),oppHist.getBfoValueChainPlayerId());
	}else if (statusId.equalsIgnoreCase(BFOStatus.LOST.name())){
		responseJSON=BFOXMLTemplates.stepFourUpdateResJSON(oppHist.getBfoOppId(), oppHist.getReason(), "SEK", compititorId);
	}else if (statusId.equalsIgnoreCase(BFOStatus.QUOTATION.name())){
		responseJSON=BFOXMLTemplates.stepThreeUpdateResJSON(oppHist.getBfoOppId(), quotationDate);
	}else if (statusId.equalsIgnoreCase(BFOStatus.CANCEL.name())){
		responseJSON=BFOXMLTemplates.stepSixUpdateResJSON(oppHist.getBfoOppId(),oppHist.getReason());
	}
	
	
	return responseJSON;
}


@Override
public String getBfoOppId(String seqId, String tableReference) 
{
	// TODO Auto-generated method stub
		return opportunityBFODAO.getBfoOppId(seqId, tableReference);
	
}



}

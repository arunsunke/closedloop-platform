package com.se.clm.mail;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.codehaus.jackson.map.ObjectMapper;

import com.se.cl.constants.BFOStatus;
import com.sun.jersey.core.util.Base64;


public class SendMail extends Object
{

	public  String getHtmlData(){
	    
		String htmlrequesturl = "http://10.179.200.82:8182/api/v1/email/convertEmailTemplate";
		StringBuffer sb = new StringBuffer();
		String jsonData = "";
		jsonData = "{\"app_unique_id\": \"closedloop\",\"event_type\": \"create_opp\", \"locale\": \"sv_se\", \"subject\": \"Welcome to Schneider Electric\",\"user_details\":{\"name\": \"John\",\"country\": \"Sweden\",\"zipCode\":1003}	}";
		StringBuffer strBuffer = new StringBuffer();
        
		try {
	   	 
			URL url = new URL(htmlrequesturl);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("POST");
	   	    con.setDoOutput(true);
	   	    con.setDoInput(true);
	   	    con.setRequestProperty("Content-Type", "application/json");
	   	    //con.setRequestProperty("Authorization", "Basic "+base64StringValue);
	   	    OutputStream outputStream = con.getOutputStream();
	   	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
	   	    outputStreamWriter.write(jsonData);
	   	    outputStreamWriter.flush();
	   	    outputStream.close();
	   	    InputStream inputStream = con.getInputStream();
	        int i;
	        while ((i = inputStream.read()) != -1) {
	               Writer writer = new StringWriter();
	               writer.write(i);
	               strBuffer.append(writer.toString());
	           }
	        
	       System.out.println("response"+strBuffer.toString());
	        

		 } catch (MalformedURLException e) {
			e.printStackTrace();
		 } catch (IOException e) {
			e.printStackTrace();
		
	     } 
	    
	    
	    return strBuffer.toString();
	  }	
	
public static void main(String [] args)
{

    try{

    	String jsonStr = new SendMail().getHtmlData(); 
        Properties props = new Properties();
		ObjectMapper mapper = new ObjectMapper();

		org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);			    
		//org.codehaus.jackson.JsonNode resultDetailNode = rootNode.path("result");
	    String subject =rootNode.path("convertedSubjectText").getTextValue();
	    String bodyHtml =rootNode.path("convertedMsgText").getTextValue();
	    System.out.println("subject"+subject);
	    System.out.println("bodyHtml"+bodyHtml);
        
	    BufferedReader br = null;

		try {

			String sCurrentLine;
           
			br = new BufferedReader(new FileReader("C:\\htmldata.txt"));

			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

       /* props.put("mail.smtp.host", "smtp.mail.yahoo.com"); // for gmail use smtp.gmail.com
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true"); 
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("sunke.arun@gmail.com", "lucky89&");
            }
        });

        mailSession.setDebug(true); // Enable the debug mode

        Message msg = new MimeMessage( mailSession );

        //--[ Set the FROM, TO, DATE and SUBJECT fields
        msg.setFrom( new InternetAddress( "raj2012@gmail.com" ) );
        msg.setRecipients( Message.RecipientType.TO,InternetAddress.parse("sunke.arun@gmail.com") );
        msg.setSentDate( new Date());
        msg.setSubject(subject);

        //--[ Create the body of the mail
        //msg.setContent("hai","text/html");
        msg.setText("Hai");
        //--[ Ask the Transport class to send our mail message
        Transport.send( msg );
*/
    }catch(Exception E){
        System.out.println( "Oops something has gone pearshaped!");
        System.out.println( E );
    }
}
}
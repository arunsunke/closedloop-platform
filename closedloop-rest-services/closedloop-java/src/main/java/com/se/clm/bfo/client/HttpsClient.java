package com.se.clm.bfo.client;

import java.util.Map;

public interface HttpsClient {
	
	 public   String getToken();
	 public   Map<String,String> sendCompositRequestToBFO(String XML,String token);
	 public   boolean sendUpdateRequestToBFO(String XML,String token,String statusId);
	 public   String getIFWBarerToken();
	 public   Map<String, String> sendOppCreateRequestToIF(Object JsonData,String bearerToken);
	 public   boolean sendUpdateRequestToIF(Object value, String statusId,	String bearerToken, String bfoOppId);
	 public   String sendAcceptRejectUpdateRequestToIF(Object JsonData, String statusId,String bearerToken,String bfoOppId);
	 public   String sendSMSToConsumer(String phonenNo, String customerName,String PartnerName,String PartnerPhno,String countryCode,int oppId);
	 public   String sendUpdateRequestToIFInstallation(Object JsonData, String statusId,String bearerToken, String bfoOppId);
	 public  String  sendSMSToPartner(String phonenNo,String customerName,String PartnerName,String oppName,String countryCode,int oppId,String cancelDate,String installationExists);
	 
	 public  String  sendSMSToConsumerWhenMeetingbooked(String phonenNo,String customerName,String PartnerName,String oppName,String custZip,String custCity,String custAddress,String meetingDate,String countryCode);

		 
}

package com.se.clm.bfo.client;



import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.se.cl.constants.BFOStatus;
import com.sun.jersey.core.util.Base64;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
 
public class HttpsClientImpl implements HttpsClient{
 
	private  @Value("${token.url}") String tokenUrl;
	private  @Value("${token.username}") String tokenUserName;
	private  @Value("${token.password}") String tokenPassword;
	private  @Value("${token.uname}") String tokenUname;
	private  @Value("${composit.url}") String compositUrl;
	private  @Value("${update.url}") String updateUrl;
	
	private  @Value("${ifwtoken.url}") String ifwTokenUrl;
	private  @Value("${ifwtoken.clientid}") String ifwClientId;
	private  @Value("${ifwtoken.clientsecret}") String ifwClientSecret;
	private  @Value("${ifwoppcreate.url}") String ifwOppCreateUrl;
	private  @Value("${ifwoppupdate.url}") String ifwOppUpdateUrl;
	
	private  @Value("${sms.username}") String smsUser;
	private  @Value("${sms.password}") String smsPassword;
	private  @Value("${sms.url}") String smsUrl;
		
	//private String smsUser = "ua499d24cb9575bb8cadc32e9e49e3854"; 
    //private String smsPassword = "20742FC78C607BB5DB0E93E3B2ED306E"; 
    //private String smsUrl = "https://api.46elks.com/a1/SMS";
	
	private static final Logger logger = LoggerFactory.getLogger(HttpsClientImpl.class);
	
	//private  String tokenUrl="https://integration.schneider-electric.com";
//	
	//private  String tokenUrl="https://integration.schneider-electric.com/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=Business%20Apps,cn=cordys,cn=PROD01,o=eur.gad.schneider-electric.com";
//	private   String tokenUserName="OAUser";
//	private   String tokenPassword="OAUser!";
//	private  String tokenUname="OAUser";
//
//	
//	private   String compositUrl="https://integration-pp.schneider-electric.com/cordys/restful/compositeservice/com.cordys.web.rest.RESTGateway.wcp?organization=o%3DBusiness%20Apps%2Ccn%3Dcordys%2Ccn%3DPROD01%2Co%3Deur.gad.schneider-electric.com&SAMLart=";
//	private  String updateUrl="https://integration-pp.schneider-electric.com/cordys/restful/updateopportunityv1/com.cordys.web.rest.RESTGateway.wcp?organization=o%3DBusiness%20Apps%2Ccn%3Dcordys%2Ccn%3DPROD01%2Co%3Deur.gad.schneider-electric.com&SAMLart=";
	
	
  public static void main(String[] args)
  {
     try {
		//System.out.println(new HttpsClientImpl().getToken());
    	// String statusId="ACCEPTED";
    	// String JsonData="";
    	// JsonData  =  "{\"closedLoopOpportunity\":{\"opportunity\":{\"valueChainPlayers\":{\"newValueChainPlayer\":{\"seContactId\":\"Contact ID\", \"isLeadingPartner\":\"false\", \"contactRole\":\"Primary customer contact\", \"seAccountId\":\"Accountid\", \"accountRole\":\"Electrical Contractor\"}}, \"stage\":\"4 - Influence & Develop\"}}}";
 		//System.out.println(new HttpsClientImpl().sendSMSToConsumer("8106498542","cust1","partner","partnerPh","91"));
  		
    	 //System.out.println(new HttpsClientImpl().sendSMSToConsumer("919620029222","cust1","partner","partnerPh","91"));
    	 //System.out.println(new HttpsClientImpl().sendSMSToConsumerWhenMeetingbooked("919740377938","cust1","partner","oppName","11111","abc","address","2015-11-11 08.23.11.0","91"));
    	 System.out.println(new HttpsClientImpl().sendSMSToPartner("","raj","partner","Opp","91",100,"2015-11-11 10.22.22","2015-11-11 08.23.11"));

    	 
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
 
  private static TrustManager[ ] get_trust_mgr() {
     TrustManager[ ] certs = new TrustManager[ ] {
        new X509TrustManager() {
           public X509Certificate[ ] getAcceptedIssuers() { return null; }
           public void checkClientTrusted(X509Certificate[ ] certs, String t) { }
           public void checkServerTrusted(X509Certificate[ ] certs, String t) { }
         }
      };
      return certs;
  }
 
  
  
  public  String getToken(){
     //String https_url = "https://integration-pp.schneider-electric.com/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=system,cn=cordys,cn=PROD01,o=eur.gad.schneider-electric.com";
     URL url;
     try {
 
    	 
    	 
            SSLContext ssl_ctx = SSLContext.getInstance("TLS");
            TrustManager[ ] trust_mgr = get_trust_mgr();
            ssl_ctx.init(null,                // key manager
                         trust_mgr,           // trust manager
                         new SecureRandom()); // random number generator
        
				try {
					HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				
				
 
		    url = new URL(tokenUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    // Guard against "bad hostname" errors during handshake.
	            con.setHostnameVerifier(new HostnameVerifier() {
	                public boolean verify(String host, SSLSession sess) {
	                    if (host.equals("localhost")) return true;
	                    else return false;
	                }
	            });
	 
    	    con.setRequestMethod("POST");
    	    con.setDoOutput(true);
    	    con.setDoInput(true);
    	    con.setRequestProperty("content-type", "application/soap+xml;charset=UTF-8");
//    	    con.setRequestProperty("SOAPAction", 
//    	       "http://integration-pp.schneider-electric.com/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=system,cn=cordys,cn=PROD01,o=eur.gad.schneider-electric.com");
//    	    
    	    con.setRequestProperty("SOAPAction", tokenUrl);
    	    	    
    	    OutputStream outputStream = con.getOutputStream();
    	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");

    	    
    	    outputStreamWriter.write( "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">             <SOAP:Header>                 <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
    	    outputStreamWriter.write( "      <wsse:UsernameToken xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"> <wsse:Username>"+tokenUserName+"</wsse:Username>                         <wsse:Password>"+tokenPassword+"</wsse:Password>                     </wsse:UsernameToken>                 </wsse:Security>             </SOAP:Header>             <SOAP:Body>                 <samlp:Request xmlns:samlp=\"urn:oasis:names:tc:SAML:1.0:protocol\" MajorVersion=\"1\" MinorVersion=\"1\">                     <samlp:AuthenticationQuery>                         <saml:Subject xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\">     <saml:NameIdentifier Format=\"urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified\">"+tokenUname+"</saml:NameIdentifier>                         </saml:Subject>                     </samlp:AuthenticationQuery>                 </samlp:Request>             </SOAP:Body>         </SOAP:Envelope>");
    	    outputStreamWriter.flush();
    	    outputStream.close();
    	    
    	    
    	    StringBuffer strBuffer = new StringBuffer();
           
            InputStream inputStream = con.getInputStream();

            int i;
            while ((i = inputStream.read()) != -1) {
                Writer writer = new StringWriter();
                writer.write(i);
                strBuffer.append(writer.toString());
            }
         
        return  parseSOAPResponse(strBuffer);
    	    
 
	 } catch (MalformedURLException e) {
		e.printStackTrace();
	 } catch (IOException e) {
		e.printStackTrace();
	 }catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	 }catch (KeyManagementException e) {
		e.printStackTrace();
      } 
     
     
     return null;
   }
 
 
  
  

  

  
  private static String parseSOAPResponse(StringBuffer StringBuffer){
	 String xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://services.web.post.list.com\"><soapenv:Header><authInfo xsi:type=\"soap:authentication\" xmlns:soap=\"http://list.com/services/SoapRequestProcessor\"><!--You may enter the following 2 items in any order--><username xsi:type=\"xsd:string\">dfasf@google.com</username><password xsi:type=\"xsd:string\">PfasdfRem91</password></authInfo></soapenv:Header></soapenv:Envelope>";
	 xml=StringBuffer.toString();
	 // System.out.println(xml);
	  DocumentBuilderFactory domFactory = DocumentBuilderFactory
	          .newInstance();
	  domFactory.setNamespaceAware(true);
	  DocumentBuilder builder=null;;
	try {
		builder = domFactory.newDocumentBuilder();
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  Document doc=null;
	try {
		doc = builder.parse(new InputSource(new StringReader(xml)));
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  XPath xpath = XPathFactory.newInstance().newXPath();
	  xpath.setNamespaceContext(new NamespaceContext() {
	
	      @Override
	      public Iterator getPrefixes(String arg0) {
	          return null;
	      }
	
	      @Override
	      public String getPrefix(String arg0) {
	          return null;
	      }
	
	      @Override
	      public String getNamespaceURI(String arg0) {
	          if("SOAP".equals(arg0)) {
	              return "http://schemas.xmlsoap.org/soap/envelope/";
	          }
	          if("samlp".equals(arg0)) {
	              return "urn:oasis:names:tc:SAML:1.0:protocol";
	          }
	        //  xmlns:samlp="urn:oasis:names:tc:SAML:1.0:protocol"
	          return null;
	      }
	  });
	  // XPath Query for showing all nodes value
	
	  try {
	      XPathExpression expr = xpath
	              .compile("/SOAP:Envelope/SOAP:Body/samlp:Response/samlp:AssertionArtifact");
	      Object result = expr.evaluate(doc, XPathConstants.NODESET);
	      NodeList nodes = (NodeList) result;
	    //  System.out.println("Got " + nodes.getLength() + " nodes");
	     // System.out.println(nodes.item(0).getTextContent());
	      return nodes.item(0).getTextContent();
	  } catch (Exception E) {
	      System.out.println(E);
	  }
	  return null;
	  

}
  
  
  

	public   Map<String,String> sendCompositRequestToBFO(String XML,String token){
		
		//String https_url = "https://integration-pp.schneider-electric.com/cordys/restful/compositeservice/com.cordys.web.rest.RESTGateway.wcp?organization=o%3DBusiness%20Apps%2Ccn%3Dcordys%2Ccn%3DPROD01%2Co%3Deur.gad.schneider-electric.com&SAMLart="+token;
		logger.info("BFO-Composit Request:"+XML);
		
		URL url;
		
		try{
		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
        TrustManager[ ] trust_mgr = get_trust_mgr();
        ssl_ctx.init(null,                // key manager
                     trust_mgr,           // trust manager
                     new SecureRandom()); // random number generator
        try {
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    url = new URL(compositUrl+token);
	    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	    // Guard against "bad hostname" errors during handshake.
            con.setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String host, SSLSession sess) {
                    if (host.equals("localhost")) return true;
                    else return false;
                }
            });
 
	    con.setRequestMethod("POST");
	    con.setDoOutput(true);
	    con.setDoInput(true);
	    con.setRequestProperty("content-type", "application/xml;charset=UTF-8");
	    OutputStream outputStream = con.getOutputStream();
	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");

	    
	    outputStreamWriter.write(XML);
	    outputStreamWriter.flush();
	    outputStream.close();
	    
	    
	    StringBuffer strBuffer = new StringBuffer();
       
        InputStream inputStream = con.getInputStream();

        int i;
        while ((i = inputStream.read()) != -1) {
            Writer writer = new StringWriter();
            writer.write(i);
            strBuffer.append(writer.toString());
        }
	    
        System.out.println("Response :"+ strBuffer);
        //parseCompositResponse(strBuffer);
        Map<String,String> responseMap=ParsersForUpdateComposite.parseComposit(strBuffer.toString());
        
        return responseMap;
		 } catch (MalformedURLException e) {
				e.printStackTrace();
			 } catch (IOException e) {
				e.printStackTrace();
			 }catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			 }catch (KeyManagementException e) {
				e.printStackTrace();
		      } 
		
		
		return null;
     

  }
	
	
public   boolean sendUpdateRequestToBFO(String XML,String token,String statusId){
		
		logger.info("BFO-update Request"+XML);
		//String https_url = "https://integration-pp.schneider-electric.com/cordys/restful/updateopportunityv1/com.cordys.web.rest.RESTGateway.wcp?organization=o%3DBusiness%20Apps%2Ccn%3Dcordys%2Ccn%3DPROD01%2Co%3Deur.gad.schneider-electric.com&SAMLart="+token;
		URL url;
		
		try{
		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
        TrustManager[ ] trust_mgr = get_trust_mgr();
        ssl_ctx.init(null,                // key manager
                     trust_mgr,           // trust manager
                     new SecureRandom()); // random number generator
        try {
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    url = new URL(updateUrl+token);
	    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	    // Guard against "bad hostname" errors during handshake.
            con.setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String host, SSLSession sess) {
                    if (host.equals("localhost")) return true;
                    else return false;
                }
            });
 
	    con.setRequestMethod("POST");
	    con.setDoOutput(true);
	    con.setDoInput(true);
	    con.setRequestProperty("content-type", "application/xml;charset=UTF-8");
	    OutputStream outputStream = con.getOutputStream();
	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");

	    
	    outputStreamWriter.write(XML);
	    outputStreamWriter.flush();
	    outputStream.close();
	    
	    
	    StringBuffer strBuffer = new StringBuffer();
       
        InputStream inputStream = con.getInputStream();

        int i;
        while ((i = inputStream.read()) != -1) {
            Writer writer = new StringWriter();
            writer.write(i);
            strBuffer.append(writer.toString());
        }
	    
        System.out.println("Response :"+ strBuffer);
        //parseCompositResponse(strBuffer);
        boolean response=false;
        if (statusId.equalsIgnoreCase(BFOStatus.QUOTATION.name())||statusId.equalsIgnoreCase(BFOStatus.WON.name()) || statusId.equalsIgnoreCase(BFOStatus.LOST.name()) ){
        	response=ParsersForUpdateComposite.parseUpdateQuoteXML(strBuffer.toString());
        }else{	
        	response=ParsersForUpdateComposite.parseUpdateXML(strBuffer.toString());
        }
        return response;
		 } catch (MalformedURLException e) {
				e.printStackTrace();
			 } catch (IOException e) {
				e.printStackTrace();
			 }catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			 }catch (KeyManagementException e) {
				e.printStackTrace();
		      } 
		
		
		return false;
     

  }
	
	private static String parseCompositResponse(StringBuffer StringBuffer){
		 String xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://services.web.post.list.com\"><soapenv:Header><authInfo xsi:type=\"soap:authentication\" xmlns:soap=\"http://list.com/services/SoapRequestProcessor\"><!--You may enter the following 2 items in any order--><username xsi:type=\"xsd:string\">dfasf@google.com</username><password xsi:type=\"xsd:string\">PfasdfRem91</password></authInfo></soapenv:Header></soapenv:Envelope>";
		 xml=StringBuffer.toString();
		  System.out.println(xml);
		  DocumentBuilderFactory domFactory = DocumentBuilderFactory
		          .newInstance();
		  domFactory.setNamespaceAware(true);
		  DocumentBuilder builder=null;;
		try {
			builder = domFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  Document doc=null;
		try {
			doc = builder.parse(new InputSource(new StringReader(xml)));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  XPath xpath = XPathFactory.newInstance().newXPath();
		  xpath.setNamespaceContext(new NamespaceContext() {
		
		      @Override
		      public Iterator getPrefixes(String arg0) {
		          return null;
		      }
		
		      @Override
		      public String getPrefix(String arg0) {
		          return null;
		      }
		
		      @Override
		      public String getNamespaceURI(String arg0) {
		          if("comp".equals(arg0)) {
		              return "http://schemas.cordys.com/default";
		          }
		          if("samlp".equals(arg0)) {
		              return "urn:oasis:names:tc:SAML:1.0:protocol";
		          }
		          if ("create".equals(arg0)){
		        	  return "http://schemas.cordys.com/bfoservices";
		          }
		        //  xmlns:samlp="urn:oasis:names:tc:SAML:1.0:protocol"
		          return null;
		      }
		  });
		  // XPath Query for showing all nodes value
		
		  try {
		      XPathExpression expr = xpath
		              .compile("/comp:create_update_Acc_Opp_ProcResponse/Opportunity/CreateOpportunity/result");
		      Object result = expr.evaluate(doc, XPathConstants.NODESET);
		      NodeList nodes = (NodeList) result;
		    //  System.out.println("Got " + nodes.getLength() + " nodes");
		      System.out.println(nodes.getLength());
		      return nodes.item(0).getTextContent();
		  } catch (Exception E) {
		      System.out.println(E);
		  }
		  return null;
		  

	}
    /*******************************************************************
     *   Method to create Opportunity  on BFO using IF Rest Web services
     *******************************************************************/
	@Override
	public Map<String, String> sendOppCreateRequestToIF(Object JsonData,String bearerToken) {
				logger.info("sendOppCreateRequestToIF:"+JsonData);
				HashMap<String,Object> jsonMapData = (HashMap<String, Object>) JsonData;
				URL url;
				Map<String,String> responseMap=null;
				try
				{
					
				 String jsonStringData="";
				 logger.info("sendOppCreateRequestToIF:::JsonData::"+JsonData);
				 if(JsonData!=null)
				 {
				      ObjectMapper objMapper = new 	ObjectMapper();
				      jsonStringData =  objMapper.writeValueAsString(jsonMapData);
				    	logger.info("jsonStringData:::::::::::"+jsonStringData);
				 }  
				SSLContext ssl_ctx = SSLContext.getInstance("TLS");
		        TrustManager[ ] trust_mgr = get_trust_mgr();
		        ssl_ctx.init(null,                // key manager
		                     trust_mgr,           // trust manager
		                     new SecureRandom()); // random number generator
		        try {
					HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			    url = new URL(ifwOppCreateUrl);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			    // Guard against "bad hostname" errors during handshake.
		            con.setHostnameVerifier(new HostnameVerifier() {
		                public boolean verify(String host, SSLSession sess) {
		                    if (host.equals("localhost")) return true;
		                    else return false;
		                }
		            });
		 
			    con.setRequestMethod("POST");
			    con.setDoOutput(true);
			    con.setDoInput(true);
			    con.setRequestProperty("content-type", "application/json;charset=UTF-8");
			    con.setRequestProperty("Accept", "application/json");
			    con.setRequestProperty("Authorization", "Bearer "+bearerToken);
			    OutputStream outputStream = con.getOutputStream();
			    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			   
			    outputStreamWriter.write(jsonStringData);
			    outputStreamWriter.flush();
			    outputStream.close();
			    
				int responseCode = con.getResponseCode();
				
				logger.info("responseCode::::sendOppCreateRequestToIF::::::"+responseCode);

				//if(responseCode==500) return responseMap; 
					
			    //if(responseCode==200)
			    {
			    
			    StringBuffer strBuffer = new StringBuffer();
		       
		        InputStream inputStream = con.getInputStream();
		        		        int i;
		        while ((i = inputStream.read()) != -1) {
		            Writer writer = new StringWriter();
		            writer.write(i);
		            strBuffer.append(writer.toString());
		        }			    
		        logger.info("Response data  :"+ strBuffer.toString());
		        
		        responseMap = parseCreateOppResponseJsonData(strBuffer.toString());

		        return responseMap;
			    
			    }
			    
				 } catch (MalformedURLException e) {
						e.printStackTrace();
					 } catch (IOException e) {
						e.printStackTrace();
					 }catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					 }catch (KeyManagementException e) {
						e.printStackTrace();
				      } 
				
								
				return null;
	}
	
public Map<String,String> parseCreateOppResponseJsonData(String jsonStr) {
		
 		Map<String,String> ResponseMap = new  LinkedHashMap <String,String>();
 		ObjectMapper mapper = new ObjectMapper();
 		if (jsonStr!=null && jsonStr.length()>0) {
	 		try {
	 			org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
			    JsonNode resultDetailNode = rootNode.path("result");
			    logger.info(resultDetailNode.path("accountId").getTextValue());			    
			    ResponseMap.put("acctId", resultDetailNode.path("accountId").getTextValue());
			    JsonNode opportunitiesNode=resultDetailNode.path("opportunity");			
			    logger.info(opportunitiesNode.path("seReference").getTextValue());
			    ResponseMap.put("oppId", opportunitiesNode.path("seReference").getTextValue());

			} catch (Exception e) {
				logger.error("Error occurred while Parsing the Values from JSON  "+ e);
				e.printStackTrace();
			} 
 		}

 		return ResponseMap;
	} 
	

/*****************************************************************************
 *   Method to Parse Opportunity Updates Response JSON data 
 ****************************************************************************/
public boolean parseOpportunityUpdaeteResponseJsonData(String jsonStr,String statusId) {
				   
	    boolean updateResult=false;
		logger.info("parseOpportunityUpdaeteResponseJsonData:::jsonStr:::"+jsonStr);
		ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0) {
 		try {
	 			org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
			    
			    if(statusId!=null && statusId.equalsIgnoreCase(BFOStatus.ACCEPTED.name()))
			    {
			    
				    JsonNode resultDetailNode = rootNode.path("result");
			    	JsonNode valurChainPlayerNode=resultDetailNode.path("valueChainPlayer");			
				    logger.info("seReference is ::::"+valurChainPlayerNode.path("seReference").getTextValue());				    
				    String seReference=valurChainPlayerNode.path("seReference").getTextValue();
				    if(seReference!=null && seReference.length()>0)
				    {
				    	updateResult=true;
				    }
			    }else if(statusId!=null && statusId.equalsIgnoreCase(BFOStatus.MEETING.name()) || statusId!=null && statusId.equalsIgnoreCase(BFOStatus.INSTALLATION.name()))
			    {
				    JsonNode resultDetailNode = rootNode.path("result");
			    	JsonNode newCustomerVisitrNode=resultDetailNode.path("newCustomerVisit");			
				    logger.info("id is ::::"+newCustomerVisitrNode.path("id").getTextValue());				    
				    String id=newCustomerVisitrNode.path("id").getTextValue();
				    if(id!=null && id.length()>0)
				    {
				    	updateResult=true;
				    }
			    }else  if (statusId.equalsIgnoreCase(BFOStatus.QUOTATION.name())||statusId.equalsIgnoreCase(BFOStatus.WON.name()) || statusId.equalsIgnoreCase(BFOStatus.LOST.name()) ||  statusId.equalsIgnoreCase(BFOStatus.CANCEL.name()) )
			    {
			    	logger.info("result::::"+statusId);			    
			    	JsonNode resultDetailNode = rootNode.path("result");
			    	JsonNode resultNotificationsNode = resultDetailNode.path("notifications");
			    	logger.info("rootNode.path(\"result\").getTextValue()"+rootNode.path("result").getTextValue());
			    	logger.info("resultNotificationsNode::::"+resultNotificationsNode);			    

                    if(resultDetailNode.has("notifications"))                    	
                    {
				    	updateResult=false;
  	
                    }else 
                    {
                    	String result = rootNode.path("result").getTextValue();
                    	logger.info("result::::"+result);			    
					    if(result==null|| result.equals(null))
					    {
					    	updateResult=true;
					    }
                    }
				}

		} catch (Exception e) {
			logger.error("Error occurred while Parsing the Values from JSON  "+ e);
			e.printStackTrace();
		} 
		}

		return updateResult;
}


/*****************************************************************************
 *   Method to Parse Opportunity Updates Response JSON data 
 ****************************************************************************/
public String parseOpportunityUpdaeteResponseJsonDataInstallation(String jsonStr,String statusId) {
				   
	    boolean updateResult=false;
	    String custVisitId="";
		logger.info("parseOpportunityUpdaeteResponseJsonDataInstallation:::jsonStr:::"+jsonStr);
		ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0) {
 		try {
 			
	 			org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);			    
			    if(statusId!=null && statusId.equalsIgnoreCase(BFOStatus.INSTALLATION.name()))
			    {
			    	org.codehaus.jackson.JsonNode resultDetailNode = rootNode.path("result");
			    	org.codehaus.jackson.JsonNode newCustomerVisitrNode=resultDetailNode.path("newCustomerVisit");	
				    if(newCustomerVisitrNode.has("id"))
				    {
					    
					    	logger.info("parseOpportunityUpdaeteResponseJsonDataInstallation: id is ::::"+newCustomerVisitrNode.path("id").getTextValue());				    
						    String id=newCustomerVisitrNode.path("id").getTextValue();
						    if(id!=null && id.length()>0)
						    {
						    	custVisitId= id;
						    }
					    	
				    	
				    }else
				    {
					    if(resultDetailNode!=null)
					    {	
					    	org.codehaus.jackson.JsonNode resultNotificationsNode = resultDetailNode.path("notifications");
					    	logger.info("parseOpportunityUpdaeteResponseJsonDataInstallation:rootNode.path(\"result\").getTextValue()"+rootNode.path("result").getTextValue());
					    	logger.info("parseOpportunityUpdaeteResponseJsonDataInstallation:resultNotificationsNode::::"+resultNotificationsNode);			    
		                    if(resultDetailNode.has("notifications"))                    	
		                    {
		                    	return custVisitId="";
		                    	
		                    }else 
		                    {
		                    	String result = rootNode.path("result").getTextValue();
		                    	logger.info("parseOpportunityUpdaeteResponseJsonDataInstallation:::result::::"+result);			    
							    if(result==null|| result.equals(null))
							    {
							    	return custVisitId="true";
							    }
		                    }
					    }
				    }
                    				    
				    
				    
			    }
		} catch (Exception e) {
			logger.error("Error occurred while Parsing the Values from JSON  "+ e);
			e.printStackTrace();
		} 
		}

		return custVisitId;
}


/*****************************************************************************
 *   Method to Parse Opportunity Updates Response JSON data 
 ****************************************************************************/
public String parseOpportunityUpdaeteAcceptRejectResponseJsonData(String jsonStr,String statusId) {
				   
	    String updateResult="";
		logger.info("parseOpportunityUpdaeteResponseJsonData:::jsonStr:::"+jsonStr);
		ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0) {
 		try {
	 			org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
			    
			    if(statusId!=null && statusId.equalsIgnoreCase(BFOStatus.ACCEPTED.name()))
			    {
			    
				    JsonNode resultDetailNode = rootNode.path("result");
			    	JsonNode valurChainPlayerNode=resultDetailNode.path("valueChainPlayer");			
				    logger.info("seReference is ::::"+valurChainPlayerNode.path("seReference").getTextValue());				    
				    String seReference=valurChainPlayerNode.path("seReference").getTextValue();
				    if(seReference!=null && seReference.length()>0)
				    {
				    	updateResult=seReference;
				    }
			    }
		} catch (Exception e) {
			logger.error("Error occurred while Parsing the Values from JSON  "+ e);
			e.printStackTrace();
		} 
		}

		return updateResult;
} 


/*******************************************************************
 *   Method to Update  Opportunity  on BFO using IF Rest Web services
 *******************************************************************/
@Override
public   boolean sendUpdateRequestToIF(Object JsonData,String statusId,String bearerToken,String bfoOppId){

	logger.info("sendUpdateRequestToIF:"+JsonData);
	URL url;
	boolean response=false;
	String jsonStringData="";
	HashMap<String,Object> jsonMapData = (HashMap<String, Object>) JsonData;
    logger.info("sendUpdateRequestToIF:::bfoOppId:::"+bfoOppId);

	try
	{

		if(jsonMapData!=null  && !bfoOppId.equalsIgnoreCase("null") && !bfoOppId.isEmpty())
		{
			ObjectMapper objMapper = new 	ObjectMapper();
			jsonStringData =  objMapper.writeValueAsString(jsonMapData);
			logger.info("jsonStringData Data is :"+jsonStringData);
			logger.info("sendUpdateRequestToIF:ifwOppUpdateUrl::::"+ifwOppUpdateUrl +"/"+bfoOppId);

			url = new URL(ifwOppUpdateUrl+"/"+bfoOppId);

			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("PUT");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Authorization", "Bearer "+bearerToken);
			con.setDoInput(true);		   
			OutputStream outputStream = con.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			outputStreamWriter.write(jsonStringData);
			outputStreamWriter.flush();
			outputStream.close();
			con.setReadTimeout(1000);
			int responseCode = con.getResponseCode();
			logger.info("responseCode::::sendUpdateRequestToIF::::::"+responseCode);
			if(responseCode==500) return response;
			if(responseCode==200)
			{	
					StringBuffer strBuffer = new StringBuffer();
					InputStream inputStream = con.getInputStream();
		
					int i=0;
					while ((i = inputStream.read()) != -1) {
						Writer writer = new StringWriter();
						writer.write(i);
						strBuffer.append(writer.toString());
					}			    
					logger.info("Response data  :"+ strBuffer.toString());
					if(strBuffer.toString()!=null && strBuffer.toString().length()>0)
					{
						response = parseOpportunityUpdaeteResponseJsonData(strBuffer.toString(),statusId);
					}
				}
				logger.info("response is"+response);
				return response;
		    }

	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}


	return response;
}

/*******************************************************************
 *   Method to Update  Opportunity Installation details on BFO using IF Rest Web services
 *******************************************************************/
@Override
public   String sendUpdateRequestToIFInstallation(Object JsonData,String statusId,String bearerToken,String bfoOppId){

	logger.info("sendUpdateRequestToIF:"+JsonData);
	URL url;
	String response="";
	String jsonStringData="";
	HashMap<String,Object> jsonMapData = (HashMap<String, Object>) JsonData;
    logger.info("sendUpdateRequestToIF:::bfoOppId:::"+bfoOppId);

	try
	{

		if(jsonMapData!=null  && !bfoOppId.equalsIgnoreCase("null") && !bfoOppId.isEmpty())
		{
			ObjectMapper objMapper = new 	ObjectMapper();
			jsonStringData =  objMapper.writeValueAsString(jsonMapData);
			logger.info("sendUpdateRequestToIFInstallation:ifwOppUpdateUrl:::jsonStringData Data is :"+jsonStringData);
			logger.info("sendUpdateRequestToIFInstallation:ifwOppUpdateUrl::::"+ifwOppUpdateUrl +"/"+bfoOppId);

			url = new URL(ifwOppUpdateUrl+"/"+bfoOppId);

			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("PUT");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Authorization", "Bearer "+bearerToken);
			con.setDoInput(true);		   
			OutputStream outputStream = con.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			outputStreamWriter.write(jsonStringData);
			outputStreamWriter.flush();
			outputStream.close();
			con.setReadTimeout(1000);
			int responseCode = con.getResponseCode();
			logger.info("sendUpdateRequestToIFInstallation:ifwOppUpdateUrl:responseCode::::sendUpdateRequestToIF::::::"+responseCode);
			if(responseCode==500) return response;
			if(responseCode==200)
			{	
					StringBuffer strBuffer = new StringBuffer();
					InputStream inputStream = con.getInputStream();
		
					int i=0;
					while ((i = inputStream.read()) != -1) {
						Writer writer = new StringWriter();
						writer.write(i);
						strBuffer.append(writer.toString());
					}			    
					logger.info("Response data  :"+ strBuffer.toString());
					if(strBuffer.toString()!=null && strBuffer.toString().length()>0)
					{
						response = parseOpportunityUpdaeteResponseJsonDataInstallation(strBuffer.toString(),statusId);
					}
				}
				logger.info("sendUpdateRequestToIFInstallation:ifwOppUpdateUrl:response is"+response);
				return response;
		    }

	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}


	return response;
}

/*******************************************************************
 *   Method to Update  Opportunity  on BFO using IF Rest Web services
 *******************************************************************/
@Override
public   String sendAcceptRejectUpdateRequestToIF(Object JsonData,String statusId,String bearerToken,String bfoOppId){

	logger.info("sendAcceptRejectUpdateRequestToIF:JsonData:"+JsonData);
	URL url;
	String response="";
	String jsonStringData="";
	HashMap<String,Object> jsonMapData = (HashMap<String, Object>) JsonData;
	try
	{

        logger.info("sendAcceptRejectUpdateRequestToIF:::bfoOppId:::"+bfoOppId);
        
        if(jsonMapData!=null  && !bfoOppId.equalsIgnoreCase("null") && !bfoOppId.isEmpty())
        {
			ObjectMapper objMapper = new 	ObjectMapper();
			jsonStringData =  objMapper.writeValueAsString(JsonData);
			logger.info("jsonStringData Data is :"+jsonStringData);
			logger.info("sendAcceptRejectUpdateRequestToIF:ifwOppUpdateUrl::::"+ ifwOppUpdateUrl +"/"+bfoOppId);
			url = new URL(ifwOppUpdateUrl +"/"+bfoOppId);
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection(); 
			con.setDoOutput(true);
			con.setRequestMethod("PUT");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Authorization", "Bearer "+bearerToken);
			con.setDoInput(true);		   
			OutputStream outputStream = con.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			outputStreamWriter.write(jsonStringData);
			outputStreamWriter.flush();
			outputStream.close();
			con.setReadTimeout(1000);
			
			int responseCode = con.getResponseCode();
			logger.info("responseCode::::sendAcceptRejectUpdateRequestToIF::::::"+responseCode);
			if(responseCode==500) return response;
			if(responseCode==200)
			{	
				StringBuffer strBuffer = new StringBuffer();
				InputStream inputStream = con.getInputStream();

				int i;
				while ((i = inputStream.read()) != -1) {
					Writer writer = new StringWriter();
					writer.write(i);
					strBuffer.append(writer.toString());
				}			    
				logger.info("Response data  :"+ strBuffer.toString());
	
				if(strBuffer.toString()!=null && strBuffer.toString().length()>0)
				{	
					response = parseOpportunityUpdaeteAcceptRejectResponseJsonData(strBuffer.toString(),statusId);
				}
				
			}
		}
		logger.info("response is"+response);
		return response;

	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}


	return response;
}

/*
 * 
 *  Method to Get the Token Using IFW
 * 
 */

@Override
public  String getIFWBarerToken(){
    //String ifwTokenUrl = "https://api-test.schneider-electric.com/token";
    //String clientId="5kopNJ8UMV5TCrWzTP75Iu6Db7Aa";
    //String clientSecret="1iiuOPmnjHVT1Sxx_PgjfaHcz3ca";
    try {
   	 
    	   byte[] base64ByteArray = Base64.encode(ifwClientId+":"+ifwClientSecret);
    	   URL url;
    	   String base64StringValue = new String(base64ByteArray, "UTF-8");
    	   //base64StringValue = "Basic " + base64StringValue; 
    	   logger.info("base64StringValue"+base64StringValue);   
    	   SSLContext ssl_ctx = SSLContext.getInstance("TLS");
           TrustManager[ ] trust_mgr = get_trust_mgr();
           ssl_ctx.init(null,                // key manager
                        trust_mgr,           // trust manager
                        new SecureRandom()); // random number generator
       
				try {
					HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		    url = new URL(ifwTokenUrl);
		    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
		    // Guard against "bad hostname" errors during handshake.
	            con.setHostnameVerifier(new HostnameVerifier() {
	                public boolean verify(String host, SSLSession sess) {
	                    if (host.equals("localhost")) return true;
	                    else return false;
	                }
	            });
	 
   	    con.setRequestMethod("POST");
   	    con.setDoOutput(true);
   	    con.setDoInput(true);
   	    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
   	    con.setRequestProperty("Authorization", "Basic "+base64StringValue);
   	    OutputStream outputStream = con.getOutputStream();
   	    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
   	    outputStreamWriter.write("grant_type=client_credentials");
   	    outputStreamWriter.flush();
   	    outputStream.close();
   	    StringBuffer strBuffer = new StringBuffer();
        InputStream inputStream = con.getInputStream();
        int i;
        while ((i = inputStream.read()) != -1) {
               Writer writer = new StringWriter();
               writer.write(i);
               strBuffer.append(writer.toString());
           }
        
       logger.info("response"+strBuffer.toString());
       return  parseIFWTokenResponse(strBuffer.toString());
   	    

	 } catch (MalformedURLException e) {
		e.printStackTrace();
	 } catch (IOException e) {
		e.printStackTrace();
	 }catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	 }catch (KeyManagementException e) {
		e.printStackTrace();
     } 
    
    
    return null;
  }

  /*
   *  Parsing IFW Token Json Response
   */
  public String parseIFWTokenResponse(String jsonStr)
  {  
	    String bearerToken="";
		logger.info("parseIFWTokenResponse:::jsonStr:::"+jsonStr);
		ObjectMapper mapper = new ObjectMapper();
		if (jsonStr!=null && jsonStr.length()>0) {
		try {
	 			    org.codehaus.jackson.JsonNode rootNode = mapper.readTree(jsonStr);
	 			    bearerToken  = rootNode.path("access_token").getTextValue();
				    //logger.info("bearerToken is ::::"+bearerToken);				    
			    
		} catch (Exception e) {
			logger.error("parseIFWTokenResponse:::Error occurred while Parsing the Values from JSON  "+ e);
			e.printStackTrace();
		} 
		}

		return bearerToken;
	   
  }
  
//  sendSMSToConsumer("9740377938","cust1","partner","");

@Override
public  String  sendSMSToConsumer(String phonenNo,String customerName,String PartnerName,String PartnerPhno,String countryCode,int oppId)
{
    //String username = "ua499d24cb9575bb8cadc32e9e49e3854"; 
    //String password = "20742FC78C607BB5DB0E93E3B2ED306E"; 
    //String smsurl = "https://api.46elks.com/a1/SMS";
    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("103.27.171.32", 80));
	String USER_AGENT = "Mozilla/5.0";
    String result="false"; 
    int responseCode=0; 
    String encodedMessage="";
    //process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; 
   // proxy.

    String finalMessage = "";
    String jsonString= "";
    try {
		
    	//finalMessage = "Hej "+customerName+" Jag har läst din förfrågan från <NAME - TBD> och ringer dig inom 1 timma. MVH "+PartnerName+", Schneider Electric";
    	//finalMessage = "Hej "+customerName+"\n Jag kommer att ringa dig idag enligt dina önskemål för att boka in en tid för installation samt besvara eventuella frågor.\n Vill du kontakta mig ringer du på "+PartnerPhno+". \n Om du vill se mina tidigare projekt och läsa kundrecensioner gör du det under följande länk <a href=\"http://wiseup.se/#/orders\"></a>. \n Med vänliga hälsningar \n <PHOTO OF THE ELECTRICIAN>  "+PartnerName+" \n WiseUp ";
    	finalMessage = "Hej "+customerName+"\n Jag kommer att ringa dig idag enligt dina önskemål för att boka in en tid för installation samt besvara eventuella frågor.\n Vill du kontakta mig ringer du på "+PartnerPhno+". \n Om du vill se mina tidigare projekt och läsa kundrecensioner gör du det på <a href=\"http://www.wiseup.se/?oppid="+oppId+"#/orders\" style=\"color: #3DCD58;\">WiseUp.</a>. \n WiseUp ";
    	logger.info("finalMessage:::"+finalMessage);
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
    //String _msg = "from=abc&to=%2B917406446615&message=hello";
    //String  msg ="from=Schneider Electric&to=%2B"+countryCode+phonenNo+"&message="+finalMessage+" http://52.17.91.0/#/orders";
    
@SuppressWarnings("deprecation")
//String  msg ="from=WiseUp&to=+"+countryCode+phonenNo+"&message="+URLEncoder.encode(finalMessage)+"";
String  msg ="from=WiseUp&to=+"+phonenNo+"&message="+URLEncoder.encode(finalMessage)+"";
    logger.info("msg"+msg);
    encodedMessage = msg;
    try {
    	
           byte[] base64ByteArray = Base64.encode(smsUser+":"+smsPassword);
    	   URL url;
    	   url = new URL(smsUrl);
		   // Create a trust manager that does not validate certificate chains
           TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
            };
            
          
     
            // Install the all-trusting trust manager
            SSLContext sc=null;
			try {
				sc = SSLContext.getInstance("SSL");
	            sc.init(null, trustAllCerts, new java.security.SecureRandom());

			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
     
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
     
		HttpsURLConnection con = (HttpsURLConnection)url.openConnection();//proxy);
		con.setDoOutput(true);
   	    con.setRequestMethod("POST");
   	    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
   	    logger.debug("Authorization Basic "+new String(base64ByteArray));
   	    con.setRequestProperty("Authorization", "Basic "+new String(base64ByteArray));
   	    con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
   	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		//wr.writeBytes("from=schneider&to=+919066175905&message=Hai");
		logger.info("encodedMessage"+encodedMessage);
   	    wr.writeBytes(encodedMessage);
   	    wr.flush();
		wr.close();
		
		responseCode = con.getResponseCode();
		logger.info("\nSending 'POST' request to URL : " + url);
		logger.info("Post parameters : " + encodedMessage);
		logger.info("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		
		String inputLine;
		
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		logger.info(response.toString());
   	  
	 } catch (MalformedURLException e) {
		e.printStackTrace();
	 } catch (Exception e) {
		e.printStackTrace();
	 }
    
    if(responseCode==200)
      result="true";
    
     return result; 
    
  }

/* Send SMS to Partner when Consumer Cancels the order.
 * (non-Javadoc)
 * @see com.se.clm.bfo.client.HttpsClient#sendSMSToPartner(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String)
 */
public  String  sendSMSToPartner(String phonenNo,String customerName,String PartnerName,String oppName,String countryCode,int oppId,String cancelDate,String installationDate)
{
    //String username = "ua499d24cb9575bb8cadc32e9e49e3854"; 
    //String password = "20742FC78C607BB5DB0E93E3B2ED306E"; 
    //String smsurl = "https://api.46elks.com/a1/SMS";
    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("103.27.171.32", 80));
	String USER_AGENT = "Mozilla/5.0";
    String result="false"; 
    int responseCode=0; 
    String encodedMessage="";
    //process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; 
   // proxy.
    
    

    //String finalMessage = "";
    StringBuffer finalMessage = new StringBuffer();
    String jsonString= "";
    try {
		
    	//finalMessage = "hai "+PartnerName+" "+oppName+" has cancelled by consumer.Please remove from calender.";
    	//finalMessage = "Order avbokad!" + "\n Order "+ oppId + "avbokad och kommer tas bort från dina aktuella ordrar." + "\n" + "Kom ihåg att ta bort din kalendarbokning "+ cancelDate;
    	
    	finalMessage.append("Order avbokad!");
    	finalMessage.append("\n");
    	finalMessage.append("Order ");
    	finalMessage.append(oppId);
    	finalMessage.append(" avbokad och kommer tas bort från dina aktuella ordrar.");
    	if(installationDate!=null && installationDate.length()>0)
    	{	
    		finalMessage.append("\n");
    		finalMessage.append("Kom ihåg att ta bort din kalendarbokning ");
    		finalMessage.append(installationDate);
    	}
    	logger.debug("finalMessage:::"+finalMessage.toString());
    	
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
    //String _msg = "from=abc&to=%2B917406446615&message=hello";
    //String  msg ="from=Schneider Electric&to=%2B"+countryCode+phonenNo+"&message="+finalMessage+" http://52.17.91.0/#/orders";
    
@SuppressWarnings("deprecation")
//String  msg ="from=WiseUp&to=+"+countryCode+phonenNo+"&message="+URLEncoder.encode(finalMessage) ;
String  msg ="from=WiseUp&to=+"+phonenNo+"&message="+URLEncoder.encode(finalMessage.toString()) ;
//String  msg ="from=WiseUp&to=+"+phonenNo+"&message="+URLEncoder.encode(finalMessage.toString());

   //URLEncoder urlEncode = null; 
   //encodedMessage = URLEncoder.encode(msg);
   
    //logger.info("msg"+msg);
    encodedMessage = msg;
    try {
    	
           byte[] base64ByteArray = Base64.encode(smsUser+":"+smsPassword);
    	   URL url;
    	   url = new URL(smsUrl);
		   // Create a trust manager that does not validate certificate chains
           TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
            };
            
            
     
            // Install the all-trusting trust manager
            SSLContext sc=null;
			try {
				sc = SSLContext.getInstance("SSL");
	            sc.init(null, trustAllCerts, new java.security.SecureRandom());

			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
     
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
     
		HttpsURLConnection con = (HttpsURLConnection)url.openConnection();//proxy);
		con.setDoOutput(true);
   	    con.setRequestMethod("POST");
   	    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
   	    //con.setRequestProperty("Authorization", "Basic dWE0OTlkMjRjYjk1NzViYjhjYWRjMzJlOWU0OWUzODU0OjIwNzQyRkM3OEM2MDdCQjVEQjBFOTNFM0IyRUQzMDZF");
   	    con.setRequestProperty("Authorization", "Basic "+new String(base64ByteArray));
   	    con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
   	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		//wr.writeBytes("from=schneider&to=+919066175905&message=Hai");
		logger.info("encodedMessage"+encodedMessage);
   	    wr.writeBytes(encodedMessage);
   	    wr.flush();
		wr.close();
		
		responseCode = con.getResponseCode();
		logger.info("\nSending 'POST' request to URL : " + url);
		logger.info("Post parameters : " + encodedMessage);
		logger.info("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		
		String inputLine;
		
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		logger.info(response.toString());
   	  
	 } catch (MalformedURLException e) {
		e.printStackTrace();
	 } catch (Exception e) {
		e.printStackTrace();
	 }
    
    if(responseCode==200)
      result="true";
    
     return result; 
    
  }

@Override
public String sendSMSToConsumerWhenMeetingbooked(String phonenNo,
		String customerName, String PartnerName, String oppName,
		String custZip, String custCity, String custAddress,String meetingDate,String countryCode) {
	// TODO Auto-generated method stub
	    //String username = "ua499d24cb9575bb8cadc32e9e49e3854"; 
	    //String password = "20742FC78C607BB5DB0E93E3B2ED306E"; 
	    //String smsurl = "https://api.46elks.com/a1/SMS";
	    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("103.27.171.32", 80));
		String USER_AGENT = "Mozilla/5.0";
	    String result="false"; 
	    int responseCode=0; 
	    String encodedMessage="";
        String meetingDateValue="";
        String meetingTime=""; 
	   // String date = "8/29/2011 11:16:12 AM";
	    if(meetingDate!=null && meetingDate.length()>0)
	    {	
	    	
	    	String[] parts = meetingDate.split(" ");
	    	if(parts!=null && parts.length>0)
	    	{	
	    		meetingDateValue = parts[0];
	    		meetingTime = parts[1].substring(0, parts[1].length()-2);
	    		logger.debug("meetingDateValue: " + parts[0]);
	    		logger.debug("meetingTime: " + meetingTime);
	    	}
	    	

	    }
	    StringBuffer finalMessage = new StringBuffer();
	    String jsonString= "";
	    try {
			
	    	//finalMessage = "Hej "+customerName+" \n Tack för ett trevligt samtal. Din beställning är nu bekräftad som en order med installation "+meetingDate+".";
	    	
	    	 finalMessage.append("Hej "+customerName);
	    	 finalMessage.append("\n");
	    	 finalMessage.append("Här kommer en påminnelse om inbokad elinstallation den ");
	    	 finalMessage.append(meetingDateValue);
	    	 finalMessage.append(" kl. ");
	    	 finalMessage.append(meetingTime);	   
	    	 finalMessage.append(".");	    	 
	    	 finalMessage.append("\n");
	    	 finalMessage.append("Med vänliga hälsningar");
	    	 finalMessage.append("\n");
	    	 //finalMessage.append(PartnerName);
	    	 //finalMessage.append("\n");
	    	 finalMessage.append("WiseUp");
	    	 
	    	 logger.debug("finalmessage"+finalMessage.toString());
	    	 
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	@SuppressWarnings("deprecation")
	    //String  msg ="from=WiseUp&to=+"+countryCode+phonenNo+"&message="+URLEncoder.encode(finalMessage);
	    String  msg ="from=WiseUp&to=+"+phonenNo+"&message="+URLEncoder.encode(finalMessage.toString());
	    encodedMessage = msg;
	    try {
	    	
	           byte[] base64ByteArray = Base64.encode(smsUser+":"+smsPassword);
	    	   URL url;
	    	   url = new URL(smsUrl);
			   // Create a trust manager that does not validate certificate chains
	           TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                        return null;
	                    }
	                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
	                    }
	                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
	                    }
	                }
	            };
	            
	            
	     
	            // Install the all-trusting trust manager
	            SSLContext sc=null;
				try {
					sc = SSLContext.getInstance("SSL");
		            sc.init(null, trustAllCerts, new java.security.SecureRandom());

				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KeyManagementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	     
	            // Create all-trusting host name verifier
	            HostnameVerifier allHostsValid = new HostnameVerifier() {
	                public boolean verify(String hostname, SSLSession session) {
	                    return true;
	                }
	            };
	     
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();//proxy);
			con.setDoOutput(true);
	   	    con.setRequestMethod("POST");
	   	    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	   	    //con.setRequestProperty("Authorization", "Basic dWE0OTlkMjRjYjk1NzViYjhjYWRjMzJlOWU0OWUzODU0OjIwNzQyRkM3OEM2MDdCQjVEQjBFOTNFM0IyRUQzMDZF");
	   	    con.setRequestProperty("Authorization", "Basic "+new String(base64ByteArray));
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	   	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			//wr.writeBytes("from=schneider&to=+919066175905&message=Hai");
			logger.info("encodedMessage"+encodedMessage);
	   	    wr.writeBytes(encodedMessage);
	   	    wr.flush();
			wr.close();
			
			responseCode = con.getResponseCode();
			logger.info("\nSending 'POST' request to URL : " + url);
			logger.info("Post parameters : " + encodedMessage);
			logger.info("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			
			String inputLine;
			
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			logger.info(response.toString());
	   	  
		 } catch (MalformedURLException e) {
			e.printStackTrace();
		 } catch (Exception e) {
			e.printStackTrace();
		 }
	    
	    if(responseCode==200)
	      result="true";
	    
	     return result; 
	    }

private int indexOf(int length) {
	// TODO Auto-generated method stub
	return 0;
}
}
package com.se.clm.bfo.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParsersForUpdateComposite {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String,String> compositeXMLValues=parseCompositeXML("");
		String oppId=compositeXMLValues.get("oppId");
		String acctId=compositeXMLValues.get("acctId");
		String contactId=compositeXMLValues.get("contactId");
		System.out.println("oppId is :"+oppId +"acctId is:"+acctId+":contactId is:"+contactId);
		
		boolean updateXMLValues=parseUpdateXML(null);
		/*String Id=updateXMLValues.get("Id");
		String success=updateXMLValues.get("success");
		System.out.println("Id is :"+Id +":::::::::success is:"+success);*/
	}
	
	public static Map<String,String> parseComposit(String xml){
		// TODO Auto-generated method stub
		Map<String,String> compositeXMLValues=parseCompositeXML(xml);
		String oppId=compositeXMLValues.get("oppId");
		String acctId=compositeXMLValues.get("acctId");
		String contactId=compositeXMLValues.get("contactId");
		System.out.println("oppId is :"+oppId +"acctId is:"+acctId+":contactId is:"+contactId);
		
		
		
		return compositeXMLValues;
		/*Map<String,String> updateXMLValues=parseUpdateXML();
		String Id=updateXMLValues.get("Id");
		String success=updateXMLValues.get("success");
		System.out.println("Id is :"+Id +":::::::::success is:"+success);*/
	}
	
	
	
	public static Map<String,String> parseCompositeXML(String xml){
		HashMap<String,String> compositeoutputValues=null;
		compositeoutputValues=new HashMap<String,String>();
		
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
		    builder = builderFactory.newDocumentBuilder();
		   // Document document = builder.parse(new FileInputStream("CompositeRes.xml"));
		    InputSource is = new InputSource(new StringReader(xml));
		    Document document = builder.parse(is);
		    document.getDocumentElement().normalize();
		   // Node node = document.getElementsByTagName("createResponse");
		    //System.out.println(node);
		    NodeList nodeLst = document.getElementsByTagName("CreateOpportunity");
		   // System.out.println(nodeLst.getLength());
		    for (int s = 0; s < nodeLst.getLength(); s++) {
		    	
		    	Node fstNode = nodeLst.item(s);
		    	 if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
		    		  
		             Element fstElmnt = (Element) fstNode;
		            // System.out.println(fstElmnt.getNodeName());
		             NodeList childNodeList=fstElmnt.getChildNodes();
		             //System.out.println(childNodeList.getLength());
		            // Node resultNode=childNodeList.item(0).getNextSibling().getFirstChild().getNextSibling().getTextContent();
		             String id=childNodeList.item(0).getNextSibling().getFirstChild().getNextSibling().getTextContent();
		             String FinalId;
		             if(id!=null){
		            
		              System.out.println(id);
		             compositeoutputValues.put("oppId", id);
		             }else{
		            	 //throw exception
		             }
		             
		            

		    	 }
		    	
		    }
		    
		    NodeList acctnodeLst = document.getElementsByTagName("Status");
			   // System.out.println(nodeLst.getLength());
			    for (int s = 0; s < acctnodeLst.getLength(); s++) {
			    	
			    	Node fstNode = acctnodeLst.item(s);
			    	 if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
			    		  
			             Element fstElmnt = (Element) fstNode;
			            // System.out.println(fstElmnt.getNodeName());
			             NodeList childNodeList=fstElmnt.getChildNodes();
			             //System.out.println(childNodeList.getLength());
			             
			             if (childNodeList.item(0).getNextSibling()!=null){
			            	 
			            	   if(childNodeList.item(0).getNextSibling().getFirstChild().getNextSibling().getTextContent()!=null){
			            		   compositeoutputValues.put("acctId", childNodeList.item(0).getNextSibling().getFirstChild().getNextSibling().getTextContent());
			            	   }
				             String acctIdStr=childNodeList.item(0).getNextSibling().getFirstChild().getNextSibling().getTextContent();
				             /*String id=resultNode.getTextContent();
				             String FinalId;
				             if(id.contains("true")){
				            	 FinalId=id.replace("true", "").trim();
				             //System.out.println(id.replace("true", "").trim());
				             compositeoutputValues.put("acctId", FinalId);
				             }else if(id.contains("false")){
				            	 FinalId=id.replace("true", "").trim();
				            	 //System.out.println(FinalId);
				            	 compositeoutputValues.put("acctId", FinalId);
				             }*/
			             }

			    	 }
			    	
			    }
			    NodeList contactnodeLst = document.getElementsByTagName("Contact");
				   // System.out.println(nodeLst.getLength());
				    for (int s = 0; s < contactnodeLst.getLength(); s++) {
				    	
				    	Node fstNode = contactnodeLst.item(s);
				    	 if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				    		  
				             Element fstElmnt = (Element) fstNode;
				            // System.out.println(fstElmnt.getNodeName());
				             NodeList childNodeList=fstElmnt.getChildNodes();
				             //System.out.println(childNodeList.getLength());
				             if (childNodeList.item(1)!=null){
					             Node resultNode=childNodeList.item(1);
					             //System.out.println(resultNode.getTextContent());
					             String id=resultNode.getTextContent();
					             String[] id1=id.split("[\\s\\xA0\\n\\t]+");
				            	 //System.out.println("+++++++++"+id1[0].trim()+id1[1].trim()+id1[2].trim());
				            	 compositeoutputValues.put("contactId" ,id1[2].trim());
				             }
			            }
				    	
				    }

		    	

		} catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}catch (SAXException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		return compositeoutputValues;
	}
	
	public static boolean parseUpdateXML(String xml){
		HashMap<String,String> updateoutputValues=null;
		updateoutputValues=new HashMap<String,String>();
		boolean response=false;
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			builder = builderFactory.newDocumentBuilder();
			 InputSource is = new InputSource(new StringReader(xml));
		    Document document = builder.parse(is);
		    document.getDocumentElement().normalize();
		    NodeList nodeLst = document.getElementsByTagName("ObjectResponse");
		    	for (int s = 0; s < nodeLst.getLength(); s++) {
		    	
		    	Node fstNode = nodeLst.item(s);
		    	 if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
		    		  
		             Element fstElmnt = (Element) fstNode;
		            // System.out.println(fstElmnt.getNodeName());
		             NodeList childNodeList=fstElmnt.getChildNodes();
		             //System.out.println(childNodeList.getLength());
		             Node resultNode=childNodeList.item(1);
		             
		             String id=resultNode.getTextContent();
		             String[] id1=id.split("[\\s\\xA0\\n\\t]+");
	            	 //System.out.println("+++++++++"+id1[0].trim()+id1[1].trim()+id1[2].trim());
		             updateoutputValues.put("contactId" ,id1[2].trim());
		             if(id1[2].trim().equalsIgnoreCase("true")){
		            	 response=true;
		             }
		             
		            /* String FinalId;
		             if(id.contains("true")){
		            	 FinalId=id.replace("true", "").trim();
		             //System.out.println(id.replace("true", "").trim());
		             updateoutputValues.put("Id", FinalId);
		             updateoutputValues.put("success", "true");
		             }else if(id.contains("false")){
		            	 FinalId=id.replace("false", "").trim();
		            	 //System.out.println(FinalId);
		            	 updateoutputValues.put("Id", FinalId);
		            	 updateoutputValues.put("success", "false");
		             }*/
		             
		            

		    	 }
		    	
		    }
			
		}catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}catch (SAXException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		return response; 
	}
	
	
	
	
	public static boolean parseUpdateQuoteXML(String xml){
		HashMap<String,String> updateoutputValues=null;
		updateoutputValues=new HashMap<String,String>();
		boolean response=false;
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			builder = builderFactory.newDocumentBuilder();
			 InputSource is = new InputSource(new StringReader(xml));
		    Document document = builder.parse(is);
		    document.getDocumentElement().normalize();
		    NodeList nodeLst = document.getElementsByTagName("Status");
		    	for (int s = 0; s < nodeLst.getLength(); s++) {
		    	
		    	Node fstNode = nodeLst.item(s);
		    	 if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
		    		  
		             Element fstElmnt = (Element) fstNode;
		            // System.out.println(fstElmnt.getNodeName());
		             NodeList childNodeList=fstElmnt.getChildNodes();
		             //System.out.println(childNodeList.getLength());
		             Node resultNode=childNodeList.item(1);
		             
		             String id=resultNode.getTextContent();
		             String[] id1=id.split("[\\s\\xA0\\n\\t]+");
	            	 //System.out.println("+++++++++"+id1[0].trim()+id1[1].trim()+id1[2].trim());
		             updateoutputValues.put("contactId" ,id1[2].trim());
		             if(id1[2].trim().equalsIgnoreCase("true")){
		            	 response=true;
		             }
		             
		            /* String FinalId;
		             if(id.contains("true")){
		            	 FinalId=id.replace("true", "").trim();
		             //System.out.println(id.replace("true", "").trim());
		             updateoutputValues.put("Id", FinalId);
		             updateoutputValues.put("success", "true");
		             }else if(id.contains("false")){
		            	 FinalId=id.replace("false", "").trim();
		            	 //System.out.println(FinalId);
		            	 updateoutputValues.put("Id", FinalId);
		            	 updateoutputValues.put("success", "false");
		             }*/
		             
		            

		    	 }
		    	
		    }
			
		}catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}catch (SAXException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		return response; 
	}


	 
}

package com.se.clm.mail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.se.cl.constants.CountryCode;

@Service("emailservice")
public class EmailServiceImpl implements IEmailService {

	/*
	 * mailSender object will be injected by the spring.
	 */


	 private JavaMailSenderImpl mailSender;

	    public void setMailSender(JavaMailSenderImpl mailSender) {
	        this.mailSender = mailSender;
	    }
	
	@Value("${mail.server.username}")
	private String mailServerUser;
	@Value("${mail.server.password}")
	private String mailServerPass;
	@Value("${mail.server.host}")
	private String mailHost;
	@Value("${mail.server.port}")
	private int mailserverPort;
	
	
	@Value("${fr.mail.server.username}")
	private String frmailServerUser;
	@Value("${fr.mail.server.password}")
	private String frmailServerPass;
	@Value("${fr.mail.server.host}")
	private String frmailHost;
	@Value("${fr.mail.server.port}")
	private int frmailserverPort;
	
	@Value("${sw.mail.server.username}")
	private String swmailServerUser;
	@Value("${sw.mail.server.password}")
	private String swmailServerPass;
	@Value("${sw.mail.server.host}")
	private String swmailHost;
	@Value("${sw.mail.server.port}")
	private int swmailserverPort;
	
	
	
	
	public void sendApprovedMail(String firstName, String toEmail, String projectLink, BigInteger projectId, String projectName, String submissionDate) {
		try {
		 StringBuilder body = new StringBuilder("Dear ");
		 body.append(firstName);
		 body.append("\n\r   We are happy to tell you that your request dated ");
		 body.append(submissionDate);
		 body.append(" to share your project \"");
		 body.append(projectName);
		 body.append(" \"(");
		 body.append(projectId);
		 body.append("), is now approved and available to all users at <a href=\"");
		 body.append(projectLink);
		 body.append("\">" + projectLink);
		 body.append("</a>");

		 String mailSubject = "Project \"" + projectName + "\" (" + projectId + ") is approved";
		 sendMailInter(toEmail, body, mailSubject);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	 }

	/**
	 *  This method sends the mail.
	 * @param toEmail
	 * @param body
	 * @param mailSubject
	 * @throws MessagingException
	 */
	private void sendMailInter(String toEmail, StringBuilder body, String mailSubject)
			throws MessagingException {
		
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		//mailSender.setHost("smtp.gmail.com");
		//mailSender.setPort(Integer.parseInt("578"));
		mailSender.setUsername("closedloopdemo@gmail.com");
		mailSender.setPassword("closedloop123");
		/*mimeMessage.setContent(o, type);
		
		Message msg = new MimeMessage(session);
		msg.setContent(message, "text/html");*/
		//mailSender.
		 MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		helper.setSentDate(new Date());
		helper.setTo(toEmail);
		helper.setFrom(mailServerUser);
		helper.setSubject(mailSubject);
		helper.setText(body.toString());
		
		
		mailSender.send(mimeMessage);
		
		
		
	}

	@Override
	public void sendMail(String toEmail, String body, String mailSubject, FileSystemResource pdfFile, FileSystemResource csvFile,String country,int templateId) throws MessagingException {
			
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		InternetAddress address=new InternetAddress();
		try {
			address.setPersonal("\"WiseUp\"");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		if (country.equalsIgnoreCase(CountryCode.AUSTRALIA.getValue())){
			mailSender.setHost(mailHost);
			mailSender.setPort(mailserverPort);
			mailSender.setUsername(mailServerUser);
			mailSender.setPassword(mailServerPass);
		}else if(country.equalsIgnoreCase(CountryCode.FRANCE.getValue())){
			mailSender.setHost(frmailHost);
			mailSender.setPort(frmailserverPort);
			mailSender.setUsername(frmailServerUser);
			mailSender.setPassword(frmailServerPass);
		}else if(country.equalsIgnoreCase(CountryCode.SWEDEN.getValue()))
		{
			mailSender.setHost(swmailHost);
			mailSender.setPort(swmailserverPort);
			mailSender.setUsername(swmailServerUser);
			mailSender.setPassword(swmailServerPass);
		}
		
		//mailSender.
		 MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		helper.setSentDate(new Date());
		helper.setTo(toEmail);
		/*if (country.equalsIgnoreCase(CountryCode.AUSTRALIA.getValue())){
			helper.setFrom(mailServerUser);
		}else if(country.equalsIgnoreCase(CountryCode.FRANCE.getValue()) || country.equalsIgnoreCase(CountryCode.SWEDEN.getValue())){
			helper.setFrom(frmailServerUser);
		}
		*/
		
		if (country.equalsIgnoreCase(CountryCode.AUSTRALIA.getValue())){
			helper.setFrom(mailServerUser);
		}else if(country.equalsIgnoreCase(CountryCode.FRANCE.getValue())){
			helper.setFrom(frmailServerUser);
		}else if(country.equalsIgnoreCase(CountryCode.SWEDEN.getValue()))
		{
			//helper.setFrom(swmailServerUser);
			try {
				helper.setFrom(swmailServerUser, "\"WiseUp\"");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		helper.setSubject(mailSubject);
		helper.setText(body,true);
		
		ClassPathResource resource=null;
		
		if(templateId==0)
		{
			 resource = new ClassPathResource("email.png");
			 helper.addInline("identifier501", resource);
			 resource = new ClassPathResource("facebook1.png");
			 helper.addInline("identifier502", resource);
			 resource = new ClassPathResource("clipsal-lio-logo-white.png");
			 helper.addInline("identifier500", resource);
			 resource = new ClassPathResource("clipsal-logo-white1.png");
			 helper.addInline("identifier503", resource);
			 resource = new ClassPathResource("instagram.png");
			 helper.addInline("identifier504", resource);
			 resource = new ClassPathResource("twitter1.png");
			 helper.addInline("identifier505", resource);
			 resource=new ClassPathResource("youtube1.png");
			 helper.addInline("identifier506",resource);
			 resource=new ClassPathResource("pinterest.png");
			 helper.addInline("identifier507",resource);
		}
		
		if(templateId==3)
		{
			resource = new ClassPathResource("dot.png");
			helper.addInline("identifier1", resource);
			resource = new ClassPathResource("logo.png");
			helper.addInline("identifier2", resource);
			resource = new ClassPathResource("PictureA_V2.jpg");
			helper.addInline("identifier3", resource);
			resource = new ClassPathResource("Unica_Class_GLASS.png");
			helper.addInline("identifier4", resource);
			resource = new ClassPathResource("Disjoncteur_Circuit_breaker_DClic_XE.png");
			helper.addInline("identifier5", resource);
			
			resource = new ClassPathResource("Coffret_Lexcom_Box_accessories.png");
			helper.addInline("identifier51", resource);
			resource = new ClassPathResource("contactez-nous.png");
			helper.addInline("identifier6", resource);
			resource = new ClassPathResource("chevron.png");
			helper.addInline("identifier7", resource);
			resource = new ClassPathResource("Picture4.jpg");
			helper.addInline("identifier8", resource);
			resource = new ClassPathResource("Picture5.jpg");
			helper.addInline("identifier9", resource);
			resource = new ClassPathResource("Picture6.jpg");
			helper.addInline("identifier10", resource);
			resource = new ClassPathResource("facebook.png");
			helper.addInline("identifier11", resource);
			resource = new ClassPathResource("twitter.png");
			helper.addInline("identifier12", resource);
			resource = new ClassPathResource("linkedin.png");
			helper.addInline("identifier13", resource);
			resource = new ClassPathResource("google.png");
			helper.addInline("identifier14", resource);
			resource = new ClassPathResource("youtube.png");
			helper.addInline("identifier15", resource);
			resource = new ClassPathResource("logo-lio.png");
			helper.addInline("identifier16", resource);
			
		}
		if(templateId==1)
		{
			resource = new ClassPathResource("dot.png");
			helper.addInline("identifier1", resource);
			resource = new ClassPathResource("logo.png");
			helper.addInline("identifier2", resource);
			resource = new ClassPathResource("main-pict.png");
			helper.addInline("identifier3", resource);
			resource = new ClassPathResource("visuel1.jpg");
			helper.addInline("identifier4", resource);
			resource = new ClassPathResource("PictureA_V2.jpg");
			helper.addInline("identifier5", resource);
			resource = new ClassPathResource("contactez-nous.png");
			helper.addInline("identifier6", resource);
			resource = new ClassPathResource("chevron.png");
			helper.addInline("identifier7", resource);
			resource = new ClassPathResource("Picture4.jpg");
			helper.addInline("identifier8", resource);
			resource = new ClassPathResource("Picture5.jpg");
			helper.addInline("identifier9", resource);
			resource = new ClassPathResource("Picture6.jpg");
			helper.addInline("identifier10", resource);
			resource = new ClassPathResource("facebook.png");
			helper.addInline("identifier11", resource);
			resource = new ClassPathResource("twitter.png");
			helper.addInline("identifier12", resource);
			resource = new ClassPathResource("linkedin.png");
			helper.addInline("identifier13", resource);
			resource = new ClassPathResource("google.png");
			helper.addInline("identifier14", resource);
			resource = new ClassPathResource("youtube.png");
			helper.addInline("identifier15", resource);
			resource = new ClassPathResource("logo-lio.png");
			helper.addInline("identifier16", resource);
			
		}
		if(templateId==4)
		{
			System.out.println("invoice template");
			resource = new ClassPathResource("facebookIcon.png");
			helper.addInline("identifier101", resource);
			resource = new ClassPathResource("lifeison_schneider_white.png");
			helper.addInline("identifier102", resource);
			resource = new ClassPathResource("Wiseup-268px.png");
			helper.addInline("identifier103", resource);
			
		}
		if(templateId==5)
		{
			
			
			resource = new ClassPathResource("johan_36.png");
			helper.addInline("identifier100", resource);
			resource = new ClassPathResource("facebookIcon.png");
			helper.addInline("identifier101", resource);
			resource = new ClassPathResource("lifeison_schneider_white.png");
			helper.addInline("identifier102", resource);
			resource = new ClassPathResource("Wiseup-268px.png");
			helper.addInline("identifier103", resource);
		}
		if(templateId==6)
		{
			
			
			resource = new ClassPathResource("facebookIcon.png");
			helper.addInline("identifier101", resource);
			resource = new ClassPathResource("lifeison_schneider_white.png");
			helper.addInline("identifier102", resource);
			resource = new ClassPathResource("Wiseup-268px.png");
			helper.addInline("identifier103", resource);
		}

		
		 //FileSystemResource file = new FileSystemResource("/home/abdennour/Documents/cv.pdf");
	    try {
			if(pdfFile!=null && pdfFile.contentLength()>100){
				
				System.out.println("content length: "+ pdfFile.contentLength());
			  System.out.println("PDF attached");
				helper.addAttachment("bomPDF.pdf", pdfFile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	    if(csvFile!=null){
	    	System.out.println("CSV attached");
	    	
	    	helper.addAttachment("bomCSV.csv", csvFile);
	    }
		
	    mailSender.getSession().setDebug(false);
	/*	MimeMultipart multipart = new MimeMultipart("related");
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(body, "text/html");
		
		
		MimeBodyPart imagePart = new MimeBodyPart();

		try {
			imagePart.attachFile("emailfooter.png");
		} catch (IOException e) {
			e.printStackTrace();
		}

		imagePart.setContentID("<" + 123 + ">");

		imagePart.setDisposition(MimeBodyPart.INLINE);*/
		
		
		mailSender.send(mimeMessage);
		
		

		
	}
	
	
	/*public static void  setFooter(){

		String cid = "";

				MimeBodyPart textPart = new MimeBodyPart();

				textPart.setText("<html><head><title>This is not usually displayed</title></head><body><div>And here's an image"
						+ "</div><div>I hope you like it!</div></body></html>",  "US-ASCII", "html");

				content.addBodyPart(textPart);

				and the image part with
				

				MimeBodyPart imagePart = new MimeBodyPart();

				imagePart.attachFile("resources/teapot.jpg");

				imagePart.setContentID("<" + cid + ">");

				imagePart.setDisposition(MimeBodyPart.INLINE);

				content.addBodyPart(imagePart);
	}*/
	
	
	/**
	 *  This method send the rejected mail to user.
	 * @param firstName User first name.
	 * @param toEmail User email id.
	 * @param bean AdminRejectProjectBean.
	 * @param projectId Project id.
	 * @param projectName project name.
	 * @param submissionDate submission date.
	 */
	
	
	public static Session buildGoogleSession() {

		  Properties mailProps = new Properties();

		  mailProps.put("mail.transport.protocol", "smtp");
		  mailProps.put("mail.host", "smtp.gmail.com");
		  //mailProps.put("mail.smtp.host", "smtp.gmail.com");
		  mailProps.put("mail.from", "closedloopdemo@gmail.com");
		  mailProps.put("mail.smtp.starttls.enable", "true");
		  mailProps.put("mail.smtp.port", "587");
		  mailProps.put("mail.smtp.auth", "false");
		  //props.setProperty("mail.smtp.host", "smtp.gmail.com");
		  // final, because we're using it in the   closure below

		  final PasswordAuthentication usernamePassword =
		    new PasswordAuthentication("closedloopdemo@gmail.com", "closedloop123");
		  Authenticator auth = new Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		      return usernamePassword;
		    }
		  };
		  Session session = Session.getInstance(mailProps, auth);
		  session.setDebug(true);
		  return session;

		}
	
	
	
		public static void main(String a[]){
			
		//send 
			
			Session session=EmailServiceImpl.buildGoogleSession();
			
			  MimeMessage message = new MimeMessage(session);


			  try{
			  // Set From: header field of the header.
		         message.setFrom(new InternetAddress("closedloopdemo@gmail.com"));

		         // Set To: header field of the header.
		         message.addRecipient(Message.RecipientType.TO,
		                                  new InternetAddress("smarni2014@gmail.com"));

		         // Set Subject: header field
		         message.setSubject("This is the Subject Line!");

		         // Now set the actual message
		         message.setText("This is actual message");

		         // Send message
		         Transport.send(message);
		         
			  }catch(Exception e){
				  e.printStackTrace();
			  }
			
		}
	
	
}

package com.se.clm.mail;

public class AUTemplate {

	public static String getTemplate(String secA, String secB) {
 
		//img src=\"cid:identifier1\
		
		StringBuilder body = new StringBuilder();
		body.append("<!doctype html>");
		body.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		body.append("<head>");
		body.append("<meta http-equiv=\"Content-Type\" content=\"text/html\"; charset=\"utf-8\" />");
		body.append("<title>Title goes here</title>");
		body.append("<style type=\"text/css\">");
		body.append(".ReadMsgBody {width: 100%; background-color: #000000;}");
		body.append(".ExternalClass {width: 100%; background-color: #000000;}");
		body.append("body	 {width: 100%; background-color: #000000; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Arial, Helvetica, sans-serif}");
		body.append("table {border-collapse: collapse;}");
		body.append("@media only screen and (max-width: 640px)  {");
		body.append("body[yahoo] .deviceWidth {width:440px!important; padding:0;}");
		body.append("body[yahoo] .center {text-align: center!important;}");
		body.append("}");
		body.append("@media only screen and (max-width: 479px) {");
		body.append("body[yahoo] .deviceWidth {width:280px!important; padding:0;}");
		body.append("body[yahoo] .center {text-align: center!important;}");
		body.append("}");
		body.append("</style>");
		body.append("</head>");
		body.append(" <body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" yahoo=\"fix\" style=\"font-family: Arial, Helvetica, sans-serif\">");
		body.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">");
		body.append("<tr>");
		body.append("<td width=\"100%\" valign=\"top\" bgcolor=\"#FFFFFF\" style=\"padding-top:0px\">");
		body.append("<table width=\"580\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"deviceWidth\">");
		body.append(" <tr>");
		body.append("<td width=\"100%\" bgcolor=\"#FFFFFF\">");
		body.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"deviceWidth\">");
		body.append("<tr>");
		body.append("<td class=\"center\" style=\"font-size: 13px; color: #000000; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 0px; vertical-align: middle; padding:0px 0px; font-style:normal\">");
		body.append("Clipsal by Schneider Electric creates Australia's brightest light&nbsp;switches");
		body.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		body.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append(" </td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</td>");
		body.append("</tr>");
		body.append(" </table>");
		body.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">");
		body.append(" <tr>");
		body.append("<td width=\"100%\" valign=\"top\" bgcolor=\"#000000\" style=\"padding-top:3px\">");
		body.append("<table width=\"580\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"deviceWidth\">");
		body.append(" <tr>");
		body.append("<td width=\"100%\" bgcolor=\"#000000\">");
		body.append("<table width=\"580\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"top\" class=\"deviceWidth\">");
		body.append("<tr>");
		body.append("<td width=\"100%\" bgcolor=\"#000000\">");
		body.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" class=\"deviceWidth\">");
		body.append("<tr>");
		body.append("<td style=\"padding:10px 3px\" class=\"left\">");
		body.append("<a href=\"#\"><img width=\"280px\" src=\"cid:identifier500\" alt=\"\" border=\"0\" /></a>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("<table width=\"580\"  class=\"deviceWidth\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" bgcolor=\"#000000\">");
		body.append(" <tr>");
		body.append(" <td style=\"font-size: 23px; color: #9FA0A4; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding:10px 8px 20px 3px\" bgcolor=\"#000000\">");
		body.append(" <table>");
		body.append("<tr>");
		body.append("<td valign=\"middle\" style=\"padding:0px 10px 10px 0\"><a href=\"#\" style=\"text-decoration: none; color: #CCCCCC; font-size: 40px; line-height: 40px; color: #CCCCCC; font-weight: bold; font-family: Arial, Helvetica, sans-serif \">"+secA+"</a></td>");
		body.append("</tr>");
		body.append("</table>");
		body.append(" </td>");
		body.append("</tr>");
		body.append(" </table>");
		body.append("<table width=\"580\"  class=\"deviceWidth\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" bgcolor=\"#000000\">");
		body.append("<tr>");
		body.append("<td valign=\"top\" style=\"padding:0\" bgcolor=\"#000000\">");
		body.append("</td>");
		body.append("</tr>");
		body.append("<tr>");
		body.append("<td style=\"font-size: 12px; color: #9FA0A4; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 24px; vertical-align: top; padding:10px 8px 10px 8px\" bgcolor=\"#000000\">");
		body.append(secB);
		body.append("<br>");
		body.append(" 1.Please note this lead has been sent to a number of certified installers");
		body.append(" <br>");
		body.append(" 2.To accept the request, please login to the Clipsal Connect mobile app.");
		body.append("<br>");
		body.append(" 3.If you do not accept the Lead within 72 hours, the lead will be reassigned to another certified installer. ");
		body.append("<br><br>");
		body.append("Thank you,");
		body.append("<br>");
		body.append("Clipsal and Schneider Electric Consumer Support Team");
		body.append("<br>");
		body.append("Email: consumer.supportau@schneider-electric.com| Tel: 1300669925 ");
		body.append("<br>");
		body.append("Monday - Friday, 8:30am - 7:30pm (AEST)");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append(" </td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("<div style=\"height:35px\">&nbsp;</div>");
		body.append("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">");
		body.append("<tr>");
		body.append("<td bgcolor=\"#393a3d\" style=\"padding:30px 0\">");
		body.append("<table width=\"580\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"deviceWidth\">");
		body.append("<tr>");
		body.append("<td>");
		body.append("<table width=\"40%\" cellpadding=\"0\" cellspacing=\"0\"  border=\"0\" align=\"left\" class=\"deviceWidth\">");
		body.append("<tr>");
		body.append("<td valign=\"top\" style=\"font-size: 10px; color: #999999; font-family: Arial, sans-serif; padding-bottom:10px\" class=\"center\">");
		body.append("<p>&copy; 2015 Schneider Electric. All&nbsp;Rights&nbsp;Reserved.</p>");
		body.append("<p>Please do not reply to this email. <br/>If you need support, please <a href=\"https://www.clipsal.com/Home-Owner/Contact-Us-Form\" style=\"color:#999999;text-decoration:underline;\">contact us</a>.</p>");
		body.append("<p>You are receiving this because you are part of our Clipsal Connect programme. If you'd like to stop being part of this programme please call 1300 669 925.</p>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("<table width=\"40%\" cellpadding=\"0\" cellspacing=\"0\"  border=\"0\" align=\"right\" class=\"deviceWidth\">");
		body.append("<tr>");
		body.append("<td valign=\"top\" style=\"font-size: 11px; color: #999999; font-weight: normal; font-family: Arial, Helvetica, sans-serif; line-height: 26px; vertical-align: top; text-align:right\" class=\"center\">");
		body.append("<a href=\"mailto:consumer.supportau@schneider-electric.com\"><img width=\"30px\" src=\"cid:identifier501\" width=\"30\" height=\"30\" alt=\"Email\" title=\"Email\" border=\"0\" /></a>");
		body.append("<a href=\"https://www.facebook.com/ClipsalAU\"><img width=\"30px\" src=\"cid:identifier502\" width=\"30\" height=\"30\" alt=\"Facebook\" title=\"Facebook\" border=\"0\" /></a>");
		body.append("<a href=\"#\"><img width=\"30px\" src=\"cid:identifier507\" width=\"30\" height=\"30\" alt=\"Pinterest\" title=\"Pinterest\" border=\"0\" /></a>");
		body.append("<a href=\"https://twitter.com/SEAustralia\"><img width=\"30px\" src=\"cid:identifier505\" width=\"30\" height=\"30\" alt=\"Twitter\" title=\"Twitter\" border=\"0\" /></a>");
		body.append("<a href=\"http://www.youtube.com/user/ClipsalbySE\"><img width=\"30px\" src=\"cid:identifier506\" width=\"30\" height=\"30\" alt=\"YouTube\" title=\"YouTube\" border=\"0\" /></a>");
		body.append("<a href=\"#\"><img width=\"30px\" src=\"cid:identifier504\" width=\"30\" height=\"30\" alt=\"Instagram\" title=\"Instagram\" border=\"0\" /></a>");
		body.append("<br />");
		body.append("<a href=\"http://clipsal.com\"><img width=\"120px\" src=\"cid:identifier503\" alt=\"\" border=\"0\" style=\"padding-top: 5px;\" /></a><br/>");
		body.append("<a href=\"tel:1300669925\" style=\"text-decoration: none; color: #999999; font-weight: normal;\">1300 669 925</a><br/>");
		body.append("<a href=\"mailto:consumer.supportau@schneider-electric.com\" style=\"text-decoration: none; color: #999999; font-weight: normal;\">consumer.supportau@schneider-electric.com</a>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</td>");
		body.append("</tr>");
		body.append("</table>");
		body.append("</body>");
		body.append("</html>");

		return body.toString();

	}
	
	
	/*public static void main(String[] args) {
		StringBuilder secA=new StringBuilder("<html><body><h1>You have a lead via Clipsal Connect");
		StringBuilder secB=new StringBuilder();
		secB.append("<br>Customer details:");
		secB.append("<br>Job number:");
		secB.append("Arun");
		secB.append("<br>Date requested:");
		secB.append("18/9/2018");
		secB.append("<br>Name:</b> ");
		secB.append("kumar");
		secB.append("<br> Postcode:");
		secB.append("3060");
		secB.append("<br>Email address: ");
		secB.append("sunke.arun@gmail.com");
		secB.append("<br>Contact number:");
		secB.append("12345");
		
		AUTemplate temp=new AUTemplate();
		System.out.println("AU Template");
		
		
		System.out.println(temp.getTemplate(secA.toString(), secB.toString()));
	}*/

}

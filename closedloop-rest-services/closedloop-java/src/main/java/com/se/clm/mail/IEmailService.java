package com.se.clm.mail;

import java.math.BigInteger;

import javax.mail.MessagingException;

import org.springframework.core.io.FileSystemResource;

public interface IEmailService {

	/**
	 *  This method sends the approved mail to user.
	 * @param firstName User first name.
	 * @param toEmail User email id.
	 * @param projectLink Project link to access.
	 * @param projectId Project Id.
	 * @param projectName Project Name.
	 * @param submissionDate submission date.
	 */
	public void sendApprovedMail(String firstName, String toEmail, String projectLink, BigInteger projectId, String projectName, String submissionDate);
	
	/**
	 *  This method send the rejected mail to user.
	 * @param firstName User first name.
	 * @param toEmail User email id.
	 * @param bean AdminRejectProjectBean.
	 * @param projectId Project id.
	 * @param projectName project name.
	 * @param submissionDate submission date.
	 */
	//public void sendRejectedMail(String firstName, String toEmail, AdminRejectProjectBean bean, BigInteger projectId, String projectName, String submissionDate);


	public void sendMail(String toEmail, String body, String mailSubject, FileSystemResource pdfFile, FileSystemResource cvsFile,String country,int templateId) throws MessagingException;
}

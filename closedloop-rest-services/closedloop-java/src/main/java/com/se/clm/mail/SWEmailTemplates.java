package com.se.clm.mail;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.se.cl.model.BOM;
import com.se.cl.util.EncryptDecryptUtil;

public class SWEmailTemplates {

	public static List<String> sendEmailtoPartnerForNewRequest(String toEMail,
			String partnerName, String projectName, String consumerName,
			String Codepostal, String montantEstimatif, String oppId,
			List<BOM> boms, String address, String city) {

		try {
			
			String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id=EncryptDecryptUtil.encrypt(oppId);
        		
        	}
			
        	StringBuilder secA = new StringBuilder(oppId);
			StringBuilder secB = new StringBuilder();
			secB.append("Hej" + " " + partnerName);
			secB.append("<br><br>Stort tack f&ouml;r din best&auml;llning  nummer"+ " "+ oppId+ " "+ "Du kan se aktuell status f&ouml;r din best&auml;llning p&aring;"+" "+ "<a href=\"http://www.wiseup.se/#/orders/"+id+"\" style=\"color: #3DCD58;\">WiseUp.</a>");
			secB.append("<br><br>En av v&aring;ra certifierade elektriker kommer kontakta dig enligt dina &ouml;nskem&aring;l f&ouml;r att boka in en tid f&ouml;r installation.");
			secB.append("<br><br>N&auml;r ni har kommit &ouml;verens om ett installationsdatum kommer vi att skicka en orderbekr&auml;ftelse.</b> ");
			secB.append("<br><br>Best&auml;llningen &auml;r registrerad f&ouml;r");
			secB.append("<br><a style=\"color: #3DCD58;\">" + consumerName+ "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + address + "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + Codepostal
					+ "</a>" + "&nbsp;" + "&nbsp;"
					+ "<a style=\"color: #3DCD58;\">" + city + "</a>");
			secB.append("<br><br>Din best&auml;llnings&ouml;versikt");
			secB.append("<br><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">");
			secB.append("<tr>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Artikel</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Antal</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">summa</th>");
			secB.append("</tr>");
			for (BOM bom : boms) {
				secB.append("<tr>");
				secB.append("<td align=\"left\"><br>" + " "
						+ bom.getProductName() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getQuantity() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getUnitCost() + " " + "kr" + "</td>");
				secB.append("</tr>");
			}
			secB.append("</table>");
			
	       String templateData="";
			
			InputStream htmlStream = null;
			htmlStream = SWEmailTemplates.class.getClassLoader().getResourceAsStream("Created.html");
			templateData = getStringFromInputStream(htmlStream);
			templateData=templateData.replace("secA", secA);
			templateData=templateData.replace("secB", secB);
			//templateData=templateData.replace("oppNumber",oppId);"
			templateData=templateData.replace("Wiselink","<a href=\"http://www.wiseup.se/#/orders/"+id+"\" style=\"color: #3DCD58;\"text-decoration:none;>WiseUp.</a>");

			String mailSubject = "Beställningsbekräftelse - ordernummer " + " "+ oppId;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(templateData);
           
			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static String getFooter() {
		return "<img src='cid:identifier1234'>";

	}

	public static List<String> sendEmailtoConsumerFollowBy(String toEMail,
			String partnerName, String company, String consumerName,
			String phNumber, String oppName, String oppId) {

		try {
			String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        	id=EncryptDecryptUtil.encrypt(oppId);
        	}

			StringBuilder secA = new StringBuilder(oppId);
			StringBuilder secB = new StringBuilder();
			secB.append("Hej " + " " + consumerName);
			secB.append("<br><br>Jag kommer att ringa dig idag enligt dina &ouml;nskem&aring;l f&ouml;r att boka in en tid f&ouml;r installation samt besvara eventuella fr&aring;gor.");
			secB.append("<br><br>Vill du kontakta mig ringer du p&aring;"+" "+"072 561 15 95");
			secB.append("<br><br> Om du vill se mina tidigare projekt och l&auml;sa kundrecensioner g&ouml;r du det p&aring; WiseUp");
			
			StringBuilder secC = new StringBuilder();
			secC.append(partnerName);
			
			String templateData="";
			InputStream htmlStream = null;
			htmlStream = SWEmailTemplates.class.getClassLoader().getResourceAsStream("Accept.html");
			templateData = getStringFromInputStream(htmlStream);
			/*FileReader fileReader = new FileReader("C:\\templates\\Accept.html");
			BufferedReader bufferReader = new BufferedReader(fileReader);
			String str="";
			while((str=bufferReader.readLine()) !=  null){
				
				templateData =	templateData+str;
				
				}*/
			templateData=templateData.replace("secA", secA);
			templateData=templateData.replace("secB", secB);
			//templateData=templateData.replace("Wiselink","<a href=\"http://www.wiseup.se/?oppid="+oppId+"#/orders\" style=\"color: #3DCD58;\">WiseUp.</a>" );
			templateData=templateData.replace("Wiselink","<a href=\"http://www.wiseup.se/#/orders/"+id+"\" style=\"color: #3DCD58;\"text-decoration:none;>WiseUp.</a>");

			
			String mailSubject = " Introduktion till din personliga elektriker  - ordernummer "+ " " + oppId;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(templateData);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoConsumerWhenPartnerBookedMeeting(
			String toEMail, String partnerName, String company,
			String consumerName, String phNumber, String oppName,
			List<BOM> boms, int oppId, String address, String zip, String city,
			String meetingDate) {
		try {
			String oppID=String.valueOf(oppId);
			String id="";
        	if(EncryptDecryptUtil.isNumeric(oppID))
        	{
        		id=EncryptDecryptUtil.encrypt(oppID);
        		
        	}
			StringBuilder secA = new StringBuilder(oppID);
			StringBuilder secB = new StringBuilder();
			secB.append("Hej" + " " + consumerName);
			secB.append("<br><br>  Tack f&ouml;r ett trevligt samtal. Din best&auml;llning &auml;r nu bekr&auml;ftad som en order med installationsdatum den "
					+ "<a style=\"color: #3DCD58;\">" + meetingDate + ".</a>");
			secB.append("<br><br>Jag bifogar en m&ouml;teskallelse som du kan l&auml;gga in i din kalender. ");
			secB.append("<br> <br>Best&auml;llningen &auml;r registrerad f&ouml;r");
			secB.append("<br><a style=\"color: #3DCD58;\">" + consumerName
					+ "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + address + "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + zip + "</a>"
					+ "&nbsp;" + "&nbsp;" + "<a style=\"color: #3DCD58;\">"
					+ city + "</a>");
			secB.append("<br><br>Din best&auml;llnings&ouml;versikt");
			secB.append("<br><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">");
			secB.append("<tr>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Artikel</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Antal</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">summa</th>");
			secB.append("</tr>");
			for (BOM bom : boms) {
				secB.append("<tr>");
				secB.append("<td align=\"left\"><br>" + " "
						+ bom.getProductName() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getQuantity() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getUnitCost() + " " + "kr" + "</td>");
				secB.append("</tr>");
			}
			secB.append("</table>");
			StringBuilder secC = new StringBuilder();
			secC.append("Johan Petttersson");
			/*String template3 = SWTemplate3.getTemplate(secA.toString(),
					secB.toString(), secC.toString());*/
			String templateData="";
			InputStream htmlStream = null;
			htmlStream = SWEmailTemplates.class.getClassLoader().getResourceAsStream("Meeting.html");
			templateData = getStringFromInputStream(htmlStream);
			templateData=templateData.replace("secA", secA);
			templateData=templateData.replace("secB", secB);
			templateData=templateData.replace("oppNumber",secA);
			String mailSubject = "Mötes- och orderbekräftelse";
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(templateData);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoConsumerValidate(String toEMail,
			String consumerName, String oppName, String zipcode,
			List<BOM> boms, String address, String oppId, String city) {

		try {
			String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id=EncryptDecryptUtil.encrypt(oppId);
        		
        	}
			StringBuilder secA = new StringBuilder(oppId);
			StringBuilder secB = new StringBuilder();
			secB.append("Hej" + " " + consumerName);
			secB.append("<br><br>Stort tack f&ouml;r din best&auml;llning  nummer"+ " "+ oppId+ " "+ "Du kan se aktuell status f&ouml;r din best&auml;llning p&aring;"+" "+ "<a href=\"http://www.wiseup.se/#/orders/"+id+"\" style=\"color: #3DCD58;\">WiseUp.</a>");
			secB.append("<br><br>En av v&aring;ra certifierade elektriker kommer kontakta dig enligt dina &ouml;nskem&aring;l f&ouml;r att boka in en tid f&ouml;r installation.");
			secB.append("<br><br>N&auml;r ni har kommit &ouml;verens om ett installationsdatum kommer vi att skicka en orderbekr&auml;ftelse.</b> ");
			secB.append("<br><br>Best&auml;llningen &auml;r registrerad f&ouml;r");
			secB.append("<br><a style=\"color: #3DCD58;\">" + consumerName+ "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + address + "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + zipcode
					+ "</a>" + "&nbsp;" + "&nbsp;"
					+ "<a style=\"color: #3DCD58;\">" + city + "</a>");
			secB.append("<br><br>Din best&auml;llnings&ouml;versikt");
			secB.append("<br><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">");
			secB.append("<tr>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Artikel</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Antal</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">summa</th>");
			secB.append("</tr>");
			for (BOM bom : boms) {
				secB.append("<tr>");
				secB.append("<td align=\"left\"><br>" + " "
						+ bom.getProductName() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getQuantity() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getUnitCost() + " " + "kr" + "</td>");
				secB.append("</tr>");
			}
			secB.append("</table>");
			
	       String templateData="";
			
			InputStream htmlStream = null;
			htmlStream = SWEmailTemplates.class.getClassLoader().getResourceAsStream("Created.html");
			templateData = getStringFromInputStream(htmlStream);
			templateData=templateData.replace("secA", secA);
			templateData=templateData.replace("secB", secB);
			//templateData=templateData.replace("oppNumber",oppId);
			templateData=templateData.replace("Wiselink","<a href=\"http://www.wiseup.se/#/orders/"+id+"\" style=\"color: #3DCD58;\"text-decoration:none;>WiseUp.</a>");

			String mailSubject = "Beställningsbekräftelse - ordernummer " + " "+ oppId;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(templateData);
           
			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static List<String> sendInvoicetoAdminWhenPartnerInstalled(
			String toEMail, String partnerName, String company,
			String consumerName, String phNumber, String oppName,
			List<BOM> boms, int oppId, String address, String zip, String city,
			String meetingDate) {
		try {
			
			String id="";
			String oppID=String.valueOf(oppId);
        	if(EncryptDecryptUtil.isNumeric(oppID))
        	{
        		
        		id=EncryptDecryptUtil.encrypt(oppID);
        	}
			StringBuilder secA = new StringBuilder(oppID);
			StringBuilder secB = new StringBuilder();
			secB.append("Hej" + " " + consumerName);
			secB.append("<br><br> Tack f&ouml;r att du valde"+"<a href=\"http://www.wiseup.se/\">WiseUp.</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + consumerName+ "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + address + "</a>");
			secB.append("<br><a style=\"color: #3DCD58;\">" + zip + "</a>"+ "&nbsp;" + "&nbsp;" + "<a style=\"color: #3DCD58;\">"+ city + "</a>");
			secB.append("<br><br>Din best&auml;llnings&ouml;versikt");
			secB.append("<br><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">");
			secB.append("<tr>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Artikel</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">Antal</th>");
			secB.append("<th align=\"left\" style=\"color: #3DCD58;\">summa</th>");
			secB.append("</tr>");
			for (BOM bom : boms) {
				secB.append("<tr>");
				secB.append("<td align=\"left\"><br>" + " "
						+ bom.getProductName() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getQuantity() + "</td>");
				secB.append("<td align=\"left\"><br>" + " "
						+ (int) bom.getUnitCost() + " " + "kr" + "</td>");
				secB.append("</tr>");
			}
			secB.append("</table>");
			
			String templateData="";
		
			InputStream htmlStream = null;
			htmlStream = SWEmailTemplates.class.getClassLoader().getResourceAsStream("Invoice.html");
			templateData = getStringFromInputStream(htmlStream);
			templateData=templateData.replace("secA", secA);
			templateData=templateData.replace("secB", secB);
			templateData=templateData.replace("Wiselink","<a href=\"http://www.wiseup.se/#/orders/"+id+"\" style=\"color: #3DCD58;\"text-decoration:none;>WiseUp.</a>");

			//templateData=templateData.replaceAll("oppNumber",oppId);
			//https://www.wiseup.se/#/privacy 
			/*String template3 = SWTemplate4.getTemplate(secA.toString(),secB.toString());*/

			String mailSubject = "Tack för att du valde WiseUp - Kvitto för betalning";
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(templateData);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoConsumer(String toEMail,
			String consumerName, String parnterName) {
		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append(consumerName + ".");
		body.append("<br> Schneider Electric vous remercie d&rsquo;avoir fait appel &agrave; ses services.");
		body.append("<br> Nous avons reçu votre demande suite &agrave; votre configuration d&rsquo;un syst&agrave;me Wiser sur notre site internet.Vous allez &ecirc;tre contact&eacute; par la soci&eacute;t&eacute;  ");
		body.append(parnterName);
		body.append("afin de planifier un rendez-vous afin de pouvoir vous proposer un devis complet et vous accompagner tout au long de votre projet.");
		body.append("<br> Vous trouverez ci-dessous les coordonn&eacute;es compl&agrave;tes de la soci&eacute;t&eacute; ");
		body.append(parnterName);
		body.append("<br> Nom du programme d&rsquo;installation");
		body.append("<br> t&eacute;l d'installateur");
		body.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant : Support client");
		body.append("<br><br>Cordialement");
		body.append("<br><br><font color=red>L&rsquo;&eacute;quipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "Un installateur Wiser Ready prendra contact avec vous dans un d&eacute;lai maximum de 72h ";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());
		return template;

	}

	public static List<String> sendEmailtoPartnerForRemainderOne(
			String toEMail, String partnerName, String projectName,
			String consumerName, String zipCode, String budget) {

		try {
			StringBuilder secA = new StringBuilder(
					"Plus que 24h pour accepter cette nouvelle opportunit&eacute; !");
			StringBuilder secB = new StringBuilder("<html><body>Bonjour ");
			secB.append(partnerName + ".");
			secB.append("<br> Vous avez re&ccedil;u un mail il y a 24h ");
			secB.append("<br><br>Veuillez vous connecter sur votre application mobile  Partenaire SE afin de visualiser cette demande");
			secB.append("<br><br>Pour rappel, cette demande de devis a &eacute;t&eacute; envoy&eacute;e a plusieurs installeurs afin d&rsquo;y r&eacute;pondre en moins de 72h .");
			secB.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant :<a href=\"http://www.schneider-electric.fr/sites/france/fr/general/contact/contact.page\">Support client</a>");
			secB.append("<br><br> Cordialement");
			secB.append("<br><br>Responsable commercial");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "Relance nouvelle  demande de devis WISER  "
					+ projectName;

			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public static List<String> sendEmailtoPartnerFOrRemainderTwo(
			String toEMail, String partnerName, String projectName,
			String consumerName, String zipCode, String budget) {
		try {

			StringBuilder secA = new StringBuilder(
					"Plus que 48h pour accepter cette nouvelle opportunit&eacute;");
			StringBuilder secB = new StringBuilder("<html><body>Bonjour ");
			secB.append(partnerName + ".");
			secB.append("<br> Vous recevez ce mail pour vous rappeler qu&rsquo;il ya 48 heures nous vous avons envoy&eacute; une nouvelle opportunit&eacute; en ce qui concerne la possibilit&eacute; d&rsquo;une ");
			secB.append(projectName + ".");
			secB.append("<br><br><b> Nom du client : </b>");
			secB.append(consumerName);
			secB.append("<br><b> Code postal : </b>");
			secB.append(zipCode);
			secB.append("<br><b> Co&ugrave;t estimatif du projet : </b>");
			secB.append(budget);
			secB.append("<br><br> Se il vous pla&eacute;t vous connecter &agrave; votre application partenaire sur votre t&eacute;l&eacute;phone mobile et voir la possibilit&eacute;");
			secB.append("<br> En r&eacute;pondant rapidement vous augmentez vos chances de gagner la possibilit&eacute; et le projet ");

			secB.append("<br><br> meilleures Regadrs");
			secB.append("<br><br><font color=red> Anthony Locke</font>");
			secB.append("<br><i> Responsable Marketing Digital System</i>");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "Deuxieme rappel: Vous avez une nouvelle occasion  "
					+ projectName;

			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoPartnerFOrRemainderThree(
			String toEMail, String partnerName, String projectName,
			String consumerName, String zipCode, String budget) {
		try {

			StringBuilder secA = new StringBuilder(
					"Plus que 72h pour accepter cette nouvelle opportunit&eacute; !");
			StringBuilder secB = new StringBuilder("<html><body>Bonjour ");
			secB.append(partnerName + ".");
			secB.append("<br> Vous recevez ce mail pour vous rappeler qu&rsquo;il ya 72 heures nous vous avons envoy&eacute; une nouvelle opportunit&eacute; en ce qui concerne la possibilit&eacute; d&rsquo;une ");
			secB.append(projectName + ".");
			secB.append("<br><br><b> Nom du client : </b>");
			secB.append(consumerName);
			secB.append("<br><b> Code postal : </b>");
			secB.append(zipCode);
			secB.append("<br><b> Co&ugrave;t estimatif du projet : </b>");
			secB.append(budget);
			secB.append("<br><br> Vous avez perdu l&rsquo;occasion");

			secB.append("<br><br> meilleures Regadrs");
			secB.append("<br><br><font color=red> Anthony Locke</font>");
			secB.append("<br><i> Responsable Marketing Digital System</i>");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "Troisi&eacute;me rappel:  Possibilit&eacute; d’un "
					+ projectName + " Suppression";

			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoConsumerforNotAnswered(
			String toEMail, String consumerName, String projectName) {

		StringBuilder body = new StringBuilder("<html><body> Bonjour ");
		body.append(consumerName + ".");
		body.append("<br> Thank you for your interest in Clipsal products.");
		body.append("<br> We are sorry, but we cannot see that any Clipsal partner in your area have accepted your request from the "
				+ projectName);
		body.append("application.");
		body.append("<br> You receive this mail to inform you that your request has been forwarded to our Customer Care Center which will contact you within 24 hours to support you further with your project.");
		body.append("<br> If you have any questions, please contact our consumer support at 01300 369 233 (8:30AM to 7:30PM (Mon � Fri) AEST) or ");
		body.append("<a >");
		body.append("send us an e-mail.");
		body.append("</a>");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "A Clipsal customer care center will contact you within 24 hours regarding your request from "
				+ projectName;

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;

	}

	public static List<String> sendEmailtoSEAdminWithNoPartnerAnswered(
			String toEMail, String opportunityID, String consumerName,
			String zipCode, String budget) {
		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("SE admin for Clipspec Cloud.");
		body.append("<br> 72 hours has passed with no partner accepting the opportunity ID ");
		body.append(opportunityID);
		body.append("<br><br><b>Name of customer: </b>");
		body.append(consumerName);
		body.append("<br><b> Zip code: </b>");
		body.append(zipCode);
		body.append("<br><b> Estimated project cost: </b>");
		body.append(budget);
		body.append("<br><br> We have sent an email to the customer informing that the Customer Care Center will contact him within 24 hours.");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "A Clipspec Cloud opportunity not accepted within 72 hours";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> sendEmailtoSEAdminWithAllPartnerDeclined(
			String toEMail, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("SE admin for Clipspec Cloud.");
		body.append("<br> It has been less than 24 hours, and all Clipsal partners declined opportunity ID ");
		body.append(opportunityID);
		body.append("<br><br><b>Name of customer: </b>");
		body.append(consumerName);
		body.append("<br><b> Zip code: </b>");
		body.append(zipCode);
		body.append("<br><b> Estimated project cost: </b>");
		body.append(budget);
		body.append("<br> We have NOT informed the customer.");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "A Clipspec Cloud opportunity not accepted within 72 hours";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;

	}

	public static List<String> sendEmailtoSEAdminWithNoPartnerFound(
			String toEMail, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("SE admin for Clipspec Cloud.");
		body.append("<br> We did not find any Clipsal partner for opportunity ID ");
		body.append(opportunityID);
		body.append("<br><br><b>Name of customer: </b>");
		body.append(consumerName);
		body.append("<br><b> Zip code: </b>");
		body.append(zipCode);
		body.append("<br><b> Estimated project cost: </b>");
		body.append(budget);
		body.append("<br> We have NOT informed the customer.");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "URGENT: A Clipspec Cloud opportunity | No partner found";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> sendEmailtoConsumerProjectCancelled(
			String toEMail, String partnerName, String opportunityID,
			String consumerName, String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("Vous avez &eacute;tiez en contact avec la soci&eacute;t&eacute;");
		body.append("<br> We did not find any Clipsal partner for opportunity ID ");
		body.append(partnerName);
		body.append(" concernant l’installation d’un syst&agrave;me Wiser, et vous avez d&eacute;cidez de ne pas donner suite. ");
		body.append("<br> Nous esp&eacute;rons cependant que vous avez &eacute;tez satisfait de la prestation que nous vous avons propos&eacute;. Vous avis nous int&eacute;resse, nous vous invitons donc &agrave; prendre quelques secondes pour nous donner votre avis, Votre avis.");
		body.append("<br> Pour toute question, vous pouvez contacter notre service client. Vous trouverez l’ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant : Support client");
		body.append("<br><br> Cordialement.");
		body.append("<br><br><font color=red>L’&eacute;quipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Annulation de votre projet d’installation du syst&agrave;me Wiser";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> emailTopartnerLeadLost(String toEMail,
			String partnerName, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append(partnerName);
		body.append("<br><br> J’ai le regret de vous annoncer que le lead Wiser &agrave; &eacute;t&eacute; accept&eacute; par un autre partenaire.");
		body.append("<br><br> ");
		body.append("<br><br><b>Nom du client: </b>");
		body.append(consumerName);
		body.append("<br><b> Code postal: </b>");
		body.append(zipCode);
		body.append("<br><b> Montant estimatif: </b>");
		body.append(budget);
		body.append("<br><br>En vous connectant rapidement sur votre application ClosedLoop vous augmenterez vos chances de remporter un lead");

		body.append("<br><br> Bien cordialement");
		body.append("<br><br><font color=red>L’&eacute;quipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Le lead &agrave; &eacute;t&eacute; accept&eacute; par un autre partenaire";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> emailToConsumerQuotation(String toEMail,
			String partnerName, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append(partnerName);
		body.append("<br><br> Schneider Electric &agrave; le plaisir de vous confirmer votre rendez vous afin de chiffre l’instalation du syst&agrave;me Wiser.");
		body.append("<br><br>Vous avez rendez-vous le <Date> &agrave; <hours> avec la soci&eacute;t&eacute; ");
		body.append(partnerName);
		body.append("<br><br>Vous avez rendez-vous le <Date> &agrave; <hours> avec la soci&eacute;t&eacute; ");
		body.append("<br> Nous vous souhaitons un bon rendez vous, et restons &agrave; votre disposition pour tout renseignement.");
		body.append("<br> Vous trouverez l’ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant : Support client.");
		body.append("<br><br> cordialement");
		body.append("<br><br><font color=red>L’&eacute;quipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Confirmation de votre rendez vous pour chiffrer l’installation du syst&agrave;me Wiser";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> emailToConsumerNoFeedBack(String toEMail,
			String partnerName, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("<br>Vous avez reçu un devis de la soci&eacute;t&eacute; ");
		body.append(partnerName);
		body.append(" concernant l’installation d’un syst&agrave;me Wiser.");
		body.append("<br><br> Nous n’avons pas de retour de votre part, avez-vous des questions ?");
		body.append("<br> Nous vous invitons &agrave; recontacter notre partenaire installateur ");
		body.append(partnerName);
		body.append("<br><br> Cependant notre service client reste &agrave; votre disposition.. Vous trouverez l’ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant : Support client");
		body.append("<br><br> cordialement");
		body.append("<br><br><font color=red>L’&eacute;quipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Confirmation de votre rendez vous pour chiffrer l’installation du syst&agrave;me Wiser";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}
	private  static String getStringFromInputStream(InputStream is)
	{			
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

}

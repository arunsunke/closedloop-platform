package com.se.clm.bfo.client;

import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import oasis.names.tc.saml._1_0.assertion.NameIdentifierType;
import oasis.names.tc.saml._1_0.assertion.SubjectType;
import oasis.names.tc.saml._1_0.protocol.AuthenticationQueryType;
import oasis.names.tc.saml._1_0.protocol.CordysFaultDetail;
import oasis.names.tc.saml._1_0.protocol.ObjectFactory;
import oasis.names.tc.saml._1_0.protocol.RequestPortType;
import oasis.names.tc.saml._1_0.protocol.RequestService;
import oasis.names.tc.saml._1_0.protocol.RequestType;

import com.cordys.schemas.general._1.FaultDetails;
import com.cordys.schemas.general._1.LocalizableMessage;


public class SoapClientTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RequestService rs=new RequestService();
		
		 HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
		    rs.setHandlerResolver(handlerResolver);
		    
		    
		System.out.println(rs.getServiceName());
		final String WS_URL="urn:oasis:names:tc:SAML:1.0:protocol";
		
		RequestPortType rpt=rs.getRequestPort();
		System.out.println(rs.getRequestPort());
		try {
			RequestType body=new RequestType();
			
			body.setRequestID("a26c837b46-b1d6-a477-8006-6144046e6c0");
			GregorianCalendar gc=new GregorianCalendar();
			Date date=new Date(System.currentTimeMillis());
			gc.setTime(date);
			XMLGregorianCalendar xgc=DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			body.setIssueInstant(xgc);
			body.setMinorVersion(BigInteger.valueOf(1));
			body.setMajorVersion(BigInteger.valueOf(1));
			AuthenticationQueryType aQueryType=new AuthenticationQueryType();
			SubjectType st=new SubjectType();
			NameIdentifierType nType=new NameIdentifierType();
			nType.setFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
			//nType.setNameQualifier(value);
			nType.setValue("SESA156830");
			//ObjectFactory objectFactory=new ObjectFactory();
			ObjectFactory  objectFactory=new ObjectFactory();
			JAXBElement<NameIdentifierType> nTypeJb=new JAXBElement<NameIdentifierType>(new QName("urn:oasis:names:tc:SAML:1.0:assertion","NameIdentifier") , NameIdentifierType.class, nType);
			
			  
			
			
			
			st.getContent().add(nTypeJb);
			aQueryType.setSubject(st);
			body.setAuthenticationQuery(aQueryType);
			System.out.println("Token :"+rpt.requestOperation(body).getAssertionArtifact());
		} catch (CordysFaultDetail e) {
			// TODO Auto-generated catch block
			 
			FaultDetails fd=new FaultDetails();
			LocalizableMessage lm=new LocalizableMessage();
			lm.setMessageCode(e.getMessage());
			fd.setLocalizableMessage(lm);
			e.getMessage();
			e.printStackTrace();
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		

	}

}

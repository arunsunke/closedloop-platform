package com.se.clm.mail;

import java.util.ArrayList;
import java.util.List;

import com.se.cl.model.BOM;

public class FREmailTemplates {

	public static List<String> sendEmailtoPartnerForNewRequest(String toEMail,
			String partnerName, String projectName, String consumerName,
			String Codepostal, String montantEstimatif) {

		try {

			StringBuilder secA = new StringBuilder(
					"Bonne nouvelle, une nouvelle opportunit&eacute; WISER pour vous !");
			StringBuilder secB = new StringBuilder();
			secB.append("Bonjour" + partnerName);
			secB.append("<br><br>  Vous venez de recevoir une demande de devis");
			secB.append("<br><br> <b>Nom du client:</b> ");
			secB.append(consumerName);
			/*
			 * secB.append("<br><br> <b>Ville:</b> "); secB.append("French");
			 */
			secB.append("<br> <b>Code postal: </b>");
			secB.append(Codepostal);
			secB.append("<br> <b>Montant estimatif:</b> ");
			secB.append(montantEstimatif);
			secB.append("<br><br> Veuillez vous connecter sur votre application mobile  Partenaire SE afin de visualiser cette demande");

			secB.append("<br><br> Pour rappel, cette demande de devis a &eacute;t&eacute; envoy&eacute;e a plusieurs installeurs afin d&rsquo;y r&eacute;pondre en moins de 72h ..");
			secB.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;&copy;es en cliquant sur le lien suivant :<a href=\"http://www.schneider-electric.fr/sites/france/fr/general/contact/\">Support client</a>");
			secB.append("<br><br>Cordialement.");
			secB.append("<br><br>" + consumerName);
			secB.append("<br>Responsable commercial");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "-  Nouvelle  demande de devis  WISER "
					+ projectName;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static String getFooter() {
		return "<img src='cid:identifier1234'>";

	}

	public static List<String> sendEmailtoConsumerFollowBy(String toEMail,
			String partnerName, String company, String consumerName,
			String phNumber,String oppName) {

		try {

			StringBuilder secA = new StringBuilder(
					"Votre demande de devis &agrave; &eacute;t&eacute; prise en charge !");
			StringBuilder secB = new StringBuilder();
			secB.append("Bonjour.");
			secB.append("<br><br>  Schneider Electric vous remercie d&rsquo;avoir fait appel &agrave; ses services.");
			secB.append("<br><br>Votre demande de devis a bien &eacute;t&eacute; prise en compte. ");
			secB.append("<br> Vous allez &eacute;tre contact&eacute; par notre partenaire, la soci&eacute;t&eacute;.");
			secB.append(partnerName+"-"+company);
			secB.append("<br> afin de planifier un rendez-vous pour &eacute;tablir un devis complet et vous accompagner tout au long de votre projet. ");
			secB.append("<br><br> Vous trouverez ci-dessous les coordonn&eacute;es compl&eacute;tes de la soci&eacute;t&eacute; :"
					+ partnerName+"-"+company);
			secB.append("<br><b>nom de l'installateur:</b>");
			secB.append(partnerName);
			secB.append("<br><b>Tel de l'installateur:</b>");
			secB.append(phNumber);
			secB.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant :<a href=\"http://home.schneider-electric.fr/fr/how-to-contact-schneider-electric/contact-schneider-electric.jsp\">Support client</a>");
			secB.append("<br><br>Cordialement.");
			secB.append("<br><br>L&rsquo;&eacute;quipe Schneider Electric France");
			
			String template3 = FRemailTemp1.getTemplate(secA.toString(),secB.toString());

			String mailSubject = " Votre demande de devis WISER a bien été attribuée "+oppName;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}
	
    
	public static List<String> sendEmailtoConsumerWhenPartnerBookedMeeting(String toEMail,
			String partnerName, String company, String consumerName,
			String phNumber,String oppName,List<BOM> bomList,int oppId,String address,String zip,String city) {

		try {

			StringBuilder secA = new StringBuilder(
					"Votre demande de devis &agrave; &eacute;t&eacute; prise en charge !");
			StringBuilder secB = new StringBuilder();
			secB.append("Bonjour.");
			secB.append("<br><br>  Schneider Electric vous remercie d&rsquo;avoir fait appel &agrave; ses services.");
			secB.append("<br><br>Votre demande de devis a bien &eacute;t&eacute; prise en compte. ");
			secB.append("<br> Vous allez &eacute;tre contact&eacute; par notre partenaire, la soci&eacute;t&eacute;.");
			secB.append(partnerName+"-"+company);
			secB.append("<br> afin de planifier un rendez-vous pour &eacute;tablir un devis complet et vous accompagner tout au long de votre projet. ");
			secB.append("<br><br> Vous trouverez ci-dessous les coordonn&eacute;es compl&eacute;tes de la soci&eacute;t&eacute; :"
					+ partnerName+"-"+company);
			secB.append("<br><b>nom de l'installateur:</b>");
			secB.append(partnerName);
			secB.append("<br><b>Tel de l'installateur:</b>");
			secB.append(phNumber);
			secB.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant :<a href=\"http://home.schneider-electric.fr/fr/how-to-contact-schneider-electric/contact-schneider-electric.jsp\">Support client</a>");
			secB.append("<br><br>Cordialement.");
			secB.append("<br><br>L&rsquo;&eacute;quipe Schneider Electric France");
			
			String template3 = FRemailTemp1.getTemplate(secA.toString(),secB.toString());

			String mailSubject = " Beställningsbekräftelse - order  "+oppId;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	
	

	public static List<String> sendEmailtoConsumerValidate(String toEMail,
			String consumerName,String oppName) {

		try {

			StringBuilder secA = new StringBuilder(
					"F&eacute;licitation, nous avons bien re&ccedil;u votre demande de devis WISER !");
			StringBuilder secB = new StringBuilder();
			secB.append("Bonjour.");
			secB.append("<br><br>  Schneider Electric vous remercie d&rsquo;avoir fait appel &agrave; ses services.");
			secB.append("<br><br>Votre demande de devis a bien &eacute;t&eacute; prise en compte . ");
			secB.append("<br> Nous reviendrons vers vous dans un d&eacute;lai maximum de 48h afin de vous communiquer le nom de notre partenaire ne charge de la r&eacute;alisation de votre devis.");
			secB.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant :<a href=\"http://home.schneider-electric.fr/fr/how-to-contact-schneider-electric/contact-schneider-electric.jsp\">Support client</a>");
			secB.append("<br><br>Cordialement.");
			secB.append("<br><br>L&rsquo;&eacute;quipe Schneider Electric France");
			String template3 = FRemailTemp1.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = " Votre demande de devis WISER à bien été prise en compte "+oppName;
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoConsumer(String toEMail,
			String consumerName, String parnterName) {
		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append(consumerName + ".");
		body.append("<br> Schneider Electric vous remercie d&rsquo;avoir fait appel à ses services.");
		body.append("<br> Nous avons reçu votre demande suite à votre configuration d&rsquo;un système Wiser sur notre site internet.Vous allez être contacté par la société  ");
		body.append(parnterName);
		body.append("afin de planifier un rendez-vous afin de pouvoir vous proposer un devis complet et vous accompagner tout au long de votre projet.");
		body.append("<br> Vous trouverez ci-dessous les coordonnées complètes de la société ");
		body.append(parnterName);
		body.append("<br> Nom du programme d&rsquo;installation");
		body.append("<br> tél d'installateur");
		body.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonnées en cliquant sur le lien suivant : Support client");
		body.append("<br><br>Cordialement");
		body.append("<br><br><font color=red>L&rsquo;équipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "Un installateur Wiser Ready prendra contact avec vous dans un délai maximum de 72h ";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());
		return template;

	}

	public static List<String> sendEmailtoPartnerForRemainderOne(
			String toEMail, String partnerName, String projectName,
			String consumerName, String zipCode, String budget) {

		try {
			StringBuilder secA = new StringBuilder(
					"Plus que 24h pour accepter cette nouvelle opportunit&eacute; !");
			StringBuilder secB = new StringBuilder("<html><body>Bonjour ");
			secB.append(partnerName + ".");
			secB.append("<br> Vous avez re&ccedil;u un mail il y a 24h ");
			secB.append("<br><br>Veuillez vous connecter sur votre application mobile  Partenaire SE afin de visualiser cette demande");
			secB.append("<br><br>Pour rappel, cette demande de devis a &eacute;t&eacute; envoy&eacute;e a plusieurs installeurs afin d&rsquo;y r&eacute;pondre en moins de 72h .");
			secB.append("<br><br>Pour toute question, vous pouvez contacter notre service client. Vous trouverez l&rsquo;ensemble de nos coordonn&eacute;es en cliquant sur le lien suivant :<a href=\"http://www.schneider-electric.fr/sites/france/fr/general/contact/contact.page\">Support client</a>");
			secB.append("<br><br> Cordialement");
			secB.append("<br><br>Responsable commercial");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "Relance nouvelle  demande de devis WISER  "
					+ projectName;

			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public static List<String> sendEmailtoPartnerFOrRemainderTwo(
			String toEMail, String partnerName, String projectName,
			String consumerName, String zipCode, String budget) {
		try {

			StringBuilder secA = new StringBuilder(
					"Plus que 48h pour accepter cette nouvelle opportunit&eacute;");
			StringBuilder secB = new StringBuilder("<html><body>Bonjour ");
			secB.append(partnerName + ".");
			secB.append("<br> Vous recevez ce mail pour vous rappeler qu&rsquo;il ya 48 heures nous vous avons envoy&eacute; une nouvelle opportunit&eacute; en ce qui concerne la possibilit&eacute; d&rsquo;une ");
			secB.append(projectName + ".");
			secB.append("<br><br><b> Nom du client : </b>");
			secB.append(consumerName);
			secB.append("<br><b> Code postal : </b>");
			secB.append(zipCode);
			secB.append("<br><b> Co&ugrave;t estimatif du projet : </b>");
			secB.append(budget);
			secB.append("<br><br> Se il vous pla&eacute;t vous connecter &agrave; votre application partenaire sur votre t&eacute;l&eacute;phone mobile et voir la possibilit&eacute;");
			secB.append("<br> En r&eacute;pondant rapidement vous augmentez vos chances de gagner la possibilit&eacute; et le projet ");

			secB.append("<br><br> meilleures Regadrs");
			secB.append("<br><br><font color=red> Anthony Locke</font>");
			secB.append("<br><i> Responsable Marketing Digital System</i>");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "Deuxieme rappel: Vous avez une nouvelle occasion  "
					+ projectName;

			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoPartnerFOrRemainderThree(
			String toEMail, String partnerName, String projectName,
			String consumerName, String zipCode, String budget) {
		try {

			StringBuilder secA = new StringBuilder(
					"Plus que 72h pour accepter cette nouvelle opportunit&eacute; !");
			StringBuilder secB = new StringBuilder("<html><body>Bonjour ");
			secB.append(partnerName + ".");
			secB.append("<br> Vous recevez ce mail pour vous rappeler qu&rsquo;il ya 72 heures nous vous avons envoy&eacute; une nouvelle opportunit&eacute; en ce qui concerne la possibilit&eacute; d&rsquo;une ");
			secB.append(projectName + ".");
			secB.append("<br><br><b> Nom du client : </b>");
			secB.append(consumerName);
			secB.append("<br><b> Code postal : </b>");
			secB.append(zipCode);
			secB.append("<br><b> Co&ugrave;t estimatif du projet : </b>");
			secB.append(budget);
			secB.append("<br><br> Vous avez perdu l&rsquo;occasion");

			secB.append("<br><br> meilleures Regadrs");
			secB.append("<br><br><font color=red> Anthony Locke</font>");
			secB.append("<br><i> Responsable Marketing Digital System</i>");

			String template3 = FRemailTemp3.getTemplate(secA.toString(),
					secB.toString());

			String mailSubject = "Troisiéme rappel:  Possibilité d’un "
					+ projectName + " Suppression";			
					
			List<String> template = new ArrayList<String>();
			template.add(mailSubject);
			template.add(template3);

			return template;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public static List<String> sendEmailtoConsumerforNotAnswered(
			String toEMail, String consumerName, String projectName) {

		StringBuilder body = new StringBuilder("<html><body> Bonjour ");
		body.append(consumerName + ".");
		body.append("<br> Thank you for your interest in Clipsal products.");
		body.append("<br> We are sorry, but we cannot see that any Clipsal partner in your area have accepted your request from the "
				+ projectName);
		body.append("application.");
		body.append("<br> You receive this mail to inform you that your request has been forwarded to our Customer Care Center which will contact you within 24 hours to support you further with your project.");
		body.append("<br> If you have any questions, please contact our consumer support at 01300 369 233 (8:30AM to 7:30PM (Mon � Fri) AEST) or ");
		body.append("<a >");
		body.append("send us an e-mail.");
		body.append("</a>");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "A Clipsal customer care center will contact you within 24 hours regarding your request from "
				+ projectName;

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;

	}

	public static List<String> sendEmailtoSEAdminWithNoPartnerAnswered(
			String toEMail, String opportunityID, String consumerName,
			String zipCode, String budget) {
		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("SE admin for Clipspec Cloud.");
		body.append("<br> 72 hours has passed with no partner accepting the opportunity ID ");
		body.append(opportunityID);
		body.append("<br><br><b>Name of customer: </b>");
		body.append(consumerName);
		body.append("<br><b> Zip code: </b>");
		body.append(zipCode);
		body.append("<br><b> Estimated project cost: </b>");
		body.append(budget);
		body.append("<br><br> We have sent an email to the customer informing that the Customer Care Center will contact him within 24 hours.");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "A Clipspec Cloud opportunity not accepted within 72 hours";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> sendEmailtoSEAdminWithAllPartnerDeclined(
			String toEMail, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("SE admin for Clipspec Cloud.");
		body.append("<br> It has been less than 24 hours, and all Clipsal partners declined opportunity ID ");
		body.append(opportunityID);
		body.append("<br><br><b>Name of customer: </b>");
		body.append(consumerName);
		body.append("<br><b> Zip code: </b>");
		body.append(zipCode);
		body.append("<br><b> Estimated project cost: </b>");
		body.append(budget);
		body.append("<br> We have NOT informed the customer.");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "A Clipspec Cloud opportunity not accepted within 72 hours";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;

	}

	public static List<String> sendEmailtoSEAdminWithNoPartnerFound(
			String toEMail, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("SE admin for Clipspec Cloud.");
		body.append("<br> We did not find any Clipsal partner for opportunity ID ");
		body.append(opportunityID);
		body.append("<br><br><b>Name of customer: </b>");
		body.append(consumerName);
		body.append("<br><b> Zip code: </b>");
		body.append(zipCode);
		body.append("<br><b> Estimated project cost: </b>");
		body.append(budget);
		body.append("<br> We have NOT informed the customer.");

		body.append("<br><br> Best Regadrs");
		body.append("<br><br><font color=red> Anthony Locke</font>");
		body.append("<br><i> Digital System Marketing Manager</i>");

		body.append("<br><br>");
		body.append(getFooter());

		body.append("</body></html>");

		String mailSubject = "URGENT: A Clipspec Cloud opportunity | No partner found";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> sendEmailtoConsumerProjectCancelled(
			String toEMail, String partnerName, String opportunityID,
			String consumerName, String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("Vous avez étiez en contact avec la société");
		body.append("<br> We did not find any Clipsal partner for opportunity ID ");
		body.append(partnerName);
		body.append(" concernant l’installation d’un système Wiser, et vous avez décidez de ne pas donner suite. ");
		body.append("<br> Nous espérons cependant que vous avez étez satisfait de la prestation que nous vous avons proposé. Vous avis nous intéresse, nous vous invitons donc à prendre quelques secondes pour nous donner votre avis, Votre avis.");
		body.append("<br> Pour toute question, vous pouvez contacter notre service client. Vous trouverez l’ensemble de nos coordonnées en cliquant sur le lien suivant : Support client");
		body.append("<br><br> Cordialement.");
		body.append("<br><br><font color=red>L’équipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Annulation de votre projet d’installation du système Wiser";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> emailTopartnerLeadLost(String toEMail,
			String partnerName, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append(partnerName);
		body.append("<br><br> J’ai le regret de vous annoncer que le lead Wiser à été accepté par un autre partenaire.");
		body.append("<br><br> ");
		body.append("<br><br><b>Nom du client: </b>");
		body.append(consumerName);
		body.append("<br><b> Code postal: </b>");
		body.append(zipCode);
		body.append("<br><b> Montant estimatif: </b>");
		body.append(budget);
		body.append("<br><br>En vous connectant rapidement sur votre application ClosedLoop vous augmenterez vos chances de remporter un lead");

		body.append("<br><br> Bien cordialement");
		body.append("<br><br><font color=red>L’équipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Le lead à été accepté par un autre partenaire";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> emailToConsumerQuotation(String toEMail,
			String partnerName, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append(partnerName);
		body.append("<br><br> Schneider Electric à le plaisir de vous confirmer votre rendez vous afin de chiffre l’instalation du système Wiser.");
		body.append("<br><br>Vous avez rendez-vous le <Date> à <hours> avec la société ");
		body.append(partnerName);
		body.append("<br><br>Vous avez rendez-vous le <Date> à <hours> avec la société ");
		body.append("<br> Nous vous souhaitons un bon rendez vous, et restons à votre disposition pour tout renseignement.");
		body.append("<br> Vous trouverez l’ensemble de nos coordonnées en cliquant sur le lien suivant : Support client.");
		body.append("<br><br> cordialement");
		body.append("<br><br><font color=red>L’équipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Confirmation de votre rendez vous pour chiffrer l’installation du système Wiser";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

	public static List<String> emailToConsumerNoFeedBack(String toEMail,
			String partnerName, String opportunityID, String consumerName,
			String zipCode, String budget) {

		StringBuilder body = new StringBuilder("<html><body>Bonjour ");
		body.append("<br>Vous avez reçu un devis de la société ");
		body.append(partnerName);
		body.append(" concernant l’installation d’un système Wiser.");
		body.append("<br><br> Nous n’avons pas de retour de votre part, avez-vous des questions ?");
		body.append("<br> Nous vous invitons à recontacter notre partenaire installateur ");
		body.append(partnerName);
		body.append("<br><br> Cependant notre service client reste à votre disposition.. Vous trouverez l’ensemble de nos coordonnées en cliquant sur le lien suivant : Support client");
		body.append("<br><br> cordialement");
		body.append("<br><br><font color=red>L’équipe Schneider Electric france</font>");
		body.append("<br><br>");
		body.append(getFooter());
		body.append("</body></html>");

		String mailSubject = "Confirmation de votre rendez vous pour chiffrer l’installation du système Wiser";

		List<String> template = new ArrayList<String>();
		template.add(mailSubject);
		template.add(body.toString());

		return template;
	}

}

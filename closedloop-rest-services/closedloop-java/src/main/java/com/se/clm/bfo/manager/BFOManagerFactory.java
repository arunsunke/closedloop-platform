package com.se.clm.bfo.manager;

public interface BFOManagerFactory {

	public BFOManager getBFOManager(String managerName);
	
}

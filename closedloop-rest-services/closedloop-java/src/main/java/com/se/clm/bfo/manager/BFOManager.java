package com.se.clm.bfo.manager;

import java.util.Map;

public interface BFOManager {
	
	
	public Map<String,String> getOpportunityRequest();
	
	public Map<String,Object> getOpportunityRequestIF();
	
	public Map<String,String> getOpportunityStatusRequests(String oppStatus);
	
	public Map<String, Object> getOpportunityStatusRequestsIF(String oppStatus);
		
	public Map<String,String> getOpportunityHistStatusRequests(String statusId);
	
	public Map<String,Object> getOpportunityHistStatusRequestsIF(String statusId);

	public void updateBFOOppStatus(Map<String,String> bfoResponse , String oppId);

	public void updateBFOOppAcceptRejectStatus (boolean response ,String seqId);
	
	public void updateBFOOppHist (boolean response , String oppId);

	public void updateBFOOppAcceptRejectStatusIF(boolean response, String seqId, String responseData,String oppId);
	
	public String getBfoOppId(String seqId,String tableReference);

	public void updateBFOOppHistInstallation(boolean responseMap,String string, String custVisitId);

	public void updateBFOOppStatusIF(Map<String, String> bfoResponse, String oppId);

	public void updateBFOOppHistIF(boolean responseMap, String oppId);
	

}

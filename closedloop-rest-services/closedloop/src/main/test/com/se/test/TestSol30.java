package com.se.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.se.cl.dao.OpportunityDAO;
import com.se.cl.model.Opportunity;
import com.se.cl.sol30.service.impl.Solution30Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class TestSol30 {
	
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	Solution30Service solution30Service;
	
	
	
	@Test
	public void testSendtoSol30(){
		
		//notificationManager.sendAcceptRejectToBFO(OppStatus.ACCEPTED.name());
		
		Opportunity opportunity=opportunityDAO.getOportunity("100");
		solution30Service.sendOpportunityToSol30(opportunity);
		
	}
	


}

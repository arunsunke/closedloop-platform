package com.se.cl.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-test.xml"})
public class TestUpdateOpportunity {
	
	@Autowired
	OpportunityManager  opportunityManager;
	@Test
	public void testReadStatus(){
		
		try {
			opportunityManager.updateOpportunityStatus("20", "976", "status", "READ");
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

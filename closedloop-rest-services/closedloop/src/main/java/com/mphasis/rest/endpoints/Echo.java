
package com.mphasis.rest.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.se.cl.dao.OpportunityDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.partner.manager.NotificationManagerFactory;
import com.se.cl.sol30.service.impl.Solution30Service;

/** Example resource class hosted at the URI path "/echo/\<stringToEcho\>/"
 */
@Component
@Path("/echo")
public class Echo {
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return supplied input string echoed back as a response of type "text/plain".
     */
	
	
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	OpportunityManager  opportunityManager;
	
	@Autowired
	NotificationManagerFactory notificationManagerFactory;
	
	@Autowired
	Solution30Service solution30Service;
	
	
    @GET 
    @Produces("text/plain")
    @Path("/slanotify")
    public void slanote(@QueryParam("step") int step,@QueryParam("country") String country) {
       
    	try {
    		if(country!=null){
    		notificationManagerFactory.getNotificationManager(country+"N").notifyPartnerForSALRemider(step,country);
    		}else{
    			notificationManagerFactory.getNotificationManager("AUN").notifyPartnerForSALRemider(step,country);
    		}
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	//return input;
    }
    
    @GET 
    @Produces("text/plain")
    @Path("/newOpNotify")
    public void newnote(@QueryParam("country") String country) {
       
    	try {
    		notificationManagerFactory.getNotificationManager(country+"N").notifyPartnerForNewOpportunity(country);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	//return input;
    }
    
    
    @GET 
    @Produces("text/plain")
    @Path("/allDecline")
    public void slaNotify(@QueryParam("country") String country) {
       
    	
    	notificationManagerFactory.getNotificationManager(country+"N").sendMailAllPartnerDeclined(country);
		
    	
    	//return input;
    }
    
    
    @GET 
    @Produces("text/plain")
    @Path("/noPartnerAccepted")
    public void noPartnerAccepted(@QueryParam("country") String country) {
       
    	
    	notificationManagerFactory.getNotificationManager(country+"N").sendMailNoPartnerAccepted(country);
		
    	
    	//return input;
    }
    
    
    
    
    
    @GET 
    @Produces("text/plain")
    @Path("/sendMail")
    public void sendMail(@QueryParam("country") String country) {
       
    	
    	notificationManagerFactory.getNotificationManager(country+"N").sendMail();
		
    	//return input;
    }
    
    

    @GET 
    @Produces("text/plain")
    @Path("/updateSol30")
    public void updateSol30() {
       
    	
    	solution30Service.updateOpportunityHistory();
		
    	//return input;
    }
   
	
    @GET 
    @Produces("text/plain")
    @Path("/slae3")
	public void sendmailSl3(@QueryParam("country") String country)
	{
    	if(country!=null){
		notificationManagerFactory.getNotificationManager(country+"N").sendMailToConsumerPartnerAccepted(country,false);
		}else{
			notificationManagerFactory.getNotificationManager("FRN").sendMailToConsumerPartnerAccepted(country,false);
		}
		
		
	}
    
    
    
}

package com.mphasis.rest.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.se.cl.dao.OpportunityAdminDAO;

@Component
@Path("/closeloop/admin") 
public class ClosedLoopAdminEndpoints {

	
	
	@Autowired
	OpportunityAdminDAO  opportunityAdminDAO;
	
	
	@GET
    @Path("/opportunities/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunities(@QueryParam("country") String country) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
         	builder.entity(opportunityAdminDAO.getOpportunities(country));
         	return builder.build(); 
  
        } catch (Exception e) { 
           e.printStackTrace();
           // logger.debug("error"+""+e); 
            RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
}

package com.mphasis.rest.endpoints;

import java.sql.Timestamp;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.Partner;
import com.se.cl.model.PartnerDevice;
import com.se.cl.partner.manager.PartnerManager;
import com.se.cl.partner.manager.PartnerManagerFactory;

@Component
@Path("/closeloopv1/partner") 
public class PartnerV1EndPoints {
	
	
	@Autowired
	OpportunityDAO opportunityDAO;
	@Autowired
	PartnerDAO partnerDAO;
	
	@Autowired
	OpportunityManager  opportunityManager;
	
	@Autowired
	PartnerManagerFactory partnerFactory;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PartnerEndPoints.class);
	
	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunitiesByPartner(@PathParam("id") String partnerId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(opportunityDAO.getOpportunitiesForPartner(partnerId));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	

	@POST
	@Path("/adddevice")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDevice(
			PartnerDevice device
			) throws AppException {
	/*public Response addDevice(
			@FormParam("userId") String userId,
			@FormParam("deviceId") String deviceId,
			@FormParam("mobileNo") String mobileNo,
			@FormParam("deviceType") String deviceType
			) throws AppException {*/
		
		 try { 
	        	ResponseBuilder builder = Response.ok(); 
	        	//PartnerDevice device =new PartnerDevice();
	        	
	        	java.util.Date dt = new java.util.Date();
				final Timestamp timestamp = new Timestamp(dt.getTime());
	        	device.setEffectFromDate(timestamp);
	        	
	        	int partnerId=opportunityManager.addDevicetoPartner(device);
	        	//builder.entity(op));
	        	JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "Device Added Successfully");
		        successObj.put("partnerId", partnerId);
		        builder.entity(successObj);
	        	return builder.build(); 
	  
	        } catch (Exception e) { 
	  
	           e.printStackTrace();
	           RestResponseError err = new RestResponseError("Error", "Error While Adding Device"); 
	           
	            ResponseBuilder builder = Response.ok(); 
	            builder.entity(err); 
	            return builder.build(); 
	  
	        } 
		
		
		
		
	}
	
	
	
	@GET
    @Path("/match")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getMatchedPartners(@QueryParam("countryCode") String countryCode,@QueryParam("regEx") String regEx,@QueryParam("origin") String origin,@QueryParam("destinations") String destinations) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok();
        	partnerFactory.getPartnerManger((countryCode!=null)?countryCode:"AU").assignPartnerToOpportunity();
        	JSONObject successObj=new JSONObject();
	        successObj.put("sucess", "true");
	        builder.entity(successObj);
        	return builder.build(); 
  
        } catch (Exception e) { 
  
        	LOGGER.error("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While Assigning the Partners"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getPartners(@QueryParam("country") String countryCode) { 
          
      
        try { 
        	LOGGER.debug("START get partner ");
        	ResponseBuilder builder = Response.ok();
        	builder.entity(partnerFactory.getPartnerManger(countryCode).getPartner(countryCode,null,null));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
        	LOGGER.error("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Partners"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@PUT
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response updatePartner(@QueryParam("id") String id,@QueryParam("zip") String zip,@QueryParam("userId") String userId,@QueryParam("email") String email) { 
          
      
        try { 
        	Partner partner=new Partner();
        	partner.setId(id);
        	partner.setUserId(userId);
        	partner.setZip(zip);
        	partner.setEmail(email);
        	partnerDAO.updatePartner(partner);
        	
        	ResponseBuilder builder = Response.ok();
        	JSONObject successObj=new JSONObject();
	        successObj.put("sucess", "true");
	        successObj.put("message", "Partner updated successfully ");
	        builder.entity(successObj);
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error updating partner"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	

}

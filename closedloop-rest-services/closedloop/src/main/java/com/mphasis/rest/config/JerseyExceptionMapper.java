package com.mphasis.rest.config;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.web.method.annotation.ErrorsMethodArgumentResolver;

import com.mphasis.rest.endpoints.RestResponseError;
import com.se.cl.exception.AppException;
import com.sun.jersey.spi.inject.Errors.ErrorMessage;

@Provider
public class JerseyExceptionMapper implements ExceptionMapper<AppException>
{

 

@Override
public Response toResponse(AppException exception) {
	// TODO Auto-generated method stub
	return Response.status(exception.getStatus())
            .entity(new RestResponseError(String.valueOf(exception.getStatus()),exception.getMessage()))
            .type(MediaType.APPLICATION_JSON).
            build();
	 // return Response.status(exception.getCode()).entity(exception.getMessage()).build();
}


 
}
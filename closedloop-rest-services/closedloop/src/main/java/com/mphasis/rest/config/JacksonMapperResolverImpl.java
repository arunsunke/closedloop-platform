package com.mphasis.rest.config;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


/**
 * @author Sudhir.Marni
 *
 */

@Provider
public class JacksonMapperResolverImpl implements ContextResolver<ObjectMapper>, JacksonMapperResolver{

	private  ObjectMapper mapper;
	
	private Logger logger = LoggerFactory.getLogger(JacksonMapperResolverImpl.class);

	@Context
	private ServletContext context;
	
	public JacksonMapperResolverImpl(){
		logger.debug("Creating ContextResolver");
	}
	
	private synchronized void init(){
		logger.debug("Initializing custom objectmapper provider");
		this.mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				false);
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS,true);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		if (context != null) {
			Boolean prettyPrint = Boolean.parseBoolean(context.getInitParameter("com.mphasis.rest.config.JacksonMapperResolver.IDENT_OUTPUT"));
			mapper.configure(SerializationFeature.INDENT_OUTPUT, prettyPrint);
		}
	}

	@Override
	public ObjectMapper getContext(Class<?> arg0) {
		if(mapper == null){
			init();
		}
		return this.mapper;
	}
public static void main(String[] args) {
	boolean bool = Boolean.parseBoolean(null);
	System.out.println(bool);
}
}

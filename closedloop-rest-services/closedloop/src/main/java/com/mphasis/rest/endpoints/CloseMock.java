package com.mphasis.rest.endpoints;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.springframework.core.io.ClassPathResource;

import com.se.cl.model.Address;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerDetails;
import com.se.cl.model.Opportunities;
import com.se.cl.model.Opportunity;
import com.se.cl.model.User;

public class CloseMock {

	
	public static Opportunities getOppotunitiesByPartner(String partnerId){
		SimpleDateFormat dateFormat=null;
		List<Opportunity> opportunityList=new ArrayList<Opportunity>();
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a"); 
		
		java.util.Date today = new java.util.Date();
		DateTime dtOrg = new DateTime(today);
		DateTime dtPlusTwo = dtOrg.plusDays(2);
		DateTime dtPlusOne = dtOrg.plusDays(1);
		DateTime dtPlusThree = dtOrg.plusDays(3);
		Opportunity op1=new Opportunity();
		op1.setId("op123");
		op1.setTitle("Opportunity One");
		op1.setShortDesc("Wiser Smart Installation");
		op1.setBudget( "2500");
		op1.setOpportunityDate(dateFormat.format(today));
		op1.setOpportunityExpDate(dateFormat.format(dtPlusTwo.toDate()));
		
		Opportunities ops=new Opportunities();
		
		opportunityList.add(op1);
		
		Opportunity op2=new Opportunity();
		op2.setId("op234");
		op2.setTitle("Opportunity Two");
		op2.setShortDesc("Wiser Smart Installation");
		op2.setBudget( "1500");
		op2.setOpportunityDate(dateFormat.format(dtPlusOne.toDate()));
		op2.setOpportunityExpDate(dateFormat.format(dtPlusThree.toDate()));
		
		opportunityList.add(op2);
		
		
		ops.setOpportunityList(opportunityList);
		
		
		return ops;
		
	}
	
public static Opportunity getOppotunity(String opId){
	
		SimpleDateFormat dateFormat=null;
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a"); 
		java.util.Date today = new java.util.Date();
		DateTime dtOrg = new DateTime(today);
		DateTime dtPlusTwo = dtOrg.plusDays(2);
		
		Opportunity op1=new Opportunity();
		op1.setTitle("Opportunity One");
		//op1.setShortDesc("Wiser Smart Installation");
		op1.setBudget( "2500");
		op1.setOpportunityDate(dateFormat.format(today));
		op1.setOpportunityExpDate(dateFormat.format(dtPlusTwo.toDate()));
		op1.setBusinessType("Retail");
		op1.setDescription("Renovation of Auto Mobile Show room");
		op1.setEmail("closedlooptest@gmail.com.com");
		op1.setPhone("123-123-4444");
		
		
		
		
		
		Address address=new Address();
		address.setCity("Stokholm");
		address.setZip("24526");
		address.setSt_name("Strike Street");
		address.setLatitude("111.23");
		address.setLongitude("-122.22");
		address.setCountryCode("US");
		op1.setAddress(address);
		
		
		try{
		op1.setBmPDF(readFile("bom.txt"));
		}catch(Exception e){
			e.printStackTrace();
		}
		BOM bom=new BOM();
		List<BOM> bomList=new ArrayList<BOM>();
		
		bom.setCategory("Plugs");
		bom.setProductId("EER21001");
		bom.setProductName("multi pin plug");
		bom.setDescription("Night glow");
		bom.setColor("White");
		bom.setQuantity(20);
		bom.setUnitCost(Float.valueOf("1234.12"));
		bom.setCurrencyCode("AUD");
		
		bomList.add(bom);

		BOM bom1=new BOM();
		bom1.setCategory("Dimmers");
		bom1.setProductId("EER21002");
		bom1.setProductName("Touch dimmer");
		bom1.setDescription("Touch dimmer desc");
		bom1.setColor("White");
		bom1.setQuantity(10);
		bom1.setUnitCost(Float.valueOf("867.20"));
		bom1.setCurrencyCode("AUD");
		bomList.add(bom1);
		
			
		op1.setBoms(bomList);
		
		
		CustomerDetails cust=new CustomerDetails();
		cust.setAddress(address);
		cust.setEmail("closedlooptest@gmail.com.com");
		cust.setFirstName("Customer");
		cust.setLastName("Last Name");
		cust.setPhone("123-123-4444");
		
		
		op1.setCustomerDetails(cust);
		
		
		return op1;
		
	}


	public static JSONObject getUsers(){
		
//		{
//		    "login":{
//		    "users":
//		    [
//		                {"username":"user1", "password": "password1"},
//		                {"username":"user2", "password": "password2"}
//		    ]
//		    }
//		}
//		
		JSONObject userJSONObj=new JSONObject();
		List<User> usersList=new ArrayList<User>();
		User user1=new User();
		user1.setUsername("user1");
		user1.setPassword("password1");
		
		usersList.add(user1);
		
		User user2=new User();
		user2.setUsername("user2");
		user2.setPassword("password2");
		
		usersList.add(user2);
		
		userJSONObj.put("users", usersList);
		
		JSONObject logicJSONObj=new JSONObject();
		
		logicJSONObj.put("login", userJSONObj);
		
		return logicJSONObj;
		
	}

public static JSONObject getUsers(String username,String password){
		
		HashMap<String,String> userMap=new HashMap<String,String>();
		userMap.put("user1", "password1");
		userMap.put("user2", "password2");
		userMap.put("user3", "password3");
		userMap.put("user4", "password4");
		
		JSONObject logicJSONObj=new JSONObject();
		JSONObject statusObj=new JSONObject();
		if(userMap.containsKey(username) && userMap.get(username).equals(password)){
			statusObj.put("Status","Success");
		}else{
			statusObj.put("Status","Fail");
		}
		
		logicJSONObj.put("login", statusObj);
		
		return logicJSONObj;
		
	}


public static String readFile( String file ) throws IOException {
	
	ClassPathResource cpr = new ClassPathResource("bom.txt");
	InputStream is = cpr.getInputStream();
	return IOUtils.toString(is);
	/*is.
    BufferedReader reader = new BufferedReader(is);
    String         line = null;
    StringBuilder  stringBuilder = new StringBuilder();
    String         ls = System.getProperty("line.separator");

    while( ( line = reader.readLine() ) != null ) {
        stringBuilder.append( line );
        stringBuilder.append( ls );
    }

    return stringBuilder.toString();*/
}

}
package com.mphasis.rest.endpoints;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mphasis.rest.config.FileStreamingOutput;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.OppStatusHistory;
import com.se.cl.model.Opportunity;
import com.se.cl.model.PartnerDevice;
import com.se.cl.model.User;

@Component
@Path("/closeloopv1") 
public class CloseLoopV1Endpoints {

	
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	OpportunityManager  opportunityManager;
	
	//private @Value("${partner-count}") String propertyField;
	
	
	@POST
    @Path("/opportunity/")
    @Produces(MediaType.APPLICATION_JSON)  
	@Consumes(MediaType.APPLICATION_JSON)
    public Response loadOppotunity(Opportunity opportunity) { 
          
		ResponseBuilder builder = Response.ok();
		Map<String, Object> resposeMap = null;

		try { 

			resposeMap = opportunityManager.addOpportunity(opportunity);
			builder.entity(resposeMap); 
			return builder.build(); 

		} catch (Exception e) { 
			
			resposeMap = new HashMap<String, Object>();
			resposeMap.put("status", "Error");
			resposeMap.put("errorMessage", "Error while saving opportunity");
			builder.entity(resposeMap); 
			return builder.build(); 

		} 
  
    } 
	
	@GET
    @Path("/opportunity/getmock")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunityMock() { 
        
        try { 
        
        	//System.out.println(propertyField);
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(CloseMock.getOppotunity(null));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@POST
    @Path("/opportunity/savemock")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response saveOppotunityMock(@QueryParam("numOfOpps") String numOfOpps,@QueryParam("zip") String zip,@QueryParam("country") String country,@QueryParam("oppdate") String oppdate,@QueryParam("email") String email) { 
        
        try { 
        
        	//System.out.println(propertyField);
        	if(oppdate.contains("|")){
        		oppdate=oppdate.replace("|", " ");
        		oppdate=oppdate.replace("|", " ");
        	}
    	
    		Opportunity opp=CloseMock.getOppotunity(null);
    		for(int i=0;i<Integer.parseInt(numOfOpps); i++){
    			
    			opp.setTitle("Demo Opportunity"+(i+1));
    			opp.getAddress().setZip(zip);
    			opp.getAddress().setCountryCode(country);
    			opp.getCustomerDetails().getAddress().setZip(zip);
    			opp.getCustomerDetails().getAddress().setCountryCode(country);
    			opp.getCustomerDetails().setEmail(email);
    			opp.setEmail(email);
    			
    			opp.setOpportunityDate(oppdate);
    			
    			 opportunityManager.addOpportunity(opp);
    			
    		}
    		
    		
    		
    		ResponseBuilder builder = Response.ok(); 
        	builder.entity("OK");
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@DELETE
    @Path("/opportunity/delete")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response deleteOppotunities(@QueryParam("userIds") String userIds) { 
        
        try { 
        
        	//System.out.println(propertyField);
        	
        	StringTokenizer stk = new StringTokenizer(userIds,"|");  
        	List<String> userIdList = new ArrayList<String>();  
        	   
        	while ( stk.hasMoreTokens() ) {  
        		userIdList.add(stk.nextToken());  
        	}  
    		
        	opportunityDAO.deleteopportunities(userIdList);	
    		
    		
    		
    		ResponseBuilder builder = Response.ok(); 
        	builder.entity("OK");
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	
	
	
	
	@GET
    @Path("/opportunity/{id}")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunitiesByPartner(@PathParam("id") String partnerId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(opportunityDAO.getOpportunitiesForPartner(partnerId));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	@GET
    @Path("/opportunity/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunities(@QueryParam("username") String userId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
         	builder.entity(opportunityDAO.getOpportunities(userId));
         	return builder.build(); 
  
        } catch (Exception e) { 
           e.printStackTrace();
           // logger.debug("error"+""+e); 
            RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 

	@GET
    @Path("/opportunity/history")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunityHistory(@QueryParam("oppId") String oppId,@QueryParam("partnerId") String partnerId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
         	builder.entity(opportunityDAO.getOpputnityHistory(oppId,partnerId));
         	return builder.build(); 
  
        } catch (Exception e) { 
           e.printStackTrace();
           // logger.debug("error"+""+e); 
            RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 

	
	

	@PUT
    @Path("/opportunity/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updatedOpportunity(@PathParam("id") String partnerId,@QueryParam("opportunityId") String opportunityId,@QueryParam("statusId") String statusId,@QueryParam("status") String status,@QueryParam("isAdminAssigned") boolean isAdminAssigned,@QueryParam("countryCode") String countryCode) throws AppException { 
          
        		ResponseBuilder builder = Response.ok(); 
        		if ((partnerId!=null && partnerId.length()>0) && (opportunityId!=null&& opportunityId.length()>0) && (status!=null && status.length()>0) && (statusId!=null && statusId.length()>0) )
        		{
        			opportunityManager.updateOpportunityStatus(partnerId, opportunityId, statusId, status,isAdminAssigned,countryCode);
        		
        		}else{
        			throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(), 400, "Opportunity Id and Partner ID and staus Id and status is mandatory " ,
        					"", null);
        		}
        		JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	
	
/*	@PUT
    @Path("/opportunity/{id}/update")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateOpportunityStatus(@PathParam("id") String partnerId,
			@QueryParam("opportunityId") String opportunityId,
			@QueryParam("statusId") String statusId,
			@QueryParam("status") String status,
			@QueryParam("reason") String reason,@QueryParam("startDateTime") String startTime,@QueryParam("endDateTime") String endTime) throws AppException {
        
        		ResponseBuilder builder = Response.ok(); 
 				opportunityManager.updateOpportunityHistory(partnerId, opportunityId, statusId,status,reason,startTime,endTime);
			 	JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	*/
	@PUT
    @Path("/opportunity/{id}/update")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOpportunityStatusJson(@PathParam("id") String partnerId, OppStatusHistory oppStatus) throws AppException {
        
        		ResponseBuilder builder = Response.ok(); 
				opportunityManager.updateOpportunityHistory(partnerId,
						oppStatus.getOpportunityId(), oppStatus.getStatusId(),
						oppStatus.getStatus(), oppStatus.getReason(),
						oppStatus.getStartTime(), oppStatus.getEndTime(),oppStatus.getType(),"CL");
				JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	
	
	
	
	@GET
    @Path("/opportunity/{id}/BOM")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response getOppotunityBOM(@PathParam("id") String oppId) throws AppException { 
          

				ResponseBuilder builder = Response.ok(); 
        		JSONObject bjson=opportunityDAO.getOpportunityBOM(oppId);
		        builder.entity(bjson);
	        	return builder.build(); 
     
	}
	
	
	@GET
	@Produces("application/pdf")
	public StreamingOutput getBOMPdf() {

	    String fileName="bom.pdf";
	    //get file from DB
	    File pdf = new File("Settings.basePath", fileName);
	    if (!pdf.exists())
	        throw new WebApplicationException(Response.Status.NOT_FOUND);

	    return new FileStreamingOutput(pdf);
	}
	
	
	
	@POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON) 
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getUsers( User user) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(CloseMock.getUsers(user.getUsername(),user.getPassword()));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	 RestResponseError err = new RestResponseError("Error", "Login Error"); 
             
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	
	
	
	
	
	
}

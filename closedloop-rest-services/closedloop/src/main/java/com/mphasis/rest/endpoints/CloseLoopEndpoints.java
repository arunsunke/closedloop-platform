package com.mphasis.rest.endpoints;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mchange.v1.db.sql.SchemaManager;
import com.mphasis.rest.config.FileStreamingOutput;
import com.se.cl.constants.CountryCode;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.manager.SpimManager;
import com.se.cl.model.BOM;
import com.se.cl.model.IMS;
import com.se.cl.model.ImageData;
import com.se.cl.model.OppStatusHistory;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityConsumerImages;
import com.se.cl.model.User;
import com.se.cl.partner.login.manager.ImsLoginManager;
import com.se.cl.partner.login.manager.KenticoLoginManager;
import com.se.cl.spim.client.SpimHandler;
import com.se.cl.util.EncryptDecryptUtil;
import com.se.clm.bfo.client.HttpsClient;

@Component
@Path("/closeloop") 
public class CloseLoopEndpoints {

	
	@Autowired
	OpportunityDAO opportunityDAO;
	
	@Autowired
	OpportunityManager  opportunityManager;
	
	@Autowired
	ImsLoginManager  imsLoginManager;
	
	@Autowired
	KenticoLoginManager  kenticoLoginManager;
	
	@Autowired
	SpimManager  spimManager;
	
	@Autowired
	HttpsClient httpsClient;

	
	
	//private @Value("${partner-count}") String propertyField;
	private static Logger logger = LoggerFactory.getLogger(CloseLoopEndpoints.class);
	
	@POST
    @Path("/opportunity/")
    @Produces(MediaType.APPLICATION_JSON)  
	@Consumes(MediaType.APPLICATION_JSON)
    public Response loadOppotunity(Opportunity opportunity) { 
          
		ResponseBuilder builder = Response.ok();
		Map<String, Object> resposeMap = null;

		try { 
			logger.info("Opportunity Create Request -Title :"+ opportunity.getTitle() );
			
			if(opportunity.getSource()!=null&& opportunity.getSource().getId()!=null && opportunity.getSource().getType()!=null){
				logger.info("Source :"+opportunity.getSource().getId() +
						" type:" +opportunity.getSource().getType());
				
			}
			
			resposeMap = opportunityManager.addOpportunity(opportunity);
			String oppId="";
			if(opportunity.getAddress()!=null&& opportunity.getAddress().getCountryCode().equalsIgnoreCase(CountryCode.SWEDEN.getValue()))
			{
				if(resposeMap!=null)
				{	
					for (Map.Entry<String, Object> entry : resposeMap.entrySet()) 
					{
					    logger.info("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					    if(entry.getKey()!=null && entry.getKey().equalsIgnoreCase("opportunityId"))
					    {
							 oppId = String.valueOf(entry.getValue());
	
					    }
					}
					logger.info("oppId::::::"+oppId);
				    //opportunityManager.updateOpportunityTitle(oppId);

				}
					
			
				logger.info("opportunity.getOppConsumerImages()"+opportunity.getOppConsumerImages());
				if(opportunity.getOppConsumerImages()!=null)
				{
					
					List<OpportunityConsumerImages> opportunityConsumerImagesList = new ArrayList<OpportunityConsumerImages>();
					opportunityConsumerImagesList = opportunity.getOppConsumerImages();
					
					List<String> ImagesArray= new ArrayList<String>();
					List<String> ImageNamesArray = new ArrayList<String>();
					
					boolean uploadSuccess=false;					
					int count=0;
					String oppTitle="";
					String imsToken="";
					String refreshToken="";
					String imageTitle="";
					String imageContent="";
					String userType="";
					String userId="";
	
					for(int i=0; i<opportunityConsumerImagesList.size();i++)
					{	                  
						count=0;
						oppTitle="";
						imsToken="";
						refreshToken="";
						imageTitle="";
						imageContent="";
						userType="";
						userId="";
						oppTitle= opportunityConsumerImagesList.get(i).getOppTitle()+"-"+oppId;
						//oppTitle= opportunityConsumerImagesList.get(i).getOppTitle();
						imageTitle= opportunityConsumerImagesList.get(i).getImageTitle();
						userType= opportunityConsumerImagesList.get(i).getUserType();
						imageContent= opportunityConsumerImagesList.get(i).getImageData();
						ImagesArray.add(imageContent);
						ImageNamesArray.add(imageTitle);
						
						userId="";
						
						logger.info("oppTitle::::"+oppTitle);
						logger.info("userType::::"+userType);
						logger.info("imageTitle::::"+imageTitle);					
						
					}
					spimManager.uploadImages(imsToken,refreshToken,userType,oppTitle,ImagesArray,ImageNamesArray,oppId,userId);
					logger.info("uploadSuccess"+uploadSuccess);
					
					
				}
			}
			
			builder.entity(resposeMap); 
			return builder.build(); 

		} catch (Exception e) { 
			
			resposeMap = new HashMap<String, Object>();
			resposeMap.put("status", "Error");
			resposeMap.put("errorMessage", "Error while saving opportunity");
			builder.entity(resposeMap); 
			e.printStackTrace();

			return builder.build(); 
		} 
  
    } 
	
	@GET
    @Path("/opportunity/getmock")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response getOppotunityMock() { 
        
        try { 
        
        	//System.out.println(propertyField);
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(CloseMock.getOppotunity(null));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@POST
    @Path("/opportunity/savemock")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response saveOppotunityMock(@QueryParam("numOfOpps") String numOfOpps,@QueryParam("zip") String zip,@QueryParam("country") String country,@QueryParam("oppdate") String oppdate,@QueryParam("email") String email) { 
        
        try { 
        
        	//System.out.println(propertyField);
        	if(oppdate.contains("|")){
        		oppdate=oppdate.replace("|", " ");
        		oppdate=oppdate.replace("|", " ");
        	}
    	
    		Opportunity opp=CloseMock.getOppotunity(null);
    		for(int i=0;i<Integer.parseInt(numOfOpps); i++){
    			
    			opp.setTitle("Demo Opportunity"+(i+1));
    			opp.getAddress().setZip(zip);
    			opp.getAddress().setCountryCode(country);
    			opp.getCustomerDetails().getAddress().setZip(zip);
    			opp.getCustomerDetails().getAddress().setCountryCode(country);
    			opp.getCustomerDetails().setEmail(email);
    			opp.setEmail(email);
    			
    			opp.setOpportunityDate(oppdate);
    			
    			 opportunityManager.addOpportunity(opp);
    			
    		}
    		
    		
    		
    		ResponseBuilder builder = Response.ok(); 
        	builder.entity("OK");
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@DELETE
    @Path("/opportunity/delete")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response deleteOppotunities(@QueryParam("userIds") String userIds) { 
        
        try { 
        
        	//System.out.println(propertyField);
        	
        	StringTokenizer stk = new StringTokenizer(userIds,"|");  
        	List<String> userIdList = new ArrayList<String>();  
        	   
        	while ( stk.hasMoreTokens() ) {  
        		userIdList.add(stk.nextToken());  
        	}  
    		
        	opportunityDAO.deleteopportunities(userIdList);	
    		
    		
    		
    		ResponseBuilder builder = Response.ok(); 
        	builder.entity("OK");
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	
	@GET
    @Path("/opportunity/{id}")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunitiesByPartner(@PathParam("id") String partnerId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(opportunityDAO.getOpportunitiesForPartner(partnerId));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	@GET
    @Path("/opportunity/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunities(@QueryParam("username") String userId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
         	builder.entity(opportunityDAO.getOpportunities(userId));
         	return builder.build(); 
  
        } catch (Exception e) { 
           e.printStackTrace();
           // logger.debug("error"+""+e); 
            RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 

	@GET
    @Path("/opportunity/history")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunityHistory(@QueryParam("oppId") String oppId,@QueryParam("partnerId") String partnerId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
         	builder.entity(opportunityDAO.getOpputnityHistory(oppId,partnerId));
         	return builder.build(); 
  
        } catch (Exception e) { 
           e.printStackTrace();
           // logger.debug("error"+""+e); 
            RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 

	
	@PUT
    @Path("/opportunity/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updatedOpportunity(@PathParam("id") String partnerId,@QueryParam("opportunityId") String opportunityId,@QueryParam("statusId") String statusId,@QueryParam("status") String status,@QueryParam("isAdminAssigned") boolean isAdminAssigned,@QueryParam("countryCode") String countryCode) throws AppException 
	{ 
          
        		ResponseBuilder builder = Response.ok(); 
        		if ((partnerId!=null && partnerId.length()>0) && (opportunityId!=null&& opportunityId.length()>0) && (status!=null && status.length()>0) && (statusId!=null && statusId.length()>0) )
        		{
        			logger.info("isAdminAssigned"+isAdminAssigned);
        			opportunityManager.updateOpportunityStatus(partnerId, opportunityId, statusId, status,isAdminAssigned,countryCode);
        		
        		}else{
        			throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(), 400, "Opportunity Id and Partner ID and staus Id and status is mandatory " ,
        					"", null);
        		}
        		JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	
/*	@PUT
    @Path("/opportunity/{id}/update")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateOpportunityStatus(@PathParam("id") String partnerId,
			@QueryParam("opportunityId") String opportunityId,
			@QueryParam("statusId") String statusId,
			@QueryParam("status") String status,
			@QueryParam("reason") String reason,@QueryParam("startDateTime") String startTime,@QueryParam("endDateTime") String endTime) throws AppException {
        
        		ResponseBuilder builder = Response.ok(); 
 				opportunityManager.updateOpportunityHistory(partnerId, opportunityId, statusId,status,reason,startTime,endTime);
			 	JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	*/
	@PUT
    @Path("/opportunity/{id}/update")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOpportunityStatusJson(@PathParam("id") String partnerId, OppStatusHistory oppStatus) throws AppException {
        
        		ResponseBuilder builder = Response.ok(); 
				opportunityManager.updateOpportunityHistory(partnerId,
						oppStatus.getOpportunityId(), oppStatus.getStatusId(),
						oppStatus.getStatus(), oppStatus.getReason(),
						oppStatus.getStartTime(), oppStatus.getEndTime(),oppStatus.getType(),"CL");
				JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	
	
	
	
	@GET
    @Path("/opportunity/{id}/BOM")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response getOppotunityBOM(@PathParam("id") String oppId) throws AppException { 
          

				ResponseBuilder builder = Response.ok(); 
        		JSONObject bjson=opportunityDAO.getOpportunityBOM(oppId);
		        builder.entity(bjson);
	        	return builder.build(); 
     
	}
	
	@GET
	@Produces("application/pdf")
	public StreamingOutput getBOMPdf() {

	    String fileName="bom.pdf";
	    //get file from DB
	    File pdf = new File("Settings.basePath", fileName);
	    if (!pdf.exists())
	        throw new WebApplicationException(Response.Status.NOT_FOUND);

	    return new FileStreamingOutput(pdf);
	}
	

	@POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON) 
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getUsers( User user,@QueryParam("countryCode") String countryCode) 
	{ 
          logger.info("countryCode::::"+countryCode);
          User response=null;  
        try { 
        
        	ResponseBuilder builder = Response.ok();
        	JSONObject successObj=new JSONObject();
        	if(countryCode!=null && countryCode.equalsIgnoreCase("AU"))
        	{
        		//KenticoLoginManagerImpl kenticoHandler = new KenticoLoginManagerImpl();
        		String authUrl="";
        		boolean kenticoResponse=false;
        		authUrl=kenticoLoginManager.getTokenUrl();
        		kenticoResponse=kenticoLoginManager.AuthenticateUser(user.getUsername(),user.getPassword(),authUrl);
	        	if (kenticoResponse)
	        		successObj.put("login", "Login Success");
			    else
			    	successObj.put("login", "Login Error");
	        	
	        	builder.entity(successObj);
	        	
        	}else if(countryCode!=null && countryCode.equalsIgnoreCase("SW"))
            {
        		//ImsLoginManagerImpl imsHandler = new ImsLoginManagerImpl();
        		String imsToken="";
        		IMS ims=null;
        		ims =imsLoginManager.getAuthenticateUserToken(user.getUsername(),user.getPassword());
	        	if (ims!=null && ims.getAccessToken().length()>0)
	        	{
	        		successObj.put("login", "Login Success");
	        		successObj.put("imsToken",ims.getAccessToken());
	        		successObj.put("refreshToken",ims.getRefreshToken());
	        		successObj.put("expiresIn",ims.getExpiresIn());
	        		
	        	}
	        	else
			    	successObj.put("login", "Login Error");
	        	
	        	builder.entity(successObj);
            		
            	
        	}else
        	{
        		response=opportunityManager.validateUser(user);
	        	if (response!=null)
	        		successObj.put("login", "Login Success");
			    else
			    	successObj.put("login", "Login Error");
	        	
	        	builder.entity(successObj);
        	}
        	
        	return builder.build(); 

  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	 RestResponseError err = new RestResponseError("Error", "Login Error");
             ResponseBuilder builder = Response.ok(); 
             builder.entity(err); 
             return builder.build(); 
  
        } 
	
	
	}
	
	
	@GET
    @Path("/opportunity/{id}/CSV")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response getOppotunityCSV(@PathParam("id") String oppId) throws AppException { 
          

				ResponseBuilder builder = Response.ok(); 
				List<BOM> bomcsvList = new ArrayList<BOM>();
				bomcsvList = opportunityDAO.getBomsCSV(oppId);
				JSONObject bjson;
				try {
					bjson = opportunityDAO.generateCsvFile(bomcsvList);
			        builder.entity(bjson);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	return builder.build(); 
     
	}
	
	
	@POST
    @Path("/opportunity/images")
    @Produces(MediaType.APPLICATION_JSON)  
	@Consumes(MediaType.APPLICATION_JSON)
    public Response postOppotunityImage(ImageData imgData,@QueryParam("imsToken") String imsToken,@QueryParam("refreshToken") String refreshToken,@QueryParam("userType") String userType,@QueryParam("oppTitle") String oppTitle,@QueryParam("imageTitle") String imageTitle,@QueryParam("oppId") String oppId,@QueryParam("userId") String userId)
	{ 
        
		ResponseBuilder builder = Response.ok();
		Map<String, Object> resposeMap = null;
	    JSONObject successObj=new JSONObject();
	   
		logger.info("postOppotunityImage Request -imsToken :"+ imsToken);
		logger.info("postOppotunityImage Request -refreshToken :"+ refreshToken);
		logger.info("postOppotunityImage Request -userType :"+ userType);
		logger.info("postOppotunityImage Request -oppTitle :"+ oppTitle);
		logger.info("postOppotunityImage Request -imageTitle :"+ imageTitle);
		logger.info("postOppotunityImage Request -oppId :"+ oppId);
		logger.info("postOppotunityImage Request -userId :"+ userId);

		
		boolean uploadSuccess = false;
		
		try { 
			if((imgData.getImageContent()!=null && imgData.getImageContent().length()>0) && (imsToken!=null && imsToken.length()>0) && (userType!=null&& userType.length()>0) && (oppTitle!=null && oppTitle.length()>0) && (imageTitle!=null && imageTitle.length()>0) )
    		{
    			
				long lStartTime = System.currentTimeMillis();
				uploadSuccess=true;
				oppTitle= oppTitle+"-"+oppId;
				spimManager.uploadPartnerImages(imsToken,refreshToken,userType, oppTitle,imgData.getImageContent(), imageTitle,oppId,userId);

				if(uploadSuccess)
                {	
                	//wait(1000);
                	successObj.put("imageupload", "Success");
                }
                else
                {
                	successObj.put("imageupload", "Failure");                	
                }
                
				
				
                long lEndTime = System.currentTimeMillis();
			 	long difference = lEndTime - lStartTime;
			 	logger.info("ClosedLoopEndPoints:::postOppotunityImage:::Total Elapsed Time to upload Images:::"+difference);
				logger.info("uploadSuccess:::"+uploadSuccess);
                
                
			}else
			{
				successObj.put("imageupload", "Failure");
			}
			
			builder.entity(successObj); 
			return builder.build(); 

		} catch (Exception e) { 
			
			resposeMap = new HashMap<String, Object>();
			resposeMap.put("status", "Error");
			resposeMap.put("errorMessage", "Error while uploading images");
			e.printStackTrace();
			builder.entity(resposeMap); 
			return builder.build(); 

		} 
  
    } 
	
	
	@GET
    @Path("/imageslist")
    @Produces(MediaType.APPLICATION_JSON)  
    public Response getOpportunityImagesList(@QueryParam("imsToken") String imsToken,@QueryParam("userType") String userType,@QueryParam("oppTitle") String oppTitle,@QueryParam("oppId") String oppId,@QueryParam("userId") String userId)
	{ 
		ResponseBuilder builder = Response.ok();
		Map<String, Object> resposeMap = null;
	    JSONObject successObj=new JSONObject();
	    
		logger.info("getOpportunityImages Request -imsToken :"+ imsToken);
		logger.info("getOpportunityImages Request -userType :"+ userType);
		logger.info("getOpportunityImages Request -oppTitle :"+ oppTitle);
		logger.info("getOpportunityImages Request -oppId :"+ oppId);
		logger.info("getOpportunityImages Request -userId :"+ userId);

		try { 
			
				oppTitle= oppTitle+"-"+oppId;
				List<Map<String,String>> responseImageObjIdList = new ArrayList<Map<String ,String>>();
				responseImageObjIdList = spimManager.getImageIdList(imsToken,userType,oppTitle,oppId,userId);
	         	builder.entity(responseImageObjIdList);
				return builder.build(); 


		} catch (Exception e) { 
			
			resposeMap = new HashMap<String, Object>();
			resposeMap.put("status", "Error");
			resposeMap.put("errorMessage", "Error while getting Image Id List");
			builder.entity(resposeMap); 
			return builder.build(); 

		} 
  
    } 
	
	@GET
    @Path("/imagecontent")
    @Produces(MediaType.APPLICATION_JSON)  
    public Response getOpportunityImagesContent(@QueryParam("imsToken") String imsToken,@QueryParam("refreshToken") String refreshToken,@QueryParam("userType") String userType,@QueryParam("oppTitle") String oppTitle,@QueryParam("objectId") String objectId)
	{ 
          
		ResponseBuilder builder = Response.ok();
		Map<String, Object> resposeMap = null;
		logger.info("getOpportunityImages Request -imsToken :"+ imsToken);
		logger.info("getOpportunityImages Request -refreshToken :"+ refreshToken);
		logger.info("getOpportunityImages Request -userType :"+ userType);
		logger.info("getOpportunityImages Request -oppTitle :"+ oppTitle);
		logger.info("getOpportunityImages Request -objectId :"+ objectId);		
		try { 
			
				long lStartTime_Full_GETIMAGELIST_BYTECONTENT = System.currentTimeMillis();
			    Map<String,Object> responseImageData = new HashMap<String ,Object>();
		        responseImageData =  spimManager.getImageContent(imsToken,refreshToken,userType,oppTitle,objectId);
				//logger.info("responseImageData:::"+responseImageData);
	         	builder.entity(responseImageData);
                long lEndTime_Full_GETIMAGELIST_BYTECONTENT = System.currentTimeMillis();
			 	long difference_Full_GETIMAGELIST_BYTECONTENT = lEndTime_Full_GETIMAGELIST_BYTECONTENT - lStartTime_Full_GETIMAGELIST_BYTECONTENT;
			 	logger.info("ClosedLoopEndPoints:::getOpportunityImagesContent:::Complete Total Elapsed To GET IMAGE BYE CONTENT:::"+difference_Full_GETIMAGELIST_BYTECONTENT);
	         	return builder.build(); 


		} catch (Exception e) { 
			
			resposeMap = new HashMap<String, Object>();
			resposeMap.put("status", "Error");
			resposeMap.put("errorMessage", "Error while getting image content");
			builder.entity(resposeMap); 
			return builder.build(); 

		} 
  
    } 
	
	
	@GET
    @Path("/opportunity/sendoppsol30")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response sendOpptoSol30(@QueryParam("oppId") String oppId,@QueryParam("zip") String zip,@QueryParam("countryCode") String countryCode) throws AppException { 
          
				Map<String, Object> resposeMap = null;
				ResponseBuilder builder = Response.ok(); 
				boolean result=false;
				result = opportunityManager.sendOpptoSol30(oppId, zip, countryCode);
				try {
					if(result)
					{
						resposeMap = new HashMap<String, Object>();
						resposeMap.put("status", "Success");
						resposeMap.put("errorMessage", "Success");
						builder.entity(resposeMap);
						return builder.build();

					}else
					{
						resposeMap = new HashMap<String, Object>();
						resposeMap.put("status", "Error");
						resposeMap.put("errorMessage", "Error while sending opportunity to sol30");
						builder.entity(resposeMap);
						return builder.build();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					resposeMap = new HashMap<String, Object>();
					resposeMap.put("status", "Error");
					resposeMap.put("errorMessage", "Error while sending opportunity to sol30");
					builder.entity(resposeMap);
					e.printStackTrace();

					return builder.build(); 
				}
				
     
	}
	
	
	@GET
    @Path("/opportunity/oppstatus")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response getOppotunityStatus(@QueryParam("oppId") String oppId) { 
        
		String response="";
    	ResponseBuilder builder = Response.ok();
	    JSONObject successObj=new JSONObject();
        try { 
        	String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id = oppId;
        		
        	}else
        	{	
        		id=EncryptDecryptUtil.decrypt(oppId);
        	}
        	response=opportunityManager.getOpportunityStatus(id);
        	
            logger.debug("getOppotunityStatus response::::"+response);
        	
        	if (response!=null)
        		successObj.put("oppstatus", response);
		    else
		    	successObj.put("Error", "Error While getting Opportunity Status");
        	
        	builder.entity(successObj);
    	
        	return builder.build(); 
        } catch (Exception e) 
        { 
            
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunity Status"); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    }
	
	@GET
    @Path("/opportunity/{id}/electrician")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response getElectrician(@PathParam("id") String oppId) throws AppException
	{ 
          
		String response="";
    	ResponseBuilder builder = Response.ok();
	    JSONObject successObj=new JSONObject();
        try { 
        	String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id = oppId;
        		
        	}else
        	{	
        		id=EncryptDecryptUtil.decrypt(oppId);
        	}
        	response=opportunityDAO.checkOPPAccepted(id);
            logger.debug("getElectrician response::::"+response);
        	
        	if (response!=null)
        		successObj.put("electrician", response);
		    else
		    	successObj.put("Error", "Electrician not available");
        	
        	builder.entity(successObj);
    	
        	return builder.build(); 
        } catch (Exception e) 
        { 
            
        	RestResponseError err = new RestResponseError("Error", "Error While getting Electrician"); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
	}
	
	@PUT
    @Path("/opportunity/cancel")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_JSON)
	public Response cancelOpportunity(OppStatusHistory oppStatus) throws AppException {
	      	    String partnerId="88888";
	      	    
	      	  partnerId = opportunityDAO.checkOPPAccepted(oppStatus.getOpportunityId()) ;
	      	  if(partnerId!=null && partnerId.length()>0)
	      	  {
	      		partnerId =partnerId;
	      	  }else
	      	  {
	      		partnerId="88888";  
	      	  }	
	      	String id="";
        	if(EncryptDecryptUtil.isNumeric(oppStatus.getOpportunityId()))
        	{
        		id = oppStatus.getOpportunityId();
        		
        	}else
        	{	
        		try {
					id=EncryptDecryptUtil.decrypt(oppStatus.getOpportunityId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
	      	  
				ResponseBuilder builder = Response.ok(); 
				opportunityManager.updateOpportunityHistoryCancel(partnerId,id, oppStatus.getStatusId(),
						oppStatus.getStatus(), oppStatus.getReason(),
						oppStatus.getStartTime(), oppStatus.getEndTime(),oppStatus.getType(),"CL");
				JSONObject successObj=new JSONObject();
		        successObj.put("success", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	
	@PUT
    @Path("/opportunity/{id}/close")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_JSON)
	public Response closeOpportunity(@PathParam("id") String partnerId, OppStatusHistory oppStatus) throws AppException {
        
        		ResponseBuilder builder = Response.ok(); 
        		String id="";
            	if(EncryptDecryptUtil.isNumeric(oppStatus.getOpportunityId()))
            	{
            		id = oppStatus.getOpportunityId();
            		
            	}else
            	{	
            		try {
						id=EncryptDecryptUtil.decrypt(oppStatus.getOpportunityId());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
				opportunityManager.updateOpportunityHistory(partnerId,id
						, oppStatus.getStatusId(),
						oppStatus.getStatus(), oppStatus.getReason(),
						oppStatus.getStartTime(), oppStatus.getEndTime(),oppStatus.getType(),"CL");
				JSONObject successObj=new JSONObject();
		        successObj.put("success", "true");
		        builder.entity(successObj);
	        	return builder.build(); 
        	
     
	}
	
	
	@GET
    @Path("/opportunity/{id}/boms")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response getBoms(@PathParam("id") String oppId) throws AppException
	{ 
		String response="";
    	ResponseBuilder builder = Response.ok();
	    JSONObject successObj=new JSONObject();
	    List<BOM> bomList = new ArrayList<BOM>();
        try { 
        	String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id = oppId;
        		
        	}else
        	{	
        		id=EncryptDecryptUtil.decrypt(oppId);
        	}
        
        	bomList =opportunityDAO.getBoms(Long.parseLong(id));
            logger.debug("getBoms bomList::::"+bomList);
            if(bomList!=null)
            	successObj.put("boms", bomList);
            else
            	successObj.put("Error", "No boms available");
        	builder.entity(successObj);    	
        	return builder.build(); 
        } catch (Exception e) 
        { 
            
        	RestResponseError err = new RestResponseError("Error", "Error While getting boms"); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
	}
	
	
	@GET
    @Path("/opportunity/sendsms")
	@Produces({ MediaType.APPLICATION_JSON})
 	
    public Response sendsms(@QueryParam("phone") String phone,@QueryParam("customer") String custNamae,@QueryParam("partner") String partnerName,@QueryParam("countryCode") String countryCode,@QueryParam("partnerPhone") String partnerPhone) throws AppException
	{ 
    	ResponseBuilder builder = Response.ok();
	    JSONObject successObj=new JSONObject();
        try { 
        
			httpsClient.sendSMSToConsumer(phone, custNamae, partnerName,partnerPhone,countryCode,100);
            logger.info("sendsms:::phone:"+phone);
            logger.info("sendsms:::custNamae:"+custNamae);
            logger.info("sendsms:::partnerName:"+partnerName);
            logger.info("sendsms:::countryCode:"+countryCode);           
            
            successObj.put("sms", "Success");
        	
            builder.entity(successObj);    	
        	return builder.build(); 
        } catch (Exception e) 
        { 
            
        	RestResponseError err = new RestResponseError("Error", "Error while sending sms"); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
	}
	
	
	@GET
    @Path("/opportunity/meetingdate")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response getMeetingDate(@QueryParam("oppId") String oppId) { 
        
		String response="";
    	ResponseBuilder builder = Response.ok();
	    JSONObject successObj=new JSONObject();
        try { 
        
        	
        	String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id = oppId;
        		
        	}else
        	{	
        		id=EncryptDecryptUtil.decrypt(oppId);
        	}
        	response=opportunityManager.getOpportunityMeetingDate(id);
        	
            logger.debug("getMeetingDate response::::"+response);
        	
        	if (response!=null)
        		successObj.put("meetingdate", response);
		    else
		    	successObj.put("Error", "Error While getting meeting  date");
        	
        	builder.entity(successObj);
    	
        	return builder.build(); 
        } catch (Exception e) 
        { 
            
        	RestResponseError err = new RestResponseError("Error", "Error While getting Meeting date"); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    }
	
	
	@GET
    @Path("/opportunity/consumername")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response getConsumerName(@QueryParam("oppId") String oppId) { 
        
		String response="";
    	ResponseBuilder builder = Response.ok();
	    JSONObject successObj=new JSONObject();
        try { 
        	logger.info("Method started::::");
        	
        	logger.info("Opportunity Id:::"+oppId);
        	
    		String id="";
        	if(EncryptDecryptUtil.isNumeric(oppId))
        	{
        		id = oppId;
        		
        	}else
        	{	
        		id=EncryptDecryptUtil.decrypt(oppId);
        	}
        	
        	logger.info("Decrypted Id:::"+id);
        	
        	logger.info("Decry value::::"+id);
        	
        	response=opportunityManager.getConsumerName(id);
        	
            logger.info("getConsumerName response::::"+response);
        	
        	if (response!=null)
        		successObj.put("meetingdate", response);
		    else
		    	successObj.put("Error", "Error While getting consumer name");
        	
        	builder.entity(successObj);
    	
        	return builder.build(); 
        } catch (Exception e) 
        {
            
        	RestResponseError err = new RestResponseError("Error", "Error While getting Consumer Name"); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    }
	
	
	//sending description parameter in post 
	@PUT
    @Path("/opportunity/updateopp/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDesc(@PathParam("id") String oppId,Opportunity opp) throws AppException {
        
		ResponseBuilder builder = Response.ok(); 
		JSONObject successObj=new JSONObject();
		boolean response=opportunityDAO.updateDesc(oppId, opp.getDescription());
        		
				if(response)
				{
					successObj.put("success", "Description updated successfully");
			        builder.entity(successObj);
				}
				else
				{
					successObj.put("failure", "error while updating Description");
			        builder.entity(successObj);
				}
				
		        
	        	return builder.build(); 
        	
     
	}
	
	
	
	/*
	 * 
	 * sending description parameter in request url
	 * @PUT
    @Path("/opportunity/updateopp/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
 	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDesc(@PathParam("id") String oppId,String desc) throws AppException {
        
		ResponseBuilder builder = Response.ok(); 
		JSONObject successObj=new JSONObject();
		boolean response=opportunityDAO.updateDesc(oppId, desc);
        		
				if(response)
				{
					successObj.put("success", "Description updated successfully");
			        builder.entity(successObj);
				}
				else
				{
					successObj.put("failure", "error while updating Description");
			        builder.entity(successObj);
				}
				
		        
	        	return builder.build(); 
        	
     
	}*/
	
	
}
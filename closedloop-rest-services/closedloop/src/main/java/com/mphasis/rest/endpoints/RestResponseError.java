package com.mphasis.rest.endpoints;
public class RestResponseError { 
      
    private String status; 
    private String errmsg; 
    
    public RestResponseError(String status,String errmsg) {
		// TODO Auto-generated constructor stub
    	this.status=status;
    	this.errmsg=errmsg;
    
    }
    
    
    
    public String getStatus() { 
        return status; 
    } 
    public void setStatus(String status) { 
        this.status = status; 
    } 
    public String getErrmsg() { 
        return errmsg; 
    } 
    public void setErrmsg(String errmsg) { 
        this.errmsg = errmsg; 
    } 
  
} 
package com.mphasis.rest.endpoints;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.se.cl.dao.OpportunityDAO;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.manager.OpportunityManager;
import com.se.cl.model.Distributor;
import com.se.cl.model.Partner;
import com.se.cl.model.PartnerDevice;
import com.se.cl.model.Supplier;
import com.se.cl.partner.manager.NotificationManagerFactory;
import com.se.cl.partner.manager.PartnerManagerFactory;

@Component
@Path("/closeloop/partner") 
public class PartnerEndPoints {
	
	
	@Autowired
	OpportunityDAO opportunityDAO;
	@Autowired
	PartnerDAO partnerDAO;
	
	@Autowired
	OpportunityManager  opportunityManager;
	
	@Autowired
	PartnerManagerFactory partnerFactory;
	
	@Autowired
	NotificationManagerFactory notificationManagerFactory;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PartnerEndPoints.class);
	
	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getOppotunitiesByPartner(@PathParam("id") String partnerId) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok(); 
        	builder.entity(opportunityDAO.getOpportunitiesForPartner(partnerId));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Opportunities"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	

	@POST
	@Path("/adddevice")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDevice(
			PartnerDevice device
			) throws AppException {
	/*public Response addDevice(
			@FormParam("userId") String userId,
			@FormParam("deviceId") String deviceId,
			@FormParam("mobileNo") String mobileNo,
			@FormParam("deviceType") String deviceType
			) throws AppException {*/
		
		 try { 
	        	ResponseBuilder builder = Response.ok(); 
	        	//PartnerDevice device =new PartnerDevice();
	        	
	        	java.util.Date dt = new java.util.Date();
				final Timestamp timestamp = new Timestamp(dt.getTime());
	        	device.setEffectFromDate(timestamp);
	        	
	        	int partnerId=opportunityManager.addDevicetoPartner(device);
	        	
	          LOGGER.info(device.getDeviceId());
	          LOGGER.info(device.getDeviceType());
	          
	        	
	        	//builder.entity(op));
	        	JSONObject successObj=new JSONObject();
		        successObj.put("sucess", "Device Added Successfully");
		        successObj.put("partnerId", partnerId);
		        builder.entity(successObj);
	        	return builder.build(); 
	  
	        } catch (Exception e) { 
	  
	           e.printStackTrace();
	           RestResponseError err = new RestResponseError("Error", "Error While Adding Device"); 
	           
	            ResponseBuilder builder = Response.ok(); 
	            builder.entity(err); 
	            return builder.build(); 
	  
	        } 
		
		
		
		
	}
	
	
	
	@GET
    @Path("/match")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getMatchedPartners(@QueryParam("countryCode") String countryCode,@QueryParam("regEx") String regEx,@QueryParam("origin") String origin,@QueryParam("destinations") String destinations) { 
          
      
        try { 
        
        	ResponseBuilder builder = Response.ok();
        	partnerFactory.getPartnerManger((countryCode!=null)?countryCode:"AU").assignPartnerToOpportunity();
        	JSONObject successObj=new JSONObject();
	        successObj.put("sucess", "true");
	        builder.entity(successObj);
        	return builder.build(); 
  
        } catch (Exception e) { 
  
        	LOGGER.error("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While Assigning the Partners"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response getPartners(@QueryParam("country") String countryCode,@QueryParam("zipCode") String zipCode,@QueryParam("distance") String distance) { 
          
      
        try { 
        	LOGGER.debug("START get partner ");
        	if (countryCode==null){
        		LOGGER.error("error no country "); 
            	RestResponseError err = new RestResponseError("Error", "Country code is mandatory"); 
                ResponseBuilder builder = Response.ok(); 
                builder.entity(err); 
                return builder.build(); 
        	}
        	
        	ResponseBuilder builder = Response.ok();
        	builder.entity(partnerFactory.getPartnerManger(countryCode).getPartner(countryCode,zipCode,distance));
        	return builder.build(); 
  
        } catch (Exception e) { 
  
        	LOGGER.error("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting Partners"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	
	@PUT
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON) 
 	
    public Response updatePartner(@QueryParam("id") String id,@QueryParam("zip") String zip,@QueryParam("userId") String userId,@QueryParam("email") String email,@QueryParam("suppplieremail") String supEmail) { 
          
      
        try { 
        	Partner partner=new Partner();
        	partner.setId(id);
        	partner.setUserId(userId);
        	partner.setZip(zip);
        	partner.setEmail(email);
        	partner.setSupplierEmail(supEmail);
        	partnerDAO.updatePartner(partner);
        	
        	ResponseBuilder builder = Response.ok();
        	JSONObject successObj=new JSONObject();
	        successObj.put("sucess", "true");
	        successObj.put("message", "Partner updated successfully ");
	        builder.entity(successObj);
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error updating partner"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
	}
	
	
	@POST
    @Path("/sendbom")
    @Produces(MediaType.APPLICATION_JSON) 
 	public Response sendBOM(Supplier supplier) { 
          
      
        try { 
        	
        	notificationManagerFactory.getNotificationManager(supplier.getCountry()+"N").sendBOM(supplier.getOppId(), supplier.getUserId(), supplier.getSupplierEmail());
        	ResponseBuilder builder = Response.ok();
        	JSONObject successObj=new JSONObject();
	        successObj.put("sucess", "true");
	        successObj.put("message", "Email sent  successfully ");
	        builder.entity(successObj);
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error sending  BOM"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    } 
	
	@POST
	@Path("/distributor/")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDistributor(Distributor distributor) throws AppException 
	{
	      
		 int result=0;		
		 ResponseBuilder builder = Response.ok(); 
		 try { 
	        	
	        	result=opportunityManager.addDistributorToPartner(distributor);
	        	JSONObject successObj=new JSONObject();
	        	if(result==1)
	        	{
	        		successObj.put("sucess", "Distributor Added Successfully");
	        		builder.entity(successObj);
	        		
	        	}else if(result==2)
	        	{
	        		RestResponseError err = new RestResponseError("Error", "Maximum 5 Distributors can be added"); 
	 	            builder.entity(err);
	        	}else
	        	{
	        		RestResponseError err = new RestResponseError("Error", "Error While Adding Distributor"); 
	 	            builder.entity(err);
	        	}
	        	
        		return builder.build();
	  
	        } catch (Exception e) { 
	  
	           e.printStackTrace();
	           RestResponseError err = new RestResponseError("Error", "Error While Adding Distributor"); 
	           builder = Response.ok(); 
	           builder.entity(err); 
	           return builder.build(); 
	  
	        } 
		
	}
	
	

	@GET
    @Path("/distributor/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getDistributor(@PathParam("id") String partnerId) { 
    List<Distributor> distList=new ArrayList<Distributor>();		
	distList=(List<Distributor>) opportunityManager.getDistList(partnerId);
	      
        try { 
        	
			ResponseBuilder builder = Response.ok(); 
        	builder.entity(distList);
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While getting list of Distributors"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
  
    }	
	
	@PUT
	@Path("/distributor/{shId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDistributor(Distributor distributor,@PathParam("shId") String shId,@QueryParam("distId") String distId)
	{
		
		
		try { 
			  	int response=0;
			response=opportunityManager.updateDistributor(distributor,shId,distId);
        	ResponseBuilder builder = Response.ok();
        	JSONObject successObj=new JSONObject();
			if(response==1)
			{
        	 
	         successObj.put("sucess", "true");
	         successObj.put("message", "Distributor updated successfully ");
	         builder.entity(successObj);
			}
			else
			{
				RestResponseError err = new RestResponseError("Error", "Error while updating Distributor"); 
	            builder.entity(err); 
			}
	        
	        
        	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error while updating Distributor"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
		

	}
	
	
	@DELETE
	@Path("/distributor/{shId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteDistributor(@PathParam("shId") String shId,@QueryParam("distId") String distId)
	{
		
		
		
try { 
	
	int response=0;
	response=opportunityManager.deleteDistributor(distId,shId);
	ResponseBuilder builder = Response.ok();	
	if(response==1)
	{
	JSONObject successObj=new JSONObject();
    successObj.put("sucess", "true");
    successObj.put("message", "Distributor deleted successfully ");
    builder.entity(successObj);
	}
	else
	{
		RestResponseError err = new RestResponseError("Error", "Error While Deleting  Distributor"); 
        builder.entity(err); 
	}
	return builder.build(); 
  
        } catch (Exception e) { 
  
           // logger.debug("error"+""+e); 
        	RestResponseError err = new RestResponseError("Error", "Error While Deleting  Distributor"); 
            ResponseBuilder builder = Response.ok(); 
            builder.entity(err); 
            return builder.build(); 
  
        } 
	}
	
}




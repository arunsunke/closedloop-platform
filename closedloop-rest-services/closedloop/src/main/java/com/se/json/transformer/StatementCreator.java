package com.se.json.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StatementCreator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StatementCreator.class);
	
	public String createStatement(String labelName,String labelValue) {
		
		String stmtStr = "INSERT { "
				+ "?property rdfs:label \""+ labelName +"\" @INSERTTARGETLANGUAGETAGHERE ."
				+ "?measurement rdfs:label \"" + labelValue +"\"@INSERTTARGETLANGUAGETAGHERE ."
				+ "WHERE {"
				+ "?product poc:hasReferenceNumber  \"INSERTREFERENCENUMBERHERE\"^^xsd:string ."
				+ "{?product ?property ?measurement"
				+ "?property rdfs:label \"OLDLABEL\" @en ."
				+ "UNION {"
				+ "?measurement rdfs:label \"OLDLABEL\"@en ."
				+ "} "
				+ "UNION { "
				+ "?measurement1 ?property1 ?measurement2 ."
				+ "?property1 rdfs:label \"OLDLABEL\"@en ."
				+ "OPTIONAL{?measurement2 rdfs:label \"OLDLABEL\"@en .}"
				+ "}"
				+ "}";	
		return stmtStr;
	}
	 	
	public List<String> JsonToStatement(String productJson) {
		
		 List<String> statementList = new ArrayList<String>();
		 JsonToValues jsonToValues = new JsonToValues();
		 Map<String, String> keyValues= jsonToValues.parseProductJson(productJson);
		 LOGGER.debug("Total number of keys with values in JSON - " + keyValues.size());
		 
		 for (Map.Entry<String, String> entry : keyValues.entrySet())
		 {
			 //LOGGER.debug(entry.getKey() + " - " + entry.getValue());
		     statementList.add(createStatement(entry.getKey(),entry.getValue()));
		 }
		 
		 for (String stmt : statementList) {
			// LOGGER.debug(stmt);
		 }
		 
		 return statementList;
	}
	
		 
}

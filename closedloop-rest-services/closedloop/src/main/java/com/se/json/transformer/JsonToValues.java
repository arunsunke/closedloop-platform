package com.se.json.transformer;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonToValues {
	private static final Logger LOGGER = LoggerFactory.getLogger(JsonToValues.class);
	private static final String JSON_DELIM =" : ";
	
	public Map<String,String> parseProductJson(String jsonStr) {
		
 		Map<String,String> jsonKeyValues = new  LinkedHashMap <String,String>();
 		ObjectMapper mapper = new ObjectMapper();
 		if (jsonStr!=null) {
	 		try {
	 			JsonNode rootNode = mapper.readTree(jsonStr);
			    JsonNode productDetailNode = rootNode.path("ProductDetail");
			    jsonKeyValues.put("product_code", productDetailNode.path("product_code").getTextValue());
			    jsonKeyValues.put("product_name", productDetailNode.path("product_name").getTextValue());
			    jsonKeyValues.put("product_description", productDetailNode.path("product_description").getTextValue());
			    jsonKeyValues.put("product_image_location", productDetailNode.path("product_image_location").getTextValue());
			    
			    JsonNode productFeaturesNode=productDetailNode.path("ProductFeatures");
				Iterator<JsonNode> ite = productFeaturesNode.path("features").getElements();
				
				while (ite.hasNext()) {
					JsonNode temp = ite.next();
					String str[] = temp.getTextValue().split(JSON_DELIM);
					
					if(str.length>1) {  // Only store in the map if both Key and Values are present
						jsonKeyValues.put(str[0],str[1]);
					}
				}

			} catch (Exception e) {
				LOGGER.error("Error occurred while getting the Values from JSON  "+ e);
				e.printStackTrace();
			} 
 		}

 		return jsonKeyValues;
	} 
	
/*	public Map<String,String> parse(JSONObject json) throws JSONException{

 		Map<String,String> out = new HashMap<String,String>();
	    Iterator<String> keys = json.keys();
	    while(keys.hasNext()){
	        String key = keys.next();
	        String val = null;
	        try{
	             JSONObject value = json.getJSONObject(key);
	             parse(value);
	        }catch(Exception e){
	        	try {
		        	JSONArray jsonArr= json.getJSONArray(key);
		        	for(int i=0; i<jsonArr.length();i++)
		        		parse(jsonArr.getJSONObject(i));
	        	} catch(Exception e2){
	        		val = json.getString(key);
	        	}
	        }

	        if(val != null){
	        	
	            out.put(key,val);
	        }
	    }
	    return out;
	}*/
}

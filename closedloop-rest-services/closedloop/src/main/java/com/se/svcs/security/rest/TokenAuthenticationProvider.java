package com.se.svcs.security.rest;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Authenticates a request based on the token passed. This is called by {@link RestTokenValidationFilter}.
 */
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private ITokenStorageService tokenStorageService;
    private String memcacheduser;
	private String memcachedpwd;

    /**
     * Returns an authentication object based on the token value contained in the authentication parameter. To do so,
     * it uses a {@link TokenStorageService}.
     * @throws AuthenticationException
     */
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    	RestAuthenticationToken authenticationRequest = (RestAuthenticationToken) authentication;
    	String tokenValue=authenticationRequest.getTokenValue();
        RestAuthenticationToken authenticationResult = new RestAuthenticationToken(tokenValue);
        if (tokenValue!=null) {
            tokenStorageService.loadUserByToken(tokenValue);
            authenticationResult = new RestAuthenticationToken(memcacheduser, memcachedpwd, authenticationRequest.getAuthorities(), tokenValue);            
        }
        return authenticationResult;
    }

	@Override
	public boolean supports(Class<?> authentication) {		
		return false;
	}

	public ITokenStorageService getTokenStorageService() {
		return tokenStorageService;
	}

	public void setTokenStorageService(ITokenStorageService tokenStorageService) {
		this.tokenStorageService = tokenStorageService;
	}

	public String getMemcacheduser() {
		return memcacheduser;
	}

	public void setMemcacheduser(String memcacheduser) {
		this.memcacheduser = memcacheduser;
	}

	public String getMemcachedpwd() {
		return memcachedpwd;
	}

	public void setMemcachedpwd(String memcachedpwd) {
		this.memcachedpwd = memcachedpwd;
	}	
	
}

package com.se.svcs.security.rest;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Authenticates a request based on the user credentials passed.
 */
public class UserCredentialsAuthenticationProvider implements AuthenticationProvider {
	
	/** The memcacheduser. */
	private String memcacheduser=null;
	
	/** The memcachedpwd. */
	private String memcachedpwd=null;

	/* 
	 * @see org.springframework.security.authentication.AuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UsernamePasswordAuthenticationToken authenticationRequest = (UsernamePasswordAuthenticationToken) authentication;
		
		String userName=(String) authenticationRequest.getPrincipal();
		String password=(String) authenticationRequest.getCredentials();
		
		if(userName==null || password==null) {
			throw new AuthenticationCredentialsNotFoundException("User Credentials are empty");
		}
		
		if(!userName.equals(memcacheduser) || !password.equals(memcachedpwd)) {
			throw new AuthenticationCredentialsNotFoundException("Invalid User Crendentials");
		}
		
		return new UsernamePasswordAuthenticationToken(userName, password, authenticationRequest.getAuthorities());
	}

	/*
	 * @see org.springframework.security.authentication.AuthenticationProvider#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {		
		return false;
	}

	/**
	 * Gets the memcacheduser.
	 *
	 * @return the memcacheduser
	 */
	public String getMemcacheduser() {
		return memcacheduser;
	}

	/**
	 * Sets the memcacheduser.
	 *
	 * @param memcacheduser the new memcacheduser
	 */
	public void setMemcacheduser(String memcacheduser) {
		this.memcacheduser = memcacheduser;
	}

	/**
	 * Gets the memcachedpwd.
	 *
	 * @return the memcachedpwd
	 */
	public String getMemcachedpwd() {
		return memcachedpwd;
	}

	/**
	 * Sets the memcachedpwd.
	 *
	 * @param memcachedpwd the new memcachedpwd
	 */
	public void setMemcachedpwd(String memcachedpwd) {
		this.memcachedpwd = memcachedpwd;
	}
}

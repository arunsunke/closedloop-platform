package com.se.svcs.security.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * Extracts credentials from a JSON body like: <code>{"username": "foo", "password": "bar"}</code>
 */
public class HttpHeaderCredentialsExtractor implements ICredentialsExtractor {

    public UsernamePasswordAuthenticationToken extractCredentials(HttpServletRequest httpServletRequest) {
        
    	String username = httpServletRequest.getHeader("username");
        String password = httpServletRequest.getHeader("password");
        
        return new UsernamePasswordAuthenticationToken(username, password);
    }

}

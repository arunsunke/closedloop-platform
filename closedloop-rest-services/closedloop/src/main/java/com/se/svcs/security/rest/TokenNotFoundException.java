package com.se.svcs.security.rest;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityCoreVersion;

/**
 * Customized Exception to indicate that Token is not found.
 */
public class TokenNotFoundException extends AuthenticationException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID=SpringSecurityCoreVersion.SERIAL_VERSION_UID;
	
	/**
	 * Instantiates a new token not found exception.
	 *
	 * @param msg the msg
	 */
	public TokenNotFoundException(String msg) {
		super(msg);
	}

}

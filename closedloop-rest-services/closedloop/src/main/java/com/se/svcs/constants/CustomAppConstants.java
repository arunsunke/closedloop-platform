package com.se.svcs.constants;

/**
 * Class Contains constants. 
 * @author Mitesh Parikh
 * @version 1.0
 */
public final class CustomAppConstants {
	
	private CustomAppConstants(){}
	
	public static final String REQUEST_URI_NOT_FOUND="Custom-app has not found anything matching the Request-URI";	
	public static final String USERID_NOT_FOUND="UserId not found in the database";
	public static final String PROJECTID_NOT_FOUND="ProjectId not found in the database";
	public static final String USERID_EMPTY="UserId cannot be empty";
	public static final String PROJECTID_EMPTY="ProjectId cannot be empty";
	public static final String IMAGEURL_EMPTY="ImageURL cannot be empty";
	public static final String INVALID_STATUS="Invalid Status. Status should be either 4 (For Incomplete Projects) or 5 (Completed Projects)";
	public static final String INVALID_ACTION="Invalid action initiated";
	public static final String GET_QUESTIONNAIRE_ERROR_DESC = "Error while retrieving the questionnaire";
	public static final String SAVE_QUESTIONNAIRE_SUCCESS_DESC = "Questionnaire Details saved successfully";
	public static final String SAVE_QUESTIONNAIRE_ERROR_DESC = "Error while saving the questionnaire";
	public static final String CREATE_PROJECT_SUCCESS_DESC = "Project created successfully";
	public static final String CREATE_PROJECT_ERROR_DESC = "Error while creating a new project";
	public static final String UPDATE_PROJECT_SUCCESS_DESC = "Project Information saved successfully";
	public static final String UPDATE_PROJECT_ERROR_DESC = "Error while updating an existing project";
	public static final String PROJECTLIST_SUCCESS_DESC = "Project details fetched successfully";
	public static final String PROJECTLIST_ERROR_DESC = "Error while retrieving the projects";	
	public static final String DELETE_PROJECT_ERROR_DESC = "Error while deleting the projects";
	public static final String DELETE_PROJECT_SUCCESS_DESC = "Project details deleted successfully";
	public static final String DELETE_PROJECT_BADREQUEST_DESC = "Project details deleted successfully";
	public static final String DELETE_PROJECT_MISMATCH_USERID = "Project was not created by this user";
	public static final String DUPLICATE_PROJECT_SUCCESS_DESC = "Duplicate Project created successfully";
	public static final String PROJECT_PUBLISH_STATUS_REQUEST = "REQUEST";
	public static final String PROJECT_PUBLISH_STATUS_APPROVED = "APPROVED";
	public static final String PROJECT_PUBLISH_STATUS_REJECTED = "REJECT";
	public static final String PROJECT_PUBLISH_STATUS_UNSHARED = "UNSHARED";
	public static final String PROJECT_PUBLISH_STATUS_BLOCKED = "BLOCKED";
	public static final String PROJECT_PUBLISH_STATUS_INITIAL = "INITIAL";
	/*Begin - Business Error Codes*/
	public static final String MANDATORY_QUESTIONS_ERROR_CODE="MANDATORY_QUESTIONS_NOT_ANSWERED";
	public static final String USERID_NOT_FOUND_ERROR_CODE="USER_CONTENT_DOES_NOT_EXIST";
	public static final String RELEVANT_PRODUCTS_NOT_AVAILABLE_FOR_USERID="RELEVANT_PRODUCTS_NOT_EXITS_FOR_USER";
	public static final String RELEVANT_PACKAGES_NOT_AVAILABLE_FOR_USERID="RELEVANT_PACKAGES_NOT_EXITS_FOR_USER";
	public static final String PROJECTID_NOT_FOUND_ERROR_CODE="INVALID_PROJECT_ID";
	public static final String INVALID_PROJECT_STATUS_ERROR_CODE="INVALID_PROJECT_STATUS";
	public static final String MISSING_IMAGE_URL_EMPTY_ERROR_CODE="MISSING_IMAGE_URL";
	public static final String INVALID_DUPLICATE_ACTION_ERROR_CODE="INVALID_DUPLICATE_ACTION";
	public static final String INVALID_PUBLISH_STATUS_ERROR_CODE="INVALID_PUBLISH_STATUS";
	public static final String MISSING_SHARED_PROJECT_DESCRIPTION="MISSING_SHARED_PROJECT_DESCRIPTION";
	public static final String EMPTY_PUBLISH_STATUS="EMPTY_PUBLISH_STATUS";
	public static final String EMPTY_SHARED_PROJECT_DESCRIPTION="EMPTY_SHARED_PROJECT_DESCRIPTION";
	public static final String PROJECT_CURRENTLY_SHARED="PROJECT_CURRENTLY_SHARED";
	public static final String PROJECT_PREVIOUSLY_REJECTED="PROJECT_PREVIOUSLY_REJECTED";
	public static final String PROJECT_PENDING_APPROVAL="PROJECT_PENDING_APPROVAL";
	public static final String PROJECT_INCOMPLETE_ERROR_CODE="PROJECT_INCOMPLETE";
	public static final String INTERNAL_SERVER_ERROR_CODE="INTERNAL_SERVER_ERROR";	
	public static final String INVALID_POST_CODE_ERROR_CODE="INVALID_POST_CODE";
	public static final String PROJSTATUS_OR_PUBLISHSTATUS_REQUIRED="PROJECTSTATUS_OR_PUBLISHSTATUS_REQUIRED";
	public static final String NOT_ALLOWED_TO_RATE_OWN_PROJECT="NOT_ALLOWED_TO_RATE_OWN_PROJECT";
	public static final String EMPTY_RATING="EMPTY_RATING";
	public static final String MAIL_SERVER_ERROR_CODE = "UNABLE_TO_SEND_MAIL";
	/*End - Business Error Codes*/
		
	public static final String DATE_FORMAT="yyyy-MM-dd";
	
	public static final String SCHNEIDER_DEFINED_TEMPLATE_TYPE="T";
	public static final String USER_LOADED_TEMPLATE_TYPE="UP";
	public static final String VALUE_ONE="1";
	public static final String VALUE_SEVEN="7";
	public static final String ADD="add";
	public static final String UPDATE="update";
	public static final String DELETE="del";
	public static final String WHERE_KEYWORD="where ";
	public static final String AND_KEYWORD=" and ";
	public static final String LIMIT_KEYWORD=" Limit ";
	public static final String SPACE=" ";
	public static final String COMMA=" , ";
	public static final String EQUALS=" = ";
	public static final String PRODUCT_TYPE_VALUE="Product";
	public static final String PACKAGE_TYPE_VALUE="Package";
	public static final String NEWEST_PROJECT="np";
	public static final String OLDEST_PROJECT="op";
	public static final String MOST_POPULAR="mp";
	public static final String PROJECT_COST_LOW="pcl";
	public static final String PROJECT_COST_HIGH="pch";
	public static final String STAR_RATING="sr";
	
	public static final String PROJECT_ID="Proj_ID";
	public static final String PROJECT_NAME="Proj_Name";
	public static final String SHARED_PROJECT_DESC="Proj_Desc";
	public static final String APPROVER_PROJECT_NAME="Proj_Approver_ProjName";
	public static final String APPROVER_PROJECT_DESC="Proj_Approver_Desc";
	public static final String APPROVER_PROJECT_REM = "Proj_Approver_Rem";
	public static final String PROJECT_BUILD_TYPE="Proj_Build_Type";
	public static final String PROJECT_ROOMS="Proj_Rooms";
	public static final String PROJECT_POST_CODE="Proj_PostCode";
	public static final String PROJECT_PROG_STATUS="Proj_Prog_Status";
	public static final String PROJECT_PUBLISH_STATUS="Proj_Publish_Stat";
	public static final String PROJECT_CREATED_ID="Proj_Created_ID";
	public static final String PROJECT_CREATED_DATE="Proj_Created_date";
	public static final String PROJECT_LASTACCESSED_DATE="Proj_LastAccessed_date";
	public static final String PROJECT_LASTACCESSED_ID="Proj_LastAccessed_ID";
	public static final String PROJECT_PLAN_TYPE="Proj_Plan_Type";
	public static final String PROJECT_INCOMPLETE="incomplete";
	public static final String PROJECT_COMPLETE="complete";
	public static final String PROJECT_APPROVED_DATE = "Proj_Approved_Date";
	public static final String PROJECT_IMAGE_PATH="Proj_Final_ImgPath";
	public static final String PROJECT_COST="Proj_Cost";
	public static final String PROJECT_PDF_PATH="Proj_PDF_Path";
	public static final String PROJECT_COMPLETION_DATE="Proj_Completion_Date";
	public static final String PROJECT_COLLAGE_IMAGE_PATH = "Proj_Collage_Img_Path";
	public static final String PLAN_TEMPLATE_TITLE="PT_Title";
	public static final String PLAN_TEMPLATE_PATH="PT_Path";
	public static final String PLAN_TEMPLATE_DESC="PT_Description";
	public static final String PLAN_TEMPLATE_TYPE="PT_Type";
	public static final String PLAN_TEMPLATE_ROOMS="PT_Rooms";
	public static final String PLAN_TEMPLATE_ICON_SIZE="PT_Icon_Size";
	public static final String PRODUCT_ID="PP_PIM_Code";
	public static final String ITEM_ID="PP_ID";
	public static final String PRODUCT_SERIAL_NUM="PP_SlNo";
	public static final String PRODUCT_X_COORDINATE="PP_X_Coor";
	public static final String PRODUCT_Y_COORDINATE="PP_Y_Coor";
	public static final String PRODUCT_TYPE="PP_PIM_Type";
	public static final String USER_EMAIL_ID="User_Email_ID";
	public static final String USER_RATED_COUNT="Users_Rated_Count";
	public static final String AVERAGE_RATING = "Average_Rat";
	public static final String TOTAL_RATING="Total_Rating";
	public static final String USER_RATING="User_Rating";
	
	public static final String SELECTQRY_USERIDCOUNT = "Select count(*) from Users where User_Email_ID = ?";
	public static final String SELECTQRY_USERID = "Select User_ID from Users where User_Email_ID = ?";	
	
	public static final String COUNT_OF_RELATED_PROJECTS = "RELPRJCNT";
	
	public static final String ADMIN_ROLE = "Admin";
	public static final String PUPL_PROD_ID = "PUPL_Prod_ID";	
	public static final String PROJECTDETAILS_PROP="project.details";
	
	public static final String  SQL_PROJ_ROOMS_AND_CON = " and Proj_Rooms = ?";
	public static final String  SQL_PROJ_BUILD_TYPE_CON = "	and Proj_Build_Type = ?";
	public static final String  SQL_PROJ_PUBLISH_CON = " and p.Proj_Publish_Stat = ?";
	public static final String  SQL_PROJ_PUBLISH_MULTIPLE_CON = " and p.Proj_Publish_Stat in ('APPROVED','REQUEST','REJECT')";
	public static final String  PROJ_PUBLISH_STATUS_ALL = "al";
	public static final String  PROJ_PUBLISH_STATUS_AP = "ap";
	public static final String  PROJ_PUBLISH_STATUS_PE = "pe";
	public static final String  PROJ_PUBLISH_STATUS_RE = "re";	
	
	public static final String PRE_EMAIL_SUBJECT = "Clipspec Cloud user ";
    public static final String POST_EMAIL_SUBJECT =	" wants to connect with you";
    public static final String EMAIL_SEND_ERROR="Error while sending the email";
    public static final String APP_REJ_ERROR ="Fail to update the Project Status";
    public static final int UPDATE_PROJECT_COMPLETE = 5;
    public static final int BOM_PACKAGE = 1;
    public static final int BOM_PRODUCT = 2;
    public static final char PRINT_PDF_FIND_ELECTRICIAN = 'E';
    public static final char PRINT_PDF_BOOK_CONSULTANT = 'C';
    public static final char PRINT_PDF_DIRECT = 'P';
	
}

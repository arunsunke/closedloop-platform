package com.se.svcs.security.rest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * Generates a JSON response using a {@link RestAuthenticationTokenJsonRenderer}.
 */
public class RestAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RestAuthenticationTokenJsonRenderer renderer;

    /**
     * Called when a user has been successfully authenticated.
     *
     * @param request the request which caused the successful authentication
     * @param response the response
     * @param authentication the <tt>Authentication</tt> object which was created during the authentication process.
     */
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
    	response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.addHeader("Cache-Control", "no-store");
        response.addHeader("Pragma", "no-cache");
        PrintWriter out = response.getWriter();
        out.print(renderer.generateJson((RestAuthenticationToken)authentication));
    }

	public RestAuthenticationTokenJsonRenderer getRenderer() {
		return renderer;
	}

	public void setRenderer(RestAuthenticationTokenJsonRenderer renderer) {
		this.renderer = renderer;
	}    
}

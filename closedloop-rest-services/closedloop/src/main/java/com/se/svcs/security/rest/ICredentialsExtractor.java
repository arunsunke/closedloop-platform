package com.se.svcs.security.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * Extracts username and password from the request and creates and {@link UsernamePasswordAuthenticationToken}
 * object
 */
public interface ICredentialsExtractor {

    public UsernamePasswordAuthenticationToken extractCredentials(HttpServletRequest httpServletRequest);

}
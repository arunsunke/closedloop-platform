package com.se.svcs.security.rest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * CsrfTokenResponseHeaderBindingFilter adds the CSRF headers details in the Http Response Header.
 */
public class CsrfTokenResponseHeaderBindingFilter extends OncePerRequestFilter {
    
    /** The Constant REQUEST_ATTRIBUTE_NAME. */
    protected static final String REQUEST_ATTRIBUTE_NAME = "_csrf";
    
    /** The Constant RESPONSE_HEADER_NAME. */
    protected static final String RESPONSE_HEADER_NAME = "X-CSRF-HEADER";
    
    /** The Constant RESPONSE_PARAM_NAME. */
    protected static final String RESPONSE_PARAM_NAME = "X-CSRF-PARAM";
    
    /** The Constant RESPONSE_TOKEN_NAME. */
    protected static final String RESPONSE_TOKEN_NAME = "X-CSRF-TOKEN";

    /* CSRFToken details are set in the Http Response Header
     * @see org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, javax.servlet.FilterChain filterChain) throws ServletException, IOException {
        CsrfToken token = (CsrfToken) request.getAttribute(REQUEST_ATTRIBUTE_NAME);

        if (token != null) {
            response.setHeader(RESPONSE_HEADER_NAME, token.getHeaderName());
            response.setHeader(RESPONSE_PARAM_NAME, token.getParameterName());
            response.setHeader(RESPONSE_TOKEN_NAME , token.getToken());
        }

        filterChain.doFilter(request, response);
    }
}

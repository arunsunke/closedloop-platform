package com.se.svcs.security.rest;

import java.util.UUID;

/**
 * A {@link TokenGenerator} implementation using {@link java.security.SecureRandom}
 */
public class UUIDTokenGenerator implements ITokenGenerator {

	/**
     * Generates a UUID based token
     *
     * @return a String token of 32 alphanumeric characters.
     */
    public String generateToken() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}

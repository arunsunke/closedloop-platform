package com.se.svcs.security.rest;

/**
 * Generates a JSON representation of a
 * {@link org.springframework.security.core.userdetails.UserDetails} object.
 */
public interface RestAuthenticationTokenJsonRenderer {

	String generateJson(RestAuthenticationToken restAuthenticationToken);

}
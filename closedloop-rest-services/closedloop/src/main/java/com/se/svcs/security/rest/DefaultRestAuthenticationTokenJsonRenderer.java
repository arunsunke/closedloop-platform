package com.se.svcs.security.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

/**
 * Generates a JSON response like the following:
 * <code>{"username":"john.doe","token":"1a2b3c4d","roles":["ROLE_ADMIN","ROLE_USER"]}</code>
 * . If the principal is an instance of {@link OauthUser}, also "email" (
 * {@link CommonProfile#getEmail()}) and "displayName" (
 * {@link CommonProfile#getDisplayName()}) will be rendered
 */
public class DefaultRestAuthenticationTokenJsonRenderer implements RestAuthenticationTokenJsonRenderer {

	public String generateJson(RestAuthenticationToken restAuthenticationToken) {
		JSONObject newTokenJson=new JSONObject();
		newTokenJson.put("access_token", restAuthenticationToken.getTokenValue());
		newTokenJson.put("token_type", "Bearer");
		newTokenJson.put("username", restAuthenticationToken.getPrincipal());
		List<String> roles=new ArrayList<String>();
		roles.add("ROLE_ADMIN");
		roles.add("ROLE_USER");
		newTokenJson.put("roles", roles);
		return newTokenJson.toJSONString();
	}
}

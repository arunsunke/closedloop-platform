package com.se.svcs.security.rest;

/**
 * Implementations of this interface must provide a token generation strategy
 */
public interface ITokenGenerator {

	/**
	 * Generates a globally unique token.
	 *
	 * @return a String based token.
	 */
	String generateToken();

}
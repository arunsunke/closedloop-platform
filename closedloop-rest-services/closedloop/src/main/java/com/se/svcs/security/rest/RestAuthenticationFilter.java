package com.se.svcs.security.rest;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

/**
 * This filter starts the initial authentication flow. It uses the configured {@link AuthenticationManager} bean, allowing
 * to use any authentication provider defined by other plugins or by the application.
 *
 * If the authentication manager authenticates the request, a token is generated using a {@link TokenGenerator} and
 * stored via {@link TokenStorageService}. Finally, a {@link AuthenticationSuccessHandler} is used to render the REST
 * response to the client.
 *
 * If there is an authentication failure, the configured {@link AuthenticationFailureHandler} will render the response.
 */

public class RestAuthenticationFilter extends GenericFilterBean {

    private ICredentialsExtractor credentialsExtractor;
    private String endpointUrl;
    private UserCredentialsAuthenticationProvider userCredentialsAuthenticationProvider;
    private AuthenticationSuccessHandler authenticationSuccessHandler;
    private AuthenticationFailureHandler authenticationFailureHandler;
    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource;
    private ITokenGenerator tokenGenerator;
    private ITokenStorageService tokenStorageService;
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;        
        
        String actualURI = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());
        if(actualURI.equals(endpointUrl)) {    	
            UsernamePasswordAuthenticationToken authenticationRequest = credentialsExtractor.extractCredentials(httpServletRequest);
            authenticationRequest.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
            try {
                //log.debug "Trying to authenticate the request"
                Authentication authenticationResult = userCredentialsAuthenticationProvider.authenticate(authenticationRequest);
                if (authenticationResult.isAuthenticated()) {
                    //log.debug "Request authenticated. Storing the authentication result in the security context"
                    //log.debug "Authentication result: ${authenticationResult}"
                    SecurityContextHolder.getContext().setAuthentication(authenticationResult);
                    String tokenValue = tokenGenerator.generateToken();
                    tokenStorageService.storeToken(tokenValue, authenticationResult.getPrincipal());
                    RestAuthenticationToken restAuthenticationToken = new RestAuthenticationToken(authenticationResult.getPrincipal(), authenticationResult.getCredentials(), authenticationResult.getAuthorities(), tokenValue);
                    authenticationSuccessHandler.onAuthenticationSuccess(httpServletRequest, httpServletResponse, restAuthenticationToken);              
                }
                
            } catch (AuthenticationException ae) {
               // log.debug "Authentication failed: ${ae.message}"
                authenticationFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, ae);
            }
       
        } else {
            chain.doFilter(request, response);
        }
    }

	public ICredentialsExtractor getCredentialsExtractor() {
		return credentialsExtractor;
	}

	public void setCredentialsExtractor(ICredentialsExtractor credentialsExtractor) {
		this.credentialsExtractor = credentialsExtractor;
	}

	public UserCredentialsAuthenticationProvider getUserCredentialsAuthenticationProvider() {
		return userCredentialsAuthenticationProvider;
	}

	public void setUserCredentialsAuthenticationProvider(UserCredentialsAuthenticationProvider userCredentialsAuthenticationProvider) {
		this.userCredentialsAuthenticationProvider = userCredentialsAuthenticationProvider;
	}

	public AuthenticationSuccessHandler getAuthenticationSuccessHandler() {
		return authenticationSuccessHandler;
	}

	public void setAuthenticationSuccessHandler(
			AuthenticationSuccessHandler authenticationSuccessHandler) {
		this.authenticationSuccessHandler = authenticationSuccessHandler;
	}

	public AuthenticationFailureHandler getAuthenticationFailureHandler() {
		return authenticationFailureHandler;
	}

	public void setAuthenticationFailureHandler(
			AuthenticationFailureHandler authenticationFailureHandler) {
		this.authenticationFailureHandler = authenticationFailureHandler;
	}

	public AuthenticationDetailsSource<HttpServletRequest, ?> getAuthenticationDetailsSource() {
		return authenticationDetailsSource;
	}

	public void setAuthenticationDetailsSource(
			AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
		this.authenticationDetailsSource = authenticationDetailsSource;
	}

	public ITokenGenerator getTokenGenerator() {
		return tokenGenerator;
	}

	public void setTokenGenerator(ITokenGenerator tokenGenerator) {
		this.tokenGenerator = tokenGenerator;
	}

	public ITokenStorageService getTokenStorageService() {
		return tokenStorageService;
	}

	public void setTokenStorageService(ITokenStorageService tokenStorageService) {
		this.tokenStorageService = tokenStorageService;
	}
    
	public String getEndpointUrl() {
		return endpointUrl;
	}

	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}
    
}

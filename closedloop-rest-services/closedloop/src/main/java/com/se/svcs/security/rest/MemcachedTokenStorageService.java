package com.se.svcs.security.rest;

import net.spy.memcached.CASValue;
import net.spy.memcached.MemcachedClient;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Stores and retrieves tokens in a memcached server. This implementation stores
 * the whole {@link UserDetails} object in memcached, leveraging it is
 * serializable.
 */
public class MemcachedTokenStorageService implements ITokenStorageService {

	/** The memcached client. */
	private MemcachedClient memcachedClient;
	
	/**  Expiration in seconds. */
	private Integer tokenExpiration;

	/**
     * Returns a principal object given the passed token value
     * @throws TokenNotFoundException if no token is found in the storage
     */
	public Object loadUserByToken(String tokenValue) throws TokenNotFoundException {
    	Object userName=findExistingUserDetails(tokenValue);
        if (userName!=null) {
            return userName;        
        } else {
            throw new TokenNotFoundException("Token not found");
        }
    }

	/**
     * Stores a token. It receives the principal to store any additional information together with the token,
     * like the username associated.
     *
     * @see org.springframework.security.core.Authentication#getPrincipal()
     */
	public void storeToken(String tokenValue, Object principal) {
       memcachedClient.set(tokenValue, tokenExpiration, principal);
    }

	/**
     * Removes a token from the storage.
     * @throws TokenNotFoundException if the given token is not found in the storage
     */
	public void removeToken(String tokenValue) throws TokenNotFoundException {
        Object userId=findExistingUserDetails(tokenValue);
        if (userId!=null) {
            memcachedClient.delete(tokenValue);
        
        } else {
            throw new TokenNotFoundException("Token not found");
        }
    }

	/**
	 * Find existing user details.
	 *
	 * @param tokenValue the token value
	 * @return the object
	 */
	private Object findExistingUserDetails(String tokenValue) {
        Object userName=null;        
		CASValue<Object> result=memcachedClient.gets(tokenValue);
		if (result!=null) {
        	userName=result.getValue();
        } 
        return userName;
    }
	
	/**
	 * Gets the memcached client.
	 *
	 * @return the memcached client
	 */
	public MemcachedClient getMemcachedClient() {
		return memcachedClient;
	}

	/**
	 * Sets the memcached client.
	 *
	 * @param memcachedClient the new memcached client
	 */
	public void setMemcachedClient(MemcachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}

	/**
	 * Gets the token expiration.
	 *
	 * @return the token expiration
	 */
	public Integer getTokenExpiration() {
		return tokenExpiration;
	}

	/**
	 * Sets the token expiration.
	 *
	 * @param tokenExpiration the new token expiration
	 */
	public void setTokenExpiration(Integer tokenExpiration) {
		this.tokenExpiration = tokenExpiration;
	}
}

package com.se.svcs.security.rest;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

/**
 * Holds the API token, passed by the client via a custom HTTP header
 */
public class RestAuthenticationToken extends UsernamePasswordAuthenticationToken {
	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;
	
	private String tokenValue=null;

	public RestAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String tokenValue) {
		super(principal, credentials, authorities);
		this.tokenValue = tokenValue;
	}

	public RestAuthenticationToken(String tokenValue) {
		super("N/A", "N/A");
		this.tokenValue = tokenValue;
	}
	
	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

}

package com.se.svcs.security.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.se.svcs.constants.CustomAppConstants;

/**
 * RFC 6750 implementation of TokenReader
 */
public class BearerTokenReader implements ITokenReader {
	
	private String headername;

    /**
     * Finds the bearer token within the specified request.  It will attempt to look in Authorization header.     *
     * @param request
     * @return
     */
    public String findToken(HttpServletRequest request, HttpServletResponse response) {
        //log.debug "Looking for bearer token in Authorization header or Form-Encoded body parameter"
        String tokenValue=null;
        if(request.getHeader(headername)!=null) {
	        if (request.getHeader(headername).startsWith("Bearer ")) {
	            //log.debug "Found bearer token in Authorization header"
	            tokenValue = request.getHeader(headername).substring(Integer.valueOf(CustomAppConstants.VALUE_SEVEN));        
	        }
        }
        return tokenValue;
    }

	public String getHeadername() {
		return headername;
	}

	public void setHeadername(String headername) {
		this.headername = headername;
	}    
    
}

package com.se.cl.model;

import java.util.List;


public class Opportunity {
	
	
	private String id;
	private String title;
	private String shortDesc;
	private String budget;
	private String opportunityDate;
	private String opportunityExpDate;
	private String businessType;
	private String phone;
	private String email;
	private String bfoAccountId;
	private String bfoOppId;
	private String status;
	private String oppCurrentStatus;
	private String opportunityAcceptedDate;
	private Partner partner;
	private boolean oppReadStatus;
	private boolean isAcceptedByMe;
	private String oppSourceType;

	
	
	private List<OpportunityConsumerImages> OppConsumerImages;
	
	public List<OpportunityConsumerImages> getOppConsumerImages() {
		return OppConsumerImages;
	}
	public void setOppConsumerImages(
			List<OpportunityConsumerImages> oppConsumerImages) {
		OppConsumerImages = oppConsumerImages;
	}
	private Source source;	
	private List<Question> questions;
	
	private String solution30Id;   
	
    private String imageContent;

	public String getSolution30Id() {
		return solution30Id;
	}
	public void setSolution30Id(String solution30Id) {
		this.solution30Id = solution30Id;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public Source getSource() {
		return source;
	}
	public void setSource(Source source) {
		this.source = source;
	}
	public boolean isOppReadStatus() {
		return oppReadStatus;
	}
	public void setOppReadStatus(boolean oppReadStatus) {
		this.oppReadStatus = oppReadStatus;
	}
	private CustomerDetails customerDetails;
	
	private Address address;
	
	private String description;
	
	private OpportunityHist opportunityHistory;
	
	private List<BOM> boms;
	
	private String bmPDF;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getBudget() {
		return budget;
	}
	public void setBudget(String budget) {
		this.budget = budget;
	}
	public String getOpportunityDate() {
		return opportunityDate;
	}
	public void setOpportunityDate(String opportunityDate) {
		this.opportunityDate = opportunityDate;
	}
	public String getOpportunityExpDate() {
		return opportunityExpDate;
	}
	public void setOpportunityExpDate(String opportunityExpDate) {
		this.opportunityExpDate = opportunityExpDate;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public OpportunityHist getOpportunityHistory() {
		return opportunityHistory;
	}
	public void setOpportunityHistory(OpportunityHist opportunityHistory) {
		this.opportunityHistory = opportunityHistory;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}
	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}
	
	public String getBmPDF() {
		return bmPDF;
	}
	public void setBmPDF(String bmPDF) {
		this.bmPDF = bmPDF;
	}
	public List<BOM> getBoms() {
		return boms;
	}
	public void setBoms(List<BOM> boms) {
		this.boms = boms;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBfoAccountId() {
		return bfoAccountId;
	}
	public void setBfoAccountId(String bfoAccountId) {
		this.bfoAccountId = bfoAccountId;
	}
	public String getBfoOppId() {
		return bfoOppId;
	}
	public void setBfoOppId(String bfoOppId) {
		this.bfoOppId = bfoOppId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public String getOppCurrentStatus() {
		return oppCurrentStatus;
	}
	
	public void setOppCurrentStatus(String oppCurrentStatus) {
		this.oppCurrentStatus = oppCurrentStatus;
	}
	public String getOpportunityAcceptedDate() {
		return opportunityAcceptedDate;
	}
	public void setOpportunityAcceptedDate(String opportunityAcceptedDate) {
		this.opportunityAcceptedDate = opportunityAcceptedDate;
	}
	public boolean isAcceptedByMe() {
		return isAcceptedByMe;
	}
	public void setAcceptedByMe(boolean isAcceptedByMe) {
		this.isAcceptedByMe = isAcceptedByMe;
	}
	
	public String getImageContent() {
		return imageContent;
	}
	public void setImageContent(String imageContent) {
		this.imageContent = imageContent;
	}
	public String getOppSourceType() {
		return oppSourceType;
	}
	public void setOppSourceType(String oppSourceType) {
		this.oppSourceType = oppSourceType;
	}
	
	
	

}

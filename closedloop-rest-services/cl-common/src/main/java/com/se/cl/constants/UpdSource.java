package com.se.cl.constants;

public enum UpdSource {

	APPACCEPT("APPACCEPT"),AUPARKED("AUPARKED"), SLA("SLA"), ACCDECLINED("ACCDECLINED"), LISTUPD("LISTUPD"),SOL30("SOL30"), BFO("BFO"),BFOIF("BFOIF");
	//
    private String value;

    private UpdSource(String value) {
            this.value = value;
    }
    
    public String getValue() {
		return value;
	}
	
}

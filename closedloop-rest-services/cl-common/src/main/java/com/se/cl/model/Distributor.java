package com.se.cl.model;

public class Distributor {
	
	private String distId;
	private String shId;
	private String distName;
	private String distEmail;
	private String contName;
	
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}
	
	public String getShId() {
		return shId;
	}
	public void setShId(String shId) {
		this.shId = shId;
	}
	public String getDistName() {
		return distName;
	}
	public void setDistName(String distName) {
		this.distName = distName;
	}
	public String getDistEmail() {
		return distEmail;
	}
	public void setDistEmail(String distEmail) {
		this.distEmail = distEmail;
	}
	public String getContName() {
		return contName;
	}
	public void setContName(String contName) {
		this.contName = contName;
	}

	
	
}

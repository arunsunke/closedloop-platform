package com.se.cl.model;

import java.sql.Timestamp;

public class PartnerDevice {
	
	private int partnerId;
	private String userId;
	private String deviceId;
	private String mobileNumber;
	private String deviceType;
	private Timestamp effectFromDate;
	private Timestamp effectToDate;
	
	
	
	public int getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Timestamp getEffectFromDate() {
		return effectFromDate;
	}
	public void setEffectFromDate(Timestamp effectFromDate) {
		this.effectFromDate = effectFromDate;
	}
	public Timestamp getEffectToDate() {
		return effectToDate;
	}
	public void setEffectToDate(Timestamp effectToDate) {
		this.effectToDate = effectToDate;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	
}

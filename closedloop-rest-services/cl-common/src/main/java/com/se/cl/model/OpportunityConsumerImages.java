package com.se.cl.model;

public class OpportunityConsumerImages
{
	
	  private String userType;
	  private String imageTitle;
	  private String oppTitle;
	  private String imageData;
	  private String mimeType;
	  
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getImageTitle() {
		return imageTitle;
	}
	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}
	public String getOppTitle() {
		return oppTitle;
	}
	public void setOppTitle(String oppTitle) {
		this.oppTitle = oppTitle;
	}
	public String getImageData() {
		return imageData;
	}
	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	  
	  
	  
}

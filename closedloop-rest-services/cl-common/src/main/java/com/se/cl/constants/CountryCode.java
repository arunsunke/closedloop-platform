package com.se.cl.constants;


public enum CountryCode {
	
	
	FRANCE("FR"),AUSTRALIA("AU"),SWEDEN("SW");
	
    private String value;

    private CountryCode(String value) {
            this.value = value;
    }
    
    public String getValue() {
		return value;
	}
    
    public static boolean contains(String code) {

        for (CountryCode c : CountryCode.values()) {
            if (c.getValue().equals(code)) {
                return true;
            }
        }

        return false;
    }
    
}

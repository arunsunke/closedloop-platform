package com.se.cl.model;

public class QuestionBFO {
	
	private String qid;
	private String qqid;
	private String bfoQuestionId;
	private String bfoAnswerId;
	private String ansType;
	public String getQid() {
		return qid;
	}
	public void setQid(String qid) {
		this.qid = qid;
	}
	public String getQqid() {
		return qqid;
	}
	public void setQqid(String qqid) {
		this.qqid = qqid;
	}
	public String getBfoQuestionId() {
		return bfoQuestionId;
	}
	public void setBfoQuestionId(String bfoQuestionId) {
		this.bfoQuestionId = bfoQuestionId;
	}
	public String getBfoAnswerId() {
		return bfoAnswerId;
	}
	public void setBfoAnswerId(String bfoAnswerId) {
		this.bfoAnswerId = bfoAnswerId;
	}
	public String getAnsType() {
		return ansType;
	}
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}
	
	
	
}

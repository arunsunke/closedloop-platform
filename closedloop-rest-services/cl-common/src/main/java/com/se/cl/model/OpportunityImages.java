package com.se.cl.model;

public class OpportunityImages
{
	
	  private String oppId;
	  private String oppTitle;
	  private String artifactFormat;
	  private String artifactName;
	  private String artifactObjectId;
	  private String userType;
	  private String artifactRootObjectId;
	  private String artifactSubFolderId;
	  private String userId;
	  private String shId;
	  private String imsId;
	  
	public String getOppId() {
		return oppId;
	}
	public void setOppId(String oppId) {
		this.oppId = oppId;
	}
	public String getArtifactFormat() {
		return artifactFormat;
	}
	public void setArtifactFormat(String artifactFormat) {
		this.artifactFormat = artifactFormat;
	}
	public String getArtifactName() {
		return artifactName;
	}
	public void setArtifactName(String artifactName) {
		this.artifactName = artifactName;
	}
	public String getArtifactObjectId() {
		return artifactObjectId;
	}
	public void setArtifactObjectId(String artifactObjectId) {
		this.artifactObjectId = artifactObjectId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getArtifactRootObjectId() {
		return artifactRootObjectId;
	}
	public void setArtifactRootObjectId(String artifactRootObjectId) {
		this.artifactRootObjectId = artifactRootObjectId;
	}
	public String getArtifactSubFolderId() {
		return artifactSubFolderId;
	}
	public void setArtifactSubFolderId(String artifactSubFolderId) {
		this.artifactSubFolderId = artifactSubFolderId;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOppTitle() {
		return oppTitle;
	}
	public void setOppTitle(String oppTitle) {
		this.oppTitle = oppTitle;
	}
	public String getShId() {
		return shId;
	}
	public void setShId(String shId) {
		this.shId = shId;
	}
	public String getImsId() {
		return imsId;
	}
	public void setImsId(String imsId) {
		this.imsId = imsId;
	}
	
	
	
	
	  

}

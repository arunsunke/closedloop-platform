package com.se.cl.model;

public class OpportunityLine {

	private String commercialReference;
	private float unitAmount;
	private float quantity;
	public String getCommercialReference() {
		return commercialReference;
	}
	public void setCommercialReference(String commercialReference) {
		this.commercialReference = commercialReference;
	}	
	public float getUnitAmount() {
		return unitAmount;
	}
	public void setUnitAmount(float unitAmount) {
		this.unitAmount = unitAmount;
	}
	public float getQuantity() {
		return quantity;
	}
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	
	
}

package com.se.cl.model;

public class OpportunityBFOHistory {

	private String partnerId;
	private String histSeqId;
	private String oppId;
	private String oppStatus;
	private String bfoOppId;
	private String bfoAccountCd;
	private String bfoContactCd;
	private String bfoAccountRole;
	private String bfoContractorRole;
	private String startDate;
	private String endDate;
	private String reason;
	private String currencyCode;
	private String bfoValueChainPlayerId;
	private String shId;
	private String bfoCustAccountId;
	private int recordCount;
	private String bfoCustVisitId;
	
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getOppId() {
		return oppId;
	}
	public void setOppId(String oppId) {
		this.oppId = oppId;
	}
	public String getOppStatus() {
		return oppStatus;
	}
	public void setOppStatus(String oppStatus) {
		this.oppStatus = oppStatus;
	}
	public String getBfoAccountCd() {
		return bfoAccountCd;
	}
	public void setBfoAccountCd(String bfoAccountCd) {
		this.bfoAccountCd = bfoAccountCd;
	}
	
	public String getBfoAccountRole() {
		return bfoAccountRole;
	}
	public void setBfoAccountRole(String bfoAccountRole) {
		this.bfoAccountRole = bfoAccountRole;
	}
	public String getBfoContractorRole() {
		return bfoContractorRole;
	}
	public void setBfoContractorRole(String bfoContractorRole) {
		this.bfoContractorRole = bfoContractorRole;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getHistSeqId() {
		return histSeqId;
	}
	public void setHistSeqId(String histSeqId) {
		this.histSeqId = histSeqId;
	}
	public String getBfoOppId() {
		return bfoOppId;
	}
	public void setBfoOppId(String bfoOppId) {
		this.bfoOppId = bfoOppId;
	}
	public String getBfoContactCd() {
		return bfoContactCd;
	}
	public void setBfoContactCd(String bfoContactCd) {
		this.bfoContactCd = bfoContactCd;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getBfoValueChainPlayerId() {
		return bfoValueChainPlayerId;
	}
	public void setBfoValueChainPlayerId(String bfoValueChainPlayerId) {
		this.bfoValueChainPlayerId = bfoValueChainPlayerId;
	}
	public String getShId() {
		return shId;
	}
	public void setShId(String shId) {
		this.shId = shId;
	}
	public String getBfoCustAccountId() {
		return bfoCustAccountId;
	}
	public void setBfoCustAccountId(String bfoCustAccountId) {
		this.bfoCustAccountId = bfoCustAccountId;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public String getBfoCustVisitId() {
		return bfoCustVisitId;
	}
	public void setBfoCustVisitId(String bfoCustVisitId) {
		this.bfoCustVisitId = bfoCustVisitId;
	}
	
	
	
	
	
}

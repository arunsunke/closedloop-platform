package com.se.cl.model;

import java.util.HashMap;

public class ClosedLoopOpportunity
{
	
	private Account account=null;
	//private BFOOpportunity opportunity =null;
	private HashMap<String,Object> opportunity =null;
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public HashMap<String, Object> getOpportunity() {
		return opportunity;
	}
	public void setOpportunity(HashMap<String, Object> opportunity) {
		this.opportunity = opportunity;
	}
	
	/*public BFOOpportunity getOpportunity() {
		return opportunity;
	}
	public void setOpportunity(BFOOpportunity opportunity) {
		this.opportunity = opportunity;
	}*/
	
	

}

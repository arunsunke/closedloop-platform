package com.se.cl.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Opportunities {
	
	@JsonProperty("Opportunities")
	private List<Opportunity> opportunityList;

	public List<Opportunity> getOpportunityList() {
		return opportunityList;
	}

	public void setOpportunityList(List<Opportunity> opportunityList) {
		this.opportunityList = opportunityList;
	}
	
	@JsonProperty("SLA")
	private Map<String,Long> oppSLA;
	
	@JsonProperty("Config")
	private Map<String,String> config;

	public Map<String, String> getConfig() {
		return config;
	}

	public void setConfig(Map<String, String> config) {
		this.config = config;
	}

	public Map<String, Long> getOppSLA() {
		return oppSLA;
	}

	public void setOppSLA(Map<String, Long> oppSLA) {
		this.oppSLA = oppSLA;
	}
	
	
	

}

package com.se.cl.model;

public class Solution30StatusResponse {
	
	private String statusId;
	private String expertFirstName;
	private String expertLastName;
	private String expertId;
	
	
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getExpertFirstName() {
		return expertFirstName;
	}
	public void setExpertFirstName(String expertFirstName) {
		this.expertFirstName = expertFirstName;
	}
	public String getExpertLastName() {
		return expertLastName;
	}
	public void setExpertLastName(String expertLastName) {
		this.expertLastName = expertLastName;
	}
	public String getExpertId() {
		return expertId;
	}
	public void setExpertId(String expertId) {
		this.expertId = expertId;
	}

}

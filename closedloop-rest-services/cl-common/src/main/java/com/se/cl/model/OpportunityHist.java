package com.se.cl.model;

import java.util.HashMap;
import java.util.List;

public class OpportunityHist {

	private List<HashMap<String,String>> oppHistory;

	public List<HashMap<String,String>> getOppHistory() {
		return oppHistory;
	}

	public void setOppHistory(List<HashMap<String,String>> oppHistory) {
		this.oppHistory = oppHistory;
	}
	
}

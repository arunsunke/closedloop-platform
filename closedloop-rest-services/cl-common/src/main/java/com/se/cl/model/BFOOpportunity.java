package com.se.cl.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BFOOpportunity {
 
	
	private String name;
	private Owner  owner;
	private String source;
	private String currencyCode;
	private String closeDate;
	private String countryCode;
	private String leadingBusiness;
	private String channel;
	private String marketSegment;
	private List<OpportunityLine> opportunityLine=null;
	private Assessment assessment=null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public String getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLeadingBusiness() {
		return leadingBusiness;
	}
	public void setLeadingBusiness(String leadingBusiness) {
		this.leadingBusiness = leadingBusiness;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getMarketSegment() {
		return marketSegment;
	}
	public void setMarketSegment(String marketSegment) {
		this.marketSegment = marketSegment;
	}	
	public List<OpportunityLine> getOpportunityLine() {
		return opportunityLine;
	}
	public void setOpportunityLine(List<OpportunityLine> opportunityLine) {
		this.opportunityLine = opportunityLine;
	}
	public Assessment getAssessment() {
		return assessment;
	}
	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}	
	
}

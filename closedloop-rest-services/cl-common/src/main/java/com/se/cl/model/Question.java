package com.se.cl.model;

public class Question {
	
	private String qid;
	
	private String id;
	private String value;
	private String ansId;	
	private String ansType;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public String getQid() {
		return qid;
	}
	public void setQid(String qid) {
		this.qid = qid;
	}
	
	public String getAnsId() {
		return ansId;
	}
	
	public void setAnsId(String ansId) 
	{
		this.ansId = ansId;
	}
	
	public String getAnsType() 
	{
		return ansType;
	}
	
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}
	
	
	
}

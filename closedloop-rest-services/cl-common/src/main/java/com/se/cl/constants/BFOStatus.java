package com.se.cl.constants;

public enum BFOStatus {


	MEETING("MEETING"),QUOTATION("QUOTATION"), WON("WON"), LOST("LOST"), INSTALLATION("INSTALLATION"),INSTALLED("INSTALLED"),ACCEPTED("ACCEPTED"),CANCEL("CANCEL");
    private String value;

    private BFOStatus(String value) {
            this.value = value;
    }
    
    public String getValue() {
		return value;
	}
    
	
}

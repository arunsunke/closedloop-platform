package com.se.cl.constants;

public enum OppStatus {
	
	
	LOADED("LOADED"),INITIAL("INITIAL"), ACCEPTED("ACCEPTED"), DECLINED("DECLINED"), WIP("WIP"),PARKED("PARKED"), CLOSED("CLOSED");
	//
    private String value;

    private OppStatus(String value) {
            this.value = value;
    }
    
    public String getValue() {
		return value;
	}
    
}

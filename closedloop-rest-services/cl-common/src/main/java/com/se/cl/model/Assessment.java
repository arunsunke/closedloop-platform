package com.se.cl.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)


public class Assessment {

	private String name;
	//private List<Response> response=null;
	private List<HashMap<String,String>> response=null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/*public List<Response> getResponse() {
		return response;
	}
	public void setResponse(List<Response> response) {
		this.response = response;
	}*/
	
	public List<HashMap<String, String>> getResponse() {
		return response;
	}
	public void setResponse(List<HashMap<String, String>> response) {
		this.response = response;
	}
	
	
	
	
}

package com.se.cl.dao;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.se.cl.exception.AppException;
import com.se.cl.model.OpportunityImages;
import com.se.cl.model.Partner;
import com.se.cl.model.PartnerDevice;

public interface PartnerDAO {

	
	public List<Partner> getPartner(String countryCode);
	public void updatePartner(Partner partner)  throws AppException;
	
	public Hashtable<String,String> getPartnerZips(String countryCode);
	public void assignOpportunityToPartner(HashMap<String,List<String>> oppPartnerList);
	
	public HashMap<String,List<String>> getPartnerListForNewOpps(String country);
	public HashMap<String,List<String>> getPartnerListForSLAReminder(int slaStep,String country);
	public List<PartnerDevice> listPartnerDeviceByIds(List<String> partnerIds) throws AppException;
	
	public List<String> getPartnersWhoAcceptsOppDidNothing();
	
	public List<String> getOppWhenNoPartnerAccepted(String country);
	public List<String> getOppWhenAllPartnerDeclined(String country);
	
	public Map<String,String> getCLconfig();
	public void updateCLconfig(String config);
	
	public List<Map<String, String>> assignOpportunityImagesToPartners(String countryCode);
	
	public void unAssignOpportunityImagesToPartners(String countryCode);
	
	public HashMap<String, String> getPartnerImsID(String partnerId);
	
	public List<OpportunityImages> getPartnerImageDetails(String oppId);
	
	public String getPartnerImsFederatedID(String partnerId);
	
	public List<String> getPartnerImsFederatedIDsByCountry(String countryCode);
	
	
}

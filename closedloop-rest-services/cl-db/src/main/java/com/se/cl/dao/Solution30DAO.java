package com.se.cl.dao;



import java.util.List;

public interface Solution30DAO {
	
	public void updateOpportunity(String oppId,String sol30RefId);
	
	public List<String> getOpportunities();
	
	

}

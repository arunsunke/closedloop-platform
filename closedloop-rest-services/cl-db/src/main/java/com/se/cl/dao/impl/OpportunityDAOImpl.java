package com.se.cl.dao.impl;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.se.cl.constants.CountryCode;
import com.se.cl.constants.OppStatus;
import com.se.cl.dao.OpportunityDAO;
import com.se.cl.exception.AppException;
import com.se.cl.model.Address;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerDetails;
import com.se.cl.model.Distributor;
import com.se.cl.model.Opportunities;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityHist;
import com.se.cl.model.OpportunityImages;
import com.se.cl.model.Partner;
import com.se.cl.model.PartnerDevice;
import com.se.cl.model.Question;
import com.se.cl.model.User;

public class OpportunityDAOImpl implements OpportunityDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OpportunityDAOImpl.class);
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm:ss a");

	//private PlatformTransactionManager transactionManager;

	private @Value("${accept-sla-step3}") Long acceptSLABy;

	private @Value("${accept-sla-step1}") Long slaStep1;
	private @Value("${accept-sla-step2}") Long slaStep2;
	private @Value("${accept-sla-step3}") Long slaStep3;
	
	private @Value("${accept-sla-step4}") Long slaStep4;


	/*public void setTransactionManager(
			PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}*/

	@Override
	public Opportunities getOpportunitiesForPartner(String partnerId) {

		Opportunities opportunities = new Opportunities();

		
		  String query = "SELECT SHOL_OPP_ID,SHOL_Status FROM stakeholderopplink WHERE SHOL_SH_ID =:SHOL_SH_ID "		;
		  LOGGER.debug("SQL :"+query); HashMap<String,Object> paramMap = new
		  HashMap<String, Object>(); paramMap.put("SHOL_SH_ID", partnerId);
		  //paramMap.put("productId", productId);
		  
		  List<Map<String,Object>> rows =
		  namedParameterJdbcTemplate.queryForList(query, paramMap);
		  
		  List<String> opportunityIds=new ArrayList<String>();
		  HashMap<String,String> oppIdStatusMap=new HashMap<String,String>();
		  for(Map<String,Object> row : rows){
		  opportunityIds.add((String.valueOf(row.get("SHOL_OPP_ID"))));
		  oppIdStatusMap.put((String.valueOf(row.get("SHOL_OPP_ID"))),
		  (String.valueOf(row.get("SHOL_Status")))); }
		 
		 paramMap = new HashMap<String, Object>();
		//List<Map<String, Object>> rows = null;
		//String query = "";
		//List<String> opportunityIds = new ArrayList<String>();
		//opportunityIds.add(oppId);
		//HashMap<String, String> oppIdStatusMap = new HashMap<String, String>();
		//oppIdStatusMap.put(oppId, "INITIAL");

		List<Opportunity> opportunityList = new ArrayList<Opportunity>();

		if (opportunityIds != null && opportunityIds.size() > 0) {

			Set<Map.Entry<String, String>> entrySet = oppIdStatusMap.entrySet();

			for (Entry entry : entrySet) {
				// System.out.println("Opp ID: " + entry.getKey() + " Status: "
				// + entry.getValue());
				// for(String opp_id:opportunityIds){

				/*
				 * query=
				 * "select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
				 * +
				 * " OPP_ZipCode, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt, OPP_Attachment, OPP_Eff_FmDt, OPP_Eff_ToDt from opportunity where OPP_ID=:OPP_ID and OPP_Status <>'DECLINED' "
				 * ;
				 */
				query = "select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
						+ " OPP_ZipCode,OPP_Cntry_Cd, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt, OPP_Eff_FmDt, OPP_Eff_ToDt,OPP_Buss_Type,cust_first_name,cust_last_name,"
						+ "cust_phone,cust_email from opportunity o,customer c where o.OPP_Consumer_ID=c.cust_id and OPP_ID=:OPP_ID and OPP_Status <>'DECLINED'  order by OPP_Eff_FmDt";

				LOGGER.debug("SQL :" + query);
				paramMap = new HashMap<String, Object>();
				paramMap.put("OPP_ID",
						Integer.parseInt(entry.getKey().toString()));
				// paramMap.put("productId", productId);

				rows = namedParameterJdbcTemplate.queryForList(query, paramMap);

				for (Map<String, Object> row : rows) {

					Opportunity opp = new Opportunity();
					HashMap<String, String> histMap = new HashMap<String, String>();

					opp.setId(String.valueOf(row.get("OPP_ID")));
					opp.setTitle(String.valueOf(row.get("OPP_Name")));
					opp.setShortDesc(String.valueOf(row.get("OPP_Short_Desc")));
					//
					opp.setBudget(String.valueOf(row.get("OPP_Budget")));

					opp.setOpportunityDate(String.valueOf(row
							.get("OPP_Eff_FmDt")));
					opp.setOpportunityExpDate(String.valueOf(row
							.get("OPP_Eff_ToDt")));

					opp.setBusinessType(String.valueOf(row.get("OPP_Buss_Type")));

					opp.setDescription(String.valueOf(row.get("OPP_Desc")));
					Address address = new Address();
					address.setCity(String.valueOf(row.get("OPP_City")));
					address.setSt_name(String.valueOf(row.get("OPP_State")));
					address.setZip(String.valueOf(row.get("OPP_ZipCode")));
					opp.setAddress(address);

					CustomerDetails cust = new CustomerDetails();
					cust.setEmail(String.valueOf(row.get("cust_email")));
					cust.setFirstName(String.valueOf(row.get("cust_first_name")));
					cust.setLastName(String.valueOf(row.get("cust_last_name")));
					cust.setPhone(String.valueOf(row.get("cust_phone")));

					/*
					 * Address caddress=new Address();
					 * caddress.setCity(String.valueOf(row.get("addr_city")));
					 * caddress
					 * .setSt_name(String.valueOf(row.get("addr_street_name")));
					 * caddress.setZip(String.valueOf(row.get("addr_zip")));
					 * caddress
					 * .setCountryCode(String.valueOf(row.get("addr_cntry_cd"
					 * ))); cust.setAddress(caddress);
					 */

					opp.setCustomerDetails(cust);

					histMap.put("opportunityStatus", entry.getValue()
							.toString());
					histMap.put("opportunityStatusUpdatedDate",
							String.valueOf(row.get("OPP_Last_UpdDt")));

					OpportunityHist opHist = new OpportunityHist();
					// opHist.setOppHistory(histMap);
					opp.setOpportunityHistory(opHist);
					opportunityList.add(opp);

				}
			}

		}
		opportunities.setOpportunityList(opportunityList);

		return opportunities;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public Opportunities getOpportunities(String username) {

		String supplierEmail = "";

		String countryCode="";
		
		Opportunities opportunities = new Opportunities();
		
		Map<String, Object> oppImages = new HashMap<String, Object>();
          

		// get partner Id using userid

		String query = "SELECT SH_ID,SH_SUP_EMAIL,SH_Country_Code from stakeholders WHERE SH_User_Name =:SHOL_USER_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_USER_ID", username);
		// paramMap.put("productId", productId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String partnerId = "";

		for (Map<String, Object> row : rows) 
		{
			partnerId = String.valueOf(row.get("SH_ID"));
			supplierEmail = String.valueOf(row.get("SH_SUP_EMAIL"));
			countryCode = String.valueOf(row.get("SH_Country_Code"));
		}
		
		LOGGER.debug("countryCode:::"+countryCode);

		query = "SELECT SHOL_OPP_ID,SHOL_Status,SHOL_SH_ID FROM stakeholderopplink WHERE SHOL_SH_ID =:SHOL_SH_ID ";
		LOGGER.debug("SQL :" + query);
		paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_SH_ID", partnerId);
		// paramMap.put("productId", productId);

		rows = namedParameterJdbcTemplate.queryForList(query, paramMap);

		List<String> opportunityIds = new ArrayList<String>();
		HashMap<String, String> oppIdStatusMap = new HashMap<String, String>();
		for (Map<String, Object> row : rows) {
			opportunityIds.add((String.valueOf(row.get("SHOL_OPP_ID"))));
			oppIdStatusMap.put((String.valueOf(row.get("SHOL_OPP_ID"))),
					(String.valueOf(row.get("SHOL_Status"))));
		}

		List<Opportunity> opportunityList = new ArrayList<Opportunity>();

		if (opportunityIds != null && opportunityIds.size() > 0) {

			Set<Map.Entry<String, String>> entrySet = oppIdStatusMap.entrySet();
			LOGGER.debug("Opportunity size :" + entrySet.size());

			for (Entry entry : entrySet) {
				System.out.println("Opp ID: " + entry.getKey() + " Status: "
						+ entry.getValue());
				// for(String opp_id:opportunityIds){

				/*
				 * query=
				 * "select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
				 * +
				 * " OPP_ZipCode, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt, OPP_Attachment, OPP_Eff_FmDt, OPP_Eff_ToDt from opportunity where OPP_ID=:OPP_ID and OPP_Status <>'DECLINED' "
				 * ;
				 */
				/*query = "select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
						+ " OPP_ZipCode,OPP_Cntry_Cd, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt, OPP_Eff_FmDt, OPP_Eff_ToDt,OPP_Buss_Type,cust_first_name,cust_last_name,"
						+ "cust_phone,cust_email,sl.OPP_Read_Status,sl.SHOL_status from opportunity o,customer c, stakeholderopplink sl where o.OPP_Consumer_ID=c.cust_id and OPP_ID=sl.SHOL_OPP_ID and OPP_ID=:OPP_ID and sl.SHOL_SH_ID=:SHOL_SH_ID   and sl.SHOL_Status <>'DECLINED'  and o.OPP_Status<>'PARKED'";
				*/
				
				query = "select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
						+ " OPP_ZipCode,OPP_Cntry_Cd, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt, OPP_Eff_FmDt, OPP_Eff_ToDt,OPP_Buss_Type,cust_first_name,cust_last_name,"
						+ "cust_phone,cust_email,sl.OPP_Read_Status,sl.SHOL_status from opportunity o,customer c, stakeholderopplink sl where o.OPP_Consumer_ID=c.cust_id and o.OPP_ID=sl.SHOL_OPP_ID and OPP_ID=:OPP_ID and sl.SHOL_SH_ID=:SHOL_SH_ID  and sl.SHOL_Status <>'DECLINED'   and o.OPP_Status<>'PARKED' ";

				
				LOGGER.debug("SQL :" + query);
				paramMap = new HashMap<String, Object>();
				paramMap.put("OPP_ID",
						Integer.parseInt(entry.getKey().toString()));
				paramMap.put("SHOL_SH_ID", partnerId);

				// paramMap.put("productId", productId);

				rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
                
				for (Map<String, Object> row : rows)
				{
                   
				   boolean cancelExists=false;
				   cancelExists = checkCancelExists(String.valueOf(row.get("OPP_ID")),partnerId);
				   if(!cancelExists)
				   {
						Opportunity opp = new Opportunity();
						
						opp.setId(String.valueOf(row.get("OPP_ID")));
						opp.setTitle(String.valueOf(row.get("OPP_Name")));
						opp.setShortDesc(String.valueOf(row.get("OPP_Short_Desc")));
						//
						opp.setBudget(String.valueOf(row.get("OPP_Budget")));
	
						opp.setOpportunityDate(String.valueOf(row
								.get("OPP_Eff_FmDt")));
						opp.setOpportunityExpDate(String.valueOf(row
								.get("OPP_Eff_ToDt")));
	
						opp.setBusinessType(String.valueOf(row.get("OPP_Buss_Type")));
	
						opp.setDescription(String.valueOf(row.get("OPP_Desc")));
						Address address = new Address();
						address.setCity(String.valueOf(row.get("OPP_City")));
						address.setSt_name(String.valueOf(row.get("OPP_State")));
						address.setZip(String.valueOf(row.get("OPP_ZipCode")));
						opp.setAddress(address);
	
						CustomerDetails cust = new CustomerDetails();
						cust.setEmail(String.valueOf(row.get("cust_email")));
						cust.setFirstName(String.valueOf(row.get("cust_first_name")));
						cust.setLastName(String.valueOf(row.get("cust_last_name")));
						cust.setPhone(String.valueOf(row.get("cust_phone")));
	
						/*
						 * Address caddress=new Address();
						 * caddress.setCity(String.valueOf(row.get("addr_city")));
						 * caddress
						 * .setSt_name(String.valueOf(row.get("addr_street_name")));
						 * caddress.setZip(String.valueOf(row.get("addr_zip")));
						 * caddress
						 * .setCountryCode(String.valueOf(row.get("addr_cntry_cd"
						 * ))); cust.setAddress(caddress);
						 */
	
						opp.setCustomerDetails(cust);
						opp.setOppReadStatus(String.valueOf(
								row.get("OPP_Read_Status")).equalsIgnoreCase("1") ? true
								: false);
						/*opp.setAcceptedByMe(String.valueOf(
								row.get("SHOL_status")).equalsIgnoreCase(OppStatus.ACCEPTED.getValue())? true
								: false);*/
						String AcceptedPartenerId="";
						AcceptedPartenerId = checkOPPAccepted(String.valueOf(row.get("OPP_ID")));
						LOGGER.info("getOpportunities:::AcceptedPartenerId"+AcceptedPartenerId);
						LOGGER.info("getOpportunities:::partnerId"+partnerId);
						if(checkOPPNoneAccepted(String.valueOf(row.get("OPP_ID"))))
						{
							opp.setAcceptedByMe(false);
							LOGGER.info("1");
						}
						else if(String.valueOf(row.get("SHOL_status")).equalsIgnoreCase("ACCEPTED") && AcceptedPartenerId.equals(partnerId))
						{
							opp.setAcceptedByMe(false);
							LOGGER.info("2");
						}else
						{
							opp.setAcceptedByMe(true);
							LOGGER.info("3");
						}					
						opp.setStatus(entry.getValue().toString());
						opp.setOpportunityAcceptedDate(String.valueOf(row
								.get("OPP_Last_UpdDt")));
	
						// String
						// histQuery="select  SHOLH_Remarks, SHOLH_Eff_FmDt, SHOLH_Eff_ToDt, SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID from stakeholderopplinkhist where (SHOLH_OPP_ID, SHOLH_Sequence) in ( select SHOLH_OPP_ID,max(SHOLH_Sequence) seqMax from stakeholderopplinkhist group by SHOLH_Status_Id,SHOLH_OPP_ID having SHOLH_SH_ID=:SHOL_SH_ID and SHOLH_OPP_ID=:SHOLH_OPP_ID  order by seqMax)";
						String histQuery = "select max(SHOLH_Sequence) seqMax,SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID from stakeholderopplinkhist group by SHOLH_Status_Id,SHOLH_OPP_ID having SHOLH_SH_ID=:SHOL_SH_ID and SHOLH_OPP_ID=:SHOLH_OPP_ID  order by seqMax";
						Map<String, Object> histparamMap = new HashMap<String, Object>();
						histparamMap.put("SHOL_SH_ID", partnerId);
						histparamMap.put("SHOLH_OPP_ID", opp.getId());
						List<Map<String, Object>> histRows = namedParameterJdbcTemplate
								.queryForList(histQuery, histparamMap);
						List<HashMap<String, String>> histMapArray = new ArrayList<HashMap<String, String>>();
						
						if(countryCode!=null && countryCode.equalsIgnoreCase("SW"))
						{
							
							for (Map<String, Object> histRow : histRows) 
							{	
								HashMap<String, String> histMap = new HashMap<String, String>();						
								LOGGER.info("currrent status"+String.valueOf(histRow.get("SHOLH_Status_Id")));
								LOGGER.info("SHOLH_OPP_ID:::"+opp.getId());
								histMap.put("statusId",	String.valueOf(histRow.get("SHOLH_Status_Id")));
								histMapArray.add(histMap);
							}
							
						}else
						{	
							for (Map<String, Object> histRow : histRows) 
							{	
								HashMap<String, String> histMap = new HashMap<String, String>();						
								LOGGER.info("currrent status"+String.valueOf(histRow.get("SHOLH_Status_Id")));
								LOGGER.info("SHOLH_OPP_ID:::"+opp.getId());
								if((String.valueOf(histRow.get("SHOLH_Status_Id")).equalsIgnoreCase("WON")) || (String.valueOf(histRow.get("SHOLH_Status_Id")).equalsIgnoreCase("LOST")))
								{	
									LOGGER.info("inside won/lost if condition");
									histMap.put("statusId",	String.valueOf(histRow.get("SHOLH_Status_Id")));
									histMapArray.add(histMap);
									//if(countryCode!=null && !countryCode.equalsIgnoreCase("SW"))
									//{	
										break;
									//}
								}else
								{
									histMap.put("statusId",	String.valueOf(histRow.get("SHOLH_Status_Id")));
									histMapArray.add(histMap);
									LOGGER.info("inside won/lost else condition");
								}	
							}
						}
						OpportunityHist opHist = new OpportunityHist();
						opHist.setOppHistory(histMapArray);
						// opp.setOpportunityHistory(opHist);
	
						if (histMapArray.size() > 0) {
							HashMap<String, String> histMapLatest = histMapArray
									.get(histMapArray.size() - 1);
							opp.setOppCurrentStatus(histMapLatest.get("statusId"));
	
						}
	
						opportunityList.add(opp);
					}
	
					}
	
				}

		}
		opportunities.setOpportunityList(opportunityList);

		Map<String, Long> oppsSLAMap = new HashMap<String, Long>();
		oppsSLAMap.put("SLA1", slaStep1);
		oppsSLAMap.put("SLA2", slaStep2);
		oppsSLAMap.put("SLA3", slaStep3);
		oppsSLAMap.put("SLA4", slaStep4);

		opportunities.setOppSLA(oppsSLAMap);

		Map<String, String> configMap = new HashMap<String, String>();
		configMap.put("SupplierEmail", supplierEmail);
		opportunities.setConfig(configMap);

		return opportunities;

	}

	public JSONObject getOpportunityBOM(String oppId) throws AppException {

		String sql = "SELECT OPP_Attachment from opportunity WHERE OPP_ID =:OPP_ID";
		// InputStream is= jdbcTemplate.queryForObject(sql, new FileMapper());
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);
		// paramMap.put("productId", productId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(sql, paramMap);

		String is = "";

		for (Map<String, Object> row : rows) {
			is = ((String.valueOf(row.get("OPP_Attachment"))));
		}

		if (is != null) {

			// byte[] bytes;
			JSONObject prfjson = new JSONObject();
			try {
				// bytes = IOUtils.toByteArray(is);

				// prfjson.put("BOM", Base64.encode(bytes));

				is.replace("/\n/g", "\\n");

				prfjson.put("BOM", is);// not converting to Base64 since it is
										// already stored as Base64
				return prfjson;

			}/*
			 * catch (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); throw new
			 * AppException(Response.Status.INTERNAL_SERVER_ERROR
			 * .getStatusCode(), 500, "Error while retreiving the BOM " , "",
			 * null); }
			 */
			catch (Exception e) {
				e.printStackTrace();
				throw new AppException(
						Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
						500, "Error while retreiving the BOM ", "", null);
			}

		} else {

			throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
					204, "BOM not available for this opportunity ", "", null);
		}

	}

	class FileMapper implements ParameterizedRowMapper<InputStream> {
		public InputStream mapRow(ResultSet rs, int rowNum) throws SQLException {

			return rs.getBinaryStream(1);
		}
	}

	private static byte[] convertPDFToByteArray() {

		InputStream inputStream = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {

			inputStream = new FileInputStream("sourcePath_of_pdf.pdf");

			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();

			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return baos.toByteArray();
	}

	@Override
	public int addDevicetoPartner(final PartnerDevice device)
			throws AppException {

		// JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String query = "SELECT SH_ID from stakeholders WHERE SH_User_Name =:SHOL_USER_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_USER_ID", device.getUserId());

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		int partnerId = 0;

		for (Map<String, Object> row : rows) {
			partnerId = Integer.parseInt(String.valueOf(row.get("SH_ID")));
		}
		if (partnerId <= 0) {
			throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
					204, "Partner not found in closed loop for given user id ",
					"", null);
		}
		final int partnerFinalId = partnerId;

		boolean deviceFound = false;
		if (device.getDeviceType().equalsIgnoreCase("IOS")) {
			query = "SELECT SHD_Device_ID FROM stakeholdersdevices WHERE SHD_SH_ID =:SHD_SH_ID";
			LOGGER.debug("SQL :" + query);
			paramMap = new HashMap<String, Object>();
			paramMap.put("SHD_SH_ID", partnerId);
			rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			for (Map<String, Object> row : rows) {
				if (String.valueOf(row.get("SHD_Device_ID")).equals(
						device.getDeviceId())) {
					deviceFound = true;
					break;
				}

			}
			if (deviceFound) {
				return partnerId;
			}

		}

		if (!deviceFound) {
			jdbcTemplate
					.update("insert into stakeholdersdevices(SHD_SH_ID,SHD_Device_ID,SHD_Mobile_No,SHD_Eff_FmDt,SHD_Device_Type) "
							+ "values (?, ?, ?,?,?) ",
							new PreparedStatementSetter() {
								public void setValues(PreparedStatement ps)
										throws SQLException {
									ps.setInt(1, partnerFinalId);
									ps.setString(2, device.getDeviceId());
									ps.setString(3, device.getMobileNumber());
									ps.setTimestamp(4,
											device.getEffectFromDate());
									ps.setString(5, device.getDeviceType());

								}
							});
		}

		return partnerId;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public long addOpportunity(Opportunity opportunity) throws AppException {

		long opportunityID = 0L;

		Timestamp opp_start_date = null;
		Timestamp opp_expiry_date = null;
		Timestamp new_opp_expiry_date = null;

		/*TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus transStatus = transactionManager.getTransaction(def);*/

		if (opportunity.getSource() != null
				&& opportunity.getSource().getId() != null
				&& !opportunity.getSource().getId().equalsIgnoreCase("CL")) {

			try {

				String query = "SELECT SH_ID from stakeholders WHERE SH_User_Name =:SHOL_USER_ID ";
				LOGGER.debug("SQL :" + query);
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("SHOL_USER_ID", opportunity.getSource().getId());

				List<Map<String, Object>> rows = namedParameterJdbcTemplate
						.queryForList(query, paramMap);

				int partnerId = 0;

				for (Map<String, Object> row : rows) {
					partnerId = Integer.parseInt(String.valueOf(row
							.get("SH_ID")));
				}

				if (partnerId == 0) {
					throw new AppException(0, 0,
							"Partner Not exists in ClosedLoop",
							"Partner Not exists in ClosedLoop", "");

				}

			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Not able to assign opportunity to partner from source");
			}
		}

		try {
			if (opportunity != null && opportunity.getOpportunityDate() != null
					&& !opportunity.getOpportunityDate().trim().equals(""))
				opp_start_date = new Timestamp(DATE_FORMAT.parse(
						opportunity.getOpportunityDate()).getTime());

			if (opportunity.getOpportunityExpDate() != null
					&& !opportunity.getOpportunityExpDate().trim().equals(""))
				opp_expiry_date = new Timestamp(DATE_FORMAT.parse(
						opportunity.getOpportunityExpDate()).getTime());

			long t = opp_start_date.getTime();
			long m = acceptSLABy * 60 * 60 * 1000;

			new_opp_expiry_date = new Timestamp(t + m);

		} catch (Exception e) {
			LOGGER.error("Error while creating TimeStamp from java.util.Date");
			throw new AppException(0, 0,
					"Error while creating TimeStamp from java.util.Date",
					"Error while creating TimeStamp from java.util.Date", "");

		}
		try {
			// add customer
			long customerID = saveCustomer(opportunity.getCustomerDetails());

			// add address
			saveAddress(opportunity.getCustomerDetails().getAddress(),
					customerID);

			// add opportunity
			opportunityID = saveOpportunity(opportunity, opp_start_date,
					new_opp_expiry_date, customerID);

			// add bom
			int serialNo = 0;
			for (BOM bom : opportunity.getBoms()) {
				serialNo++;
				saveBOM(bom, opportunityID, serialNo);
			}

			// add questionnaire

			if (opportunity.getQuestions() != null) {
				saveQuestions(opportunity.getQuestions(), opportunityID);
			}
			
			//save record in sla table
			saveSLA(opportunityID);
			
			
			//save opportunity Images
			
			/*if (opportunity.getOppConsumerImages() != null) 
			{
				//saveQuestions(opportunity.getQuestions(), opportunityID);
				
			} */
			

			//transactionManager.commit(transStatus);

		} catch (Exception e) {
			e.printStackTrace();
			//transactionManager.rollback(transStatus);
			throw new AppException(
					Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), 500,
					"Error While Saving Opportunity", "", null);
		}


		return opportunityID;
	}

	private void saveSLA(final long opportunityID) {
		
		final String INSERT_SLA_QUERY = "INSERT INTO opportunitysla(OPPSLA_OPPID) "
				+ "VALUES(?)";
		int out = jdbcTemplate.update(INSERT_SLA_QUERY,
				new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps)
							throws SQLException {
						ps.setLong(1, opportunityID);
					}
				});
		if (out != 0) {
			LOGGER.debug("SLA Record saved");
		} else {
			LOGGER.error("Failed saving SLA record for oppId :"+opportunityID+" this will have problem in sending notifications");
			
		}
		
	}

	private void saveQuestions(List<Question> questions,
			final long opportunityID) throws AppException {

		if (questions != null) {

			for (final Question question : questions) {

				final String INSERT_BOM_QUERY = "INSERT INTO opportunityquestions(OPP_ID, Q_ID,Q_QID, Q_DESC, Q_ANSWER, Q_ANSWERT_TYP) "
						+ "VALUES(?,?,?,?,?,?)";
				int out = jdbcTemplate.update(INSERT_BOM_QUERY,
						new PreparedStatementSetter() {
							@Override
							public void setValues(PreparedStatement ps)
									throws SQLException {
								ps.setLong(1, opportunityID);
								ps.setString(2, question.getId());
								ps.setString(3, question.getQid());
								ps.setString(4, "questionDesc");
								ps.setString(5, question.getValue());
								ps.setString(6, "T");

							}
						});

				if (out != 0) {
					LOGGER.debug("Question saved");
				} else {
					LOGGER.error("Question save failed");
					throw new AppException(
							Response.Status.NO_CONTENT.getStatusCode(), 204,
							"Question save failed", "", null);
				}

			}

		}

	}

	private long saveOpportunity(final Opportunity opportunity,
			final Timestamp opp_start_date, final Timestamp opp_expiry_date,
			final long customerID) throws AppException {

		long generatedId = 0L;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final String INSERT_OPPORTUNITY_QUERY = "INSERT INTO opportunity(OPP_Upstream_ID,OPP_Name,OPP_Short_Desc,OPP_Desc,OPP_Consumer_ID,"
				+ "OPP_BFO_ID,OPP_Address,OPP_Locality,OPP_City,OPP_State,OPP_ZipCode,OPP_Cntry_Cd,OPP_Phone,OPP_Fax,OPP_Status,OPP_Last_UpdDt,"
				+ "OPP_Attachment,OPP_Budget,OPP_Eff_FmDt,OPP_Eff_ToDt,OPP_New_Notification,OPP_SLA_Notification,OPP_Buss_Type,OPP_Source_Id,OPP_Source_type,OPP_Time_Zone) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int out = jdbcTemplate.update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						INSERT_OPPORTUNITY_QUERY, new String[] { "OPP_ID" });
				ps.setInt(1, 121); // this is same as BFO id , we can remove
									// later
				ps.setString(2, opportunity.getTitle());
				ps.setString(3, opportunity.getTitle()); // title is good for
															// now
				ps.setString(4, opportunity.getDescription());
				ps.setLong(5, customerID); // this id will be updated from SLA
											// monitor JOB once we get response
											// from BFO
				ps.setString(6, null); // this id will be updated from SLA
										// monitor JOB once we get response from
										// BFO
				ps.setString(7, opportunity.getAddress().getSt_name()); // OPP_Address
																		// --street
																		// name
																		// from
																		// address
																		// object
				ps.setString(8, null); // OPP_Locality -- leav it for now
				ps.setString(9, opportunity.getAddress().getCity());
				ps.setString(10, opportunity.getAddress().getSt_name());
				ps.setString(11, opportunity.getAddress().getZip());
				ps.setString(12, opportunity.getAddress().getCountryCode());

				ps.setString(13, opportunity.getPhone()); // OPP_Phone -- for
															// now we can use
															// the phone number
															// from customer
				ps.setString(14, opportunity.getEmail()); // OPP_Fax
				ps.setString(15, OppStatus.LOADED.name());
				ps.setTimestamp(
						16,
						new Timestamp(
								getTimeForTZ(
										opportunity.getSource().getTimeZone() != null ? opportunity
												.getSource().getTimeZone()
												: "CET").getTime()));
				ps.setString(17, opportunity.getBmPDF()); // OPP_Attachment
				ps.setDouble(18, Double.parseDouble(opportunity.getBudget()));
				ps.setTimestamp(19, opp_start_date);
				ps.setTimestamp(20, opp_expiry_date);
				ps.setInt(21, 0);
				//ps.setString(22, "0|0|0");
				ps.setString(22, "0|0|0|0");
				ps.setString(23, opportunity.getBusinessType());

				if (opportunity.getSource() != null) {
					ps.setString(
							24,
							(opportunity.getSource().getId() != null ? opportunity
									.getSource().getId() : ""));
					ps.setString(
							25,
							(opportunity.getSource().getType() != null ? opportunity
									.getSource().getType() : ""));
				} else {
					ps.setString(24, "CL");
					ps.setString(25, "CL");
				}
				ps.setString(
						26,
						opportunity.getSource().getTimeZone() != null ? opportunity
								.getSource().getTimeZone() : "CET");

				return ps;
			}
		}, keyHolder);

		if (out != 0) {
			generatedId = keyHolder.getKey().longValue();
			LOGGER.debug("Opportunity saved with id={}", generatedId);
		} else {
			LOGGER.error("Opportunity save failed");
			throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
					204, "Opportunity save failed ", "", null);
		}

		return generatedId;
	}

	private void saveBOM(final BOM bom, final long opportunityID,
			final int serialNo) throws AppException {

		final String INSERT_BOM_QUERY = "INSERT INTO opportunitybom(OBOM_OPP_ID,OBOM_SlNo,OBOM_Category,OBOM_Prod_ID,OBOM_Name,OBOM_Desc,OBOM_Colour,OBOM_Std_Incl_Qty,OBOM_Var_Qty,OBOM_Tot_Qty,OBOM_Unit_Cost,OBOM_Tot_Cost,OBOM_Currency_Cd,OBOM_Discount) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int out = jdbcTemplate.update(INSERT_BOM_QUERY,
				new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps)
							throws SQLException {
						ps.setLong(1, opportunityID);
						ps.setInt(2, serialNo); // OBOM_SlNo
						ps.setString(3, bom.getCategory());
						ps.setString(4, bom.getProductId());
						ps.setString(5, bom.getProductName());
						ps.setString(6, bom.getDescription());
						ps.setString(7, bom.getColor());
						ps.setFloat(8, 0);// not used need to remove in next
											// version
						ps.setFloat(9, 0);// not used need to remove in next
											// version
						ps.setFloat(10, bom.getQuantity());
						ps.setFloat(11, 0); // not used need to remove in next
											// version
						ps.setFloat(12, bom.getUnitCost()); // unit cost is
															// total cost
						ps.setString(13, bom.getCurrencyCode()); // Currency
																	// Code
						ps.setString(14, bom.getDiscount()); // discount
						
					}
				});

		if (out != 0) {
			LOGGER.debug("BOM saved");
		} else {
			LOGGER.error("BOM save failed");
			throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
					204, "BOM save failed ", "", null);
		}

	}

	private long saveCustomer(final CustomerDetails customerDetails)
			throws AppException {

		long generatedId = 0L;
		String query = "SELECT cust_id FROM customer WHERE cust_email =:cust_email";
		LOGGER.debug("SQL :" + query);
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cust_email", customerDetails.getEmail());

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		boolean allreject = false;
		for (Map<String, Object> row : rows) {

			generatedId = Long.valueOf(row.get("cust_id").toString());
		}

		// if customer exists don't insert new rec
		if (generatedId == 0L) 
		{
			KeyHolder keyHolder = new GeneratedKeyHolder();
			final String INSERT_CUSTOMER_QUERY = "INSERT INTO customer(cust_first_name,cust_last_name, cust_phone, cust_email) VALUES(?, ?, ?,?)";

			int out = jdbcTemplate.update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(
							INSERT_CUSTOMER_QUERY, new String[] { "cust_id" });
					ps.setString(1, customerDetails.getFirstName());
					ps.setString(2, customerDetails.getLastName());
					ps.setString(3, customerDetails.getPhone());
					ps.setString(4, customerDetails.getEmail());

					return ps;
				}
			}, keyHolder);

			if (out != 0) {
				generatedId = keyHolder.getKey().longValue();
				LOGGER.debug("Customer saved with id={}", generatedId);
			} else {
				LOGGER.error("Customer save failed");
				throw new AppException(
						Response.Status.NO_CONTENT.getStatusCode(), 204,
						"Customer save failed ", "", null);
			}

		}else
		{
			//10AUGUST2015 Added code to update the customer details
			
			LOGGER.debug("inside update block...for customer...");
			String update_query = "update customer set cust_first_name=?,cust_last_name=?, cust_phone=?  where cust_email=?";
			LOGGER.debug("SQL update_query :" + update_query);
			Object[] args = new Object[] { customerDetails.getFirstName(), customerDetails.getLastName(), customerDetails.getPhone(), customerDetails.getEmail()};
			int out = jdbcTemplate.update(update_query, args);
			if (out != 0) {
				LOGGER.debug("Customer update successfully");
			} else {
				LOGGER.error("Customer update failed");
				throw new AppException(
						Response.Status.NO_CONTENT.getStatusCode(), 204,
						"Customer update failed ", "", null);
			}
			
		}

		return generatedId;
	}

	private void saveAddress(final Address address, final long customerId)
			throws AppException {

		final String INSERT_BOM_QUERY = "INSERT INTO address (addr_customer_id, addr_primary, addr_address_line1, addr_address_line2, addr_street_name, addr_city, addr_zip,addr_cntry_cd, addr_latitude, addr_longitude) "
				+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

		int out = jdbcTemplate.update(INSERT_BOM_QUERY,
				new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps)
							throws SQLException {
						ps.setLong(1, customerId);
						ps.setInt(2, 1); // 2 <{addr_primary: 0}>,
						ps.setString(3, null); // 3 <{addr_address_line1: }>,
						ps.setString(4, null); // 4 <{addr_address_line2: }>,
						ps.setString(5, address.getSt_name());
						ps.setString(6, address.getCity());
						ps.setString(7, address.getZip());
						ps.setString(8, address.getCountryCode());
						ps.setDouble(
								9,
								(address.getLatitude() != null) ? Double
										.parseDouble(address.getLatitude())
										: Double.valueOf(0));
						ps.setDouble(
								10,
								(address.getLongitude() != null) ? Double
										.parseDouble(address.getLongitude())
										: Double.valueOf(0));
					}
				});

		if (out != 0) {
			LOGGER.debug("Address saved");
		} else {
			LOGGER.error("Address save failed");
			throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
					204, "Address save failed ", "", null);
		}

	}

	@Override
	public Opportunity loadOpportunityById(long opportunityId)
			throws AppException {

		Opportunity opportunity = getOppotunityById(opportunityId);

		opportunity.setBoms(getBoms(opportunityId));
		CustomerDetails customerDetails = getcustomerDetails(opportunity
				.getCustomerDetails().getId());
		Address address = getAddress(customerDetails.getId());
		customerDetails.setAddress(address);
		opportunity.setCustomerDetails(customerDetails);
		opportunity.setAddress(address);

		return opportunity;
	}

	private Opportunity getOppotunityById(long opportunityId)
			throws AppException {

		final String SELECT_OPPOTUNITY_QUERY = "SELECT OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Short_Desc, OPP_Desc, OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State, OPP_ZipCode, "
				+ "OPP_Phone, OPP_Fax, OPP_Status, OPP_Last_UpdDt, OPP_Attachment, OPP_Budget, OPP_Eff_FmDt, OPP_Eff_ToDt FROM opportunity WHERE OPP_ID = :OPPORTUNITY_ID";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPPORTUNITY_ID", opportunityId);

		Opportunity opportunity = namedParameterJdbcTemplate.queryForObject(
				SELECT_OPPOTUNITY_QUERY, paramMap,
				new RowMapper<Opportunity>() {

					@Override
					public Opportunity mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Opportunity opp = new Opportunity();

						opp.setId(rs.getString("OPP_ID"));
						opp.setTitle(rs.getString("OPP_Name"));
						opp.setShortDesc(rs.getString("OPP_Name"));
						opp.setBudget(rs.getString("OPP_Budget"));
						opp.setOpportunityDate(rs.getString("OPP_Eff_FmDt"));
						opp.setOpportunityExpDate(rs.getString("OPP_Eff_ToDt"));
						opp.setDescription(rs.getString("OPP_Desc"));
						opp.setBmPDF(rs.getString("OPP_Attachment"));

						CustomerDetails customer = new CustomerDetails();
						customer.setId(rs.getLong("OPP_Consumer_ID"));

						opp.setCustomerDetails(customer);

						return opp;
					}

				});

		return opportunity;
	}

	@Override
	public List<BOM> getBoms(long opportunityId) {

		final String SELECT_BOMS_QUERY = "SELECT OBOM_OPP_ID, OBOM_SlNo, OBOM_Category, OBOM_Prod_ID, OBOM_Name, OBOM_Desc, OBOM_Colour, OBOM_Std_Incl_Qty, OBOM_Var_Qty, OBOM_Tot_Qty, OBOM_Unit_Cost, "
				+ "OBOM_Tot_Cost,OBOM_Discount FROM opportunitybom WHERE OBOM_OPP_ID = :OPPORTUNITY_ID";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPPORTUNITY_ID", opportunityId);

		List<BOM> bomList = new ArrayList<BOM>();
		BOM bom = null;

		List<Map<String, Object>> bomRows = namedParameterJdbcTemplate
				.queryForList(SELECT_BOMS_QUERY, paramMap);
		for (Map<String, Object> row : bomRows) {

			bom = new BOM();
			bom.setCategory(String.valueOf(row.get("OBOM_Category")));
			bom.setProductId(String.valueOf(row.get("OBOM_Prod_ID")));
			bom.setProductName(String.valueOf(row.get("OBOM_Name")));
			bom.setDescription(String.valueOf(row.get("OBOM_Desc")));
			bom.setColor(String.valueOf(row.get("OBOM_Colour")));
			// bom.setStdInclQty(String.valueOf(row.get("OBOM_Std_Incl_Qty")));
			// bom.setVarQty(Float.valueOf(String.valueOf(row.get("OBOM_Var_Qty"))));
			bom.setQuantity(Integer.valueOf(String.valueOf(row
					.get("OBOM_Tot_Qty"))));
			bom.setUnitCost(Float.valueOf(String.valueOf(row
					.get("OBOM_Tot_Cost"))));

			bom.setDiscount(String.valueOf(row.get("OBOM_Discount")));
			
			bomList.add(bom);
			
			
		}

		return bomList;
	}

	private CustomerDetails getcustomerDetails(long customerId) {

		final String SELECT_CUSTOMER_QUERY = "SELECT cust_id, cust_first_name,cust_last_name, cust_phone, cust_email FROM customer WHERE cust_id = :CUSTOMER_ID  LIMIT 1";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("CUSTOMER_ID", customerId);

		CustomerDetails customerDetails = namedParameterJdbcTemplate
				.queryForObject(SELECT_CUSTOMER_QUERY, paramMap,
						new RowMapper<CustomerDetails>() {

							@Override
							public CustomerDetails mapRow(ResultSet rs,
									int rowNum) throws SQLException {
								CustomerDetails customer = new CustomerDetails();
								customer.setId(rs.getLong("cust_id"));
								customer.setFirstName(rs
										.getString("cust_first_name"));
								customer.setLastName(rs
										.getString("cust_last_name"));
								customer.setPhone(rs.getString("cust_phone"));
								customer.setEmail(rs.getString("cust_email"));

								return customer;
							}

						});

		return customerDetails;
	}

	private Address getAddress(long customerId) {

		final String SELECT_ADDRESS_QUERY = "SELECT addr_id, addr_customer_id, addr_primary, addr_address_line1, addr_address_line2, "
				+ "addr_street_name, addr_city, addr_zip, addr_latitude, addr_longitude FROM address WHERE addr_customer_id = :CUSTOMER_ID  LIMIT 1";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("CUSTOMER_ID", customerId);

		Address address = namedParameterJdbcTemplate.queryForObject(
				SELECT_ADDRESS_QUERY, paramMap, new RowMapper<Address>() {

					@Override
					public Address mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Address addr = new Address();

						addr.setSt_name(rs.getString("addr_street_name"));
						addr.setCity(rs.getString("addr_city"));
						addr.setZip(rs.getString("addr_zip"));
						addr.setLatitude(rs.getString("addr_latitude"));
						addr.setLongitude(rs.getString("addr_longitude"));

						return addr;
					}

				});

		return address;
	}

	@Override
	public HashMap<String, String> getOpportunityIdAndZip(OppStatus status,
			String country) {

		HashMap<String, String> opportunityMap = new HashMap<String, String>();
		// String query =
		// "select OPP_ID,OPP_ZipCode,OPP_Cntry_Cd from opportunity  where OPP_Status ="+"'"+status.name()+"' AND OPP_Source_Id ='CL'  AND OPP_Source_type ='CL' AND OPP_Cntry_Cd='"+country+"'"
		// ;
		String query = "select OPP_ID,OPP_ZipCode,OPP_Cntry_Cd from opportunity  where OPP_Status ="
				+ "'"
				+ status.name()
				+ "' AND OPP_Source_Id ='CL'   AND OPP_Cntry_Cd='"
				+ country
				+ "'";

		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		for (Map<String, Object> row : rows) {
			opportunityMap.put(
					String.valueOf(row.get("OPP_ID")),
					String.valueOf(row.get("OPP_Cntry_Cd")) + ","
							+ String.valueOf(row.get("OPP_ZipCode")));

		}
		return opportunityMap;
	}

	
	//@smarni .. this method needs serious re factoring 
	@Override
	public void updateOppHistory(final String partnerId, final String oppId,
			final String statusId, final String status, final String reason,
			final String startTime, final String endTime, String type,
			final String source) throws AppException {

		// store history

		// get history id from oop partner link table
		String oppStatus = "";
		String query = "SELECT SHOL_Status FROM stakeholderopplink WHERE SHOL_OPP_ID =:SHOL_OPP_ID AND SHOL_SH_ID=:SHOL_SH_ID";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		// SHOL_SH_ID --partner id
		paramMap.put("SHOL_OPP_ID", oppId);
		paramMap.put("SHOL_SH_ID", partnerId);

		// SHOL_ID-- auto id in partner opp link table refference to history
		// table
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		for (Map<String, Object> row : rows) {
			oppStatus = String.valueOf(row.get("SHOL_Status"));
		}

		if (oppStatus == null
				|| !oppStatus.equalsIgnoreCase(OppStatus.ACCEPTED.name())) {
			throw new AppException(Response.Status.CONFLICT.getStatusCode(),
					409,
					"Opportunity Should be Accept Before updating the Status",
					"", null);

		}

		// final String oppHistIdfinal=oppHistId;

		java.util.Date dt = new java.util.Date();

		// String currentTime = sdf.format(dt);
		// DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		java.util.Date date = null;
		java.util.Date date1 = null;
		boolean exception = false;
		try {
			date = (java.util.Date) DATE_FORMAT.parse(startTime);
			date1 = (java.util.Date) DATE_FORMAT.parse(endTime);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			exception = true;
		}
		Timestamp starttimeConv = new Timestamp(dt.getTime());
		Timestamp endtimeConv = new Timestamp(dt.getTime());
		if (!exception) {
			starttimeConv = new Timestamp(date.getTime());
			endtimeConv = new Timestamp(date1.getTime());

		}
		final Timestamp starttimeConvfinal = starttimeConv;
		final Timestamp endtimeConvfinal = endtimeConv;

		// final Timestamp timestamp = new Timestamp(dt.getTime());

		if (type.equalsIgnoreCase("status")) {
			
			if(source.equalsIgnoreCase("SOL30")){
				
				 query = "SELECT SHOLH_SH_ID FROM stakeholderopplinkhist WHERE SHOLH_OPP_ID =:SHOLH_OPP_ID AND SHOLH_SH_ID=:SHOLH_SH_ID AND SHOLH_Status_Id=:SHOLH_Status_Id";

				 paramMap = new HashMap<String, Object>();
				// SHOL_SH_ID --partner id
				paramMap.put("SHOLH_OPP_ID", oppId);
				paramMap.put("SHOLH_SH_ID", partnerId);
				paramMap.put("SHOLH_Status_Id",statusId);

				// SHOL_ID-- auto id in partner opp link table refference to history
				// table
				rows = namedParameterJdbcTemplate
						.queryForList(query, paramMap);
				String histFound=null;
				for (Map<String, Object> row : rows) {
					histFound = String.valueOf(row.get("SHOLH_SH_ID"));
					LOGGER.debug("history exists");
				}
				if(histFound==null){
					jdbcTemplate
					.update("insert into  stakeholderopplinkhist(SHOLH_SH_ID, SHOLH_OPP_ID, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,SHOLH_UPDATE_BFO,SHOLH_Status_Id,SHOLH_Remarks,SHOL_Source) "
							+ "values (?, ?, ?,?,?,?,?,?,?) ",
							new PreparedStatementSetter() {
								public void setValues(PreparedStatement ps)
										throws SQLException {
									ps.setString(1, partnerId);
									ps.setString(2, oppId);
									ps.setString(3, status);
									ps.setTimestamp(4, starttimeConvfinal);
									ps.setTimestamp(5, endtimeConvfinal);
									ps.setInt(6, 0);// set 0 at the initial save
													// , once update sent to BFO
													// .. set to 1
									ps.setString(7, statusId);
									ps.setString(8, reason);
									ps.setString(9, source);
								}
							});
				}
				
				
			}else{
			jdbcTemplate
					.update("insert into  stakeholderopplinkhist(SHOLH_SH_ID, SHOLH_OPP_ID, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,SHOLH_UPDATE_BFO,SHOLH_Status_Id,SHOLH_Remarks,SHOL_Source) "
							+ "values (?, ?, ?,?,?,?,?,?,?) ",
							new PreparedStatementSetter() {
								public void setValues(PreparedStatement ps)
										throws SQLException {
									ps.setString(1, partnerId);
									ps.setString(2, oppId);
									ps.setString(3, status);
									ps.setTimestamp(4, starttimeConvfinal);
									ps.setTimestamp(5, endtimeConvfinal);
									ps.setInt(6, 0);// set 0 at the initial save
													// , once update sent to BFO
													// .. set to 1
									ps.setString(7, statusId);
									ps.setString(8, reason);
									ps.setString(9, source);
								}
							});
			}

		} else {

			jdbcTemplate
					.update("insert into  stakeholdereventhist(SHOLH_SH_ID, SHOLH_OPP_ID, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,SHOLH_UPDATE_BFO,SHOLH_Status_Id,SHOLH_Remarks) "
							+ "values (?, ?, ?,?,?,?,?,?) ",
							new PreparedStatementSetter() {
								public void setValues(PreparedStatement ps)
										throws SQLException {
									ps.setString(1, partnerId);
									ps.setString(2, oppId);
									ps.setString(3, status);
									ps.setTimestamp(4, starttimeConvfinal);
									ps.setTimestamp(5, endtimeConvfinal);
									ps.setInt(6, 0);// set 0 at the initial save
													// , once update sent to BFO
													// .. set to 1
									ps.setString(7, statusId);
									ps.setString(8, reason);
								}
							});

		}

		// return null;

	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public Opportunity updateOppStatus(String partnerId, String oppId,
			String status,boolean isAdminAssigned) throws AppException {

		/*TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus transStatus = transactionManager.getTransaction(def);*/

		String query = "SELECT OPP_Status,OPP_Name,OPP_Budget,OPP_ZipCode,cust_first_name,cust_email,OPP_Cntry_Cd from opportunity o ,customer c WHERE o.OPP_Consumer_ID=c.cust_id AND OPP_ID =:OPP_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String CurrStatusId = "";
		Opportunity opp = new Opportunity();
		for (Map<String, Object> row : rows) {
			CurrStatusId = ((String.valueOf(row.get("OPP_Status"))));
			opp.setStatus(CurrStatusId);

			Address address = new Address();
			address.setZip(String.valueOf(row.get("OPP_ZipCode")));
			address.setCountryCode(String.valueOf(row.get("OPP_Cntry_Cd")));
			opp.setAddress(address);
			opp.setBudget(String.valueOf(row.get("OPP_Budget")));
			opp.setTitle(String.valueOf(row.get("OPP_Name")));

			CustomerDetails cust = new CustomerDetails();
			cust.setFirstName(String.valueOf(row.get("cust_first_name")));
			cust.setEmail(String.valueOf(row.get("cust_email")));
			opp.setCustomerDetails(cust);

		}

		if (status.equalsIgnoreCase(OppStatus.ACCEPTED.name())
				&& !CurrStatusId.equalsIgnoreCase(OppStatus.INITIAL.name())) {

			throw new AppException(Response.Status.CONFLICT.getStatusCode(),
					409, "Opportunity Already Accepted By Other Partner ", "",
					null);

		} else {

			if (status.equalsIgnoreCase(OppStatus.ACCEPTED.name())) {

				try {
					
					LOGGER.info("isAdminAssigned"+isAdminAssigned);
					if(isAdminAssigned)
					{
						LOGGER.info("isAdminAssigned"+isAdminAssigned);
						assignOpportunityToPartner(Long.valueOf(oppId).longValue(),Integer.parseInt(partnerId));
						
					}					
					
					query = "update stakeholderopplink set SHOL_Status=? where SHOL_OPP_ID =? and SHOL_SH_ID=?";
					LOGGER.debug("SQL :" + query);
					Object[] args = new Object[] { status, oppId, partnerId };
					int out = jdbcTemplate.update(query, args);

					java.util.Date dt = new java.util.Date();
					// need to fix the service to send timezone
					final Timestamp timestamp = new Timestamp(
							getTimeForTZ(null).getTime());

					query = "update opportunity set OPP_Status=?,OPP_Last_UpdDt=? where OPP_ID=?";
					args = new Object[] { status, timestamp, oppId };
					out = jdbcTemplate.update(query, args);

					// Revoke opportunity from other partners

					// for france/sweden  do not revoke opportunity , batch job will
					// Remove after 48 hrs

					if ((!opp.getAddress().getCountryCode().equalsIgnoreCase(CountryCode.FRANCE.getValue())) && (!opp.getAddress().getCountryCode().equalsIgnoreCase(CountryCode.SWEDEN.getValue()))) 
					{

						query = "delete from stakeholderopplink where  SHOL_OPP_ID =? and SHOL_SH_ID<>? and SHOL_Status <>'DECLINED'";
						args = new Object[] { oppId, partnerId };
						out = jdbcTemplate.update(query, args);
					}else if ((opp.getAddress().getCountryCode().equalsIgnoreCase(CountryCode.FRANCE.getValue())) && (isAdminAssigned==true)) 
					{
						
						query = "delete from stakeholderopplink where  SHOL_OPP_ID =? and SHOL_SH_ID<>? and SHOL_Status <>'DECLINED'";
						args = new Object[] { oppId, partnerId };
						out = jdbcTemplate.update(query, args);
						
					}

					// get partner object

					query = "SELECT SH_Full_Name from stakeholders WHERE SH_ID =:SH_ID ";
					LOGGER.debug("SQL :" + query);
					paramMap = new HashMap<String, Object>();
					paramMap.put("SH_ID", partnerId);

					rows = namedParameterJdbcTemplate.queryForList(query,
							paramMap);

					Partner partner = new Partner();
					for (Map<String, Object> row : rows) {
						partner.setName(String.valueOf(row.get("SH_Full_Name")));
					}
					if (partner != null) {
						opp.setPartner(partner);
					}
					//transactionManager.commit(transStatus);

					// send email to consumer

				} catch (Exception e) {
					e.printStackTrace();
					//transactionManager.rollback(transStatus);
					throw new AppException(
							Response.Status.INTERNAL_SERVER_ERROR
									.getStatusCode(),
							500, "Problem in accepting the opportunity", "",
							null);
				}

			}// in case of rejecting one of the customer ??
			else {

				try {
					query = "update stakeholderopplink set SHOL_Status=? where SHOL_OPP_ID =? and SHOL_SH_ID=?";
					LOGGER.debug("SQL :" + query);
					Object[] args = new Object[] { status, oppId, partnerId };
					int out = jdbcTemplate.update(query, args);

					java.util.Date dt = new java.util.Date();

					final Timestamp timestamp = new Timestamp(
							getTimeForTZ(null).getTime());

					/*
					 * query =
					 * "update opportunity set OPP_Status=?,OPP_Last_UpdDt=? where OPP_ID=?"
					 * ; args = new Object[] {status,timestamp,oppId}; out =
					 * jdbcTemplate.update(query, args);
					 * 
					 * //revoke opportunity from other partners
					 * 
					 * query =
					 * "delete from stakeholderopplink where  SHOL_OPP_ID =? and SHOL_SH_ID<>?"
					 * ;
					 * 
					 * args = new Object[] {oppId, partnerId}; out =
					 * jdbcTemplate.update(query, args);
					 */

					//transactionManager.commit(transStatus);
				} catch (Exception e) {
					e.printStackTrace();
					//transactionManager.rollback(transStatus);
					throw new AppException(
							Response.Status.INTERNAL_SERVER_ERROR
									.getStatusCode(),
							500,
							"Problem in accepting/rejecting the  the opportunity",
							"", null);
				}

			}

		}

		return opp;

	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateOppStatusSystem(String oppId, String status,
			String statusRemarks) {

		//TransactionDefinition def = new DefaultTransactionDefinition();
		//TransactionStatus transStatus = transactionManager.getTransaction(def);

		try {
			String query = "update opportunity set OPP_Status=?,OPP_Status_Remarks=? where OPP_ID=?";
			Object[] args = new Object[] { status, statusRemarks, oppId };
			int out = jdbcTemplate.update(query, args);

			//transactionManager.commit(transStatus);
		} catch (Exception e) {
			e.printStackTrace();
			//transactionManager.rollback(transStatus);

		}

	}

	/**
	 * to update read status
	 */

	@Override
	public void updateOppReadStatus(String oppId, String partnerId, int status)
	{

		try {
			String query = "update stakeholderopplink set OPP_Read_Status=? where SHOL_OPP_ID=? and SHOL_SH_ID=?";
			Object[] args = new Object[] { status, oppId, partnerId };
			int out = jdbcTemplate.update(query, args);
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Override
	public List<PartnerDevice> listPartnerDevice(String deviceType)
			throws AppException {

		final String SELECT_PARTNER_DEVICES_QUERY = "SELECT SHD_ID, SHD_SH_ID, SHD_Device_ID, SHD_Device_Type, SHD_Mobile_No, SHD_Eff_FmDt, "
				+ "SHD_Eff_ToDt FROM stakeholdersdevices WHERE SHD_Device_Type = :PARTNER_DEVICE_TYPE";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("PARTNER_DEVICE_TYPE", deviceType);

		List<PartnerDevice> deviceList = new ArrayList<PartnerDevice>();
		PartnerDevice device = null;

		List<Map<String, Object>> deviceRows = namedParameterJdbcTemplate
				.queryForList(SELECT_PARTNER_DEVICES_QUERY, paramMap);
		for (Map<String, Object> row : deviceRows) {

			device = new PartnerDevice();
			device.setPartnerId(Integer.parseInt(String.valueOf(row
					.get("SHD_SH_ID"))));
			// device.setUserId(String.valueOf(row.get("SHD_SH_ID")));
			device.setDeviceId(String.valueOf(row.get("SHD_Device_ID")));
			device.setMobileNumber(String.valueOf(row.get("SHD_Mobile_No")));
			device.setDeviceType(String.valueOf(row.get("SHD_Device_Type")));
			device.setEffectFromDate(Timestamp.valueOf(String.valueOf(row
					.get("SHD_Eff_FmDt"))));
			device.setEffectToDate(Timestamp.valueOf(String.valueOf(row
					.get("SHD_Eff_ToDt"))));

			deviceList.add(device);
		}

		return deviceList;
	}

	@Override
	public void updateOppNotiStatus(List<String> oppIds, String noteType,
			int slaStep) {
		// TODO Auto-generated method stub

		if (noteType.equalsIgnoreCase("SLA")) {
			for (String oppId : oppIds) {
				String query = "";
				if (slaStep == 0) {
					query = "update opportunity set OPP_New_Notification=? where OPP_ID=?";
					LOGGER.debug("SQL :" + query);
					Object[] args = new Object[] { "1", oppId };
					int out = jdbcTemplate.update(query, args);
					System.out.println("success :" + out);

				} else {
					query = "update opportunity set OPP_SLA_Notification=? where OPP_ID=?";
					LOGGER.debug("SQL :" + query);
					Object[] args = null;
					
					/*
					if (slaStep == 1)
					{
						args = new Object[] { "1|0|0", oppId };
					} else if (slaStep == 2) {
						args = new Object[] { "1|1|0", oppId };
					} else if (slaStep == 3) {
						args = new Object[] { "1|1|1", oppId };
					}*/
					
					if (slaStep == 1)
					{
						args = new Object[] { "1|0|0|0", oppId };
					} else if (slaStep == 2) {
						args = new Object[] { "1|1|0|0", oppId };
					} else if (slaStep == 3) {
						args = new Object[] { "1|1|1|0", oppId };
					}else if (slaStep == 4) {
						args = new Object[] { "1|1|1|1", oppId };
					}
					
					int out = jdbcTemplate.update(query, args);
					LOGGER.info("updateOppNotiStatus:::slaStep :" + slaStep);
					LOGGER.info("updateOppNotiStatus:::success :" + out);

				}

			}
		} else if (noteType.equalsIgnoreCase("NEWOPP")) {
			for (String oppId : oppIds) {
				String query = "update opportunity set OPP_New_Notification=? where OPP_ID=?";

				LOGGER.debug("SQL :" + query);
				Object[] args = new Object[] { 1, oppId };
				int out = jdbcTemplate.update(query, args);
				System.out.println("success :" + out);
			}
		}

	}

	@Override
	public List<String> getOppsRejectedByAllPatners() {
		// TODO Auto-generated method stub

		String query = "SELECT OPP_ID from opportunity WHERE OPP_Status =:OPP_Status ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_Status", OppStatus.INITIAL.name());

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		List<String> oppIds = new ArrayList<String>();

		for (Map<String, Object> row : rows) {

			oppIds.add(String.valueOf(row.get("OPP_ID")));
			// CurrStatusId=((String.valueOf(row.get("OPP_Status"))));
		}

		List<String> rejectedOps = new ArrayList<String>();
		if (oppIds != null & oppIds.size() > 0) {

			for (String oppId : oppIds) {
				query = "SELECT SHOL_Status FROM stakeholderopplink WHERE SHOL_OPP_ID =:SHOL_OPP_ID";
				LOGGER.debug("SQL :" + query);
				paramMap = new HashMap<String, Object>();
				paramMap.put("SHOL_SH_ID", oppId);

				rows = namedParameterJdbcTemplate.queryForList(query, paramMap);

				List<String> opportunityIds = new ArrayList<String>();
				boolean allreject = false;
				for (Map<String, Object> row : rows) {
					if (String.valueOf(row.get("SHOL_Status"))
							.equalsIgnoreCase(OppStatus.DECLINED.name())) {
						allreject = true;
					} else {
						allreject = false;
						break;

					}
				}
				if (allreject) {
					rejectedOps.add(oppId);
				}

			}

		}
		return rejectedOps;
	}

	@Override
	public Opportunity getOportunity(String partnerId, String oppId) {
		// TODO Auto-generated method stub

		String query = "SELECT OPP_Status,OPP_City,OPP_Address,OPP_Eff_FmDt,OPP_Name,OPP_Budget,OPP_ZipCode,cust_first_name,cust_email,OPP_Attachment,cust_phone from opportunity o ,customer c WHERE o.OPP_Consumer_ID=c.cust_id AND OPP_ID =:OPP_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String CurrStatusId = "";
		Opportunity opp = new Opportunity();
		for (Map<String, Object> row : rows) {
			CurrStatusId = ((String.valueOf(row.get("OPP_Status"))));
			opp.setId(oppId);
			opp.setStatus(CurrStatusId);
			opp.setOpportunityDate(String.valueOf(row.get("OPP_Eff_FmDt")));
			opp.setBmPDF(String.valueOf(row.get("OPP_Attachment")));
			Address address = new Address();
			address.setZip(String.valueOf(row.get("OPP_ZipCode")));
			address.setCity(String.valueOf(row.get("OPP_City")));
			address.setSt_name(String.valueOf(row.get("OPP_Address")));
			opp.setAddress(address);
			opp.setBudget(String.valueOf(row.get("OPP_Budget")));
			opp.setTitle(String.valueOf(row.get("OPP_Name")));

			CustomerDetails cust = new CustomerDetails();
			cust.setFirstName(String.valueOf(row.get("cust_first_name")));
			cust.setEmail(String.valueOf(row.get("cust_email")));
			cust.setPhone(String.valueOf(row.get("cust_phone")));
			opp.setCustomerDetails(cust);

		}

		query = "SELECT SH_Full_Name,SH_Email from stakeholders WHERE SH_ID =:SH_ID ";
		LOGGER.debug("SQL :" + query);
		paramMap = new HashMap<String, Object>();
		paramMap.put("SH_ID", partnerId);

		rows = namedParameterJdbcTemplate.queryForList(query, paramMap);

		Partner partner = new Partner();
		for (Map<String, Object> row : rows) {
			partner.setName(String.valueOf(row.get("SH_Full_Name")));
			partner.setEmail(String.valueOf(row.get("SH_Email")));
		}
		if (partner != null) {
			opp.setPartner(partner);
		}

		return opp;
	}

	@Override
	public Opportunity getOportunity(String oppId) {
		// TODO Auto-generated method stub

		String query = "SELECT OPP_Status,OPP_Name,OPP_Budget,OPP_ZipCode,cust_first_name,cust_last_name,cust_email,OPP_City from opportunity o ,customer c WHERE o.OPP_Consumer_ID=c.cust_id AND OPP_ID =:OPP_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String CurrStatusId = "";
		Opportunity opp = new Opportunity();
		for (Map<String, Object> row : rows) {
			CurrStatusId = ((String.valueOf(row.get("OPP_Status"))));
			opp.setId(oppId);
			opp.setStatus(CurrStatusId);

			Address address = new Address();
			address.setZip(String.valueOf(row.get("OPP_ZipCode")));
			address.setCity(String.valueOf(row.get("OPP_City")));
			opp.setAddress(address);
			opp.setBudget(String.valueOf(row.get("OPP_Budget")));
			opp.setTitle(String.valueOf(row.get("OPP_Name")));

			CustomerDetails cust = new CustomerDetails();
			cust.setFirstName(String.valueOf(row.get("cust_first_name")));
			cust.setLastName(String.valueOf(row.get("cust_last_name")));

			cust.setEmail(String.valueOf(row.get("cust_email")));
			opp.setCustomerDetails(cust);

		}

		return opp;
	}

	/**
	 * this method is for to set to PARKED
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateOppStatus(List<String> oppId, OppStatus statusVal)
			throws AppException {

		/*TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus transStatus = transactionManager.getTransaction(def);*/
		java.util.Date dt = new java.util.Date();
		final Timestamp timestamp = new Timestamp(getTimeForTZ(null).getTime());
		try {
			String query = "update opportunity set OPP_Status=:OPP_Status,OPP_Last_UpdDt=:OPP_Last_UpdDt where OPP_ID IN (:OPP_IDs)";
			Object[] args = new Object[] { statusVal.name(), timestamp, oppId };
			//int out = jdbcTemplate.update(query, args);
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("OPP_IDs", oppId);
			paramMap.put("OPP_Status", statusVal.name());
			paramMap.put("OPP_Last_UpdDt", timestamp);
			int out = namedParameterJdbcTemplate.update(query, paramMap);//  .update(query, args);

			// revoke opportunity from all partners

			query = "delete from stakeholderopplink where  SHOL_OPP_ID =? ";

			args = new Object[] { oppId };
			out = jdbcTemplate.update(query, args);

			//transactionManager.commit(transStatus);
		} catch (Exception e) {
			e.printStackTrace();
			//transactionManager.rollback(transStatus);
			throw new AppException(
					Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), 500,
					"Problem in updating status", "", null);
		}

	}

	/**
	 * temp delete funcationality later we need to remove this
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	public void deleteopportunities(List<String> userIds) {

		String query = "SELECT SH_ID from stakeholders WHERE SH_User_Name IN (:ids) ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("ids", userIds);
		// paramMap.put("productId", productId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		List<String> partnerIds = new ArrayList<String>();

		for (Map<String, Object> row : rows) {
			partnerIds.add(String.valueOf(row.get("SH_ID")));
		}

		query = "SELECT SHOL_OPP_ID from stakeholderopplink WHERE SHOL_SH_ID IN (:ids) ";
		paramMap.put("ids", partnerIds);
		rows = namedParameterJdbcTemplate.queryForList(query, paramMap);

		List<String> OppIds = new ArrayList<String>();
		for (Map<String, Object> row : rows) {
			OppIds.add(String.valueOf(row.get("SHOL_OPP_ID")));
		}

		//TransactionDefinition def = new DefaultTransactionDefinition();
		//TransactionStatus transStatus = transactionManager.getTransaction(def);

		try {

			Object[] args = null;
			int out = 0;
			for (String OppId : OppIds) {

				query = "delete from stakeholderopplink where  SHOL_OPP_ID=? ";

				args = new Object[] { OppId };
				out = jdbcTemplate.update(query, args);

				query = "delete from stakeholderopplinkhist where  SHOLH_OPP_ID=? ";

				args = new Object[] { OppId };
				out = jdbcTemplate.update(query, args);

				query = "delete from opportunitybom where  OBOM_OPP_ID =? ";

				args = new Object[] { OppId };
				out = jdbcTemplate.update(query, args);

				query = "delete from opportunity where  OPP_ID=? ";

				args = new Object[] { OppId };
				out = jdbcTemplate.update(query, args);

			}

			//transactionManager.commit(transStatus);

		} catch (Exception e) {
			e.printStackTrace();
			//transactionManager.rollback(transStatus);
		}

	}

	@Override
	public void assignOpportunityToPartner(final Long oppId, final int partnerId) {
		LOGGER.debug("assign partner in opp DAO");
		
		java.util.Date dt = new java.util.Date();
		final Timestamp timestamp = new Timestamp(getTimeForTZ(null).getTime());

		jdbcTemplate
				.update("insert into  stakeholderopplink(SHOL_OPP_ID, SHOL_SH_ID, SHOL_Remarks, SHOL_Status,SHOL_Eff_FmDt,SHOL_Eff_ToDt) "
						+ "values (?, ?, ?,?,?,?) ",
						new PreparedStatementSetter() {
							public void setValues(PreparedStatement ps)
									throws SQLException {
								ps.setLong(1, oppId);
								ps.setInt(2, partnerId);
								ps.setString(3, "Auto Matched");
								ps.setString(4, OppStatus.ACCEPTED.name());
								ps.setTimestamp(5, timestamp);
								ps.setTimestamp(6, timestamp);

							}
						});

		// update the opportunity status to INITIAL-- need to transaction
		// attributes later,bothe the above and the below should be in
		// transaction
		/*String query = "update opportunity set OPP_Status=?,OPP_Last_UpdDt=? where OPP_ID=?";
		Object[] args = new Object[] { OppStatus.ACCEPTED.name(), timestamp,
				oppId };
		int out = jdbcTemplate.update(query, args);*/

	}

	public Date getTimeForTZ(String timeZone) {

		Date currDate = new Date();
		Date dateCet = getDateInTimeZone(currDate, (timeZone != null
				&& timeZone.length() > 0 ? timeZone : "CET"));
		return dateCet;
	}

	public Date getDateInTimeZone(Date currentDate, String timeZoneId) {

		TimeZone tz = TimeZone.getTimeZone(timeZoneId);
		Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
		mbCal.setTimeInMillis(currentDate.getTime());
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
		cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));
		return cal.getTime();
	}

	@Override
	public OpportunityHist getOpputnityHistory(String oppId, String partnerId) {
		// TODO Auto-generated method stub
		String histQuery = "";
		Map<String, Object> histparamMap = null;
		if (partnerId != null) {
			histQuery = "select  SHOLH_Remarks, SHOLH_Eff_FmDt, SHOLH_Eff_ToDt, SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID,SHOLH_Remarks from stakeholderopplinkhist where (SHOLH_OPP_ID, SHOLH_Sequence) in ( select SHOLH_OPP_ID,max(SHOLH_Sequence) seqMax from stakeholderopplinkhist group by SHOLH_Status_Id,SHOLH_OPP_ID having SHOLH_OPP_ID=:SHOLH_OPP_ID and SHOLH_SH_ID=:SHOLH_SH_ID order by seqMax)";
			histparamMap = new HashMap<String, Object>();
			histparamMap.put("SHOLH_SH_ID", partnerId);
			histparamMap.put("SHOLH_OPP_ID", oppId);

		} else {
			histQuery = "select  SHOLH_Remarks, SHOLH_Eff_FmDt, SHOLH_Eff_ToDt, SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID,SHOLH_Remarks from stakeholderopplinkhist where (SHOLH_OPP_ID, SHOLH_Sequence) in ( select SHOLH_OPP_ID,max(SHOLH_Sequence) seqMax from stakeholderopplinkhist group by SHOLH_Status_Id,SHOLH_OPP_ID having SHOLH_OPP_ID=:SHOLH_OPP_ID order by seqMax)";
			histparamMap = new HashMap<String, Object>();
			histparamMap.put("SHOLH_OPP_ID", oppId);
		}

		List<Map<String, Object>> histRows = namedParameterJdbcTemplate
				.queryForList(histQuery, histparamMap);
		List<HashMap<String, String>> histMapArray = new ArrayList<HashMap<String, String>>();
		for (Map<String, Object> histRow : histRows) {
			HashMap<String, String> histMap = new HashMap<String, String>();
			histMap.put("status", String.valueOf(histRow.get("SHOLH_Status")));
			histMap.put("statusId",
					String.valueOf(histRow.get("SHOLH_Status_Id")));
			histMap.put("startTime",
					String.valueOf(histRow.get("SHOLH_Eff_FmDt")));
			histMap.put("endTime",
					String.valueOf(histRow.get("SHOLH_Eff_ToDt")));
			histMap.put("reason", String.valueOf(histRow.get("SHOLH_Remarks")));
			histMapArray.add(histMap);
		}

		// get event history

		// histQuery="select  SHOLH_Remarks, SHOLH_Eff_FmDt, SHOLH_Eff_ToDt, SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID from stakeholdereventhist where SHOLH_OPP_ID=:SHOLH_OPP_ID ";
		histQuery = "select  SHOLH_Remarks, SHOLH_Eff_FmDt, SHOLH_Eff_ToDt, SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID from stakeholdereventhist where (SHOLH_OPP_ID, SHOLH_Sequence) in ( select SHOLH_OPP_ID,max(SHOLH_Sequence) seqMax from stakeholdereventhist group by SHOLH_Status_Id,SHOLH_OPP_ID having SHOLH_OPP_ID=:SHOLH_OPP_ID  order by seqMax)";

		histparamMap = new HashMap<String, Object>();
		// histparamMap.put("SHOL_SH_ID", partnerId);
		histparamMap.put("SHOLH_OPP_ID", oppId);

		histRows = namedParameterJdbcTemplate.queryForList(histQuery,
				histparamMap);

		for (Map<String, Object> histRow : histRows) {
			HashMap<String, String> histMap = new HashMap<String, String>();
			histMap.put("status", String.valueOf(histRow.get("SHOLH_Status")));
			histMap.put("statusId",
					String.valueOf(histRow.get("SHOLH_Status_Id")));
			histMap.put("startTime",
					String.valueOf(histRow.get("SHOLH_Eff_FmDt")));
			histMap.put("endTime",
					String.valueOf(histRow.get("SHOLH_Eff_ToDt")));
			histMapArray.add(histMap);
		}

		OpportunityHist opHist = new OpportunityHist();
		opHist.setOppHistory(histMapArray);

		return opHist;
	}

	@Override
	public void unassignOpportunityFromPartner(String oppId) {
		// TODO Auto-generated method stub
		String query = "delete from stakeholderopplink where  SHOL_OPP_ID =? ";

		Object[] args = new Object[] { oppId };
		int out = jdbcTemplate.update(query, args);
	}

	/**
	 * JIRA CL-403 get list of oppID and parner who are not accepeted the
	 * opportunity from past 48 hrs , some one else accepted the same
	 * opportunity;
	 * 
	 * @return
	 */
	@Override
	public List<String> getOppAndPartner(String country) {

		String query = "select SHOL_OPP_ID,SHOL_SH_ID from stakeholderopplink where SHOL_OPP_ID in (SELECT OPP_ID FROM opportunity where TIMESTAMPDIFF(HOUR,  OPP_Eff_FmDt, CONVERT_TZ(sysdate(),'SYSTEM',OPP_Time_Zone))  >48 and OPP_Status='ACCEPTED'  and OPP_Cntry_Cd=:country) and SHOL_Status='INITIAL' ";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String CurrStatusId = "";
		List<String> returnList = new ArrayList<String>();

		Opportunity opp = new Opportunity();
		for (Map<String, Object> row : rows) {
			returnList.add(String.valueOf(row.get("SHOL_OPP_ID")) + "#"
					+ String.valueOf(row.get("SHOL_SH_ID")));

		}

		return returnList;

	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void revokeOppFromPartner(String oppId, String partnerId) {
		//TransactionDefinition def = new DefaultTransactionDefinition();
		//TransactionStatus transStatus = transactionManager.getTransaction(def);

		try {

			String query = "delete from stakeholderopplink where  SHOL_OPP_ID =? and SHOL_SH_ID=? and SHOL_Status <>'DECLINED'";
			Object[] args = new Object[] { oppId, partnerId };
			int out = jdbcTemplate.update(query, args);

			//transactionManager.commit(transStatus);

		} catch (Exception e) {
			e.printStackTrace();
			//transactionManager.rollback(transStatus);
		}

	}

	@Override
	public User getUser(User user) {
		User responseUser = null;
		try {

			// TODO Auto-generated method stub
			String query = "SELECT SH_User_Name,SH_Password from stakeholders where UPPER(SH_User_Name) =:USER_NAME and SH_Password=:PASSWORD";
			LOGGER.debug("SQL :" + query);
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("USER_NAME", user.getUsername().toUpperCase());
			paramMap.put("PASSWORD", user.getPassword());
			List<Map<String, Object>> rows = namedParameterJdbcTemplate
					.queryForList(query, paramMap);
			for (Map<String, Object> row : rows) {
				responseUser = new User();
				responseUser
						.setUsername(String.valueOf(row.get("SH_User_Name")));
				responseUser
						.setPassword(String.valueOf(row.get("SH_Password")));
		
			}
		} catch (Exception e) {
			LOGGER.debug("SQL :" + e.getMessage());
		}
		return responseUser;
	}

	@Override
	public List<BOM> getBomsCSV(String opportunityId) {

		final String SELECT_BOMS_QUERY = "SELECT OBOM_OPP_ID, OBOM_SlNo, OBOM_Category, OBOM_Prod_ID, OBOM_Name, OBOM_Desc, OBOM_Colour, OBOM_Std_Incl_Qty, OBOM_Var_Qty, OBOM_Tot_Qty, OBOM_Unit_Cost, "
				+ "OBOM_Tot_Cost FROM opportunitybom WHERE OBOM_OPP_ID = :OPPORTUNITY_ID";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPPORTUNITY_ID", opportunityId);

		List<BOM> bomList = new ArrayList<BOM>();
		BOM bom = null;
		List<Map<String, Object>> bomRows = namedParameterJdbcTemplate
				.queryForList(SELECT_BOMS_QUERY, paramMap);
		for (Map<String, Object> row : bomRows) {

			bom = new BOM();
			bom.setCategory(String.valueOf(row.get("OBOM_Category")));
			bom.setProductId(String.valueOf(row.get("OBOM_Prod_ID")));
			bom.setProductName(String.valueOf(row.get("OBOM_Name")));
			bom.setDescription(String.valueOf(row.get("OBOM_Desc")));
			bom.setColor(String.valueOf(row.get("OBOM_Colour")));
			bom.setQuantity(Integer.valueOf(String.valueOf(row
					.get("OBOM_Tot_Qty"))));
			bom.setUnitCost(Float.valueOf(String.valueOf(row
					.get("OBOM_Tot_Cost"))));
			bomList.add(bom);
		}

		return bomList;
	}

	public JSONObject generateCsvFile(List<BOM> boms) {
		StringBuilder writer = new StringBuilder();

		writer.append("Product Id");
		writer.append(',');
		writer.append("Category");
		writer.append(',');
		writer.append("Description");
		writer.append(',');
		writer.append("Color");
		writer.append(',');
		writer.append("Quantity");
		writer.append(',');
		writer.append("Total Cost");

		for (BOM bom : boms) {

			writer.append('\n');
			writer.append(bom.getProductId());
			writer.append(',');
			writer.append(bom.getCategory());
			writer.append(',');
			writer.append(bom.getDescription());
			writer.append(',');
			writer.append(bom.getColor());
			writer.append(',');
			writer.append("" + bom.getQuantity());
			writer.append(',');
			writer.append("" + bom.getUnitCost());
		}
		JSONObject prfjson = new JSONObject();
		prfjson.put("CSV", writer.toString());
		return prfjson;

	}

	@Override
	public int addDistributorToPartener(final Distributor distributor) {
		int response = 0;
		String query = "SELECT SH_ID from stakeholders WHERE SH_ID =:SH_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SH_ID", distributor.getShId());

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		int partnerId = 0;

		for (Map<String, Object> row : rows) {
			partnerId = Integer.parseInt(String.valueOf(row.get("SH_ID")));
		}

		LOGGER.debug("partnerId :" + partnerId);

		if (partnerId <= 0) {
			try {
				throw new AppException(
						Response.Status.NO_CONTENT.getStatusCode(), 204,
						"Partner not found in closed loop for given user id ",
						"", null);
			} catch (AppException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String querydistributors = "SELECT count(*) as cnt  from distributors WHERE SH_ID =:SH_ID ";
		LOGGER.debug("SQL :" + querydistributors);
		Map<String, Object> paramDistCountMap = new HashMap<String, Object>();
		paramDistCountMap.put("SH_ID", distributor.getShId());

		List<Map<String, Object>> distrows = namedParameterJdbcTemplate
				.queryForList(querydistributors, paramDistCountMap);

		int distributorsCount = 0;

		int result = 0;

		for (Map<String, Object> row : distrows) {
			distributorsCount = Integer
					.parseInt(String.valueOf(row.get("cnt")));

		}

		LOGGER.debug("distributorsCount :" + distributorsCount);

		if (distributorsCount >= 5) {
			response = 2;
		} else {

			result = jdbcTemplate
					.update("insert into distributors(SH_ID,DISTRIBUTOR_NAME,DISTRIBUTOR_EMAIL,CONTACT_PERSON) "
							+ "values (?, ?, ?,?) ",
							new PreparedStatementSetter() {
								public void setValues(PreparedStatement ps)
										throws SQLException {
									ps.setString(1, distributor.getShId());
									ps.setString(2, distributor.getDistName());
									ps.setString(3, distributor.getDistEmail());
									ps.setString(4, distributor.getContName());

								}
							});
		}

		if (result == 1) {
			response = 1;
		}

		return response;

	}

	public List<Distributor> getDistList(String shId) {

		ArrayList<Distributor> listDist = new ArrayList<Distributor>();

		String query = "SELECT   ID, DISTRIBUTOR_NAME,DISTRIBUTOR_EMAIL, CONTACT_PERSON  FROM distributors where SH_ID=:SH_ID";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SH_ID", shId);
		List<Distributor> distList = new ArrayList<Distributor>();
		Distributor distributor = null;
		List<Map<String, Object>> distRows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);
		for (Map<String, Object> row : distRows) {

			Distributor dist = new Distributor();
			dist.setDistId(String.valueOf(row.get("ID")));
			dist.setDistName(String.valueOf(row.get("DISTRIBUTOR_NAME")));
			dist.setDistEmail(String.valueOf(row.get("DISTRIBUTOR_EMAIL")));
			dist.setContName(String.valueOf(row.get("CONTACT_PERSON")));
			distList.add(dist);
		}

		return distList;

	}

	@Override
	public int updateDistributor(Distributor distributor, String shId,	String distId) {

		int result=0;
		
		String update_query = "update  distributors set  DISTRIBUTOR_NAME=?, DISTRIBUTOR_EMAIL=?, CONTACT_PERSON=? where ID=? and SH_ID=?";
	
		int i=jdbcTemplate.update(update_query,new Object[]{distributor.getDistName(),distributor.getDistEmail(),distributor.getContName(),distId,shId});

		if(i>0)
		{
			result= 1;
		}
		else
		{
			result= 0;
		}
		
		return result;
		
	}

	@Override
	public int deleteDistributor(String distId, String shId) {
		Distributor distributor = new Distributor();
		int result =0;
		String delete_query = "delete from distributors where ID=? and SH_ID=?";
		int i = jdbcTemplate.update(delete_query, new Object[]{distId,shId});
        if(i>0)
        {
        	result =1;
        	
        }
        else
        {
        	result=0;
        }
        return result;
	}

	@Override
	public boolean checkLinkExists(String oppId, String partnerId) {
		// TODO Auto-generated method stub
		//LOGGER.debug("o");
		String query = "select SHOL_OPP_ID,SHOL_SH_ID from stakeholderopplink where SHOL_OPP_ID=:SHOL_OPP_ID and SHOL_SH_ID=:SHOL_SH_ID ";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_OPP_ID", oppId);
		paramMap.put("SHOL_SH_ID", partnerId);
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);
		for (Map<String, Object> row : rows) {
			System.out.println("record found");
			return true;

		}
		System.out.println("record not found :"+ oppId);
		
		return false;
	}

	@Override
	public int udateOpportunity(String oppId, String status) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getPartnerId(String userId) {
		// TODO Auto-generated method stub
		
		String query = "SELECT SH_ID from stakeholders WHERE SH_User_Name =:SHOL_USER_ID ";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_USER_ID", userId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String partnerId = null;

		for (Map<String, Object> row : rows) {
			partnerId = String.valueOf(row
					.get("SH_ID"));
		}

		return partnerId;
	}
	
	@Override
	public String checkOPPAccepted(String oppId) {
		// TODO Auto-generated method stub
		String partnerId="";
		String query = "select SHOL_OPP_ID,SHOL_SH_ID from stakeholderopplink where SHOL_OPP_ID=:SHOL_OPP_ID and SHOL_Status='ACCEPTED'";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_OPP_ID", oppId);
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);
		for (Map<String, Object> row : rows)
		{
			partnerId = String.valueOf(row
					.get("SHOL_SH_ID"));
			LOGGER.debug("checkOPPAccepted:partnerId"+partnerId);

		}
		return partnerId;
	}
	
	public boolean checkOPPNoneAccepted(String oppId) {
		// TODO Auto-generated method stub
		String partnerId="";
		String query = "select SHOL_OPP_ID,SHOL_SH_ID,SHOL_status from stakeholderopplink where SHOL_OPP_ID=:SHOL_OPP_ID";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_OPP_ID", oppId);
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);
		for (Map<String, Object> row : rows)
		{
			if(String.valueOf(row.get("SHOL_status")).equalsIgnoreCase(OppStatus.ACCEPTED.getValue()))
			{
				return false;
			}

		}
		return true;
	}
	
	
	/**
	 * this method used for N4 N5 N6 notifications
	 * @param hrs
	 * @return
	 */
	@Override
	public Map<String,Object> getPartnerDeviceForNotifications(int hrs,String ntype,String country){
		
		List<PartnerDevice> partnerDeviceList=new ArrayList<PartnerDevice>();
		Map<String,String> oppCustMap=new HashMap<String,String>();
		Map<String,String> partCustMap=new HashMap<String,String>();
		Map<String,String> partnerOppMap=new HashMap<String,String>();
		Map<String,String> partnerDateMap=new HashMap<String,String>();
		
		String query="";
		if(ntype.equalsIgnoreCase("n4"))
		{
           //query="SELECT max(SHOLH_Sequence),SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o where l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_NOTI4=0   group by sholh_opp_id having SHOLH_Status_Id='MEETING' and SHOLH_SH_ID<>99999 and datediff(curdate(), SHOLH_Eff_FmDt)>"+hrs;
            query="SELECT max(SHOLH_Sequence),SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o ,stakeholders s where l.sholh_sh_id=s.sh_id and l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_NOTI4=0 and s.SH_Country_Code='"+country+"' group by sholh_opp_id having SHOLH_Status_Id='MEETING' and SHOLH_SH_ID<>99999 and datediff(curdate(), SHOLH_Eff_FmDt)>"+hrs;
           
		}
		else if(ntype.equalsIgnoreCase("n5"))
		{
		   //query="SELECT max(SHOLH_Sequence),SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o where l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_NOTI5=0   group by sholh_opp_id having SHOLH_Status_Id='MEETING' and SHOLH_SH_ID<>99999 and datediff(curdate(), SHOLH_Eff_FmDt)>"+hrs;
		   query="SELECT max(SHOLH_Sequence),SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o ,stakeholders s where l.sholh_sh_id=s.sh_id and l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_NOTI5=0 and s.SH_Country_Code='"+country+"' group by sholh_opp_id having SHOLH_Status_Id='MEETING' and SHOLH_SH_ID<>99999 and datediff(curdate(), SHOLH_Eff_FmDt)>"+hrs;

		}
		else if(ntype.equalsIgnoreCase("n6"))
		{
			//query="SELECT max(SHOLH_Sequence),SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o where l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_NOTI6=0   group by sholh_opp_id having SHOLH_Status_Id='MEETING' and SHOLH_SH_ID<>99999 and datediff(curdate(), SHOLH_Eff_FmDt)>"+hrs;
		    query="SELECT max(SHOLH_Sequence),SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o ,stakeholders s where l.sholh_sh_id=s.sh_id and l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_NOTI6=0 and s.SH_Country_Code='"+country+"' group by sholh_opp_id having SHOLH_Status_Id='MEETING' and SHOLH_SH_ID<>99999 and datediff(curdate(), SHOLH_Eff_FmDt)>"+hrs;

		}
		else if(ntype.equalsIgnoreCase("CANCEL"))
		{
			//query = "SELECT SHOLH_Sequence,SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o,stakeholderopplink shl where l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_CANCEL=0 and shl.SHOL_SH_ID=l.SHOLH_SH_ID and shl.SHOL_OPP_ID=l.SHOLH_OPP_ID and SHOL_Status='ACCEPTED' and  SHOLH_Status_Id='CANCEL' and SHOLH_SH_ID<>99999";
			query = "SELECT SHOLH_Sequence,SHOLH_SH_ID,SHOLH_Status,SHOLH_Status_Id,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist l,opportunitysla o,stakeholderopplink shl,stakeholders s where s.SH_ID=l.SHOLH_SH_ID and s.SH_Country_Code='"+country+"' and l.SHOLH_OPP_ID=o.OPPSLA_OPPID and  o.OPPSLA_CANCEL=0 and shl.SHOL_SH_ID=l.SHOLH_SH_ID and shl.SHOL_OPP_ID=l.SHOLH_OPP_ID and SHOL_Status='ACCEPTED' and  SHOLH_Status_Id='CANCEL' and SHOLH_SH_ID<>99999";
			 
		}
	     
		LOGGER.debug("getPartnerDeviceForNotifications:query:::"+query);
		
        Map<String, Object> paramMap = new HashMap<String, Object>();
		
		List<Map<String, Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<String> partnerList=new ArrayList<String>();
		Map<String,String> oppPartnerMap=new HashMap<String,String>();
		
		List<Integer> oppIdList=new ArrayList<Integer>();
	    	
		for (Map<String, Object> row : rows)
		{
			partnerList.add(String.valueOf(row.get("SHOLH_SH_ID")));
			oppPartnerMap.put(String.valueOf(row.get("SHOLH_OPP_ID")), String.valueOf(row.get("SHOLH_SH_ID")));
			oppIdList.add(Integer.parseInt(String.valueOf(row.get("SHOLH_OPP_ID"))));
			partnerOppMap.put(String.valueOf(row.get("SHOLH_SH_ID")),String.valueOf(row.get("SHOLH_OPP_ID")));
			partnerDateMap.put(String.valueOf(row.get("SHOLH_SH_ID")),String.valueOf(row.get("SHOLH_Eff_FmDt")));
			
		}
				
		if(partnerList!=null && partnerList.size()>0)
		{
			
			query="SELECT c.cust_first_name,o.opp_id,o.opp_name from customer c,opportunity o where o.OPP_Consumer_ID=c.cust_id and  o.opp_id in (:opp_id)";
			
			LOGGER.debug("getPartnerDeviceForNotifications:query:::"+query);

			paramMap = new HashMap<String, Object>();
			paramMap.put("opp_id", oppIdList);
		
			rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			 
			for (Map<String, Object> row : rows)
			{
				
				oppCustMap.put(String.valueOf(row.get("opp_id")), String.valueOf(row.get("cust_first_name")));
				
			}
			
			//oppPartnerMap
			for(Map.Entry<String, String> entry:oppPartnerMap.entrySet())
			{
				
				//partner Id,customer Name
				partCustMap.put(entry.getValue(), oppCustMap.get(entry.getKey()));
			    
			}
			
			query="SELECT SHD_ID, SHD_SH_ID, SHD_Device_ID, SHD_Device_Type, SHD_Mobile_No, SHD_Eff_FmDt, SHD_Eff_ToDt from stakeholdersdevices "
					+ " where SHD_SH_ID in (:SHD_SH_ID) ";
			
			LOGGER.debug("getPartnerDeviceForNotifications:query:::"+query);
			paramMap = new HashMap<String, Object>();
			paramMap.put("SHD_SH_ID", partnerList);
			
			rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			
			for (Map<String, Object> row : rows)
			{
				PartnerDevice pDevice =new PartnerDevice();
				pDevice.setDeviceId(String.valueOf(row.get("SHD_Device_ID")));
				pDevice.setDeviceType(String.valueOf(row.get("SHD_Device_Type")));
				pDevice.setPartnerId(Integer.parseInt(String.valueOf(row.get("SHD_SH_ID"))));
				pDevice.setMobileNumber(String.valueOf(row.get("SHD_Mobile_No")));
				LOGGER.debug("device ids adding to list:"+String.valueOf(row.get("SHD_Device_ID"))+":::device type::"+String.valueOf(row.get("SHD_Device_Type"))+":::PartnerId:"+String.valueOf(row.get("SHD_SH_ID")));
				partnerDeviceList.add(pDevice);
			}
		}
		
		Map<String,Object> returnMap=new HashMap<String,Object>();
		returnMap.put("PARTNERCUST", partCustMap);
		returnMap.put("DEVICELIST", partnerDeviceList);
		returnMap.put("OPPLIST", oppIdList);
		returnMap.put("PARTNEROPPID",partnerOppMap);
		returnMap.put("PARTNERDATE",partnerDateMap);

		return returnMap;
	}
	
	
	@Override
	public void udateOpportunityImages(String oppId,final OpportunityImages opportunityImages)
	{
		try
		{
            String partnerId="";
			if(opportunityImages.getUserType()!=null && opportunityImages.getUserType().equalsIgnoreCase("partner"))
			{
				if(opportunityImages.getUserId()!=null && opportunityImages.getUserId().length()>0)
				{
					partnerId= getPartnerId(opportunityImages.getUserId());
					opportunityImages.setShId(partnerId);
				}
				LOGGER.debug("partnerId"+partnerId);
				
			}	
			
			final String INSERT_OPP_IMAGES_QUERY = "INSERT INTO opportunity_artifacts(OPP_ID,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID,OPP_TITLE,OPP_STATUS) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?)";

			int out = jdbcTemplate.update(INSERT_OPP_IMAGES_QUERY,
					new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps)
						throws SQLException {

					ps.setString(1,opportunityImages.getOppId());
					ps.setString(2,opportunityImages.getArtifactFormat());
					ps.setString(3,opportunityImages.getArtifactName());
					ps.setString(4,opportunityImages.getArtifactObjectId());
					ps.setString(5,opportunityImages.getUserType());
					ps.setString(6,opportunityImages.getArtifactRootObjectId());
					ps.setString(7,opportunityImages.getArtifactSubFolderId());
					ps.setString(8,opportunityImages.getShId());
					ps.setString(9,opportunityImages.getOppTitle());
					ps.setString(10,OppStatus.LOADED.name());
				}
			});
			
			String update_query = "update stakeholders set SH_IMS_ID=? where SH_ID=?";
		
			int i=0;
			try
			{
				if((opportunityImages.getImsId()!=null && opportunityImages.getImsId().length()>0) &&  (opportunityImages.getShId()!=null && opportunityImages.getShId().length()>0))
				i=jdbcTemplate.update(update_query,new Object[]{opportunityImages.getImsId(),opportunityImages.getShId()});
				
			}catch(Exception e)
			{
				LOGGER.error("Error while updating the partner IMSID"+e.getMessage());
			}

			if(i>0)
			{
				LOGGER.debug("partner IMSID updated successfully..");
			}	
			
			

			if (out != 0) {
				LOGGER.debug("OPP IMAGES saved");
			} else {
				LOGGER.error("OPP IMAGES failed");
				throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
						204, "OPP IMAGES save failed ", "", null);
			}

		}catch(Exception e)
		{
			e.printStackTrace();
		}


	}
	
	@Override
	public void udateOpportunityConsumerImages(String oppId,final List<OpportunityImages> opportunityImages)
	{
		try
		{
            String partnerId="";
            
            for(final OpportunityImages oppImages:opportunityImages)
            {
				final String INSERT_OPP_IMAGES_QUERY = "INSERT INTO opportunity_artifacts(OPP_ID,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID,OPP_TITLE,OPP_STATUS) "
						+ "VALUES(?,?,?,?,?,?,?,?,?,?)";
	
				int out = jdbcTemplate.update(INSERT_OPP_IMAGES_QUERY,
						new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps)
							throws SQLException {
	
						ps.setString(1,oppImages.getOppId());
						ps.setString(2,oppImages.getArtifactFormat());
						ps.setString(3,oppImages.getArtifactName());
						ps.setString(4,oppImages.getArtifactObjectId());
						ps.setString(5,oppImages.getUserType());
						ps.setString(6,oppImages.getArtifactRootObjectId());
						ps.setString(7,oppImages.getArtifactSubFolderId());
						ps.setString(8,oppImages.getShId());
						ps.setString(9,oppImages.getOppTitle());
						ps.setString(10,OppStatus.LOADED.name());
					}
				});
				
				String update_query = "update stakeholders set SH_IMS_ID=? where SH_ID=?";
			
				int j=0;
				try
				{
					if((oppImages.getImsId()!=null && oppImages.getImsId().length()>0) &&  (oppImages.getShId()!=null && oppImages.getShId().length()>0))
					j=jdbcTemplate.update(update_query,new Object[]{oppImages.getImsId(),oppImages.getShId()});
					
				}catch(Exception e)
				{
					LOGGER.error("Error while updating the partner IMSID"+e.getMessage());
				}
				if(j>0)
				{
					LOGGER.debug("partner IMSID updated successfully..");
				}	
				
				
	
				if (out != 0) {
					LOGGER.debug("OPP IMAGES saved");
				} else {
					LOGGER.error("OPP IMAGES failed");
					throw new AppException(Response.Status.NO_CONTENT.getStatusCode(),
							204, "OPP IMAGES save failed ", "", null);
				}
            }
	
		}catch(Exception e)
		{
			e.printStackTrace();
		}


	}


	@Override
	public List<OpportunityImages> getOportunityImages(String oppId,String oppTitle,String userId) 
	{		
		OpportunityImages opportunityImages=null;
		String partnerId="";
		String query="";
		List<OpportunityImages>  opportunityImagesList = new ArrayList<OpportunityImages>();
		if(userId!=null && userId.length()>0)
		{
			partnerId= getPartnerId(userId);
		}
		LOGGER.debug("partnerId"+partnerId);
		
		try 
		{
		
		Map<String, Object> paramMap = new HashMap<String, Object>();

		if((partnerId!=null && partnerId.length()>0) &&  (oppId!=null && oppId.length()>0))	
		{
			//query = "SELECT OPP_ID,OPP_TITLE,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,ARTIFACT_ROOT_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID from opportunity_artifacts WHERE OPP_ID=:OPP_ID and SH_ID=:SH_ID";
			//LOGGER.debug("SQL :" + query);
			//paramMap.put("OPP_ID", oppId);
			//paramMap.put("SH_ID", partnerId);
			query = "SELECT OPP_ID,OPP_TITLE,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,ARTIFACT_ROOT_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID from opportunity_artifacts WHERE OPP_TITLE=:OPP_TITLE";
			LOGGER.debug("SQL::SELECT OPP_ID,OPP_TITLE,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,ARTIFACT_ROOT_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID from opportunity_artifacts WHERE OPP_TITLE='"+oppTitle+"'");
			paramMap.put("OPP_TITLE", oppTitle);
			
		}else
		{
         	query = "SELECT OPP_ID,OPP_TITLE,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,ARTIFACT_ROOT_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID from opportunity_artifacts WHERE OPP_TITLE=:OPP_TITLE";
			LOGGER.debug("SQL::SELECT OPP_ID,OPP_TITLE,ARTIFACT_FORMAT,ARTIFACT_NAME,ARTIFACT_IMAGE_OBJECT_ID,ARTIFACT_ROOT_OBJECT_ID,USER_TYPE,ARTIFACT_ROOT_OBJECT_ID,ARTIFACT_SUB_FOLDER_OBJECT_ID,SH_ID from opportunity_artifacts WHERE OPP_TITLE='"+oppTitle+"'");
			paramMap.put("OPP_TITLE", oppTitle);

		}	
		
		List<Map<String, Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
           
		for (Map<String, Object> row : rows)
		{
			 LOGGER.info("OPP_ID:::"+String.valueOf(row.get("OPP_ID")));
			 LOGGER.info("OPP_TITLE:::"+String.valueOf(row.get("OPP_TITLE")));
			 
			 opportunityImages = new OpportunityImages();
			 opportunityImages.setOppId(String.valueOf(row.get("OPP_ID")));
			 opportunityImages.setOppTitle(String.valueOf(row.get("OPP_TITLE")));
			 opportunityImages.setArtifactName(String.valueOf(row.get("ARTIFACT_NAME")));
			 opportunityImages.setArtifactFormat(String.valueOf(row.get("ARTIFACT_FORMAT")));
			 opportunityImages.setArtifactObjectId(String.valueOf(row.get("ARTIFACT_IMAGE_OBJECT_ID")));
			 opportunityImages.setUserType(String.valueOf(row.get("USER_TYPE")));
			 opportunityImages.setArtifactRootObjectId(String.valueOf(row.get("ARTIFACT_ROOT_OBJECT_ID")));
			 opportunityImages.setArtifactSubFolderId(String.valueOf(row.get("ARTIFACT_SUB_FOLDER_OBJECT_ID")));
			 opportunityImages.setShId(String.valueOf(row.get("SH_ID")));
			 opportunityImagesList.add(opportunityImages);
		}
		
		}catch(Exception e)
		{
			LOGGER.info("Exception"+e.getMessage());
		}
		return opportunityImagesList;		
		
	}	
	
	
	public List<OpportunityImages> getOportunityConsumerImages(String oppId,String oppTitle,String userId) 
	{		
		OpportunityImages opportunityImages=null;
		String partnerId="";
		String query="";
		List<OpportunityImages>  opportunityImagesList = new ArrayList<OpportunityImages>();
		if(userId!=null && userId.length()>0)
		{
			partnerId= getPartnerId(userId);
		}
		LOGGER.debug("partnerId"+partnerId);
		
		try 
		{
		
		Map<String, Object> paramMap = new HashMap<String, Object>();

		if((partnerId!=null && partnerId.length()>0) &&  (oppId!=null && oppId.length()>0))	
		{
			query = "SELECT a.* from opportunity_artifacts a,opportunity_partner_consumer_images b WHERE a.OPP_ID=b.OPP_ID and b.SH_ID=:SH_ID and b.OPP_Id=:OPP_ID";
			paramMap.put("OPP_ID", oppId);
			paramMap.put("SH_ID", partnerId);
			
		}
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		for (Map<String, Object> row : rows)
		{
			 opportunityImages = new OpportunityImages();
			 opportunityImages.setOppId(String.valueOf(row.get("OPP_ID")));
			 opportunityImages.setOppTitle(String.valueOf(row.get("OPP_TITLE")));
			 opportunityImages.setArtifactName(String.valueOf(row.get("ARTIFACT_NAME")));
			 opportunityImages.setArtifactFormat(String.valueOf(row.get("ARTIFACT_FORMAT")));
			 opportunityImages.setArtifactObjectId(String.valueOf(row.get("ARTIFACT_IMAGE_OBJECT_ID")));
			 opportunityImages.setUserType(String.valueOf(row.get("USER_TYPE")));
			 opportunityImages.setArtifactRootObjectId(String.valueOf(row.get("ARTIFACT_ROOT_OBJECT_ID")));
			 opportunityImages.setArtifactSubFolderId(String.valueOf(row.get("ARTIFACT_SUB_FOLDER_OBJECT_ID")));
			 opportunityImages.setShId(String.valueOf(row.get("SH_ID")));
			 opportunityImagesList.add(opportunityImages);
		}
		
		}catch(Exception e)
		{
			LOGGER.info("Exception"+e.getMessage());
		}
		return opportunityImagesList;		
		
	}
	@Override
	public String getOpportunityStatus(String oppId) 
	{
		// TODO Auto-generated method stub
		String oppStatus="";
		try 
		{
		
		String query = "select OPP_Status from opportunity where OPP_ID=:OPP_ID";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);
		
		List<Map<String, Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		for (Map<String, Object> row : rows)
		{
			LOGGER.info("record found:step1 opportunity table");
			oppStatus = String.valueOf(row.get("OPP_Status"));
		}
		
		String query1 = "select SHOL_Status from stakeholderopplink where SHOL_OPP_ID=:OPP_ID";
		List<Map<String, Object>> rows1 = namedParameterJdbcTemplate.queryForList(query1, paramMap);
		for (Map<String, Object> row : rows1)
		{
			LOGGER.info("record found:step2");
			oppStatus = String.valueOf(row.get("SHOL_Status"));

		}
		
		String query2 = "select SHOLH_Status_Id from stakeholderopplinkhist where SHOLH_OPP_ID=:OPP_ID";
		List<Map<String, Object>> rows2 = namedParameterJdbcTemplate.queryForList(query2, paramMap);
		for (Map<String, Object> row : rows2)
		{
			LOGGER.info("record found:step3");
			oppStatus = String.valueOf(row.get("SHOLH_Status_Id"));
		}
		
		}catch(Exception e)
		{
			LOGGER.error("getOpportunityStatus::Error while getting opp status:::"+e.getMessage());
		}
		
		return oppStatus;
		
	}	
	
	@Override
		public void updateOppHistoryCancel(final String partnerId, final String oppId,
				final String statusId, final String status, final String reason,
				final String startTime, final String endTime, String type,
				final String source) throws AppException {

			java.util.Date dt = new java.util.Date();

			java.util.Date date = null;
			java.util.Date date1 = null;
			boolean exception = false;
			try {
				date = (java.util.Date) DATE_FORMAT.parse(startTime);
				date1 = (java.util.Date) DATE_FORMAT.parse(endTime);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				exception = true;
			}
			Timestamp starttimeConv = new Timestamp(dt.getTime());
			Timestamp endtimeConv = new Timestamp(dt.getTime());
			if (!exception) {
				starttimeConv = new Timestamp(date.getTime());
				endtimeConv = new Timestamp(date1.getTime());

			}
			final Timestamp starttimeConvfinal = starttimeConv;
			final Timestamp endtimeConvfinal = endtimeConv;

			if (type.equalsIgnoreCase("status")) {
				
								jdbcTemplate
						.update("insert into  stakeholderopplinkhist(SHOLH_SH_ID, SHOLH_OPP_ID, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,SHOLH_UPDATE_BFO,SHOLH_Status_Id,SHOLH_Remarks,SHOL_Source) "
								+ "values (?, ?, ?,?,?,?,?,?,?) ",
								new PreparedStatementSetter() {
									public void setValues(PreparedStatement ps)
											throws SQLException {
										ps.setString(1, partnerId);
										ps.setString(2, oppId);
										ps.setString(3, status);
										ps.setTimestamp(4, starttimeConvfinal);
										ps.setTimestamp(5, endtimeConvfinal);
										ps.setInt(6, 0);
										ps.setString(7, statusId);
										ps.setString(8, reason);
										ps.setString(9, source);
									}
								});
				}			
			
		}
	
	
	public boolean checkCancelExists(String oppId,String partnerId)
	{
		boolean cancelExists =false;
		String histQuery = "select max(SHOLH_Sequence) seqMax,SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID from stakeholderopplinkhist group by SHOLH_Status_Id,SHOLH_OPP_ID having (SHOLH_SH_ID=:SHOL_SH_ID || SHOLH_SH_ID=88888) and SHOLH_OPP_ID=:SHOLH_OPP_ID  order by seqMax";
		Map<String, Object> histparamMap = new HashMap<String, Object>();
		histparamMap.put("SHOL_SH_ID", partnerId);
		histparamMap.put("SHOLH_OPP_ID", oppId);
		List<Map<String, Object>> histRows = namedParameterJdbcTemplate.queryForList(histQuery, histparamMap);
		for (Map<String, Object> histRow : histRows) 
		{	
			HashMap<String, String> histMap = new HashMap<String, String>();						
			LOGGER.debug("checkCancelExists:::currrent status"+String.valueOf(histRow.get("SHOLH_Status_Id")));
			if((String.valueOf(histRow.get("SHOLH_Status_Id")).equalsIgnoreCase("CANCEL")) )
			{	
				cancelExists=true;
				break;
			}
		}
		return cancelExists;
	}
	@Override
	public String getOpportunityMeetingDate(String oppId)
	{
		// TODO Auto-generated method stub
		String meetingDate="";
		try 
		{
			
		String query2 = "select SHOLH_Eff_FmDt from stakeholderopplinkhist where SHOLH_OPP_ID=:OPP_ID";
		LOGGER.info("getOpportunityMeetingDate:query2"+query2);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);
		List<Map<String, Object>> rows2 = namedParameterJdbcTemplate.queryForList(query2, paramMap);
		for (Map<String, Object> row : rows2)
		{
			//LOGGER.info("getOpportunityMeetingDate:query2"+query2);
			meetingDate = String.valueOf(row.get("SHOLH_Eff_FmDt"));
		}
		
		}catch(Exception e)
		{
			LOGGER.error("getOpportunityMeetingDate::Error while getting opp meeting date:::"+e.getMessage());
		}
		
		return meetingDate;

	}
	@Override
	public String getOpportunityConsumerName(String oppId) {
		// TODO Auto-generated method stub
				String consumerName="";
				try 
				{
					
					String query="select o.opp_id,o.OPP_Name,c.cust_first_name,c.cust_email from opportunity o,customer c where "
					+ "o.OPP_Consumer_ID=c.cust_id and o.OPP_ID=:OPP_ID";

				LOGGER.info("getOpportunityConsumerName:query::"+query);
				
				Map<String, Object> paramMap = new HashMap<String, Object>();
				
				paramMap.put("OPP_ID", oppId);
				List<Map<String, Object>> rows2 = namedParameterJdbcTemplate.queryForList(query, paramMap);
				
				for (Map<String, Object> row : rows2)
				{
					consumerName = String.valueOf(row.get("cust_first_name"));
				}
				
				}catch(Exception e)
				{
					LOGGER.error("getOpportunityConsumerName::Error while getting opp consumer name:::"+e.getMessage());
				}
				
				return consumerName;
	}
	
	@Override
	public void updateOpportunityTitle(String oppId) {
		// TODO Auto-generated method stub
		
		    String query="select OPP_Name from opportunity where OPP_ID=:OPP_ID";
            String oppTitle="";
			LOGGER.info("updateOpportunityTitle:query:"+query);
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("OPP_ID", oppId);
			List<Map<String, Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			for (Map<String, Object> row : rows)
			{
				oppTitle = String.valueOf(row.get("OPP_Name"));
			}
		
			String updatedOppTitle = oppTitle+"-"+oppId;
			if(oppTitle!=null && oppTitle.length()>0)
			{	
				String updatequery = "update opportunity set OPP_Name=? where OPP_ID =?";
				LOGGER.debug("updateOpportunityTitle SQL :" + updatequery);
				Object[] args = new Object[] { updatedOppTitle, oppId};
				int out = jdbcTemplate.update(updatequery, args);
				LOGGER.debug("out :" + out);
			}

		//return null;
	}
	
	@Override
	public String  checkInstallationExists(String oppId,String partnerId)
	{
		String installDate ="";
		String histQuery = "select max(SHOLH_Sequence) seqMax,SHOLH_Status_Id,SHOLH_SH_ID,SHOLH_OPP_ID,SHOLH_Eff_FmDt from stakeholderopplinkhist group by SHOLH_Status_Id,SHOLH_OPP_ID having SHOLH_SH_ID=:SHOL_SH_ID  and SHOLH_OPP_ID=:SHOLH_OPP_ID  order by seqMax";
		Map<String, Object> histparamMap = new HashMap<String, Object>();
		histparamMap.put("SHOL_SH_ID", partnerId);
		histparamMap.put("SHOLH_OPP_ID", oppId);
		List<Map<String, Object>> histRows = namedParameterJdbcTemplate.queryForList(histQuery, histparamMap);
		for (Map<String, Object> histRow : histRows) 
		{	
			LOGGER.debug("checkInstallationExists:::currrent status"+String.valueOf(histRow.get("SHOLH_Status_Id")));
			if((String.valueOf(histRow.get("SHOLH_Status_Id")).equalsIgnoreCase("INSTALLATION")) )
			{	
				installDate = String.valueOf(histRow.get("SHOLH_Eff_FmDt"));
				break;
			}
		}
		return installDate;
	}
	
	
	public boolean updateDesc(String oppId,String desc)
	{
		
		String Query="update opportunity set OPP_Desc=? where OPP_ID=?";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("OPP_ID", oppId);
		paramMap.put("OPP_Desc",desc);
		
		Object[] args = new Object[] { desc, oppId};
		LOGGER.debug("Opp Id :" + oppId);
		LOGGER.debug("Description :" + desc);
		
		LOGGER.debug("updateOpportunity Desc  SQL :" + Query);
		int out = jdbcTemplate.update(Query, args);
		
	
		
		LOGGER.debug("out :" + out);
		if(out==1)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
}

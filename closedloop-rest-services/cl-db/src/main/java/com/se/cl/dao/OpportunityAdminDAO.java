package com.se.cl.dao;

import com.se.cl.model.Opportunities;

public interface OpportunityAdminDAO {
	
	public Opportunities getOpportunities(String country);

}

package com.se.cl.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.se.cl.constants.OppStatus;
import com.se.cl.dao.PartnerDAO;
import com.se.cl.exception.AppException;
import com.se.cl.model.OpportunityImages;
import com.se.cl.model.Partner;
import com.se.cl.model.PartnerDevice;

public class PartnerDAOImpl implements PartnerDAO {

	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	

	private static final Logger LOGGER = LoggerFactory.getLogger(PartnerDAOImpl.class);
	
	private @Value("${accept-sla}") Long acceptSLABy;

	private @Value("${accept-sla-step1}") Long slaStep1;
	private @Value("${accept-sla-step2}") Long slaStep2;
	private @Value("${accept-sla-step3}") Long slaStep3;
	private @Value("${accept-sla-step4}") Long slaStep4;
	
	/*private PlatformTransactionManager transactionManager;

	public void setTransactionManager(
	      PlatformTransactionManager transactionManager) {
	      this.transactionManager = transactionManager;
	 }*/
	
	
	@Override
	public Hashtable<String, String> getPartnerZips(String countryCode) {
		
		Hashtable<String, String> zipList=new Hashtable<String, String>();
		
		String query = "SELECT SH_ID,SH_ZipCode,SH_Country_Code FROM stakeholders where SH_Country_Code=:SH_Country_Code";
		//String query = "SELECT distinct s.SH_ID,SH_ZipCode,SH_Country_Code FROM stakeholders s,stakeholdersdevices d  where s.SH_ID =d.SHD_SH_ID and SH_Country_Code=:SH_Country_Code and SHD_Device_ID is not null";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SH_Country_Code", countryCode);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for(Map<String,Object> row : rows){
			zipList.put(String.valueOf(row.get("SH_ID")),String.valueOf(row.get("SH_Country_Code"))+","+ String.valueOf(row.get("SH_ZipCode")));
			
		}
		
		
		return zipList;
	}



	public void assignOpportunityToPartner() {
		//get the opportunities with status INITIAL
		
		
		
			    
		
	}

	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	@Override
	public void assignOpportunityToPartner(
			HashMap<String, List<String>> oppPartnerList) {
		java.util.Date dt = new java.util.Date();
		final Timestamp timestamp = new Timestamp(dt.getTime());
		// TransactionDefinition def = new DefaultTransactionDefinition();
	    // TransactionStatus transStatus = transactionManager.getTransaction(def);
		Set<Entry<String, List<String>>> entrySet = oppPartnerList.entrySet();
		
		for (Entry<String, List<String>> entry : entrySet) {
		   System.out.println("oppId: " + entry.getKey() + " PartnerList: " + entry.getValue());
			
			
			//get partners matched to the opportunity
		    final String oppIdfinal=entry.getKey().toString(); 
		    
			for(String partnerId:(List<String>)entry.getValue()){
				
				final String partnerIdfinal=partnerId;
			    jdbcTemplate.update("insert into  stakeholderopplink(SHOL_OPP_ID, SHOL_SH_ID, SHOL_Remarks, SHOL_Status,SHOL_Eff_FmDt,SHOL_Eff_ToDt) "
			        + "values (?, ?, ?,?,?,?) ", new PreparedStatementSetter() {
			      public void setValues(PreparedStatement ps) throws SQLException {
			        ps.setString(1, oppIdfinal);
			        ps.setString(2, partnerIdfinal);
			        ps.setString(3, "Auto Matched");
			        ps.setString(4,  OppStatus.INITIAL.name());
			        ps.setTimestamp(5, timestamp);
			        ps.setTimestamp(6, timestamp);
			       
			      }
			    });		
		 
			}
			 //update the opportunity status to INITIAL-- need to transaction attributes later,bothe the above and the below should be in transaction
			
			//move below update to common method 
		    /* String query = "update opportunity set OPP_Status=?,OPP_Last_UpdDt=? where OPP_ID=?";
			 Object[] args = new Object[] {OppStatus.INITIAL.name(),timestamp,oppIdfinal};
			 int out = jdbcTemplate.update(query, args);*/
		
		
		}
		
		 //transactionManager.commit(transStatus);
		
	}



	@Override
	public HashMap<String,List<String>> getPartnerListForNewOpps(String country) {

		List<String> partnerIds=new ArrayList<String>();
		List<String> oppPartnerList=new ArrayList<String>();
		String query = "SELECT OPP_ID FROM opportunity WHERE OPP_New_Notification =:OPP_New_Notification and OPP_Cntry_Cd=:country";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_New_Notification", 0);
		paramMap.put("country", country);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		for(Map<String,Object> row : rows){
			opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
			opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
		}
		
		if (opportunityIds!=null && opportunityIds.size()>0)
		{
		
		
			query = "SELECT SHOL_SH_ID,SHOL_OPP_ID FROM stakeholderopplink WHERE SHOL_OPP_ID IN (:SHOL_OPP_IDs) ";
			LOGGER.debug("SQL :"+query);
			paramMap = new HashMap<String, Object>();
			paramMap.put("SHOL_OPP_IDs", opportunityIds);
			
			 rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			for(Map<String,Object> row : rows){
				partnerIds.add((String.valueOf(row.get("SHOL_SH_ID"))));
				oppPartnerList.add(String.valueOf(row.get("SHOL_OPP_ID"))+"|"+String.valueOf(row.get("SHOL_SH_ID")));
			}
		
		}
		
		
		HashMap<String,List<String>>  oppParnerMap=new HashMap<String,List<String>>();
		
		oppParnerMap.put("PARTNER",partnerIds);
		oppParnerMap.put("OPPORTUNITY",opportunityIdsStr);
		
		oppParnerMap.put("OPP-PARTNER", oppPartnerList);
		
		return oppParnerMap;
	}
	
	
	
	@Override
	public List<PartnerDevice> listPartnerDeviceByIds(List<String> partnerIds) throws AppException {
		
		final String SELECT_PARTNER_DEVICES_QUERY = "SELECT SHD_ID, SHD_SH_ID, SHD_Device_ID, SHD_Device_Type, SHD_Mobile_No, SHD_Eff_FmDt, "
				+ "SHD_Eff_ToDt FROM stakeholdersdevices WHERE SHD_SH_ID IN (:PARTNERIDS)";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("PARTNERIDS", partnerIds);
		
		List<PartnerDevice> deviceList = new ArrayList<PartnerDevice>();
		PartnerDevice device = null;
		
		List<Map<String,Object>> deviceRows = namedParameterJdbcTemplate.queryForList(SELECT_PARTNER_DEVICES_QUERY, paramMap);
		for(Map<String,Object> row : deviceRows){
			
			device = new PartnerDevice();
			device.setPartnerId(Integer.parseInt(String.valueOf(row.get("SHD_SH_ID"))));
			//device.setUserId(String.valueOf(row.get("SHD_SH_ID")));
			device.setDeviceId(String.valueOf(row.get("SHD_Device_ID")));
			device.setMobileNumber(String.valueOf(row.get("SHD_Mobile_No")));
			device.setDeviceType(String.valueOf(row.get("SHD_Device_Type")));
			device.setEffectFromDate(Timestamp.valueOf(String.valueOf(row.get("SHD_Eff_FmDt"))));
			device.setEffectToDate(Timestamp.valueOf(String.valueOf(row.get("SHD_Eff_ToDt"))));
			
			deviceList.add(device);
		}
		
		return deviceList; 
	}



	@Override
	public HashMap<String, List<String>> getPartnerListForSLAReminder(int slaStep,String country) {

		
		List<String> partnerIds=new ArrayList<String>();
		List<String> oppPartnerList=new ArrayList<String>();
		
		String query = "SELECT OPP_ID,TIMESTAMPDIFF(HOUR,  OPP_Eff_FmDt, CONVERT_TZ(sysdate(),'SYSTEM',OPP_Time_Zone)) as diff,OPP_SLA_Notification FROM opportunity WHERE OPP_Status =:OPP_Status and OPP_Cntry_Cd=:country ";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_Status", OppStatus.INITIAL.name());
		paramMap.put("country",country);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		
		Long slaDuration=24L;
		
		if (slaStep==1){
			slaDuration=slaStep1;
		}else if (slaStep==2){
			slaDuration=slaStep2;
		}else  if (slaStep==3){
			slaDuration=slaStep3;
		}else  if (slaStep==4){
			slaDuration=slaStep4;
		}
					
		for(Map<String,Object> row : rows){
			
			String slaNote=String.valueOf(row.get("OPP_SLA_Notification"));
			boolean getOpp=false;
			String[] steps = slaNote.split("\\|");
			String step = steps[slaStep-1]; 
			//String step = steps[(steps.length)-1];
			if(step.equals("0")){
				getOpp=true;
			}
			
			if(getOpp){
				if ((Integer.valueOf(row.get("diff").toString()))> slaDuration){
					opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
					
					opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
				}
			}
			
		}
		
	
		
		if (opportunityIds!=null && opportunityIds.size()>0){
		
		
			 query = "SELECT SHOL_SH_ID,SHOL_OPP_ID FROM stakeholderopplink WHERE SHOL_OPP_ID IN (:SHOL_OPP_IDs) and SHOL_Status <>'DECLINED'";
			LOGGER.debug("SQL :"+query);
			paramMap = new HashMap<String, Object>();
			paramMap.put("SHOL_OPP_IDs", opportunityIds);
		
			rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			for(Map<String,Object> row : rows){
				partnerIds.add(String.valueOf(row.get("SHOL_SH_ID")));
				oppPartnerList.add(String.valueOf(row.get("SHOL_OPP_ID"))+"|"+String.valueOf(row.get("SHOL_SH_ID")));
			}
			
			
		
		}
		
		
		HashMap<String,List<String>>  oppParnerMap=new HashMap<String,List<String>>();
		
		oppParnerMap.put("PARTNER",partnerIds);
		oppParnerMap.put("OPPORTUNITY",opportunityIdsStr);
		oppParnerMap.put("OPP-PARTNER", oppPartnerList);
		return oppParnerMap;
	}



	@Override
	public List<Partner> getPartner(String countryCode) {
		// TODO Auto-generated method stub
		List<Partner> partnerList=new ArrayList<Partner>();
		String query = "SELECT SH_ID, SH_Full_Name,SH_City, SH_State, SH_ZipCode,SH_Phone, SH_User_Name,SH_Email,SH_Country_Code,SH_IS_ADMIN FROM stakeholders where SH_Country_Code=:SH_Country_Code";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SH_Country_Code",countryCode);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for(Map<String,Object> row : rows){
			Partner partner=new Partner();
			partner.setId(String.valueOf(row.get("SH_ID")));
			partner.setName(String.valueOf(row.get("SH_Full_Name")));
			partner.setCity(String.valueOf(row.get("SH_City")));
			partner.setState(String.valueOf(row.get("SH_State")));
			partner.setZip(String.valueOf(row.get("SH_ZipCode")));
			partner.setPhone(String.valueOf(row.get("SH_Phone")));
			partner.setUserId(String.valueOf(row.get("SH_User_Name")));
			partner.setEmail(String.valueOf(row.get("SH_Email")));
			partner.setCountry(String.valueOf(row.get("SH_Country_Code")));
			partner.setAdmin(String.valueOf(row.get("SH_IS_ADMIN")).equals("1")?true:false);
			
			partnerList.add(partner);
		
		}
		
		return partnerList;
	}


	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updatePartner(Partner partner) throws AppException {
		// TODO Auto-generated method stub
		if(partner.getSupplierEmail()==null){
			try{
					 String query="";
					 Object[] args =null;
					if (partner.getUserId()!=null && partner.getUserId().length()>0 ){
					  query = "update stakeholders set SH_User_Name=? where SH_ID=?";
					  args = new Object[] {partner.getUserId(),partner.getId()};
					  jdbcTemplate.update(query, args);
					}
					if(partner.getZip()!=null &&  partner.getZip().length()>0){
						query = "update stakeholders set SH_ZipCode=? where SH_ID=?";
						  args = new Object[] {partner.getZip(),partner.getId()};
						   jdbcTemplate.update(query, args);
						
					}
					if(partner.getEmail()!=null &&  partner.getEmail().length()>0){
						query = "update stakeholders set SH_Email=? where SH_ID=?";
						  args = new Object[] {partner.getEmail(),partner.getId()};
						   jdbcTemplate.update(query, args);
						
					}
		
			}catch(Exception e){
				e.printStackTrace();
			}
		
		}else{
			updateSupplierEmail(partner);
		}
		
	}


	private void updateSupplierEmail(Partner partner) throws AppException {
		// TODO Auto-generated method stub
		if(partner.getSupplierEmail().length()>0 && partner.getUserId()!=null){
			 String query="";
			 Object[] args =null;
			
			query = "SELECT SH_ID from stakeholders WHERE SH_User_Name =:SHOL_USER_ID ";
			LOGGER.debug("SQL :"+query);
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("SHOL_USER_ID", partner.getUserId());
			//paramMap.put("productId", productId);
			
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			
			String partnerId="";

			for(Map<String,Object> row : rows){
				partnerId=((String.valueOf(row.get("SH_ID"))));
			}
			
			if(partnerId.length()<=0){
				throw new AppException(Response.Status.NOT_FOUND.getStatusCode(), 404, "Partner Not found " ,"", null);
			}
		  query = "update stakeholders set SH_SUP_EMAIL=? where SH_ID=?";
		  args = new Object[] {partner.getSupplierEmail(),partnerId};
		  jdbcTemplate.update(query, args);
		  
		}
	   
	}


	@Override
	public List<String> getPartnersWhoAcceptsOppDidNothing() {
		
		
		List<String> partnerIds=new ArrayList<String>();
		String query = "SELECT OPP_ID,TIMESTAMPDIFF(HOUR,  OPP_Eff_FmDt,CONVERT_TZ(sysdate(),'SYSTEM',OPP_Time_Zone)) as diff FROM opportunity WHERE OPP_Status =:OPP_Status ";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_Status", OppStatus.ACCEPTED.name());
		
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		for(Map<String,Object> row : rows){
			if ((Integer.valueOf(row.get("diff").toString()))<1){
				opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
				
				opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
			}
			
			}
		
	/*	if (opportunityIds!=null && opportunityIds.size()>0){
		
		
			 query = "SELECT SHOL_SH_ID,SHOL_OPP_ID FROM stakeholderopplink WHERE SHOL_OPP_ID IN (:SHOL_OPP_IDs) and SHOL_Status <>'DECLINED' and SHOL_Status <>'CLOSED'";
			LOGGER.debug("SQL :"+query);
			//List<WorkflowActivityVO> list = new ArrayList<WorkflowActivityVO>();
			 paramMap = new HashMap<String, Object>();
			 paramMap.put("SHOL_OPP_IDs", opportunityIds);
			//paramMap.put("SHOLH_Status", OppStatus.DECLINED.name());
			
			//paramMap.put("productId", productId);
			
			 rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			for(Map<String,Object> row : rows){
				oppPartnerMap.put((String.valueOf(row.get("SHOL_OPP_ID"))), (String.valueOf(row.get("SHOL_SH_ID"))));
				partnerIds.add((String.valueOf(row.get("SHOL_SH_ID"))));
			}
		
		}*/
		
		//check partner has done anything by looking at history table
		
		if (opportunityIds!=null && opportunityIds.size()>0){
			
			 query = "SELECT SHOLH_SH_ID FROM stakeholderopplinkhist WHERE SHOLH_OPP_ID NOT IN (:SHOL_OPP_IDs)";
				LOGGER.debug("SQL :"+query);
				paramMap = new HashMap<String, Object>();
				paramMap.put("SHOL_OPP_IDs", opportunityIds);
				
				//paramMap.put("productId", productId);
				
				 rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
				for(Map<String,Object> row : rows){
					partnerIds.add((String.valueOf(row.get("SHOL_SH_ID"))));
				}
		}
		
		//return oppParnerMap;
		
		
		
		return partnerIds;
	}


	@Override
	public List<String> getOppWhenNoPartnerAccepted(String country) {
		// TODO Auto-generated method stub
		
		
		String query = "SELECT OPP_ID,TIMESTAMPDIFF(HOUR,  OPP_Eff_FmDt, CONVERT_TZ(sysdate(),'SYSTEM',OPP_Time_Zone)) as diff FROM opportunity WHERE OPP_Status =:OPP_Status and OPP_Cntry_Cd=:country ";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_Status", OppStatus.INITIAL.name());
		paramMap.put("country", country);
		
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		for(Map<String,Object> row : rows){
			
			//if the opp is in initial state until after first SLA that means nobody accepted
			if ((Integer.valueOf(row.get("diff").toString()))>slaStep3){
				opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
				
				opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
			}
			
			}
		
			return opportunityIdsStr;
	}


	@Override
	public List<String> getOppWhenAllPartnerDeclined(String country) {
		List<String> OppIdsForReturn=new ArrayList<String>();
		String query = "SELECT OPP_ID FROM opportunity WHERE OPP_Status =:OPP_Status and OPP_Cntry_Cd=:country";
		LOGGER.debug("SQL :"+query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPP_Status", OppStatus.INITIAL.name());
		paramMap.put("country", country);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		for(Map<String,Object> row : rows){
				opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
				opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
			
			}
		
			
		if (opportunityIds!=null && opportunityIds.size()>0){
			
			 for(String oppId:opportunityIdsStr){
			
				 	query = "SELECT SHOLH_SH_ID,SHOL_Status FROM stakeholderopplinkhist WHERE SHOLH_OPP_ID =:SHOL_OPP_ID";
					LOGGER.debug("SQL :"+query);
					paramMap = new HashMap<String, Object>();
					paramMap.put("SHOL_OPP_ID", oppId);
					
					//paramMap.put("productId", productId);
					
					 rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
					 boolean allDecline=false;
					//String partnerId="";
					for(Map<String,Object> row : rows){
						//partnerId=String.valueOf(row.get("SHOL_SH_ID"));
						if(String.valueOf(row.get("SHOL_Status")).equals(OppStatus.DECLINED.name())){
							allDecline=true;
						}else{
							allDecline=false;
							break;
						}
					}
					
					if(allDecline){
						OppIdsForReturn.add(oppId);
					}
					
			 }
		}
		

		
		return OppIdsForReturn;
	}


	@Override
	public Map<String, String> getCLconfig() {

		Map<String, String> clConfig = new HashMap<String, String>();
		String query = "SELECT CL_FR_PART_OR_SOL30 FROM clconfig";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);


		for (Map<String, Object> row : rows) {

			clConfig.put("CL_FR_PART_OR_SOL30",
					String.valueOf(row.get("CL_FR_PART_OR_SOL30")));

		}

		return clConfig;
	}


	@Override
	public void updateCLconfig(String config) {
		// TODO Auto-generated method stub
		 String query="";
		 Object[] args =null;
	
		  query = "update clconfig set CL_FR_PART_OR_SOL30=?";
		  args = new Object[] {config};
		 jdbcTemplate.update(query, args);
	}



	@Override
	public List<Map<String,String>> assignOpportunityImagesToPartners(String countryCode)
	{
		// TODO Auto-generated method stub
		
		List<String> partnerIds=new ArrayList<String>();
		List<String> oppPartnerList=new ArrayList<String>();
		
		List<Map<String,String>> oppList = new ArrayList<Map<String,String>>();
		    
		
		try 
		{
		
		//check consumer updoladed images in opportunity_artifacts table;
		String partnerId="";
		String query1 = "select OPP_ID,OPP_TITLE from opportunity_artifacts where USER_TYPE=:CONSUMER";
		LOGGER.debug("SQL query1:"+query1);
		Map<String, Object> paramOPPArtifactsMap = new HashMap<String, Object>();
		paramOPPArtifactsMap = new HashMap<String, Object>();
		paramOPPArtifactsMap.put("CONSUMER", "CONSUMER");
		List<Map<String,Object>> artifactRows = namedParameterJdbcTemplate.queryForList(query1, paramOPPArtifactsMap);

		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		for(Map<String,Object> row : artifactRows)
		{
			opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
			opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
			
		}
		String query2="",query3="";
		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (opportunityIds!=null && opportunityIds.size()>0)
		{
		
		
			query2 = "SELECT SHOL_SH_ID,SHOL_OPP_ID FROM stakeholderopplink WHERE SHOL_OPP_ID IN (:SHOL_OPP_IDs) and SHOL_Status=:STATUS";
			LOGGER.debug("SQL :"+query2);
			paramMap = new HashMap<String, Object>();
			paramMap.put("SHOL_OPP_IDs", opportunityIds);
			paramMap.put("STATUS", OppStatus.INITIAL.name());
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query2, paramMap);

			 rows = namedParameterJdbcTemplate.queryForList(query2, paramMap);
			for(final Map<String,Object> row : rows)
			{
				if(checkOpportunityPartner(String.valueOf(row.get("SHOL_SH_ID")),String.valueOf(row.get("SHOL_OPP_ID"))))
				{
					LOGGER.info("Opportunity Id Already Exists for Partner");
					
				}else
				{
					
					Map<String,String> oppPartnersMap = new HashMap<String,String>();
					oppPartnersMap.put("oppId", String.valueOf(row.get("SHOL_OPP_ID")));
					oppPartnersMap.put("partnerId", String.valueOf(row.get("SHOL_SH_ID")));
					oppList.add(oppPartnersMap);
					 jdbcTemplate.update("insert into  opportunity_partner_consumer_images(OPP_ID,SH_ID) "
						        + "values (?, ?) ", new PreparedStatementSetter() {
						      public void setValues(PreparedStatement ps) throws SQLException
						      {
						        ps.setString(1, String.valueOf(row.get("SHOL_OPP_ID")));
						        ps.setString(2, String.valueOf(row.get("SHOL_SH_ID")));
						       
						      }
						    });		
					 
				}
			}
		}
		
		}catch(Exception e)
		{
			e.printStackTrace();
			
			
		}
		return oppList;
	}
	
	
	@Override
	public void unAssignOpportunityImagesToPartners(String countryCode)
	{
		// TODO Auto-generated method stub
		
		List<String> partnerIds=new ArrayList<String>();
		List<String> oppPartnerList=new ArrayList<String>();
		
		//check consumer updoladed images in opportunity_artifacts table;
		String partnerId="";
		String query1 = "select OPP_ID,OPP_TITLE from opportunity_artifacts where USER_TYPE:=CONSUMER";
		Map<String, Object> paramOPPArtifactsMap = new HashMap<String, Object>();
		paramOPPArtifactsMap = new HashMap<String, Object>();
		paramOPPArtifactsMap.put("CONSUMER", "CONSUMER");
		List<Map<String,Object>> artifactRows = namedParameterJdbcTemplate.queryForList(query1, paramOPPArtifactsMap);

		List<Integer> opportunityIds=new ArrayList<Integer>();
		List<String> opportunityIdsStr=new ArrayList<String>();
		for(Map<String,Object> row : artifactRows)
		{
			opportunityIds.add((Integer.valueOf(row.get("OPP_ID").toString())));
			opportunityIdsStr.add((String.valueOf(row.get("OPP_ID"))));
			
		}
		String query2="";
		Map<String, Object> paramMap = new HashMap<String, Object>();

		if (opportunityIds!=null && opportunityIds.size()>0)
		{
		
			query2 = "SELECT SHOL_SH_ID,SHOL_OPP_ID FROM stakeholderopplink WHERE SHOL_OPP_ID IN (:SHOL_OPP_IDs) and SHOL_Status=:STATUS ";
			LOGGER.debug("SQL :"+query2);
			paramMap = new HashMap<String, Object>();
			paramMap.put("SHOL_OPP_IDs", opportunityIds);
			paramMap.put("STATUS", OppStatus.ACCEPTED.name());
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query2, paramMap);
			rows = namedParameterJdbcTemplate.queryForList(query2, paramMap);
			for(final Map<String,Object> row : rows)
			{
				if(checkOpportunityPartner(String.valueOf(row.get("SHOL_SH_ID")),String.valueOf(row.get("SHOL_OPP_ID"))))
				{
					LOGGER.info("Opportunity Id Already Exists for Partner");
					String delete_query = "delete from opportunity_partner_consumer_images where OPP_ID=? and SH_ID not in(?) ";
					 Object[] args = new Object[] {String.valueOf(row.get("SHOL_OPP_ID")),String.valueOf(row.get("SHOL_SH_ID"))};
					 int out = jdbcTemplate.update(delete_query, args);
				    	
				}
			}
		}
	}

	
	public boolean checkOpportunityPartner(String partnerId,String oppId)
	{
		

		boolean recordFound = false;
		String query = "SELECT OPP_ID,SH_ID FROM opportunity_partner_consumer_images WHERE SH_ID =:SH_ID and OPP_ID=:OPP_ID";
		LOGGER.debug("SQL :" + query);
		Map paramMap = new HashMap<String, Object>();
		paramMap.put("SH_ID", partnerId);
		paramMap.put("OPP_ID", oppId);
		
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		for (Map<String, Object> row : rows) 
		{
			if (String.valueOf(row.get("SH_ID"))!=null && String.valueOf(row.get("SH_ID")).length()>0)
			{
				recordFound = true;
				break;
			}

		}
			return recordFound;
		
	}	
	
	@Override
	public HashMap<String,String> getPartnerImsID(String partnerId)
	{
		
		String partnerImsId="";
		String emailId="";
		HashMap<String,String> partnerDetailsMap = new HashMap<String,String>();
		String query = "SELECT SH_IMS_ID,SH_User_Name FROM stakeholders WHERE SH_ID =:SH_ID";
		LOGGER.debug("SQL :" + query);
		Map paramMap = new HashMap<String, Object>();
		paramMap.put("SH_ID", partnerId);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		for (Map<String, Object> row : rows) 
		{
			
			partnerDetailsMap.put("partnerImsId",String.valueOf(row.get("SH_IMS_ID")));
			partnerDetailsMap.put("userId",String.valueOf(row.get("SH_User_Name")));

			
		}	
		
		return partnerDetailsMap;
	}
	@Override
	public List<OpportunityImages> getPartnerImageDetails(String oppId)
	{
		
		OpportunityImages opportunityImages=null;
		List<OpportunityImages> opportunityImagesList = new ArrayList<OpportunityImages>();
		try 
		{
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
			String query = "SELECT * from opportunity_artifacts where OPP_ID=:OPP_ID and user_type='CONSUMER'";
			paramMap.put("OPP_ID", oppId);
			
	    LOGGER.debug("getPartnerImageDetails:::query:::"+query); 
			
		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		for (Map<String, Object> row : rows)
		{
			 opportunityImages = new OpportunityImages();
			 opportunityImages.setOppId(String.valueOf(row.get("OPP_ID")));
			 opportunityImages.setArtifactObjectId(String.valueOf(row.get("ARTIFACT_IMAGE_OBJECT_ID")));
			 opportunityImagesList.add(opportunityImages);
		}
		
		}catch(Exception e)
		{
			LOGGER.info("Exception"+e.getMessage());
		}
		return opportunityImagesList;	
	}
	
	@Override
	public String getPartnerImsFederatedID(String partnerId)
	{
		
		String partnerImsId="";
		String query = "SELECT SH_IMS_ID,SH_User_Name FROM stakeholders WHERE SH_ID =:SH_ID";
		LOGGER.debug("SQL :" + query);
		Map paramMap = new HashMap<String, Object>();
		paramMap.put("SH_ID", partnerId);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		for (Map<String, Object> row : rows) 
		{
			
			partnerImsId = String.valueOf(row.get("SH_IMS_ID"));			
		}	
		
		return partnerImsId;
	}

	@Override
	public List<String> getPartnerImsFederatedIDsByCountry(String countryCode)
	{
		
		List<String> partnerList = new ArrayList<String>();
		String query = "SELECT SH_IMS_ID,SH_User_Name FROM stakeholders WHERE SH_Country_Code=:countryCode and SH_ID not in(88888)";
		LOGGER.debug("SQL :" + query);
		Map paramMap = new HashMap<String, Object>();
		paramMap.put("countryCode", countryCode);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		for (Map<String, Object> row : rows) 
		{
			if(String.valueOf(row.get("SH_IMS_ID"))!=null && String.valueOf(row.get("SH_IMS_ID")).length()>0)
			{	
				partnerList.add(String.valueOf(row.get("SH_IMS_ID")));
			}
		}	
		
		return partnerList;
	}	
	
}

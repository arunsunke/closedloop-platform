package com.se.cl.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.se.cl.dao.OpportunityBFODAO;
import com.se.cl.model.Address;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerDetails;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityBFOHistory;
import com.se.cl.model.OpportunityHist;
import com.se.cl.model.Question;
import com.se.cl.model.QuestionBFO;

public class OpportunityBFODAOImpl implements OpportunityBFODAO{

	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(OpportunityDAOImpl.class);

	/*private PlatformTransactionManager transactionManager;

	
	public void setTransactionManager(
	      PlatformTransactionManager transactionManager) {
	      this.transactionManager = transactionManager;
	 }*/
	@Override
	public List<Opportunity> getPendinOpportunities(String country) {
		
				List<Opportunity> opportunityList=new ArrayList<Opportunity>();
				String query="select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
						+ " OPP_ZipCode,OPP_Cntry_Cd, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt,  OPP_Eff_FmDt, OPP_Eff_ToDt,cust_first_name,cust_last_name,"
						+ "cust_phone,cust_email,addr_street_name,addr_city,addr_zip,addr_cntry_cd from opportunity o,customer c,address a where o.OPP_Consumer_ID=c.cust_id and c.cust_id=a.addr_customer_id and OPP_Cntry_Cd=:country and  OPP_BFO_ID IS NULL ";
					
				LOGGER.debug("SQL :"+query);		
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("country", country);
				//paramMap.put("productId", productId);
				
				List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
				
				for(Map<String,Object> row : rows){
					
					 Opportunity opp=new Opportunity();
					 HashMap<String,String> histMap=new HashMap<String,String>();
					
					opp.setId(String.valueOf(row.get("OPP_ID")));
					opp.setTitle(String.valueOf(row.get("OPP_Name")));
					opp.setShortDesc(String.valueOf(row.get("OPP_Short_Desc")));
					//
					opp.setBudget(String.valueOf(row.get("OPP_Budget")));
					
					opp.setOpportunityDate(String.valueOf(row.get("OPP_Eff_FmDt")));
					opp.setOpportunityExpDate(String.valueOf(row.get("OPP_Eff_ToDt")));
					opp.setBusinessType(String.valueOf(row.get("SHOLH_OPP_ID")));
					opp.setDescription(String.valueOf(row.get("OPP_Desc")));
						Address address=new Address();
						address.setCity(String.valueOf(row.get("OPP_City")));
						address.setSt_name(String.valueOf(row.get("OPP_State")));
						address.setZip(String.valueOf(row.get("OPP_ZipCode")));
						address.setCountryCode(String.valueOf(row.get("OPP_Cntry_Cd")));
					opp.setAddress(address);
					
						CustomerDetails cust=new CustomerDetails();
						cust.setEmail(String.valueOf(row.get("cust_email")));
						cust.setFirstName(String.valueOf(row.get("cust_first_name")));
						cust.setLastName(String.valueOf(row.get("cust_last_name")));
						cust.setPhone(String.valueOf(row.get("cust_phone")));
						
						Address caddress=new Address();
						caddress.setCity(String.valueOf(row.get("addr_city")));
						caddress.setSt_name(String.valueOf(row.get("addr_street_name")));
						caddress.setZip(String.valueOf(row.get("addr_zip")));
						caddress.setCountryCode(String.valueOf(row.get("addr_cntry_cd")));
						cust.setAddress(caddress);
					
					opp.setCustomerDetails(cust);
					
					histMap.put("opportunityStatus",String.valueOf(row.get("OPP_Status")) );
					histMap.put("opportunityStatusUpdatedDate",String.valueOf(row.get("OPP_Last_UpdDt")));
						
					List<HashMap<String,String>> histMapArray=new ArrayList<HashMap<String,String>>();
					histMapArray.add(histMap);
					OpportunityHist opHist=new OpportunityHist();
					
					opHist.setOppHistory(histMapArray);
					opp.setOpportunityHistory(opHist);
					//get bom for opp
					List<BOM> boms=getOpportunityBOM(String.valueOf(row.get("OPP_ID")));
					if (boms!=null){
						opp.setBoms(boms);
					}
					
					//get questions
					List<Question> questions=null;//getOpportunityQs(String.valueOf(row.get("OPP_ID")));
					if(questions!=null){
						opp.setQuestions(questions);
					}
					
					opportunityList.add(opp);
					
				}
					
			
		return opportunityList;
	}
	
	
	@Override
	public List<Opportunity> getPendinOpportunitiesIF(String country) {
		
		
				List<Opportunity> opportunityList=new ArrayList<Opportunity>();
			
				String query="select OPP_Source_type,OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
						+ " OPP_ZipCode,OPP_Cntry_Cd, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt,  OPP_Eff_FmDt, OPP_Eff_ToDt,cust_first_name,cust_last_name,"
						+ "cust_phone,cust_email,addr_street_name,addr_city,addr_zip,addr_cntry_cd from opportunity o,customer c,address a where o.OPP_Consumer_ID=c.cust_id and c.cust_id=a.addr_customer_id and OPP_Cntry_Cd=:country and  OPP_BFO_ID IS NULL and TOTAL_RETRIES <=5";
					
				LOGGER.debug("SQL :"+query);		
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("country", country);
				//paramMap.put("productId", productId);
				
				List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
				
				for(Map<String,Object> row : rows){
					
					 Opportunity opp=new Opportunity();
					 HashMap<String,String> histMap=new HashMap<String,String>();
					
					opp.setId(String.valueOf(row.get("OPP_ID")));
					opp.setTitle(String.valueOf(row.get("OPP_Name")));
					opp.setShortDesc(String.valueOf(row.get("OPP_Short_Desc")));
					//
					opp.setBudget(String.valueOf(row.get("OPP_Budget")));
					
					opp.setOpportunityDate(String.valueOf(row.get("OPP_Eff_FmDt")));
					opp.setOpportunityExpDate(String.valueOf(row.get("OPP_Eff_ToDt")));
					opp.setBusinessType(String.valueOf(row.get("SHOLH_OPP_ID")));
					opp.setDescription(String.valueOf(row.get("OPP_Desc")));
					
					opp.setOppSourceType(String.valueOf(row.get("OPP_Source_type")));
					
					Address address=new Address();
					address.setCity(String.valueOf(row.get("OPP_City")));
					address.setSt_name(String.valueOf(row.get("OPP_State")));
					address.setZip(String.valueOf(row.get("OPP_ZipCode")));
					address.setCountryCode(String.valueOf(row.get("OPP_Cntry_Cd")));
				    opp.setAddress(address);
				
					CustomerDetails cust=new CustomerDetails();
					cust.setEmail(String.valueOf(row.get("cust_email")));
					cust.setFirstName(String.valueOf(row.get("cust_first_name")));
					cust.setLastName(String.valueOf(row.get("cust_last_name")));
					cust.setPhone(String.valueOf(row.get("cust_phone")));
					
					Address caddress=new Address();
					caddress.setCity(String.valueOf(row.get("addr_city")));
					caddress.setSt_name(String.valueOf(row.get("addr_street_name")));
					caddress.setZip(String.valueOf(row.get("addr_zip")));
					caddress.setCountryCode(String.valueOf(row.get("addr_cntry_cd")));
					cust.setAddress(caddress);
					
					opp.setCustomerDetails(cust);
					
					histMap.put("opportunityStatus",String.valueOf(row.get("OPP_Status")) );
					histMap.put("opportunityStatusUpdatedDate",String.valueOf(row.get("OPP_Last_UpdDt")));
						
					List<HashMap<String,String>> histMapArray=new ArrayList<HashMap<String,String>>();
					histMapArray.add(histMap);
					OpportunityHist opHist=new OpportunityHist();
					
					opHist.setOppHistory(histMapArray);
					opp.setOpportunityHistory(opHist);
					//get bom for opp
					List<BOM> boms=getOpportunityBOM(String.valueOf(row.get("OPP_ID")));
					if (boms!=null){
						opp.setBoms(boms);
					}
					
					//get questions
					List<Question> questions=getOpportunityQsIF(String.valueOf(row.get("OPP_ID")));
					if(questions!=null){
						opp.setQuestions(questions);
					}
					
					opportunityList.add(opp);
					
				}
					
			
		return opportunityList;
	}

		
	/*private List<Question> getOpportunityQs(String oppId) {

		  String sql = "SELECT OPP_ID, Q_ID,Q_QID, Q_DESC, Q_ANSWER, Q_ANSWERT_TYP from opportunityquestions WHERE OPP_ID =:OPP_ID";
		  
		  Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("OPP_ID", oppId);
			//paramMap.put("productId", productId);
			
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);
			
			List<Question> questions=new ArrayList<Question>();

			for(Map<String,Object> row : rows)
			{
				Question question=new Question();				
				
				question.setQid(String.valueOf(row.get("Q_QID")));
				question.setId(String.valueOf(row.get("Q_ID")));
				question.setValue(String.valueOf(row.get("Q_ANSWER")));
				
				questions.add(question);
				
			}
			
		  return questions;
		
	}*/
	
	
	/*private List<Question> getOpportunityQs(String oppId) {

	  String sql = "SELECT OPP_ID, Q_ID,Q_QID, Q_DESC, Q_ANSWER, Q_ANSWERT_TYP from opportunityquestions WHERE OPP_ID =:OPP_ID";
	  LOGGER.info("getOpportunityQs:::sql:::"+sql);
	  LOGGER.info("getOpportunityQs:::oppId:::"+oppId);
	  String sqlBfo="";
	  Map<String, Object> paramMap = new HashMap<String, Object>();
	  Map<String, Object> paramMapBFO = new HashMap<String, Object>();
		paramMap.put("OPP_ID", oppId);
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);		
		List<Question> questions=new ArrayList<Question>();
		for(Map<String,Object> row : rows)
		{
			Question question=new Question();
			//question.setQid(String.valueOf(row.get("Q_QID")));
			//question.setId(String.valueOf(row.get("Q_ID")));
			//question.setValue(String.valueOf(row.get("Q_ANSWER")));
			
			sqlBfo = "SELECT BFO_QUESTION_ID, BFO_ANSWER_ID, ANSWER_TYPE from questions_answers_bfo_mapping WHERE Q_ID =:Q_ID AND Q_QID=:Q_QID";
			LOGGER.info("getOpportunityQs:::sqlBFO:::"+sqlBfo);
			paramMapBFO.put("Q_ID", String.valueOf(row.get("Q_ID")));
			paramMapBFO.put("Q_QID", String.valueOf(row.get("Q_QID")));
			List<Map<String,Object>> rowsBFO = namedParameterJdbcTemplate.queryForList(sqlBfo, paramMapBFO);
			for(Map<String,Object> rowbfo : rowsBFO)
			{
				question.setId(String.valueOf(rowbfo.get("BFO_QUESTION_ID")));
				question.setAnsId(String.valueOf(rowbfo.get("BFO_ANSWER_ID")));
				question.setValue(String.valueOf(row.get("Q_ANSWER")));
				question.setAnsType(String.valueOf(rowbfo.get("ANSWER_TYPE")));
				
				LOGGER.info("getOpportunityQs:::BFO_QUESTION_ID:::"+String.valueOf(rowbfo.get("BFO_QUESTION_ID")));
			    LOGGER.info("getOpportunityQs:::BFO_ANSWER_ID:::"+String.valueOf(rowbfo.get("BFO_ANSWER_ID")));
			    LOGGER.info("getOpportunityQs:::Q_ANSWER:::"+String.valueOf(row.get("Q_ANSWER")));
   			    LOGGER.info("getOpportunityQs:::ANSWER_TYPE:::"+String.valueOf(rowbfo.get("ANSWER_TYPE")));

			}
			
			questions.add(question);
			
		}
		
	  return questions;
	
}*/
	
	private List<Question> getOpportunityQs(String oppId) {

		  String sql = "SELECT OPP_ID, Q_ID,Q_QID, Q_DESC, Q_ANSWER, Q_ANSWERT_TYP from opportunityquestions WHERE OPP_ID =:OPP_ID";
		  LOGGER.info("getOpportunityQs:::sql:::"+sql);
		  LOGGER.info("getOpportunityQs:::oppId:::"+oppId);
		  String sqlBfo="";
		  String ANSWER_ID="";
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  Map<String, Object> paramMapBFO = new HashMap<String, Object>();
			paramMap.put("OPP_ID", oppId);
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);		
			List<Question> questions=new ArrayList<Question>();
			for(Map<String,Object> row : rows)
			{
				Question question=new Question();
				//question.setQid(String.valueOf(row.get("Q_QID")));
				//question.setId(String.valueOf(row.get("Q_ID")));
				//question.setValue(String.valueOf(row.get("Q_ANSWER")));
				
				ArrayList<String> ansArray = new ArrayList<String>();
				for(int i=0; i<30;i++)
				{
					ansArray.add(Integer.toString(i));
				}
				
				ANSWER_ID = String.valueOf(row.get("Q_ANSWER"));
				LOGGER.info("getOpportunityQs:::ANSWER_ID:::"+ANSWER_ID);

				if(ansArray!=null && ansArray.contains(ANSWER_ID))
				{	
					sqlBfo = "SELECT BFO_QUESTION_ID, BFO_ANSWER_ID, ANSWER_TYPE from questions_answers_bfo_mapping WHERE Q_ID =:Q_ID AND Q_QID=:Q_QID";
					LOGGER.info("getOpportunityQs:::sqlBFO:::"+sqlBfo);
					paramMapBFO.put("Q_ID", String.valueOf(row.get("Q_ID")));
					paramMapBFO.put("Q_QID", String.valueOf(row.get("Q_QID")));
				}
				else
				{
					sqlBfo = "SELECT BFO_QUESTION_ID, BFO_ANSWER_ID, ANSWER_TYPE from questions_answers_bfo_mapping WHERE Q_QID =:Q_QID AND ANSWER_ID=:ANSWER_ID";
					LOGGER.info("getOpportunityQs:::sqlBFO:::"+sqlBfo);
					paramMapBFO.put("Q_QID", String.valueOf(row.get("Q_QID")));
					paramMapBFO.put("ANSWER_ID", ANSWER_ID);
				}
				List<Map<String,Object>> rowsBFO = namedParameterJdbcTemplate.queryForList(sqlBfo, paramMapBFO);
				for(Map<String,Object> rowbfo : rowsBFO)
				{
					question.setId(String.valueOf(rowbfo.get("BFO_QUESTION_ID")));
					question.setAnsId(String.valueOf(rowbfo.get("BFO_ANSWER_ID")));
					question.setValue(String.valueOf(row.get("Q_ANSWER")));
					question.setAnsType(String.valueOf(rowbfo.get("ANSWER_TYPE")));
					
					LOGGER.info("getOpportunityQs:::BFO_QUESTION_ID:::"+String.valueOf(rowbfo.get("BFO_QUESTION_ID")));
				    LOGGER.info("getOpportunityQs:::BFO_ANSWER_ID:::"+String.valueOf(rowbfo.get("BFO_ANSWER_ID")));
				    LOGGER.info("getOpportunityQs:::Q_ANSWER:::"+String.valueOf(row.get("Q_ANSWER")));
	   			    LOGGER.info("getOpportunityQs:::ANSWER_TYPE:::"+String.valueOf(rowbfo.get("ANSWER_TYPE")));

				}
				
				questions.add(question);
				
			}
			
		  return questions;
		
	}
	
	private List<Question> getOpportunityQsIF(String oppId) {

		  String sql = "SELECT OPP_ID, Q_ID,Q_QID, Q_DESC, Q_ANSWER, Q_ANSWERT_TYP from opportunityquestions WHERE OPP_ID =:OPP_ID";
		  LOGGER.info("getOpportunityQs:::sql:::"+sql);
		  LOGGER.info("getOpportunityQs:::oppId:::"+oppId);
		  String sqlBfo="";
		  String ANSWER_ID="";
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  Map<String, Object> paramMapBFO = new HashMap<String, Object>();
			paramMap.put("OPP_ID", oppId);
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);		
			List<Question> questions=new ArrayList<Question>();
			for(Map<String,Object> row : rows)
			{
				Question question=null;
				//question.setQid(String.valueOf(row.get("Q_QID")));
				//question.setId(String.valueOf(row.get("Q_ID")));
				//question.setValue(String.valueOf(row.get("Q_ANSWER")));
				
				ArrayList<String> ansArray = new ArrayList<String>();
				for(int i=0; i<30;i++)
				{
					ansArray.add(Integer.toString(i));
				}
				
				ANSWER_ID = String.valueOf(row.get("Q_ANSWER"));
				LOGGER.info("getOpportunityQs:::ANSWER_ID:::"+ANSWER_ID);

				if(ansArray!=null && ansArray.contains(ANSWER_ID))
				{	
					sqlBfo = "SELECT BFO_QUESTION_ID_SEREFERENCE, BFO_ANSWER_ID_SEREFERENCE, ANSWER_TYPE from questions_answers_bfo_mapping WHERE Q_ID =:Q_ID AND Q_QID=:Q_QID";
					LOGGER.info("getOpportunityQs:::sqlBFO:::"+sqlBfo);
					paramMapBFO.put("Q_ID", String.valueOf(row.get("Q_ID")));
					paramMapBFO.put("Q_QID", String.valueOf(row.get("Q_QID")));
				}
				else
				{
					sqlBfo = "SELECT BFO_QUESTION_ID_SEREFERENCE, BFO_ANSWER_ID_SEREFERENCE, ANSWER_TYPE from questions_answers_bfo_mapping WHERE Q_QID =:Q_QID AND ANSWER_ID=:ANSWER_ID";
					LOGGER.info("getOpportunityQs:::sqlBFO:::"+sqlBfo);
					paramMapBFO.put("Q_QID", String.valueOf(row.get("Q_QID")));
					paramMapBFO.put("ANSWER_ID", ANSWER_ID);
				}
				List<Map<String,Object>> rowsBFO = namedParameterJdbcTemplate.queryForList(sqlBfo, paramMapBFO);
				question=new Question();
				for(Map<String,Object> rowbfo : rowsBFO)
				{
					question.setId(String.valueOf(rowbfo.get("BFO_QUESTION_ID_SEREFERENCE")));
					question.setAnsId(String.valueOf(rowbfo.get("BFO_ANSWER_ID_SEREFERENCE")));
					question.setValue(String.valueOf(row.get("Q_ANSWER")));
					question.setAnsType(String.valueOf(rowbfo.get("ANSWER_TYPE")));					
					LOGGER.info("getOpportunityQs:::BFO_QUESTION_ID:::"+String.valueOf(rowbfo.get("BFO_QUESTION_ID_SEREFERENCE")));
				    LOGGER.info("getOpportunityQs:::BFO_ANSWER_ID:::"+String.valueOf(rowbfo.get("BFO_ANSWER_ID_SEREFERENCE")));
				    LOGGER.info("getOpportunityQs:::Q_ANSWER:::"+String.valueOf(row.get("Q_ANSWER")));
	   			    LOGGER.info("getOpportunityQs:::ANSWER_TYPE:::"+String.valueOf(rowbfo.get("ANSWER_TYPE")));

				}
				
				questions.add(question);

			}
			
		  return questions;
		
	}
	
	/*  
	    Method to get the Bfo Questions and Answers List
	 
	private List<QuestionBFO> getOpportunityBFOQs(String oppId) {

		  String sql = "SELECT OPP_ID, Q_ID,Q_QID, Q_DESC, Q_ANSWER, Q_ANSWERT_TYP from questions_answers_bfo_mapping WHERE OPP_ID =:OPP_ID";
		  
		  Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("OPP_ID", oppId);
			//paramMap.put("productId", productId);
			
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);
			
			List<QuestionBFO> questionsbfolist =new ArrayList<QuestionBFO>();

			for(Map<String,Object> row : rows){
				QuestionBFO questionbfo=new QuestionBFO();
				
				questionbfo.setQid(String.valueOf(row.get("Q_ID")));
				questionbfo.setQqid(String.valueOf(row.get("Q_QID")));
				questionbfo.setBfoQuestionId(String.valueOf(row.get("BFO_QUESTION_ID")));
				questionbfo.setBfoAnswerId(String.valueOf(row.get("BFO_ANSWER_ID")));
				questionbfo.setAnsType(String.valueOf(row.get("ANSWER_TYPE")));
				questionsbfolist.add(questionbfo);
				
			}
			
		  return questionsbfolist;
		
	}*/
	
	
	
	private  List<BOM> getOpportunityBOM(String oppId) {
		

		  String sql = "SELECT  OBOM_Category, OBOM_Prod_ID, OBOM_Name, OBOM_Desc,"
		  		+ " OBOM_Tot_Qty, OBOM_Tot_Cost, OBOM_Currency_Cd from opportunitybom WHERE OBOM_OPP_ID =:OBOM_OPP_ID";
		  //InputStream is= jdbcTemplate.queryForObject(sql, new FileMapper());
		  Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("OBOM_OPP_ID", oppId);
			List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);
			
			List<BOM> boms=new ArrayList<BOM>();

			for(Map<String,Object> row : rows){
				BOM bom=new BOM();
				bom.setCategory(String.valueOf(row.get("OBOM_Category")));
				bom.setCurrencyCode(String.valueOf(row.get("OBOM_Currency_Cd")));
				bom.setProductId(String.valueOf(row.get("OBOM_Prod_ID")));
				bom.setQuantity(Integer.parseInt(row.get("OBOM_Tot_Qty").toString()));
				bom.setUnitCost(Float.parseFloat(row.get("OBOM_Tot_Cost").toString()));
				
				boms.add(bom);
				
			}
			
		  return boms;
		



}
	

	@Override
	public List<OpportunityBFOHistory> getPendingOppStatus(String oppStatus,String country) {
		
		
		List<OpportunityBFOHistory> oppHistory=new ArrayList<OpportunityBFOHistory>();
		
		String query="select  l.SHOL_ID,SHOL_OPP_ID, l.SHOL_Status, SHOL_Remarks,"
				+ "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,o.OPP_BFO_ID "
				+ "from stakeholderopplink l,opportunity o,stakeholders s where l.SHOL_OPP_ID=o.OPP_ID AND l.SHOL_SH_ID=s.SH_ID AND"
				+ " l.SHOL_BFO_STATUS=0 AND l.SHOL_Status ='"+oppStatus+"'  AND o.OPP_Cntry_Cd='"+country+"'" ;
		
		LOGGER.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		
		 
		for(Map<String,Object> row : rows){
			
			OpportunityBFOHistory hist=new OpportunityBFOHistory();
			 HashMap<String,String> histMap=new HashMap<String,String>();
			
			 hist.setHistSeqId(String.valueOf(row.get("SHOL_ID")));
			 hist.setOppId(String.valueOf(row.get("SHOL_OPP_ID")));
			 //hist.setPartnerId(String.valueOf(row.get("SHOLH_SH_ID")));
			 hist.setReason(String.valueOf(row.get("SHOL_Remarks")));
			 hist.setOppStatus(String.valueOf(row.get("SHOL_Status")));
			 hist.setBfoAccountCd(String.valueOf(row.get("SH_BFO_ACC_ID")));
			 hist.setBfoAccountRole(null);
			 hist.setBfoContactCd(String.valueOf(row.get("SH_BFO_CNT_ID")));
			 hist.setBfoContractorRole(null);
			 hist.setBfoOppId(String.valueOf(row.get("OPP_BFO_ID")));
			 
			 oppHistory.add(hist);
			 
			 
		}
		
		
		
		return oppHistory;
	}
	
	@Override
	public List<OpportunityBFOHistory> getPendingOppStatusIF(String oppStatus,String country) {
		
		
		List<OpportunityBFOHistory> oppHistory=new ArrayList<OpportunityBFOHistory>();
		
		String query="select  l.SHOL_ID,SHOL_OPP_ID, l.SHOL_Status, SHOL_Remarks,"
				+ "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,o.OPP_BFO_ID "
				+ "from stakeholderopplink l,opportunity o,stakeholders s where l.SHOL_OPP_ID=o.OPP_ID AND l.SHOL_SH_ID=s.SH_ID AND"
				+ " l.SHOL_BFO_STATUS=0 AND l.SHOL_Status ='"+oppStatus+"'  AND o.OPP_Cntry_Cd='"+country+"' AND l.TOTAL_RETRIES <=5" ;
		//AND SHOL_SH_ID<>99999
		LOGGER.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		
		 
		for(Map<String,Object> row : rows){
			
			OpportunityBFOHistory hist=new OpportunityBFOHistory();
			 HashMap<String,String> histMap=new HashMap<String,String>();
			
			 hist.setHistSeqId(String.valueOf(row.get("SHOL_ID")));
			 hist.setOppId(String.valueOf(row.get("SHOL_OPP_ID")));
			 //hist.setPartnerId(String.valueOf(row.get("SHOLH_SH_ID")));
			 hist.setReason(String.valueOf(row.get("SHOL_Remarks")));
			 hist.setOppStatus(String.valueOf(row.get("SHOL_Status")));
			 hist.setBfoAccountCd(String.valueOf(row.get("SH_BFO_ACC_ID")));
			 hist.setBfoAccountRole(null);
			 hist.setBfoContactCd(String.valueOf(row.get("SH_BFO_CNT_ID")));
			 hist.setBfoContractorRole(null);
			 hist.setBfoOppId(String.valueOf(row.get("OPP_BFO_ID")));
			 
			 oppHistory.add(hist);
			 
			 
		}
		
		
		
		return oppHistory;
	}

	@Override
	public List<OpportunityBFOHistory> getPendingOppHistoryStatus(String statusId,String country) {
		
	String query="";	
	List<OpportunityBFOHistory> oppHistory=new ArrayList<OpportunityBFOHistory>();
		
	/*	String query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
				+ "c.OPP_BFO_Contact_id,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,b.OBOM_Currency_Cd "
				+ "from stakeholderopplinkhist l,opportunity o,customer c,opportunitybom b  where o.opp_id=b.OBOM_OPP_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND   l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id";*/
	
	if(statusId!=null && statusId.equalsIgnoreCase("WON"))
	{
		query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
                + "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,o.SH_BFO_VCP_ID,s.SH_ID,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,b.OBOM_Currency_Cd "
                + "from stakeholderopplinkhist l,opportunity o,customer c,opportunitybom b,stakeholders s,stakeholderopplink ll  where ll.SHOL_OPP_ID=o.OPP_ID AND ll.SHOL_SH_ID=s.SH_ID and o.opp_id=b.OBOM_OPP_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND ll.SHOL_Status='ACCEPTED' AND l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id AND o.OPP_Cntry_Cd='"+country+"'";
    		
	}
    else		
	{ 
	 
    	query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
                + "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,o.SH_BFO_VCP_ID,s.SH_ID,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,b.OBOM_Currency_Cd "
                + "from stakeholderopplinkhist l,opportunity o,customer c,opportunitybom b,stakeholders s,stakeholderopplink ll  where ll.SHOL_OPP_ID=o.OPP_ID AND ll.SHOL_SH_ID=s.SH_ID and o.opp_id=b.OBOM_OPP_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND   l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id AND o.OPP_Cntry_Cd='"+country+"'";
    	
	 }
			
		LOGGER.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_BFO_Status_Id", statusId);
		//paramMap.put("productId", productId);
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		
		 
		for(Map<String,Object> row : rows){
			
			OpportunityBFOHistory hist=new OpportunityBFOHistory();
			 HashMap<String,String> histMap=new HashMap<String,String>();
			
			 hist.setHistSeqId(String.valueOf(row.get("SHOLH_Sequence")));
			 hist.setOppId(String.valueOf(row.get("SHOLH_OPP_ID")));
			 hist.setReason(String.valueOf(row.get("SHOLH_Remarks")));
			 hist.setOppStatus(String.valueOf(row.get("SHOLH_Status")));
			 hist.setBfoAccountCd(String.valueOf(row.get("SH_BFO_ACC_ID")));
			 hist.setBfoAccountRole(null);
			 hist.setBfoContactCd(String.valueOf(row.get("SH_BFO_CNT_ID")));
			 hist.setBfoContractorRole(null);
			 hist.setBfoOppId(String.valueOf(row.get("OPP_BFO_ID")));
			 hist.setStartDate(String.valueOf(row.get("SHOLH_Eff_FmDt")));
			 hist.setEndDate(String.valueOf(row.get("SHOLH_Eff_ToDt")));
			 hist.setCurrencyCode(String.valueOf(row.get("OBOM_Currency_Cd")));
			 hist.setBfoValueChainPlayerId(String.valueOf(row.get("SH_BFO_VCP_ID")));
			 hist.setShId(String.valueOf(row.get("SH_ID")));;
			 
			 oppHistory.add(hist);
			 
			 
		}
		
		
		
		return oppHistory;
	}
	
	@Override
	public List<OpportunityBFOHistory> getPendingOppHistoryStatusIF(String statusId,String country) {
		
	String query="";	
	List<OpportunityBFOHistory> oppHistory=new ArrayList<OpportunityBFOHistory>();
		
	/*	String query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
				+ "c.OPP_BFO_Contact_id,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,b.OBOM_Currency_Cd "
				+ "from stakeholderopplinkhist l,opportunity o,customer c,opportunitybom b  where o.opp_id=b.OBOM_OPP_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND   l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id";*/
	
	if(statusId!=null && statusId.equalsIgnoreCase("WON"))
	{
		query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
                + "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,s.SH_BFO_VCP_ID,s.SH_ID,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,b.OBOM_Currency_Cd "
                + "from stakeholderopplinkhist l,opportunity o,customer c,opportunitybom b,stakeholders s,stakeholderopplink ll  where ll.SHOL_OPP_ID=o.OPP_ID AND ll.SHOL_SH_ID=s.SH_ID and o.opp_id=b.OBOM_OPP_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND ll.SHOL_Status='ACCEPTED' AND l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id AND o.OPP_Cntry_Cd='"+country+"' AND l.TOTAL_RETRIES <=5";
    		
	}
    else		
	{ 
	 
    	query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
                + "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,s.SH_BFO_VCP_ID,s.SH_ID,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,b.OBOM_Currency_Cd "
                + "from stakeholderopplinkhist l,opportunity o,customer c,opportunitybom b,stakeholders s,stakeholderopplink ll  where ll.SHOL_OPP_ID=o.OPP_ID AND ll.SHOL_SH_ID=s.SH_ID and o.opp_id=b.OBOM_OPP_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND   l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id AND o.OPP_Cntry_Cd='"+country+"' AND l.TOTAL_RETRIES <=5";
    	
	 }
			
		LOGGER.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_BFO_Status_Id", statusId);
		//paramMap.put("productId", productId);
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		
		 
		for(Map<String,Object> row : rows){
			
			OpportunityBFOHistory hist=new OpportunityBFOHistory();
			 HashMap<String,String> histMap=new HashMap<String,String>();
			
			 hist.setHistSeqId(String.valueOf(row.get("SHOLH_Sequence")));
			 hist.setOppId(String.valueOf(row.get("SHOLH_OPP_ID")));
			 hist.setReason(String.valueOf(row.get("SHOLH_Remarks")));
			 hist.setOppStatus(String.valueOf(row.get("SHOLH_Status")));
			 hist.setBfoAccountCd(String.valueOf(row.get("SH_BFO_ACC_ID")));
			 hist.setBfoAccountRole(null);
			 hist.setBfoContactCd(String.valueOf(row.get("SH_BFO_CNT_ID")));
			 hist.setBfoContractorRole(null);
			 hist.setBfoOppId(String.valueOf(row.get("OPP_BFO_ID")));
			 hist.setStartDate(String.valueOf(row.get("SHOLH_Eff_FmDt")));
			 hist.setEndDate(String.valueOf(row.get("SHOLH_Eff_ToDt")));
			 hist.setCurrencyCode(String.valueOf(row.get("OBOM_Currency_Cd")));
			 hist.setBfoValueChainPlayerId(String.valueOf(row.get("SH_BFO_VCP_ID")));
			 hist.setShId(String.valueOf(row.get("SH_ID")));;
			 
			 oppHistory.add(hist);
			 
			 
		}
		
		
		
		return oppHistory;
	}
	
	
	@Override
	public List<OpportunityBFOHistory> getPendingOppHistoryStatusForInstallation(String statusId,String country) {
		
	String query="";
	int recordCount=0;
	List<OpportunityBFOHistory> oppHistory=new ArrayList<OpportunityBFOHistory>();
	
	
	if(statusId!=null && statusId.equalsIgnoreCase("INSTALLATION"))
	{
		query="select SHOLH_Sequence,SHOLH_OPP_ID, SHOLH_Remarks, SHOLH_Status,SHOLH_Eff_FmDt,SHOLH_Eff_ToDt,"
                + "s.SH_BFO_CNT_ID,s.SH_BFO_ACC_ID,s.SH_BFO_VCP_ID,s.SH_ID,c.OPP_BFO_Acc_Code,o.OPP_BFO_ID,c.OPP_BFO_CUST_VISIT_ID "
                + "from stakeholderopplinkhist l,opportunity o,customer c,stakeholders s,stakeholderopplink ll  where ll.SHOL_OPP_ID=o.OPP_ID AND ll.SHOL_SH_ID=s.SH_ID and l.SHOLH_OPP_ID=o.OPP_ID and o.OPP_Consumer_ID=c.cust_id AND ll.SHOL_Status='ACCEPTED' AND l.SHOLH_UPDATE_BFO=0 and l.SHOLH_Status_Id=:SHOL_BFO_Status_Id AND o.OPP_Cntry_Cd='"+country+"' AND l.TOTAL_RETRIES <=5";
	}
			
		LOGGER.info("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_BFO_Status_Id", statusId);
		//paramMap.put("productId", productId);
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for(Map<String,Object> row : rows){
			
			 OpportunityBFOHistory hist=new OpportunityBFOHistory();
			 HashMap<String,String> histMap=new HashMap<String,String>();
			
			 recordCount = getRecordCount(statusId,String.valueOf(row.get("SHOLH_OPP_ID")));
			 LOGGER.info("getPendingOppHistoryStatusForInstallation:::recordCount"+recordCount);
			 
			 hist.setHistSeqId(String.valueOf(row.get("SHOLH_Sequence")));
			 hist.setOppId(String.valueOf(row.get("SHOLH_OPP_ID")));
			 hist.setReason(String.valueOf(row.get("SHOLH_Remarks")));
			 hist.setOppStatus(String.valueOf(row.get("SHOLH_Status")));
			 hist.setBfoAccountCd(String.valueOf(row.get("SH_BFO_ACC_ID")));
			 hist.setBfoAccountRole(null);
			 hist.setBfoContactCd(String.valueOf(row.get("SH_BFO_CNT_ID")));
			 hist.setBfoContractorRole(null);
			 hist.setBfoOppId(String.valueOf(row.get("OPP_BFO_ID")));
			 hist.setStartDate(String.valueOf(row.get("SHOLH_Eff_FmDt")));
			 hist.setEndDate(String.valueOf(row.get("SHOLH_Eff_ToDt")));
			 hist.setCurrencyCode(String.valueOf(row.get("OBOM_Currency_Cd")));
			 hist.setBfoValueChainPlayerId(String.valueOf(row.get("SH_BFO_VCP_ID")));
			 hist.setShId(String.valueOf(row.get("SH_ID")));
			 hist.setBfoCustAccountId(String.valueOf(row.get("OPP_BFO_Acc_Code")));
			 hist.setRecordCount(recordCount);
			 hist.setBfoCustVisitId(String.valueOf(row.get("OPP_BFO_CUST_VISIT_ID")));
			 oppHistory.add(hist);
			 
		}
	
		
		return oppHistory;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateBFOOppStatus(Map<String,String> bfoResponse , String oppId) {
		// TODO Auto-generated method stub
		
		
		/*TransactionDefinition def = new DefaultTransactionDefinition();
	     TransactionStatus transStatus = transactionManager.getTransaction(def);*/
		try{
		java.util.Date dt = new java.util.Date();
		
		final Timestamp timestamp = new Timestamp(dt.getTime());
		
			String query = "update opportunity set OPP_BFO_ID=? , OPP_Last_UpdDt=? where OPP_ID=?";
			 
		
		 
			Object[] args = new Object[] {bfoResponse.get("oppId"),timestamp,oppId};
		 
			int out = jdbcTemplate.update(query, args);
		 
			
			query = "SELECT OPP_Consumer_ID from opportunity WHERE OPP_ID =:OPP_ID ";
			LOGGER.debug("SQL :"+query);
			HashMap<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("OPP_ID", oppId);
			
			List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			
			String cust_id="";
			for(Map<String,Object> row : rows){
				cust_id=(String.valueOf(row.get("OPP_Consumer_ID")));
			}
		
			
			if(bfoResponse.get("acctId")!=null && bfoResponse.get("contactId")!=null){
				query = "update customer set  OPP_BFO_Acc_Code=? , OPP_BFO_Contact_id=? where cust_id=?";
				args = new Object[] {bfoResponse.get("acctId"),bfoResponse.get("contactId"),cust_id};
			 	out = jdbcTemplate.update(query, args);
				
			}
			
			
			//commit
			// transactionManager.commit(transStatus);
			
			 
		}catch(Exception e){
			e.printStackTrace();
			
			//roll back
			// transactionManager.rollback(transStatus);
			
		}
		
		
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateBFOOppStatusIF(Map<String,String> bfoResponse , String oppId) {
		// TODO Auto-generated method stub
		
		String query="";
		int out=0;
		/*TransactionDefinition def = new DefaultTransactionDefinition();
	     TransactionStatus transStatus = transactionManager.getTransaction(def);*/
		try{
			
			if(bfoResponse!=null)
			{
			    java.util.Date dt = new java.util.Date();
			
			    final Timestamp timestamp = new Timestamp(dt.getTime());
			
				query = "update opportunity set OPP_BFO_ID=? , OPP_Last_UpdDt=? where OPP_ID=?";
			 
			 
				Object[] args = new Object[] {bfoResponse.get("oppId"),timestamp,oppId};
			 
			    out = jdbcTemplate.update(query, args);
			 
				
				query = "SELECT OPP_Consumer_ID from opportunity WHERE OPP_ID =:OPP_ID ";
				LOGGER.debug("SQL :"+query);
				HashMap<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("OPP_ID", oppId);
				
				List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
				
				String cust_id="";
				for(Map<String,Object> row : rows){
					cust_id=(String.valueOf(row.get("OPP_Consumer_ID")));
				}
			
				if(bfoResponse.get("contactId")!=null){
					query = "update customer set  OPP_BFO_Contact_id=? where cust_id=?";
					args = new Object[] {bfoResponse.get("contactId"),cust_id};
				 	out = jdbcTemplate.update(query, args);
					
				}	
				
				if(bfoResponse.get("acctId")!=null) {
					query = "update customer set  OPP_BFO_Acc_Code=? where cust_id=?";
					args = new Object[] {bfoResponse.get("acctId"),cust_id};
				 	out = jdbcTemplate.update(query, args);
					
				}			
				
			}
			
			String retry_query = "SELECT TOTAL_RETRIES from opportunity WHERE OPP_ID =:OPP_ID ";
			LOGGER.info("updateBFOOppStatus:retry_query:"+retry_query);
			HashMap<String, Object> paramMapRetries = new HashMap<String, Object>();
			paramMapRetries.put("OPP_ID", oppId);
			
			List<Map<String,Object>>  rowsRetries = namedParameterJdbcTemplate.queryForList(retry_query, paramMapRetries);
			
			int totalRetries=0;
			
			for(Map<String,Object> row : rowsRetries)
			{
				totalRetries=(Integer.parseInt(String.valueOf(row.get("TOTAL_RETRIES"))));
				LOGGER.info("updateBFOOppStatus:oppId::"+oppId+"totalRetries::"+totalRetries);
				totalRetries = totalRetries+1;
				query = "update opportunity set TOTAL_RETRIES=? where OPP_ID=?";
				Object[] args1 = new Object[] {totalRetries,oppId};
			 	out = jdbcTemplate.update(query, args1);
			}
			
			//commit
			// transactionManager.commit(transStatus);
			
			 
		}catch(Exception e){
			e.printStackTrace();
			
			//roll back
			// transactionManager.rollback(transStatus);
			
		}
		
		
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateBFOOppHist(boolean bfoResponse , String histSeqId) {
		// TODO Auto-generated method stub
		
		/*TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus transStatus = transactionManager.getTransaction(def);*/
		try {
			
			String query = "update stakeholderopplinkhist set SHOLH_UPDATE_BFO=? where SHOLH_Sequence=?";

			Object[] args = new Object[] { "1",	histSeqId };

			int out = jdbcTemplate.update(query, args);
					// commit
			//transactionManager.commit(transStatus);

		} catch (Exception e) {
			e.printStackTrace();
			// roll back
			//transactionManager.rollback(transStatus);

		}
		
		
		
	} 
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateBFOOppHistIF(boolean bfoResponse , String histSeqId) {
		// TODO Auto-generated method stub
		
		

		/*TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus transStatus = transactionManager.getTransaction(def);*/
		try {
			
			String query = "update stakeholderopplinkhist set SHOLH_UPDATE_BFO=? where SHOLH_Sequence=?";

			Object[] args = new Object[] { "1",	histSeqId };

			int out = jdbcTemplate.update(query, args);
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			String retry_query = "SELECT TOTAL_RETRIES from stakeholderopplinkhist WHERE SHOLH_Sequence =:SHOLH_Sequence";
			LOGGER.info("updateBFOOppHist::retry_query :"+retry_query);
			
			HashMap<String, Object> paramMapRetries = new HashMap<String, Object>();
			
			paramMapRetries.put("SHOLH_Sequence", histSeqId);
			
			List<Map<String,Object>>  rowsRetries = namedParameterJdbcTemplate.queryForList(retry_query, paramMapRetries);
			
			int totalRetries=0;
			
			for(Map<String,Object> row : rowsRetries)
			{
				totalRetries=(Integer.parseInt(String.valueOf(row.get("TOTAL_RETRIES"))));
				LOGGER.info("updateBFOOppHist:histSeqId::"+histSeqId+"totalRetries::"+totalRetries);

				totalRetries = totalRetries+1;
				query = "update stakeholderopplinkhist set TOTAL_RETRIES=? where SHOLH_Sequence=?";
				args = new Object[] {totalRetries,histSeqId};
			 	out = jdbcTemplate.update(query, args);
			}
			
			
			
					// commit
			//transactionManager.commit(transStatus);

		} catch (Exception e) {
			e.printStackTrace();
			// roll back
			//transactionManager.rollback(transStatus);

		}
		
		
		
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateBFOOppAcceptRejectStatus(boolean bfoResponse, String oppId,String seqId) {
		
		
	/*	TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus transStatus = transactionManager.getTransaction(def);*/
		
		try {
			
			String query = "update stakeholderopplink set SHOL_BFO_STATUS=? where SHOL_ID=?";

			Object[] args = new Object[] { "1",	seqId };

			int out = jdbcTemplate.update(query, args);
					// commit
			//transactionManager.commit(transStatus);

		} catch (Exception e) {
			e.printStackTrace();
			// roll back
			//transactionManager.rollback(transStatus);

		}
		
			
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateBFOOppAcceptRejectStatusIF(boolean bfoResponse, String oppId,String seqId,String responseData) {
		
		String partnerId="";
		try {
			
			if(bfoResponse)
			{	
				String query = "update stakeholderopplink set SHOL_BFO_STATUS=? where SHOL_ID=?";
				Object[] args = new Object[] { "1",	seqId };
				jdbcTemplate.update(query, args);
				LOGGER.info("responseData::::::"+responseData);
			}
			if(responseData!=null && responseData.length()>0)
			{
				partnerId = getPartnerId(seqId);
			    LOGGER.info("partnerId:::::::::"+partnerId);
				if(partnerId!=null && partnerId.length()>0)
				{
					
					/*
					String shupdatequery = "update stakeholders set SH_BFO_VCP_ID=? where SH_ID=?";
					Object[] shdata = new Object[] { responseData,partnerId};
					jdbcTemplate.update(shupdatequery, shdata);
					*/
					
					String shupdatequery = "update opportunity set SH_BFO_VCP_ID=? where OPP_BFO_ID=?";
					Object[] shdata = new Object[] { responseData,oppId};
					jdbcTemplate.update(shupdatequery, shdata);
					
				}	
			}
			
            Map<String, Object> paramMap = new HashMap<String, Object>();
			
			String retry_query = "SELECT TOTAL_RETRIES from stakeholderopplink WHERE SHOL_ID =:SHOL_ID";
			
			LOGGER.info("updateBFOOppAcceptRejectStatusIF::retry_query :"+retry_query);
			
			HashMap<String, Object> paramMapRetries = new HashMap<String, Object>();
			
			paramMapRetries.put("SHOL_ID", seqId);
			
			List<Map<String,Object>>  rowsRetries = namedParameterJdbcTemplate.queryForList(retry_query, paramMapRetries);
			
			int totalRetries=0;
			int out =0;
			
			for(Map<String,Object> row : rowsRetries)
			{
				totalRetries=(Integer.parseInt(String.valueOf(row.get("TOTAL_RETRIES"))));
				LOGGER.info("updateBFOOppAcceptRejectStatusIF:seqId::"+seqId+"totalRetries::"+totalRetries);
				totalRetries = totalRetries+1;
				String query = "update stakeholderopplink set TOTAL_RETRIES=? where SHOL_ID=?";
				Object[] args1 = new Object[] {totalRetries,seqId};
			 	out = jdbcTemplate.update(query, args1);
			}
	
		} catch (Exception e) {
			e.printStackTrace();
	
		}
			
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	public String getPartnerId(String SHOL_Sequence) {
		// TODO Auto-generated method stub
		
		String query = "SELECT SHOL_SH_ID from stakeholderopplink WHERE SHOL_ID =:SHOL_ID";
		LOGGER.debug("SQL :" + query);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOL_ID", SHOL_Sequence);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		String partnerId = null;

		for (Map<String, Object> row : rows) {
			partnerId = String.valueOf(row
					.get("SHOL_SH_ID"));
		}

		return partnerId;
	}


	@Override
	public String getBfoOppId(String seqId, String tableReference) 
	{
		// TODO Auto-generated method stub
		String oppId="";
		
		String query="";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		LOGGER.info("getBfoOppId:seqId"+seqId);
		LOGGER.info("getBfoOppId:tableReference"+tableReference);

		try
		{
		
			if(tableReference!=null && tableReference.equalsIgnoreCase("OPPLINK"))
			{
				query = "SELECT OPP_BFO_ID from opportunity o,stakeholderopplink l WHERE o.OPP_ID=l.SHOL_OPP_ID and l.SHOL_ID =:SHOL_ID";
			
				paramMap.put("SHOL_ID", seqId);
	
			}else if(tableReference!=null && tableReference.equalsIgnoreCase("OPPHISTORY"))	
			{
				query = "SELECT OPP_BFO_ID from opportunity o,stakeholderopplinkhist h WHERE o.OPP_ID=h.SHOLH_OPP_ID and h.SHOLH_Sequence =:SHOLH_Sequence";
				
				paramMap.put("SHOLH_Sequence", seqId);
	
			}
			
			LOGGER.info("SQL :" + query);
	
			List<Map<String, Object>> rows = namedParameterJdbcTemplate
					.queryForList(query, paramMap);
	
			for (Map<String, Object> row : rows) {
				oppId = String.valueOf(row
						.get("OPP_BFO_ID"));
			}
	
			
			}catch(Exception e)
		    {
				LOGGER.error("Exception:getBfoOppId"+e.getMessage());
				e.printStackTrace();
		    }
		
			return oppId;

	
	      }
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	public int getRecordCount(String SHOLH_BFO_Status_Id,String oppId) {
		// TODO Auto-generated method stub
		LOGGER.info("getRecordCount");
        String query="";  
    	query="select count(*) as cnt from stakeholderopplinkhist l  where l.SHOLH_OPP_ID=:SHOLH_OPP_ID and l.SHOLH_Status_Id=:SHOLH_BFO_Status_Id";
        LOGGER.info("getRecordCount:::query : select count(*) as cnt from stakeholderopplinkhist l where l.SHOLH_OPP_ID='"+oppId+"'  and l.SHOLH_Status_Id='"+SHOLH_BFO_Status_Id+"'");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("SHOLH_BFO_Status_Id", SHOLH_BFO_Status_Id);
		paramMap.put("SHOLH_OPP_ID", oppId);

		List<Map<String, Object>> rows = namedParameterJdbcTemplate
				.queryForList(query, paramMap);

		int recordCount = 0;

		for (Map<String, Object> row : rows) 
		{
			recordCount = Integer.parseInt(String.valueOf(row.get("cnt")));
		}

		return recordCount;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	public void updateBFOOppHistInstallation(boolean bfoResponse, String histSeqId,String custVisitId) 
	{
		// TODO Auto-generated method stub
		try {
			
			String query = "update stakeholderopplinkhist set SHOLH_UPDATE_BFO=? where SHOLH_Sequence=?";

			Object[] args = new Object[] { "1",	histSeqId };

			int out = jdbcTemplate.update(query, args);
			
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			String retry_query = "SELECT TOTAL_RETRIES from stakeholderopplinkhist WHERE SHOLH_Sequence =:SHOLH_Sequence";
			LOGGER.info("updateBFOOppHistInstallation::retry_query :"+retry_query);
			
			HashMap<String, Object> paramMapRetries = new HashMap<String, Object>();
			
			paramMapRetries.put("SHOLH_Sequence", histSeqId);
			
			List<Map<String,Object>>  rowsRetries = namedParameterJdbcTemplate.queryForList(retry_query, paramMapRetries);
			
			int totalRetries=0;
			
			for(Map<String,Object> row : rowsRetries)
			{
				totalRetries=(Integer.parseInt(String.valueOf(row.get("TOTAL_RETRIES"))));
				LOGGER.info("updateBFOOppHistInstallation:histSeqId::"+histSeqId+"totalRetries::"+totalRetries);

				totalRetries = totalRetries+1;
				query = "update stakeholderopplinkhist set TOTAL_RETRIES=? where SHOLH_Sequence=?";
				args = new Object[] {totalRetries,histSeqId};
			 	out = jdbcTemplate.update(query, args);
			}
			
			if(custVisitId!=null && custVisitId.length()>0 && !custVisitId.equalsIgnoreCase("true"))
			{
				updateCustVisitID(custVisitId,histSeqId);
			}
					// commit
			//transactionManager.commit(transStatus);

		} catch (Exception e)
		{
			LOGGER.error("Exception::::"+e.getMessage());
			e.printStackTrace();
			// roll back
			//transactionManager.rollback(transStatus);
	    }
		
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	public void updateCustVisitID(String custVisitId,String histSeqId)
	{
		
		String query="";
		try
		{
			
			LOGGER.info("updateCustVisitID:custVisitId :"+custVisitId);
			LOGGER.info("updateCustVisitID:histSeqId :"+histSeqId);

			query = "SELECT o.OPP_Consumer_ID from opportunity o,stakeholderopplinkhist h where o.OPP_ID=h.SHOLH_OPP_ID and h.SHOLH_Sequence=:SHOLH_Sequence";
			LOGGER.info("updateCustVisitID:SQL :"+query);
			HashMap<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("SHOLH_Sequence", histSeqId);
			
			List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
			
			String cust_id="";
			for(Map<String,Object> row : rows){
				cust_id=(String.valueOf(row.get("OPP_Consumer_ID")));
			}
			
			LOGGER.info("updateCustVisitID:::cust_id:::"+cust_id);
			
			LOGGER.info("updateCustVisitID::custVisitId:::"+custVisitId);
		
			if(custVisitId!=null && custVisitId.length()>0)
			{
				String update_query ="update customer set  OPP_BFO_CUST_VISIT_ID=? where cust_id=?";
				LOGGER.info("update_query:::update customer set OPP_BFO_CUST_VISIT_ID='"+custVisitId+"' where cust_id="+cust_id+"");
				Object[] update_args = new Object[] {custVisitId,cust_id};
			 	int out = jdbcTemplate.update(update_query, update_args);
				LOGGER.info("out:::"+out);
			}				
			
		}catch(Exception e)
		{
		  LOGGER.error("Exception:::"+e.getMessage());	
		  e.printStackTrace();	
		}
		
		
	}
}

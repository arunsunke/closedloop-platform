package com.se.cl.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.se.cl.dao.OpportunityAdminDAO;
import com.se.cl.model.Address;
import com.se.cl.model.CustomerDetails;
import com.se.cl.model.Opportunities;
import com.se.cl.model.Opportunity;

public class OpportunityAdminDAOImpl implements OpportunityAdminDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OpportunityAdminDAOImpl.class);
	
	
	
	@Override
	public Opportunities getOpportunities(String country) {

		 List<Opportunity> opportunities=new ArrayList<Opportunity>();
		
		
		// get partner Id using userid

		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String query = "select OPP_ID, OPP_Upstream_ID, OPP_Name, OPP_Desc, OPP_Short_Desc,OPP_Consumer_ID, OPP_BFO_ID, OPP_Address, OPP_Locality, OPP_City, OPP_State,"
						+ " OPP_ZipCode,OPP_Cntry_Cd, OPP_Phone, OPP_Fax, OPP_Status,OPP_Budget, OPP_Last_UpdDt, OPP_Eff_FmDt, OPP_Eff_ToDt,OPP_Buss_Type,cust_first_name,cust_last_name,"
						+ "cust_phone,cust_email,sl.OPP_Read_Status,sl.SHOL_status,o.OPP_SLO30_ID from opportunity o,customer c, stakeholderopplink sl where o.OPP_Consumer_ID=c.cust_id and OPP_ID=sl.SHOL_OPP_ID  o.OPP_Cntry_Cd=:OPP_Cntry_Cd ";

				LOGGER.debug("SQL :" + query);
				paramMap = new HashMap<String, Object>();
				
				paramMap.put("OPP_Cntry_Cd", country);

				// paramMap.put("productId", productId);

				List<Map<String, Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);

				for (Map<String, Object> row : rows) {

					Opportunity opp = new Opportunity();

					opp.setId(String.valueOf(row.get("OPP_ID")));
					opp.setTitle(String.valueOf(row.get("OPP_Name")));
					opp.setShortDesc(String.valueOf(row.get("OPP_Short_Desc")));
					//
					opp.setBudget(String.valueOf(row.get("OPP_Budget")));

					opp.setOpportunityDate(String.valueOf(row
							.get("OPP_Eff_FmDt")));
					opp.setOpportunityExpDate(String.valueOf(row
							.get("OPP_Eff_ToDt")));

					opp.setBusinessType(String.valueOf(row.get("OPP_Buss_Type")));

					opp.setDescription(String.valueOf(row.get("OPP_Desc")));
					Address address = new Address();
					address.setCity(String.valueOf(row.get("OPP_City")));
					address.setSt_name(String.valueOf(row.get("OPP_State")));
					address.setZip(String.valueOf(row.get("OPP_ZipCode")));
					opp.setAddress(address);

					CustomerDetails cust = new CustomerDetails();
					cust.setEmail(String.valueOf(row.get("cust_email")));
					cust.setFirstName(String.valueOf(row.get("cust_first_name")));
					cust.setLastName(String.valueOf(row.get("cust_last_name")));
					cust.setPhone(String.valueOf(row.get("cust_phone")));

					/*
					 * Address caddress=new Address();
					 * caddress.setCity(String.valueOf(row.get("addr_city")));
					 * caddress
					 * .setSt_name(String.valueOf(row.get("addr_street_name")));
					 * caddress.setZip(String.valueOf(row.get("addr_zip")));
					 * caddress
					 * .setCountryCode(String.valueOf(row.get("addr_cntry_cd"
					 * ))); cust.setAddress(caddress);
					 */

					opp.setCustomerDetails(cust);
					opp.setOppReadStatus(String.valueOf(
							row.get("OPP_Read_Status")).equalsIgnoreCase("1") ? true
							: false);
					/*opp.setAcceptedByMe(String.valueOf(
							row.get("SHOL_status")).equalsIgnoreCase(OppStatus.ACCEPTED.getValue())? true
							: false);*/
									
					opp.setSolution30Id(String.valueOf(row.get("OPP_SLO30_ID")));
					opportunities.add(opp);

				}

			
				Opportunities opps=new Opportunities();
				opps.setOpportunityList(opportunities);
		

		return opps;

	}

}

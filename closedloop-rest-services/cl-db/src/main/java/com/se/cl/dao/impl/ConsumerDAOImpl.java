package com.se.cl.dao.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.se.cl.dao.ConsumerDAO;
import com.se.cl.model.BOM;
import com.se.cl.model.CustomerPartner;

public class ConsumerDAOImpl implements ConsumerDAO{

	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(ConsumerDAOImpl.class);

	
	@Override
	public List<CustomerPartner> getConsumerWithPartnersForAcceptedOpps(String country) {

		List<CustomerPartner> custList=new ArrayList<CustomerPartner>();
		String query="select o.opp_id,o.OPP_Name,c.cust_phone,c.cust_first_name,c.cust_email,s.SH_Full_Name,s.SH_Phone,s.SH_CompanyWebsite from opportunity o,customer c,stakeholderopplink l,stakeholders s,opportunitysla sl where "
				+ "o.OPP_Consumer_ID=c.cust_id"
				+ " and o.OPP_ID=l.SHOL_OPP_ID"
				+ " and l.SHOL_SH_ID=s.SH_ID"
				+ " and o.OPP_ID=sl.OPPSLA_OPPID"
				+ " and OPP_Cntry_Cd=:country and  OPPSLA_Consumer_Noti1=0 and SHOL_Status='ACCEPTED'";
			
		logger.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);
		//paramMap.put("productId", productId);
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for(Map<String,Object> row : rows){
			
			CustomerPartner cust=new CustomerPartner();
			
			cust.setCustomerEmail(String.valueOf(row.get("cust_email")));
			cust.setCustomerName(String.valueOf(row.get("cust_first_name")));
			cust.setPartnerFirstName(String.valueOf(row.get("SH_Full_Name")));
			cust.setPartnerLastName(String.valueOf(row.get("OPP_Name")));
			cust.setPartnerPhone(String.valueOf(row.get("SH_Phone")));
			cust.setOppId(Integer.parseInt(String.valueOf(row.get("opp_id"))));
			cust.setCompany(String.valueOf(row.get("SH_CompanyWebsite")));
			cust.setCustomerPhone(String.valueOf(row.get("cust_phone")));
			custList.add(cust);
		}
		

		
		return custList;
	}
	
	public List<CustomerPartner> getpartnerwhenCancelledOpp(String country) 
	{
		List<CustomerPartner> custList=new ArrayList<CustomerPartner>();
		String query="select OPP_ID,OPP_Name,OPP_Name,cust_first_name,cust_phone,SH_Phone,SHOLH_Status_Id,SH_Full_Name,SHOLH_Eff_FmDt,SHOLH_SH_ID from opportunity o ,customer c,stakeholderopplinkhist sh,stakeholders s,opportunitysla sl,stakeholderopplink shl where "
				+ "o.OPP_Consumer_ID=c.cust_id"
				+ " and o.OPP_ID=sh.SHOLH_OPP_ID"
				+ " and sh.SHOLH_SH_ID=s.SH_ID"
				+ " and sh.SHOLH_SH_ID=shl.SHOL_SH_ID"
				+ " and sh.SHOLH_OPP_ID=shl.SHOL_OPP_ID"
				+ " and o.OPP_ID=sl.OPPSLA_OPPID"
				+ " and OPP_Cntry_Cd=:country and  SHOL_Status='ACCEPTED' and  SHOLH_Status_Id='CANCEL' and OPPSLA_CANCEL_SMS=0";
		
		logger.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for(Map<String,Object> row : rows){
		
			CustomerPartner cust=new CustomerPartner();
			
				 cust.setOppName(String.valueOf(row.get("OPP_Name")));
				 cust.setOppId(Integer.parseInt(String.valueOf(row.get("OPP_ID"))));
			     cust.setPartnerPhone(String.valueOf(row.get("SH_Phone")));
			     cust.setCustomerName(String.valueOf(row.get("cust_first_name")));
			     cust.setPartnerFirstName(String.valueOf(row.get("SH_Full_Name")));
			     cust.setMeetingDate(String.valueOf(row.get("SHOLH_Eff_FmDt")));
			     cust.setPartnerId(String.valueOf(row.get("SHOLH_SH_ID")));

			   
			custList.add(cust);
		}
		
		return custList;
	}
	
	public List<CustomerPartner> getConsumerWhenMeetingBooked(String country,String notifyType) 
	{
		List<CustomerPartner> custList=new ArrayList<CustomerPartner>();
	
		String query="";
		if(notifyType!=null && notifyType.equalsIgnoreCase("SMS"))
		{	
			 
			query = "select sh.sholh_sh_id,sh.sholh_opp_id from opportunity o ,stakeholderopplinkhist sh,opportunitysla sla where o.OPP_ID=sh.SHOLH_OPP_ID and sh.SHOLH_Status_Id='INSTALLATION' and o.OPP_Cntry_Cd=:country and o.opp_id=sla.oppsla_oppid and sla.OPPSLA_MEETING_SMS=0";

			
		}else if(notifyType!=null && notifyType.equalsIgnoreCase("EMAIL"))
		{
			
			query = "select sh.sholh_sh_id,sh.sholh_opp_id from opportunity o ,stakeholderopplinkhist sh,opportunitysla sla where o.OPP_ID=sh.SHOLH_OPP_ID and sh.SHOLH_Status_Id='INSTALLATION' and o.OPP_Cntry_Cd=:country and o.opp_id=sla.oppsla_oppid and sla.OPPSLA_MEETING_EMAIL=0";
		}

		logger.debug("SQL :"+query);	 
		List<Integer> oppIdList=new ArrayList<Integer>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for (Map<String, Object> row : rows)
		{
			oppIdList.add(Integer.parseInt(String.valueOf(row.get("SHOLH_OPP_ID"))));
		
		} 	    
		
		if(oppIdList!=null && oppIdList.size()>0)
		{
			query="select o.opp_id,o.OPP_Name,s.SH_Phone,s.SH_Full_Name,s.SH_Phone,s.SH_CompanyWebsite,sh.SHOLH_Eff_FmDt from opportunity o,stakeholderopplinkhist sh,stakeholders s where "
					+ " o.OPP_ID=sh.SHOLH_OPP_ID"
					+ " and sh.SHOLH_SH_ID=s.SH_ID"
					+ " and OPP_Cntry_Cd=:country and  SHOLH_Status_Id='INSTALLATION'"
			        + " and  o.opp_id in (:opp_id)";
			
			logger.debug("SQL :"+query);	 
			paramMap = new HashMap<String, Object>();
			paramMap.put("opp_id", oppIdList);
			paramMap.put("country", country);

			rows = namedParameterJdbcTemplate.queryForList(query, paramMap);			
			for(Map<String,Object> row : rows)
			{
				   String cust_first_name="";
				   String cust_email="";
				   String cust_id="";
				   String cust_phone="";
				   String cust_zip="";
				   String cust_city="";
				   String cust_address="";
					CustomerPartner cust=new CustomerPartner();
	                String query1="select c.cust_first_name,c.cust_email,c.cust_phone,c.cust_id from opportunity o,customer c where  o.OPP_Consumer_ID=c.cust_id and o.OPP_ID=:OPP_ID";
					logger.info("getOpportunityConsumerName:query1::"+query1);
					paramMap = new HashMap<String, Object>();
					paramMap.put("OPP_ID", String.valueOf(row.get("opp_id")));
					List<Map<String, Object>> rows1 = namedParameterJdbcTemplate.queryForList(query1, paramMap);
					for (Map<String, Object> row1 : rows1)
					{
						cust_first_name = String.valueOf(row1.get("cust_first_name"));
						cust_email =  String.valueOf(row1.get("cust_email"));
						cust_phone =  String.valueOf(row1.get("cust_phone"));
						logger.debug("cust_phone:::"+cust_phone);
						cust_id = String.valueOf(row1.get("cust_id"));						
					}
					
					String query2="select a.addr_street_name,a.addr_city,a.addr_zip from address a,customer c where c.cust_id=a.addr_customer_id and c.cust_id=:CUST_ID";
					logger.info("query2::"+query2);
					paramMap = new HashMap<String, Object>();
					paramMap.put("CUST_ID", cust_id);
					List<Map<String, Object>> rows2 = namedParameterJdbcTemplate.queryForList(query2, paramMap);
						
					for (Map<String, Object> row2 : rows2)
					{
						cust_address = String.valueOf(row2.get("addr_street_name"));
						cust_zip =  String.valueOf(row2.get("addr_zip"));
						cust_city =  String.valueOf(row2.get("addr_city"));
					} 
								
					cust.setPartnerFirstName(String.valueOf(row.get("SH_Full_Name")));
					cust.setPartnerLastName(String.valueOf(row.get("OPP_Name")));
					cust.setPartnerPhone(String.valueOf(row.get("SH_Phone")));
					cust.setOppId(Integer.parseInt(String.valueOf(row.get("opp_id"))));
					cust.setCompany(String.valueOf(row.get("SH_CompanyWebsite")));
					
					java.text.SimpleDateFormat dateformat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
					//String str=dateformat.format(String.valueOf(row.get("SHOLH_Eff_FmDt")));
					Date date;
					try {
					date = dateformat.parse(String.valueOf(row.get("SHOLH_Eff_FmDt")));
					cust.setMeetingDate(dateformat.format(date));
					} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}					
					
					//String date=dateFormat.format(String.valueOf(row.get("SHOLH_Eff_FmDt")));
				    //cust.setMeetingDate(String.valueOf(row.get("SHOLH_Eff_FmDt")));
					//cust.setMeetingDate(date);
					
					cust.setCustomerEmail(cust_email);
					cust.setCustomerName(cust_first_name);
					cust.setCustomerPhone(cust_phone);                    
					cust.setCustomerCity(cust_city);
					cust.setCustomerZip(cust_zip);
                    cust.setCustomerAddress(cust_address);
                    //cust.setOppId(Integer.parseInt(String.valueOf(row.get("opp_id"))));
        			cust.setBomList(getBomsCSV(String.valueOf(row.get("opp_id"))));
				  	custList.add(cust);
			}
		
		}
	
			return custList;
	}
	
	public List<BOM> getBomsCSV(String opportunityId) {

		final String SELECT_BOMS_QUERY = "SELECT OBOM_OPP_ID, OBOM_SlNo, OBOM_Category, OBOM_Prod_ID, OBOM_Name, OBOM_Desc, OBOM_Colour, OBOM_Std_Incl_Qty, OBOM_Var_Qty, OBOM_Tot_Qty, OBOM_Unit_Cost, "
				+ "OBOM_Tot_Cost FROM opportunitybom WHERE OBOM_OPP_ID = :OPPORTUNITY_ID";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("OPPORTUNITY_ID", opportunityId);

		List<BOM> bomList = new ArrayList<BOM>();
		BOM bom = null;
		List<Map<String, Object>> bomRows = namedParameterJdbcTemplate
				.queryForList(SELECT_BOMS_QUERY, paramMap);
		for (Map<String, Object> row : bomRows) {

			bom = new BOM();
			bom.setCategory(String.valueOf(row.get("OBOM_Category")));
			bom.setProductId(String.valueOf(row.get("OBOM_Prod_ID")));
			bom.setProductName(String.valueOf(row.get("OBOM_Name")));
			bom.setDescription(String.valueOf(row.get("OBOM_Desc")));
			bom.setColor(String.valueOf(row.get("OBOM_Colour")));
			bom.setQuantity(Integer.valueOf(String.valueOf(row
					.get("OBOM_Tot_Qty"))));
			bom.setUnitCost(Float.valueOf(String.valueOf(row
					.get("OBOM_Tot_Cost"))));
			
			bom.setDiscount(String.valueOf(row.get("OBOM_Discount")));

			bomList.add(bom);
		}

		return bomList;
	}

	
	@Override
	public void updateOppSLA(int oppId,String type) {
		// TODO Auto-generated method stub
		String query ="";
		Object[] args=null;
		int out=0;
		if(type.equals("NOTI1")){
		 query = "update opportunitysla set OPPSLA_Consumer_Noti1=? where OPPSLA_OPPID =? ";
		 logger.debug("SQL :" + query);
		 args = new Object[] { 1, oppId };
		 out = jdbcTemplate.update(query, args);
		}else if(type.equals("NEWOPP")){
			query = "update opportunitysla set OPPSLA_Consumer_New=? where OPPSLA_OPPID =? ";
			 logger.debug("SQL :" + query);
			 args = new Object[] { 1, oppId };
			 out = jdbcTemplate.update(query, args);
		}
		else if(type!=null && type.equalsIgnoreCase("CANCEL"))
		{
			 query = "update opportunitysla set OPPSLA_CANCEL_SMS=? where OPPSLA_OPPID =? ";
			 logger.debug("SQL :" + query);
			 args = new Object[] { 1, oppId };
			 out = jdbcTemplate.update(query, args);
			 logger.debug("oppId :" + oppId);
			 logger.debug("out :" + out);

		}
		else if(type.equals("MEETING"))
		{
			query = "update opportunitysla set OPPSLA_MEETING_SMS=? where OPPSLA_OPPID =? ";
			 logger.debug("SQL :" + query);
			 args = new Object[] { 1, oppId };
			 out = jdbcTemplate.update(query, args);
		}else if(type.equals("MEETING_EMAIL"))
		{
			query = "update opportunitysla set OPPSLA_MEETING_EMAIL=? where OPPSLA_OPPID =? ";
			 logger.debug("SQL :" + query);
			 args = new Object[] { 1, oppId };
			 out = jdbcTemplate.update(query, args);
		}else if(type.equals("INSTALLED_EMAIL"))
		{
			query = "update opportunitysla set OPPSLA_INSTALLED_EMAIL=? where OPPSLA_OPPID =? ";
			 logger.debug("SQL :" + query);
			 args = new Object[] { 1, oppId };
			 out = jdbcTemplate.update(query, args);
		}
		
	}
	
	public void updateOppNOTI(List<Integer> oppIdList,String type) {
		// TODO Auto-generated method stub
		String query ="";
		Object[] args=null;
		int out=0;
		
		for(int oppId:oppIdList)
		{
			if(type.equals("N4")){
			 query = "update opportunitysla set OPPSLA_NOTI4=? where OPPSLA_OPPID =? ";
			 logger.debug("SQL :" + query);
			 args = new Object[] { 1, oppId };
			 out = jdbcTemplate.update(query, args);
			}else if(type.equals("N5")){
				query = "update opportunitysla set OPPSLA_NOTI5=? where OPPSLA_OPPID =? ";
				 logger.debug("SQL :" + query);
				 args = new Object[] { 1, oppId };
				 out = jdbcTemplate.update(query, args);
			}
			else if(type.equals("N6"))
			{
				query = "update opportunitysla set OPPSLA_NOTI6=? where OPPSLA_OPPID =? ";
				 logger.debug("SQL :" + query);
				 args = new Object[] { 1, oppId };
				 out = jdbcTemplate.update(query, args);
			}else if(type!=null && type.equalsIgnoreCase("CANCEL"))
			{
				query = "update opportunitysla set OPPSLA_CANCEL=? where OPPSLA_OPPID =? ";
				 logger.debug("SQL :" + query);
				 logger.debug("opportunitysla :::oppId"+oppId);
				 args = new Object[] { 1, oppId };
				 out = jdbcTemplate.update(query, args);
				 logger.debug("updateOppNOTI:::out"+out);
			}
		}
		
	}
	@Override
	public List<CustomerPartner> getCustomersWhoSendsTheNewOpps(String country) {
		// TODO Auto-generated method stub
		
		List<CustomerPartner> custList=new ArrayList<CustomerPartner>();
		//SELECT OPP_Status,OPP_City,OPP_Address,OPP_Eff_FmDt,OPP_Name,OPP_Budget,OPP_ZipCode,cust_first_name,cust_email,OPP_Attachment,cust_phone from opportunity o ,customer c WHERE o.OPP_Consumer_ID=c.cust_id AND OPP_ID =:OPP_ID 
		String query="select o.opp_id,o.OPP_City,o.OPP_Address,o.OPP_ZipCode,o.OPP_Name,c.cust_first_name,c.cust_email from opportunity o,customer c,opportunitysla sl where "
				+ "o.OPP_Consumer_ID=c.cust_id"
				+ " and o.OPP_ID=sl.OPPSLA_OPPID"
				+ " and OPP_Cntry_Cd=:country and  OPPSLA_Consumer_New=0";
			
		logger.debug("SQL :"+query);		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);
		//paramMap.put("productId", productId);
		
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for(Map<String,Object> row : rows){
			
			CustomerPartner cust=new CustomerPartner();
			
			cust.setCustomerEmail(String.valueOf(row.get("cust_email")));
			cust.setCustomerName(String.valueOf(row.get("cust_first_name")));
			cust.setOppId(Integer.parseInt(String.valueOf(row.get("opp_id"))));
			cust.setPartnerLastName(String.valueOf(row.get("OPP_Name")));
			cust.setCustomerZip(String.valueOf(row.get("OPP_ZipCode")));
			cust.setCustomerAddress(String.valueOf(row.get("OPP_Address")));
			cust.setCustomerCity(String.valueOf(row.get("OPP_City")));
			custList.add(cust);
		}
		
		
		return custList;
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.se.cl.dao.ConsumerDAO#getConsumerWhenPartnerInstalled(java.lang.String, java.lang.String)
	 */
	@Override

	public List<CustomerPartner> getConsumerWhenPartnerInstalled(String country,String notifyType) 
	{
		List<CustomerPartner> custList=new ArrayList<CustomerPartner>();
	
		String query="";
		
		query = "select sh.sholh_sh_id,sh.sholh_opp_id from opportunity o ,stakeholderopplinkhist sh,opportunitysla sla where o.OPP_ID=sh.SHOLH_OPP_ID and sh.SHOLH_Status_Id='INSTALLED' and o.OPP_Cntry_Cd=:country and o.opp_id=sla.oppsla_oppid and sla.OPPSLA_INSTALLED_EMAIL=0";
		
		logger.debug("SQL :"+query);	 
		List<Integer> oppIdList=new ArrayList<Integer>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);
		List<Map<String,Object>>  rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		for (Map<String, Object> row : rows)
		{
			oppIdList.add(Integer.parseInt(String.valueOf(row.get("SHOLH_OPP_ID"))));
		
		} 	    
		
		if(oppIdList!=null && oppIdList.size()>0)
		{
			query="select o.opp_id,o.OPP_Name,s.SH_Phone,s.SH_Full_Name,s.SH_Phone,s.SH_CompanyWebsite,sh.SHOLH_Eff_FmDt from opportunity o,stakeholderopplinkhist sh,stakeholders s where "
					+ " o.OPP_ID=sh.SHOLH_OPP_ID"
					+ " and sh.SHOLH_SH_ID=s.SH_ID"
					+ " and OPP_Cntry_Cd=:country and  SHOLH_Status_Id='INSTALLED'"
			        + " and  o.opp_id in (:opp_id)";
			
			logger.debug("SQL :"+query);	 
			paramMap = new HashMap<String, Object>();
			paramMap.put("opp_id", oppIdList);
			paramMap.put("country", country);

			rows = namedParameterJdbcTemplate.queryForList(query, paramMap);			
			for(Map<String,Object> row : rows)
			{
				   String cust_first_name="";
				   String cust_email="";
				   String cust_id="";
				   String cust_phone="";
				   String cust_zip="";
				   String cust_city="";
				   String cust_address="";
					CustomerPartner cust=new CustomerPartner();
	                String query1="select c.cust_first_name,c.cust_email,c.cust_phone,c.cust_id from opportunity o,customer c where  o.OPP_Consumer_ID=c.cust_id and o.OPP_ID=:OPP_ID";
					logger.info("getOpportunityConsumerName:query1::"+query1);
					paramMap = new HashMap<String, Object>();
					paramMap.put("OPP_ID", String.valueOf(row.get("opp_id")));
					List<Map<String, Object>> rows1 = namedParameterJdbcTemplate.queryForList(query1, paramMap);
					for (Map<String, Object> row1 : rows1)
					{
						cust_first_name = String.valueOf(row1.get("cust_first_name"));
						cust_email =  String.valueOf(row1.get("cust_email"));
						cust_phone =  String.valueOf(row1.get("cust_phone"));
						logger.debug("cust_phone:::"+cust_phone);
						cust_id = String.valueOf(row1.get("cust_id"));						
					}
					
					String query2="select a.addr_street_name,a.addr_city,a.addr_zip from address a,customer c where c.cust_id=a.addr_customer_id and c.cust_id=:CUST_ID";
					logger.info("query2::"+query2);
					paramMap = new HashMap<String, Object>();
					paramMap.put("CUST_ID", cust_id);
					List<Map<String, Object>> rows2 = namedParameterJdbcTemplate.queryForList(query2, paramMap);
						
					for (Map<String, Object> row2 : rows2)
					{
						cust_address = String.valueOf(row2.get("addr_street_name"));
						cust_zip =  String.valueOf(row2.get("addr_zip"));
						cust_city =  String.valueOf(row2.get("addr_city"));
					} 
								
					cust.setPartnerFirstName(String.valueOf(row.get("SH_Full_Name")));
					cust.setPartnerLastName(String.valueOf(row.get("OPP_Name")));
					cust.setPartnerPhone(String.valueOf(row.get("SH_Phone")));
					//cust.setOppId(Integer.parseInt(String.valueOf(row.get("opp_id"))));
					cust.setCompany(String.valueOf(row.get("SH_CompanyWebsite")));
					cust.setMeetingDate(String.valueOf(row.get("SHOLH_Eff_FmDt")));
					cust.setCustomerEmail(cust_email);
					cust.setCustomerName(cust_first_name);
					cust.setCustomerPhone(cust_phone);                    
					cust.setCustomerCity(cust_city);
					cust.setCustomerZip(cust_zip);
                    cust.setCustomerAddress(cust_address);
                    cust.setOppId(Integer.parseInt(String.valueOf(row.get("opp_id"))));
        			cust.setBomList(getBomsCSV(String.valueOf(row.get("opp_id"))));
				  	custList.add(cust);
			}
		
		}
	
			return custList;
	}
	
}

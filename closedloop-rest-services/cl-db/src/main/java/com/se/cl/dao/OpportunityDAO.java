package com.se.cl.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.se.cl.constants.OppStatus;
import com.se.cl.exception.AppException;
import com.se.cl.model.BOM;
import com.se.cl.model.Distributor;
import com.se.cl.model.Opportunities;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityHist;
import com.se.cl.model.OpportunityImages;
import com.se.cl.model.PartnerDevice;
import com.se.cl.model.User;

public interface OpportunityDAO {
	
	
	public Opportunities getOpportunitiesForPartner(String partnerId);
	public Opportunities getOpportunities(String username);
	public OpportunityHist getOpputnityHistory(String oppId,String partnerId);
	public JSONObject getOpportunityBOM(String oppId) throws AppException;
	
	public void  updateOppHistory(String partnerId,String oppId,String statusId,String status,String reason,String startTime,String endTime,String type,String source) throws AppException;
	public Opportunity  updateOppStatus(String partnerId,String oppId,String statusVal,boolean isAdminAssigned) throws AppException;
	public void  updateOppStatus(List<String> oppId,OppStatus statusVal) throws AppException;
	
	public int addDevicetoPartner(PartnerDevice device) throws AppException;
	
	public long addOpportunity(Opportunity opportunity) throws AppException;
	public Opportunity loadOpportunityById(long opportunityId) throws AppException;
	
	public HashMap<String,String> getOpportunityIdAndZip(OppStatus status,String country);
	
	public List<PartnerDevice> listPartnerDevice(String deviceType) throws AppException;
	
	public void updateOppNotiStatus(List<String> oppIds,String noteType,int slaStep);
	public void updateOppReadStatus(String oppId,String partnerId,int status);
	
	public void updateOppStatusSystem(String oppId,String status,String statusRemarks) ;
	
	public Opportunity getOportunity(String partnerId,String oppId);
	public Opportunity getOportunity(String oppId);
	public void deleteopportunities(List<String> userIds);
	
	public List<BOM> getBoms(long opportunityId);
	
	public List<BOM> getBomsCSV(String opportunityId) ;
	
	public List<String>  getOppsRejectedByAllPatners();
	public  void assignOpportunityToPartner( final Long oppId,final int partnerId );
	
	public void unassignOpportunityFromPartner(String oppId);
	public void revokeOppFromPartner(String oppId,String partnerId);
	public List<String> getOppAndPartner(String country);
	public User getUser(User user);
	
	public JSONObject generateCsvFile(List<BOM> bomcsvList) throws IOException;
	
	public int addDistributorToPartener(Distributor distributor);
	
	public List<Distributor> getDistList(String partnerId);
		
	public int deleteDistributor(String distId,String shId);
	
	public int updateDistributor(Distributor distributor, String distId,String shId);
	
	public boolean checkLinkExists(String oppId,String partnerId);
	
	public int udateOpportunity(String oppId,String status);
	
	public String getPartnerId(String userId);
	
	public Map<String,Object> getPartnerDeviceForNotifications(int hrs,String ntype,String country);

	public void udateOpportunityImages(String oppId,OpportunityImages opportunityImages) throws AppException;
		
	public List<OpportunityImages> getOportunityImages(String oppTitle,String oppId,String userId) ;
	
	public String getOpportunityStatus(String oppId);
	
	public String checkOPPAccepted(String oppId);
	
	public void updateOppHistoryCancel(String partnerId, String oppId,
			String statusId, String status, String reason, String startTime,
			String endTime, String type, String source) throws AppException;
	
	
	public String getOpportunityMeetingDate(String oppId);
	
	public String getOpportunityConsumerName(String oppId);
	
	public void udateOpportunityConsumerImages(String oppId,List<OpportunityImages> opportunityImages);
	
	public void updateOpportunityTitle(String oppId);
	
	public String checkInstallationExists(String oppId,String partnerId);
	
	public boolean updateDesc(String oppId,String desc);
 

}

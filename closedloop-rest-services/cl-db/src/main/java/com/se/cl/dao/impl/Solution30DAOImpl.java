package com.se.cl.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.se.cl.constants.OppStatus;
import com.se.cl.dao.Solution30DAO;
import com.se.cl.model.Address;
import com.se.cl.model.CustomerDetails;
import com.se.cl.model.Opportunities;
import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityHist;

public class Solution30DAOImpl implements Solution30DAO{
	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(Solution30DAOImpl.class);
		/*private PlatformTransactionManager transactionManager;

				
	
	
	public void setTransactionManager(
	      PlatformTransactionManager transactionManager) {
	      this.transactionManager = transactionManager;
	 }*/
	
	
	@Transactional(propagation=Propagation.REQUIRES_NEW,rollbackFor=Exception.class)
	@Override
	public void updateOpportunity(String oppId, String sol30RefId) {
		
		
		/* TransactionDefinition def = new DefaultTransactionDefinition();
	     TransactionStatus transStatus = transactionManager.getTransaction(def);*/
	     java.util.Date dt = new java.util.Date();
	     final Timestamp timestamp = new Timestamp(getTimeForTZ(null).getTime());
	     final String oppStatus=OppStatus.ACCEPTED.getValue();
	     try{
		 String query = "update opportunity set OPP_SLO30_ID=?,OPP_Last_UpdDt=?,OPP_Status=? where OPP_ID=?";
		 Object[] args = new Object[] {sol30RefId,timestamp,oppStatus,oppId};
		 int out = jdbcTemplate.update(query, args);
		 
		 LOGGER.info("Solution30DAOImpl:updateOpportunity:::sol30RefId::::"+sol30RefId+":::OppId::::"+oppId);
		 LOGGER.info("Solution30DAOImpl:updateOpportunity:::query"+query);
		 LOGGER.info("Solution30DAOImpl:updateOpportunity:::out"+out);
				 
		// transactionManager.commit(transStatus);
	     }catch(Exception e)
	     {
				e.printStackTrace();
				//transactionManager.rollback(transStatus);
				/*throw new AppException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), 500, "Problem in updating status" ,
						"", null);*/
		 }
		
		
		
	}
	
	public Date getTimeForTZ(String timeZone) {

		Date currDate = new Date();
		Date dateCet = getDateInTimeZone(currDate,(timeZone!=null&&timeZone.length()>0?timeZone:"CET"));
		return dateCet;
	   }

	public  Date getDateInTimeZone(Date currentDate, String timeZoneId) {
	       
	       TimeZone tz = TimeZone.getTimeZone(timeZoneId);
	       Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
	       mbCal.setTimeInMillis(currentDate.getTime());
	       Calendar cal = Calendar.getInstance();
	       cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
	       cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
	       cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
	       cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
	       cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
	       cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
	       cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));
	       return cal.getTime();
	   }


	/** 
	 * opportunities which are sent to solution 30 and history is not 6
	 */
	@Override
	public List<String> getOpportunities() {
		
		//get partner Id using userid
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String query = "SELECT OPP_ID  FROM opportunity WHERE OPP_SLO30_ID is not null and OPP_SLO30_ID <>'999111999' and opp_id not in (select distinct SHOLH_OPP_ID from stakeholderopplinkhist where SHOLH_Status_Id='LOST') ";
		LOGGER.debug("SQL :"+query);
		paramMap = new HashMap<String, Object>();
		
		List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(query, paramMap);
		
		List<String> opportunityIds=new ArrayList<String>();
		for(Map<String,Object> row : rows){
			opportunityIds.add(String.valueOf(row.get("OPP_ID")));
		}
		
		return opportunityIds;
		
		
	
	}

	
}

package com.se.cl.dao;

import java.util.List;
import java.util.Map;

import com.se.cl.model.Opportunity;
import com.se.cl.model.OpportunityBFOHistory;

public interface OpportunityBFODAO {

	public List<Opportunity> getPendinOpportunities(String country);
	public List<OpportunityBFOHistory> getPendingOppStatus(String oppStatus,String country);
	
	public List<OpportunityBFOHistory> getPendingOppHistoryStatus(String statusId,String country);
			
	public void updateBFOOppStatus(Map<String,String> bfoResponse , String oppId);
	public void updateBFOOppAcceptRejectStatus (boolean bfoResponse , String oppId,String seqId);
	public void updateBFOOppAcceptRejectStatusIF (boolean bfoResponse , String oppId,String seqId, String responseData);
	public void updateBFOOppHist (boolean bfoResponse , String oppId);
	public List<Opportunity> getPendinOpportunitiesIF(String country);
	
	public String getBfoOppId(String seqId,String tableReference);
	
	public List<OpportunityBFOHistory> getPendingOppHistoryStatusForInstallation(String statusId,String country);
	
	public void updateBFOOppHistInstallation(boolean bfoResponse, String oppId,String custVisitId);
	
	
	//Newly Added Methods for IFW
	
	public List<OpportunityBFOHistory> getPendingOppStatusIF(String oppStatus,	String country);
	
	public List<OpportunityBFOHistory> getPendingOppHistoryStatusIF(String statusId,String country);

	public void updateBFOOppStatusIF(Map<String, String> bfoResponse, String oppId);
	
	public void updateBFOOppHistIF(boolean bfoResponse, String histSeqId);
	

}

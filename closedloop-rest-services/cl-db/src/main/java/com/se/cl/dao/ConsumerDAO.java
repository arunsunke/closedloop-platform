package com.se.cl.dao;

import java.util.List;

import com.se.cl.model.CustomerDetails;
import com.se.cl.model.CustomerPartner;

public interface ConsumerDAO {
/* 
 * method to get all customer  and partners  details whose opportunities accepted 
 */
	public List<CustomerPartner> getConsumerWithPartnersForAcceptedOpps(String country);
	
	public void updateOppSLA(int oppId,String type);
	public void updateOppNOTI(List<Integer> oppId,String type);
	
	public List<CustomerPartner> getCustomersWhoSendsTheNewOpps(String country);

	public List<CustomerPartner> getpartnerwhenCancelledOpp(String country);
	
	public List<CustomerPartner> getConsumerWhenMeetingBooked(String country, String notificationType);
	
	public List<CustomerPartner> getConsumerWhenPartnerInstalled(String country, String notificationType);


}
